/**
 * establece el foco en el primer control de entrada de texto
 */
function focusOnFirstInputText() {
	try {
		var forms = document.forms;
		for (var i = 0; i < forms.length; i++) {
			for (var j = 0; j < forms[i].length; j++) {
				if (!forms[i][j].readonly != undefined
						&& forms[i][j].type != "hidden"
						&& forms[i][j].disabled != true
						&& forms[i][j].style.display != 'none'
						&& forms[i][j].type == 'text') {
					forms[i][j].focus();
					return;
				}
			}
		}
	} catch (err) {
		// alert(err);
	}
}

window.onload = focusOnFirstInputText;