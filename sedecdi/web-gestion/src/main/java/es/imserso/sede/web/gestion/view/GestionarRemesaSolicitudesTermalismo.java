package es.imserso.sede.web.gestion.view;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import es.imserso.sede.model.RemesaDescarga;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.util.DateUtil;
import es.imserso.sede.web.gestion.remesasTermalismo.RemesaTermalismo;


@Model
public class GestionarRemesaSolicitudesTermalismo implements Serializable {

	private static final long serialVersionUID = 1L;


	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	private Date fechaDesde;
	
	
	private Date fechaHasta;
	
	private boolean forzarReenvio=false;

	
	private String mensaje;
	

	@Inject
	private RemesaTermalismo remesaTermalismo;


	private List<RemesaDescarga> listaRemesasDescarga;


	
	@PostConstruct
	public void inicio(){
		Calendar cal = Calendar.getInstance();
		setFechaDesde(null);
		listaRemesasDescarga = findRemesasas();
		if (listaRemesasDescarga != null && listaRemesasDescarga.size() > 0) {

			fechaDesde = listaRemesasDescarga.get(0).getFechaHasta();

			cal.setTime(fechaDesde);
			cal.add(Calendar.DAY_OF_MONTH, 1);
			setFechaDesde(cal.getTime());
		}

		setFechaHasta(new Date());

		cal.setTime(fechaHasta);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		cal.getTime();
		setFechaHasta(cal.getTime());

		if (fechaDesde != null && fechaDesde.after(fechaHasta))
			setFechaDesde(fechaHasta);
	}
	

	
	private List<RemesaDescarga> findRemesasas() {
		return entityManager.createNamedQuery("remesaTermalismo.fechas", RemesaDescarga.class).setParameter("codigoSIA", TipoTramite.TERMALISMO.getSia()).getResultList();
	}

	public String lanzar()  {

		if (validaciones()){		
			try {
				remesaTermalismo.generarRemesa(fechaDesde, fechaHasta);
			} catch (Exception e) {
				System.out.println("ERROR");
				mensaje = "Ha ocurrido un error: "+e.getMessage();
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								mensaje,null));
			}
		}

		return null;
	}


	private boolean validaciones() {
		boolean lanzar = true;

	
		if (getFechaHasta() == null) {
			mensaje = "Fecha Hasta no puede ser nula";
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							mensaje,null));
			lanzar = false;
		}
		if (fechaDesde != null) {
			fechaDesde = DateUtil.preparaFechaDesde(fechaDesde);
		}

		fechaHasta = DateUtil.preparaFechaHasta(fechaHasta);
		if (getFechaHasta().after(new Date())
				|| (getFechaDesde() != null && getFechaHasta().before(
						getFechaDesde()))) {
			mensaje = "Fecha Hasta no puede ser mayor que Fecha Actual ni menor que Fecha Desde";
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							mensaje,null));
			
			lanzar = false;
		}

		if (getFechaHasta().after(new Date())) {
			mensaje = "Fecha Hasta no puede ser mayor que Fecha Actual";
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							mensaje,null));
			lanzar = false;
		}

		if (!forzarEnvio()) {
			mensaje = "Fecha Desde y Fecha Hasta de pantalla se solapan con Fecha Desde y Hasta de la tabla";
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							mensaje,null));			
			lanzar = false;
		}

		return lanzar;
		
	}
	
	
	private Boolean forzarEnvio() {
		if (isForzarReenvio())
			return true;
		getListaRemesasDescarga();
		if (listaRemesasDescarga != null && listaRemesasDescarga.size() > 0
				&& fechaDesde != null) {

			for (RemesaDescarga remesaDescarga : listaRemesasDescarga) {
				if (remesaDescarga.getFechaDesde() != null) {
					if ( (fechaDesde.after(remesaDescarga.getFechaDesde()) || 
							DateUtil.parseDateGuiones(fechaDesde).toString().equals(remesaDescarga.getFechaDesde().toString()) )						
						   && ( fechaHasta.before(remesaDescarga.getFechaHasta()) ||  
						    DateUtil.parseDateGuiones(fechaHasta).toString().equals(remesaDescarga.getFechaHasta().toString()) )
						) {
						return false;
					}
				} else {
					if ((fechaHasta.before(remesaDescarga.getFechaHasta()) || DateUtil.parseDateGuiones(fechaHasta).toString().
							equals(remesaDescarga.getFechaHasta().toString()))) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public Date getFechaDesde() {		
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public boolean isForzarReenvio() {
		return forzarReenvio;
	}

	public void setForzarReenvio(boolean forzarReenvio) {
		this.forzarReenvio = forzarReenvio;
	}

	public List<RemesaDescarga> getListaRemesasDescarga() {
		if (listaRemesasDescarga==null || listaRemesasDescarga.isEmpty()){
			listaRemesasDescarga = findRemesasas();
		}
		return listaRemesasDescarga;
	}

	public void setListaRemesasDescarga(List<RemesaDescarga> listaRemesasDescarga) {
		this.listaRemesasDescarga = listaRemesasDescarga;
	}
}
