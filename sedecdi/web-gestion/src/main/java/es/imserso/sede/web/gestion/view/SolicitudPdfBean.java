package es.imserso.sede.web.gestion.view;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import es.imserso.sede.model.Idioma;
import es.imserso.sede.model.SolicitudPdf;
import es.imserso.sede.web.gestion.util.Utils;

/**
 * Backing bean for SolicitudPdf entities.
 * <p/>
 * This class provides CRUD functionality for all SolicitudPdf entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class. 
 */

@Named
@Stateful
@ConversationScoped
public class SolicitudPdfBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;


	private SolicitudPdf solicitudPdf;
	
	private UIComponent fileUpload;




	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		return "create?faces-redirect=true";
	}

	public void retrieve() {
		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.example = new SolicitudPdf();
			this.solicitudPdf = this.example;
		} else {
			this.solicitudPdf = findById(getId());
		}
	}

	public SolicitudPdf findById(Long id) {

		return this.entityManager.find(SolicitudPdf.class, id);
	}

	/*
	 * Support updating and deleting SolicitudPdf entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (solicitudPdf.getValor()==null){
				FacesContext.getCurrentInstance().addMessage(fileUpload.getClientId(), new FacesMessage("Seleccione un archivo"));					
				return null;
			}
			if (this.id == null) {
				this.solicitudPdf.setIdioma(entityManager.find(Idioma.class, solicitudPdf.getIdioma().getId()));
				this.entityManager.persist(this.solicitudPdf);
				this.entityManager.flush();
				return "search?faces-redirect=true";
			} else {				
				this.entityManager.merge(this.solicitudPdf);
				this.entityManager.flush();
				return "view?faces-redirect=true&id=" + this.solicitudPdf.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}
	

	public String delete() {
		this.conversation.end();

		try {
			SolicitudPdf deletableEntity = findById(getId());

			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}
	
	public void listener(FileUploadEvent event) throws Exception {
		UploadedFile item = event.getUploadedFile();
		solicitudPdf.setValor(item.getData());
	}

	public String clearUploadData() {
		solicitudPdf.setValor(null);
		return null;
	}

	/*
	 * Support searching SolicitudPdf entities with pagination
	 */

	private int page;
	private long count;
	private List<SolicitudPdf> pageItems;

	private SolicitudPdf example = new SolicitudPdf();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public SolicitudPdf getExample() {
		return this.example;
	}

	public void setExample(SolicitudPdf example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<SolicitudPdf> root = countCriteria.from(SolicitudPdf.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<SolicitudPdf> criteria = builder.createQuery(SolicitudPdf.class);
		root = criteria.from(SolicitudPdf.class);
		TypedQuery<SolicitudPdf> query = this.entityManager
				.createQuery(criteria.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<SolicitudPdf> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		byte[] valor = this.example.getValor();
		if (valor != null) {
			predicatesList.add(builder.equal(root.get("valor"), valor));
		}
		String nombre = this.example.getNombre();
		if (nombre != null && !"".equals(nombre)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String> get("nombre")), '%' + nombre.toLowerCase() + '%'));
		}
		String descripcion = this.example.getDescripcion();
		if (descripcion != null && !"".equals(descripcion)) {
			predicatesList.add(builder.like(builder.lower(root.<String> get("descripcion")),
					'%' + descripcion.toLowerCase() + '%'));
		}
		Idioma idioma = this.example.getIdioma();
		if (idioma != null) {
			predicatesList.add(builder.equal(root.get("idioma"), idioma));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<SolicitudPdf> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back SolicitudPdf entities (e.g. from inside
	 * an HtmlSelectOneMenu)
	 */

	public List<SolicitudPdf> getAll() {

		CriteriaQuery<SolicitudPdf> criteria = this.entityManager.getCriteriaBuilder().createQuery(SolicitudPdf.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(SolicitudPdf.class))).getResultList();
	}

	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final SolicitudPdfBean ejbProxy = this.sessionContext.getBusinessObject(SolicitudPdfBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return ejbProxy.findById(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((SolicitudPdf) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private SolicitudPdf add = new SolicitudPdf();

	public SolicitudPdf getAdd() {
		return this.add;
	}

	public SolicitudPdf getAdded() {
		SolicitudPdf added = this.add;
		this.add = new SolicitudPdf();
		return added;
	}
	
	public void verPdf(){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			Utils.doDownload(facesContext,
					solicitudPdf.getValor(),
					solicitudPdf.getNombre(), null);
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
		}
		facesContext.responseComplete();
	}
	
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SolicitudPdf getSolicitudPdf() {
		return this.solicitudPdf;
	}

	public void setSolicitudPdf(SolicitudPdf solicitudPdf) {
		this.solicitudPdf = solicitudPdf;
	}
	

	public UIComponent getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(UIComponent fileUpload) {
		this.fileUpload = fileUpload;
	}
}
