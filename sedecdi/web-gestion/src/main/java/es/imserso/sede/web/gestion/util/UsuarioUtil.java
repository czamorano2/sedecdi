package es.imserso.sede.web.gestion.util;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;


import es.imserso.sede.model.ROLES;
import es.imserso.sede.util.exception.SedeRuntimeException;



/**
 * Clase que se encarga de lo relacionado con el usuario y los permisos.
 * 
 * @author 02858341
 *
 */
@Named
public class UsuarioUtil implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Logger log;

	/**
	 * Is user Logged In?
	 * 
	 * @return true if user has been authenticated.
	 */
	public boolean isAuthenticated() {
		log.debug("comprobando si está autenticado ...");
		return ((HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest()).getUserPrincipal() != null;
	}

	/**
	 * @param rolename
	 * @return true si el usuario tiene el rol
	 */
	public boolean hasRole(@NotNull String rolename) {
		log.debug("comprobando role: " + rolename + " ...");
		if (!enumRolescontains(rolename)){
			throw new SedeRuntimeException("El rol " + rolename + " no es ninguno de los roles de la enumeración Roles");
		}
		return ((HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest()).isUserInRole(rolename);
	}
	
	/**
	 * Comprueba si el string del rol es uno de los elementos de la enumeración Roles
	 * @param rolename
	 * @return
	 */
	private boolean enumRolescontains(@NotNull String rolename) {			
	    for (ROLES rol : ROLES.values()) {
	        if (rol.name().equals(rolename)) {
	            return true;
	        }
	    }
	    return false;
	}	
}
