package es.imserso.sede.web.gestion.view;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.Part;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import es.imserso.sede.model.Idioma;
import es.imserso.sede.model.JustificantePdf;
import es.imserso.sede.web.gestion.util.Utils;


/**
 * Backing bean for JustificantePdf entities.
 * <p/>
 * This class provides CRUD functionality for all JustificantePdf entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class JustificantePdfBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private UIComponent fileUploadjustificante;



	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private JustificantePdf justificantePdf;

	public JustificantePdf getJustificantePdf() {
		return this.justificantePdf;
	}

	public void setJustificantePdf(JustificantePdf justificantePdf) {
		this.justificantePdf = justificantePdf;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.example = new JustificantePdf();
			this.justificantePdf = this.example;
		} else {
			this.justificantePdf = findById(getId());
		}
	}

	public JustificantePdf findById(Long id) {

		return this.entityManager.find(JustificantePdf.class, id);
	}

	/*
	 * Support updating and deleting JustificantePdf entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (justificantePdf.getValor()==null){
				FacesContext.getCurrentInstance().addMessage(fileUploadjustificante.getClientId(), new FacesMessage("Seleccione un archivo"));					
				return null;
			}
			if (this.id == null) {
			 	this.justificantePdf.setIdioma(this.entityManager.find(Idioma.class, justificantePdf.getIdioma().getId()));
				this.entityManager.persist(this.justificantePdf);
				this.entityManager.flush();
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.justificantePdf);
				this.entityManager.flush();
				return "view?faces-redirect=true&id="
						+ this.justificantePdf.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	
	public void listener(FileUploadEvent event) throws Exception {
		UploadedFile item = event.getUploadedFile();
		justificantePdf.setValor(item.getData());
	}

	public String clearUploadData() {
		justificantePdf.setValor(null);
		return null;
	}

	public static String getSubmittedFileName(Part filePart)
    {
        String header = filePart.getHeader("content-disposition");
        if(header == null)
            return null;
        for(String headerPart : header.split(";"))
        {
            if(headerPart.trim().startsWith("filename"))
            {
                return headerPart.substring(headerPart.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
	
	
	public String delete() {
		this.conversation.end();

		try {
			JustificantePdf deletableEntity = findById(getId());

			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public void verPdf(){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			Utils.doDownload(facesContext,
					justificantePdf.getValor(),
					"plantilla_justificante_pdf", null);
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
		}
	}
	
	
	public void saveValorPlantilla(){
		System.out.println("");
	}
	/*
	 * Support searching JustificantePdf entities with pagination
	 */

	private int page;
	private long count;
	private List<JustificantePdf> pageItems;

	private JustificantePdf example = new JustificantePdf();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public JustificantePdf getExample() {
		return this.example;
	}

	public void setExample(JustificantePdf example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<JustificantePdf> root = countCriteria.from(JustificantePdf.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<JustificantePdf> criteria = builder
				.createQuery(JustificantePdf.class);
		root = criteria.from(JustificantePdf.class);
		TypedQuery<JustificantePdf> query = this.entityManager
				.createQuery(criteria.select(root).where(
						getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(
				getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<JustificantePdf> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		byte[] valor = this.example.getValor();
		if (valor != null) {
			predicatesList.add(builder.equal(root.get("valor"), valor));
		}
		String nombre = this.example.getNombre();
		if (nombre != null && !"".equals(nombre)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("nombre")),
					'%' + nombre.toLowerCase() + '%'));
		}
		String descripcion = this.example.getDescripcion();
		if (descripcion != null && !"".equals(descripcion)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("descripcion")),
					'%' + descripcion.toLowerCase() + '%'));
		}
		Idioma idioma = this.example.getIdioma();
		if (idioma != null) {
			predicatesList.add(builder.equal(root.get("idioma"), idioma));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<JustificantePdf> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back JustificantePdf entities (e.g. from inside
	 * an HtmlSelectOneMenu)
	 */

	public List<JustificantePdf> getAll() {

		CriteriaQuery<JustificantePdf> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(JustificantePdf.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(JustificantePdf.class)))
				.getResultList();
	}

	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final JustificantePdfBean ejbProxy = this.sessionContext
				.getBusinessObject(JustificantePdfBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context,
					UIComponent component, String value) {

				return ejbProxy.findById(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context,
					UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((JustificantePdf) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private JustificantePdf add = new JustificantePdf();

	public JustificantePdf getAdd() {
		return this.add;
	}

	public JustificantePdf getAdded() {
		JustificantePdf added = this.add;
		this.add = new JustificantePdf();
		return added;
	}
	
	public UIComponent getFileUploadjustificante() {
		return fileUploadjustificante;
	}

	public void setFileUploadjustificante(UIComponent fileUploadjustificante) {
		this.fileUploadjustificante = fileUploadjustificante;
	}

}
