package es.imserso.sede.web.gestion.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.jboss.logging.Logger;

import es.imserso.sede.model.Usuario;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.model.Tramite;
import java.util.Iterator;

/**
 * Backing bean for Usuario entities.
 * <p/>
 * This class provides CRUD functionality for all Usuario entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@SessionScoped
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private static final String MSG_ALERT_NO_USER_IN_AD = "ALERTA: este usuario existe en la base de datos pero no tiene role de Sede gestión en el Active Directory. Por favor, consulte con informática.";

	private static final String MSG_ALERT_NO_USER_IN_DB = "ATENCIÓN: Este usuario existe en el Active Directory pero hasta que no le dé de alta aquí no es un usuario de la aplicación (click en el botón 'alta')";

	private static final String MSG_ALERT_NO_USER_CENTERS = "ATENCIÓN: Este usuario no tiene asignado ningún centro.";

	/**
	 * nombre de usuario seleccionado para modificar
	 */
	private String selectedUserName;
	

	/**
	 * usuario seleccionado para modificar
	 */
	private Usuario selectedUser;
	
	/**
	 * tramites disponibles para asignar al usuario seleccionado.
	 */
	private List<Tramite> avalaibleOptions;
	
	/**
	 * tramites asignados al usuario seleccionado.
	 */	
	private List<Tramite> assignedOptions;
	
	public void setAssignedOptions(List<Tramite> assignedOptions) {
		this.assignedOptions = assignedOptions;
	}

	@Inject
	protected Logger log;

	public List<Tramite> getAvalaibleOptions() {		
		return avalaibleOptions;
		
	}

	public void setAvalaibleOptions(List<Tramite> avalaibleOptions) {
		this.avalaibleOptions = avalaibleOptions;
	}

	public List<Tramite> getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(List<Tramite> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	/**
	 * centros asignados al usuario seleccionado.
	 */
	private List<Tramite> selectedOptions;

	/*
	 * Support creating and retrieving Usuario entities
	 */

	public Usuario getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(Usuario selectedUser) {
		this.selectedUser = selectedUser;
	}

	public String getSelectedUserName() {
		return selectedUserName;
	}

	public void setSelectedUserName(String selectedUserName) {
		this.selectedUserName = selectedUserName;
	}

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Usuario usuario;

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.example = new Usuario();
			this.usuario = this.example;
		} else {
			this.usuario = findById(getId());
		}
	}

	public Usuario findById(Long id) {

		return this.entityManager.find(Usuario.class, id);
	}

	/*
	 * Support updating and deleting Usuario entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.usuario);
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.usuario);
				return "view?faces-redirect=true&id=" + this.usuario.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Usuario deletableEntity = findById(getId());
			Iterator<Tramite> iterTramites = deletableEntity.getTramites()
					.iterator();
			for (; iterTramites.hasNext();) {
				Tramite nextInTramites = iterTramites.next();
				nextInTramites.getUsuarios().remove(deletableEntity);
				iterTramites.remove();
				this.entityManager.merge(nextInTramites);
			}
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}
	
	
	/**
	 * @return lista con los usuarios (tanto del LDAP como de la bbdd)
	 */
	public List<NombreComentario> getUserList() {
		List<NombreComentario> list = new ArrayList<NombreComentario>();

		List<Usuario> dbList = getAll();
		// cargamos los usuarios del ldap
		PropertyComponent propertyComponent = (PropertyComponent) UtilsCDI
				.getBeanByContext(PropertyComponent.class);
		for (String user : propertyComponent.getUserList()) {
			// comprobamos si está el usuario en la bbdd
			boolean encontrado=false;
			for (Usuario usuario:dbList){				
				if (user.equals(usuario.getUsuario())) {
					encontrado=true;
					// comprobamos si tiene tramites asociados
					if (getTramitesPorUsuario(user) <= 0) {
						list.add(new NombreComentario(user,
								MSG_ALERT_NO_USER_CENTERS,
								NombreComentario.MESSAGE_COLOR_WARNING));
					} else {
						list.add(new NombreComentario(user, ""));
					}
				}			
			}
			if (!encontrado){
				list.add(new NombreComentario(user, MSG_ALERT_NO_USER_IN_DB,
					NombreComentario.MESSAGE_COLOR_INFO, false));
			}
		}

		// cargamos los usuarios de la bbdd
		for (Usuario user : dbList) {
			if (!alreadyExistsInList(list, user.getUsuario())) {
				// comprobamos si está el usuario en el Active Directory
				if (propertyComponent.getUserList().contains(user)) {
					if (getTramitesPorUsuario(user.getUsuario()) <= 0) {
						list.add(new NombreComentario(user.getUsuario(),
								MSG_ALERT_NO_USER_CENTERS,
								NombreComentario.MESSAGE_COLOR_WARNING));
					} else {
						list.add(new NombreComentario(user.getUsuario(), ""));
					}
				} else {
					list.add(new NombreComentario(user.getUsuario(),
							MSG_ALERT_NO_USER_IN_AD,
							NombreComentario.MESSAGE_COLOR_ALERT));
				}
			}
		}
		return list;

	}

	
	/**
	 * @return verdadero si el usuario ya existe en la lista de usuarios
	 */
	private boolean alreadyExistsInList(List<NombreComentario> list, String user) {
		for (NombreComentario element : list) {
			if (user.equals(element.getNombre())) {
				return true;
			}
		}
		return false;
	}


	
	/**
	 * @return da de alta el usuario en la bbd y redirige a la vista de edición
	 *         de usuarios-centros
	 */
	public String createUser(String username) {
		try {
			List<Usuario> usuarios = findByName(username);
			if (usuarios == null || usuarios.isEmpty()) {
				Usuario usuario = new Usuario();
				usuario.setUsuario(username);
				entityManager.persist(usuario);
				entityManager.flush();
				setSelectedUserName(username);
				setSelectedUser(usuario);
				return "usuarioEdit";

			} else {
				String errmsg = "el usuario " + username
						+ " ya exist en la base de datos!";
				log.error(errmsg);
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, errmsg,
								errmsg));
			}

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, errmsg,
							errmsg));
		}
		return null;
	}

	/**
	 * @return lista de trámites ya asignados al usuario
	 */
	public List<Tramite> getAssignedOptions() {
		return assignedOptions;
	}
	
	
	/**
	 * @return lista de trámites ya asignados al usuario
	 */
	public List<Tramite> tramitesAsignados() {		
		assignedOptions=new ArrayList<>();
		assignedOptions.addAll(selectedUser.getTramites());
		return assignedOptions;
	}
	
	
	public void assign(Tramite tramite) {
		try {
			if (tramite != null && getSelectedUser() != null) {
				log.info("se asigna el tramite " + tramite.toString()+ " al usuario " + getSelectedUserName());
				selectedUser = entityManager.find(Usuario.class, selectedUser.getId());
				selectedUser.addTramite(tramite);
				entityManager.flush();
				entityManager.clear();
				if (assignedOptions==null || assignedOptions.isEmpty()){
					assignedOptions=new ArrayList<>();
				}
				assignedOptions.add(tramite);
				avalaibleOptions.remove(tramite);
				log.info("tramite asignado!");
			}
		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, errmsg,
							errmsg));
		}
	}
	
	

	
	
	public String unassign(Tramite tramite) {

		try {
			if (tramite != null && getSelectedUser() != null) {
				log.info("se desasigna el tramite " + tramite.toString()
						+ " del usuario " + getSelectedUserName());
				//desasignamos el tramite 
				List<Tramite> list=null;
				selectedUser = entityManager.find(Usuario.class, selectedUser.getId());
				selectedUser.removeTramite(tramite);				
				entityManager.flush();
				entityManager.clear();
				assignedOptions.remove(tramite);
				avalaibleOptions.add(tramite);
				log.info("tramite desasignado!");

				// si el usuario no tiene tramites asignados se elimina de la bbdd
				if (selectedUser.getTramites()==null || selectedUser.getTramites().isEmpty()) {
					log.info("se elimina el usuario al no tener centros asignados!");
					setSelectedUser(entityManager.merge(getSelectedUser()));
					entityManager.remove(getSelectedUser());
					entityManager.flush();
					entityManager.clear();
					log.info("usuario eliminado!");
					return volver();
				}
				
			}
		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, errmsg,
							errmsg));
		}
		return null;
	}
	
	/**
	 * @return lista de centros ya asignados al usuario
	 */
	public int getNumAssignedOptions() {
		if (getSelectedUser() != null) {
			return getSelectedUser().getTramites().size();
		}
		return -1;
	}
	/**
	 * @return vuelve a la vista de selección de usuario
	 */
	public String volver() {
		return "usuarioSearch";
	}

	
	/**
	 * @return redirige a la vista de edición de usuarios-centros
	 */
	public String selectUser(String username) {
		try {
			setSelectedUserName(username);
			setSelectedUser(entityManager.merge(this.findByName(username).get(0)));
			tramitesAsignados();
			tramitesSinAsignar();
			return "usuarioEdit";

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, errmsg,
							errmsg));
		}
		return null;
	}
	/*
	 * Support searching Usuario entities with pagination
	 */

	private void tramitesSinAsignar() {
		avalaibleOptions= new ArrayList<>();
		avalaibleOptions = (List<Tramite>) entityManager.createQuery("select t from Tramite t, Usuario u where u.id = :usuarioId and not t in elements(u.tramites)").setParameter("usuarioId", selectedUser.getId()).getResultList();		
	}

	private int page;
	private long count;
	private List<Usuario> pageItems;

	private Usuario example = new Usuario();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Usuario getExample() {
		return this.example;
	}

	public void setExample(Usuario example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Usuario> root = countCriteria.from(Usuario.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<Usuario> criteria = builder.createQuery(Usuario.class);
		root = criteria.from(Usuario.class);
		TypedQuery<Usuario> query = this.entityManager.createQuery(criteria
				.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(
				getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Usuario> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String usuario = this.example.getUsuario();
		if (usuario != null && !"".equals(usuario)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("usuario")),
					'%' + usuario.toLowerCase() + '%'));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<Usuario> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Usuario entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<Usuario> getAll() {

		CriteriaQuery<Usuario> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(Usuario.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(Usuario.class))).getResultList();
	}

	
	public int getTramitesPorUsuario(String usuario) {
		int cont=0;
		CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Usuario> query = criteriaBuilder.createQuery(Usuario.class);
		Root<Usuario> c = query.from(Usuario.class);
		query.select(c);
		ParameterExpression<String> p = criteriaBuilder.parameter(String.class);
		query.where(criteriaBuilder.equal(c.get("usuario"), usuario));
		
		List<Usuario> resultList = entityManager.createQuery(query).getResultList();
		if (resultList!=null && !resultList.isEmpty()){
			cont = resultList.size();
		}
	
		return cont;
	}
	
	public List<Usuario> findByName(String usuario) {		
		CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Usuario> query = criteriaBuilder.createQuery(Usuario.class);
		Root<Usuario> c = query.from(Usuario.class);
		query.select(c);
		ParameterExpression<String> p = criteriaBuilder.parameter(String.class);
		query.where(criteriaBuilder.equal(c.get("usuario"), usuario));
		return entityManager.createQuery(query).getResultList();
	}

	
	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final UsuarioBean ejbProxy = this.sessionContext
				.getBusinessObject(UsuarioBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context,
					UIComponent component, String value) {

				return ejbProxy.findById(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context,
					UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Usuario) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Usuario add = new Usuario();

	public Usuario getAdd() {
		return this.add;
	}

	public Usuario getAdded() {
		Usuario added = this.add;
		this.add = new Usuario();
		return added;
	}
	
	
	public class NombreComentario {
		private String nombre;
		private String comentario;
		/**
		 * Indica si el usuario está habilitado paraeditado en la aplicación
		 * (por ejemplo, si no existe en la bbdd no puede ser editado)
		 */
		private boolean userEnabled;

		static final String MESSAGE_COLOR_INFO = "#A65712";
		static final String MESSAGE_COLOR_WARNING = "#AFB50B";
		static final String MESSAGE_COLOR_ALERT = "red";

		private String messageColor;

		public NombreComentario(String nombre, String comentario) {
			this.nombre = nombre;
			this.comentario = comentario;
			this.messageColor = MESSAGE_COLOR_INFO;
			this.userEnabled = true;
		}

		public NombreComentario(String nombre, String comentario,
				String messageColor) {
			this.nombre = nombre;
			this.comentario = comentario;
			this.messageColor = messageColor;
			this.userEnabled = true;
		}

		public NombreComentario(String nombre, String comentario,
				String messageColor, boolean userEnabled) {
			this.nombre = nombre;
			this.comentario = comentario;
			this.messageColor = messageColor;
			this.userEnabled = userEnabled;
		}

		/**
		 * @return the nombre
		 */
		public String getNombre() {
			return nombre;
		}

		/**
		 * @param nombre
		 *            the nombre to set
		 */
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		/**
		 * @return the comentario
		 */
		public String getComentario() {
			return comentario;
		}

		/**
		 * @param comentario
		 *            the comentario to set
		 */
		public void setComentario(String comentario) {
			this.comentario = comentario;
		}

		/**
		 * @return the userEnabled
		 */
		public boolean getUserEnabled() {
			return userEnabled;
		}

		/**
		 * @param userEnabled
		 *            the userEnabled to set
		 */
		public void setUserEnabled(boolean userEnabled) {
			this.userEnabled = userEnabled;
		}

		/**
		 * @return the messageColor
		 */
		public String getMessageColor() {
			return messageColor;
		}

		/**
		 * @param messageColor
		 *            the messageColor to set
		 */
		public void setMessageColor(String messageColor) {
			this.messageColor = messageColor;
		}
	}
}

