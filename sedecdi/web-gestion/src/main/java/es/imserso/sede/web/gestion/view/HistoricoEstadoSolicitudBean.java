package es.imserso.sede.web.gestion.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import es.imserso.sede.model.HistoricoEstadoSolicitud;
import es.imserso.sede.model.Estado;
import es.imserso.sede.model.Solicitud;

/**
 * Backing bean for HistoricoEstadoSolicitud entities.
 * <p/>
 * This class provides CRUD functionality for all HistoricoEstadoSolicitud
 * entities. It focuses purely on Java EE 6 standards (e.g.
 * <tt>&#64;ConversationScoped</tt> for state management,
 * <tt>PersistenceContext</tt> for persistence, <tt>CriteriaBuilder</tt> for
 * searches) rather than introducing a CRUD framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class HistoricoEstadoSolicitudBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving HistoricoEstadoSolicitud entities
	 */

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private HistoricoEstadoSolicitud historicoEstadoSolicitud;

	public HistoricoEstadoSolicitud getHistoricoEstadoSolicitud() {
		return this.historicoEstadoSolicitud;
	}

	public void setHistoricoEstadoSolicitud(
			HistoricoEstadoSolicitud historicoEstadoSolicitud) {
		this.historicoEstadoSolicitud = historicoEstadoSolicitud;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.historicoEstadoSolicitud = this.example;
		} else {
			this.historicoEstadoSolicitud = findById(getId());
		}
	}

	public HistoricoEstadoSolicitud findById(Long id) {

		return this.entityManager.find(HistoricoEstadoSolicitud.class, id);
	}

	/*
	 * Support updating and deleting HistoricoEstadoSolicitud entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.historicoEstadoSolicitud);
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.historicoEstadoSolicitud);
				return "view?faces-redirect=true&id="
						+ this.historicoEstadoSolicitud.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			HistoricoEstadoSolicitud deletableEntity = findById(getId());
			Solicitud solicitud = deletableEntity.getSolicitud();
			solicitud.getHistoricos().remove(deletableEntity);
			deletableEntity.setSolicitud(null);
			this.entityManager.merge(solicitud);
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching HistoricoEstadoSolicitud entities with pagination
	 */

	private int page;
	private long count;
	private List<HistoricoEstadoSolicitud> pageItems;

	private HistoricoEstadoSolicitud example = new HistoricoEstadoSolicitud();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public HistoricoEstadoSolicitud getExample() {
		return this.example;
	}

	public void setExample(HistoricoEstadoSolicitud example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<HistoricoEstadoSolicitud> root = countCriteria
				.from(HistoricoEstadoSolicitud.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<HistoricoEstadoSolicitud> criteria = builder
				.createQuery(HistoricoEstadoSolicitud.class);
		root = criteria.from(HistoricoEstadoSolicitud.class);
		TypedQuery<HistoricoEstadoSolicitud> query = this.entityManager
				.createQuery(criteria.select(root).where(
						getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(
				getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<HistoricoEstadoSolicitud> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String usuario = this.example.getUsuario();
		if (usuario != null && !"".equals(usuario)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("usuario")),
					'%' + usuario.toLowerCase() + '%'));
		}
		Solicitud solicitud = this.example.getSolicitud();
		if (solicitud != null) {
			predicatesList.add(builder.equal(root.get("solicitud"), solicitud));
		}
		Estado estado = this.example.getEstado();
		if (estado != null) {
			predicatesList.add(builder.equal(root.get("estado"), estado));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<HistoricoEstadoSolicitud> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back HistoricoEstadoSolicitud entities (e.g.
	 * from inside an HtmlSelectOneMenu)
	 */

	public List<HistoricoEstadoSolicitud> getAll() {

		CriteriaQuery<HistoricoEstadoSolicitud> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(
						HistoricoEstadoSolicitud.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(HistoricoEstadoSolicitud.class)))
				.getResultList();
	}

	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final HistoricoEstadoSolicitudBean ejbProxy = this.sessionContext
				.getBusinessObject(HistoricoEstadoSolicitudBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context,
					UIComponent component, String value) {

				return ejbProxy.findById(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context,
					UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((HistoricoEstadoSolicitud) value)
						.getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private HistoricoEstadoSolicitud add = new HistoricoEstadoSolicitud();

	public HistoricoEstadoSolicitud getAdd() {
		return this.add;
	}

	public HistoricoEstadoSolicitud getAdded() {
		HistoricoEstadoSolicitud added = this.add;
		this.add = new HistoricoEstadoSolicitud();
		return added;
	}
}
