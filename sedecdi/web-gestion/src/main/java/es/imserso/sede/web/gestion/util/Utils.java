package es.imserso.sede.web.gestion.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public class Utils {

	

	/**
	 * Envia un array de bytes al ServletResponse output stream.
	 * 
	 * @param req
	 *            El request
	 * @param resp
	 *            El response
	 * @param fileByteArray
	 *            El array de bytes del fichero que se va a descargar
	 * @param original_filename
	 *            El nombre que recibira el cliente
	 * @param mimetype
	 *            El tipo mime del fichero que se va a descargar. Si es null se
	 *            usara 'application/pdf'
	 */
	public static void doDownload(FacesContext facesContext, byte[] fileByteArray, String defaultFilename,
			String mimetype) throws IOException {

		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

		int length = 0;
		ServletOutputStream op = response.getOutputStream();

		//
		// response
		//
		//
		response.setContentType((mimetype != null) ? mimetype : "application/pdf");
		response.setContentLength((int) fileByteArray.length);
		if (defaultFilename == null) {
			response.setHeader("Content-Disposition", "application/pdf" + "; filename=\"\"");
		} else {
			response.setHeader("Content-Disposition", "application/pdf" + "; filename=\"" + defaultFilename + "\"");

		}

		//
		// stream
		//
		InputStream is = new ByteArrayInputStream(fileByteArray);

		while ((is != null) && ((length = is.read(fileByteArray)) != -1)) {
			op.write(fileByteArray, 0, length);
		}

		is.close();
		op.flush();
		op.close();
	}

}
