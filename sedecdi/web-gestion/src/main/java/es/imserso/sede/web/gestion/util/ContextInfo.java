package es.imserso.sede.web.gestion.util;

import java.security.Principal;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;

/**
 * EJB que actúa como proveedor de información del contexto.
 * <p>
 * El contexto de sesión se inyecta usando la anotación Resource.
 * 
 * @author 11825775
 *
 */
@Stateless
@Named
public class ContextInfo {

	@Inject
	private Logger log;

	/**
	 * Session context injected using the resource annotation
	 */
	@Resource
	private SessionContext ctx;

	/**
	 * Secured EJB method using security annotations
	 */
	public String getSecurityInfo() {
		
		String sPrincipal = null;
		Principal principal = ctx.getCallerPrincipal();
		if (principal != null) {
			sPrincipal = principal.toString();
			log.debug("principal: " + sPrincipal);
		}
		return sPrincipal;
	}

	/**
	 * @return nombre del usuario logado
	 */
	@Produces
	@Named
	public String getUserPrincipal() {
		String sPrincipal = null;
		Principal principal = ctx.getCallerPrincipal();
		if (principal != null) {
			sPrincipal = principal.toString();
			log.debug("principal: " + sPrincipal);
		}
		return sPrincipal;
	}

	/**
	 * @return true si existe un usuario logado.
	 */
	@Produces
	@Named
	public boolean isLoggedIn() {
		return !((HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest()).getSession().isNew();
	}

	

}
