package es.imserso.sede.web.gestion.remesasTermalismo;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.hibernate.Session;
import org.jboss.logging.Logger;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.impl.termalismo.TermalismoDTO;
import es.imserso.sede.model.RemesaDescarga;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.model.Tramite;
import es.imserso.sede.service.converter.ConverterException;
import es.imserso.sede.service.converter.impl.PdfUtil;
import es.imserso.sede.util.DateUtil;
import es.imserso.sede.util.FicheroWriter;
import es.imserso.sede.util.GlobalTerma;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.web.gestion.util.PruebaBean;

@Stateful
@TransactionManagement(TransactionManagementType.BEAN)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@AccessTimeout(60000)
public class RemesaTermalismo {

	protected String strFechaDesde;
	@Inject
	private Logger log;
	private String nombreFichero;

	private String encodingString;

	private static final String NOMBRE_FICHERO_REMESA_PREFIX = "SEDE_";


	@Inject
	private FicheroWriter ficheroWriter;

	private String rutaFichero;

	private StringBuffer linea;

	TermalismoDTO termalismoDTO;

	private String date;
	private String balnearios;
	private String plazasSolicitadas;
	private String turnosSolicitados;
	private String tratamientos;
	private String requisitos;
	private String informe;
	private String pension;
	private BigDecimal fraction;
	private BigDecimal entero;
	private String importe;
	private String nivelAfectacion;
	private String articulacionesAfectadas;
	private String padecimiento;
	private String viasRespAfectadas;
	private String tipoDuracionTurno;
	private String consentimiento;
	private Hashtable<String, String> extractData;
	private List<Solicitud> listaSolicitudesTermalismo;

	@PersistenceContext(unitName = "primary", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	private Session session;

	@Inject
	private PruebaBean pruebaBean;

	public void generarRemesa(Date fechaDesde, Date fechaHasta) throws SedeException {

		try {

			log.debug("preparando el Encoding para el fichero de salida...");
			encodingString = "Latin1";
			log.info("encoding para el fichero de salida: {0}", encodingString, null);

			if (fechaDesde == null) {
				strFechaDesde = "Hasta_";

			} else {
				strFechaDesde = DateUtil.parseDateToStringNoSeparator(fechaDesde) + "_";
				log.debug("preparando fecha desde...");

			}

			log.debug("componiendo nombre de fichero de salida...");
			nombreFichero = NOMBRE_FICHERO_REMESA_PREFIX + strFechaDesde
					+ DateUtil.parseDateToStringNoSeparator(fechaHasta);

			log.info("nombre de fichero de salida: {0}", nombreFichero, null);

			log.debug("preparando fecha hasta... (asignándole horas hasta las 23:59:59,999)");

			// TODO en qué ruta dejamos el fichero??
			rutaFichero = "E:/tmp";

			session = ((Session) entityManager.getDelegate()).getSessionFactory().openSession();

			session.beginTransaction();

			// buscamos las solicitudes de termalismo que coincidan su fecha de
			// alta entre fecha desde y hasta
			listaSolicitudesTermalismo = entityManager.createNamedQuery("solicitudesTermalismo.remesa", Solicitud.class)
					.setParameter("codigoSIA", TipoTramite.TURISMO.getSia()).setParameter("fechaDesde", fechaDesde)
					.setParameter("fechaHasta", fechaHasta).getResultList();

			if (listaSolicitudesTermalismo != null && listaSolicitudesTermalismo.size() > 0) {

				ficheroWriter.abrirFichero(encodingString, rutaFichero, nombreFichero, "", false);

				for (Solicitud solicitud : listaSolicitudesTermalismo) {
					// obtenemos el pdf de registro y y escribimos en el fichero
					// extractData = PdfUtil.extractData(solicitud);
					termalismoDTO = (TermalismoDTO) Utils.setValuesFromHashtable(createTermalismoDtoInstance(), extractData);
					escribeFichero(termalismoDTO);
				}
				altaRemesaTermalismo(fechaDesde, fechaHasta);
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "No se han encontrado solicitudes", null));
			}

			// TODO quitar para cuando tengamos solicitudes de termalismo. SÓLO
			// para probar que escribe en fichero correctamente
			SOLOPARAPRUEBAS(fechaDesde, fechaHasta);

			ficheroWriter.cerrarFichero();
		} catch (Exception e) {
			throw new SedeException(e.getMessage());
		}

	}

	private void SOLOPARAPRUEBAS(Date fechaDesde, Date fechaHasta)
			throws ConverterException, IllegalAccessException, SedeException {
		ficheroWriter.abrirFichero(encodingString, rutaFichero, nombreFichero, "", false);
		extractData = PdfUtil.extractData(pruebaBean.generarPdfTermalismoPrueba());
		termalismoDTO = (TermalismoDTO) Utils.setValuesFromHashtable(createTermalismoDtoInstance(), extractData);
		escribeFichero(termalismoDTO);
		altaRemesaTermalismo(fechaDesde, fechaHasta);
	}

	private void altaRemesaTermalismo(Date fechaDesde, Date fechaHasta) {
		RemesaDescarga remesaDescarga = new RemesaDescarga();
		remesaDescarga.setFechaDescarga(new Date());
		remesaDescarga.setFechaDesde(fechaDesde);
		remesaDescarga.setFechaHasta(fechaHasta);
		// comprobamos que la lista no esté
		if (listaSolicitudesTermalismo != null && listaSolicitudesTermalismo.size() > 0)
			remesaDescarga.setNumeroSolicitudes(String.valueOf(listaSolicitudesTermalismo.size()));
		else
			remesaDescarga.setNumeroSolicitudes("0");

		remesaDescarga.setTramite(entityManager.createNamedQuery("tramite.PorCodigoSia", Tramite.class)
				.setParameter("codigoSIA", TipoTramite.TERMALISMO.getSia()).getResultList().get(0));
		entityManager.persist(remesaDescarga);
		entityManager.flush();
		session.getTransaction().commit();
	}

	private void escribeFichero(TermalismoDTO termalismoDTO) throws SedeException {
		
		/** COMENTADO POR CARLOS
		linea = new StringBuffer();
		// fecha registro
		linea.append(DateUtil.parseDateToStringNoSeparator(termalismoDTO.getFisc()));
		// numero expediente
		linea.append(Utils.lpad(termalismoDTO.getNreg().toString(), 12, '0'));
		// apellidos y nombre
		linea.append(
				Utils.buildName(termalismoDTO.getNombre(), termalismoDTO.getApellido1(), termalismoDTO.getApellido2()));
		// clave sexo solicitante
		linea.append(!Utils.isEmpty(termalismoDTO.getSexo()) ? Utils.rpad(termalismoDTO.getSexo(), 1, ' ')
				: Utils.rpad("", 1, ' '));
		// clave estado civil
		linea.append(!Utils.isEmpty(termalismoDTO.getEciv()) ? Utils.rpad(termalismoDTO.getEciv(), 1, ' ')
				: Utils.rpad("", 1, ' '));
		// fecha nacimiento solicitante
		linea.append(buildDate(termalismoDTO.getFenadia(), termalismoDTO.getFenames(), termalismoDTO.getFenaanio()));
		// nif solicitante
		linea.append(buildNIF(termalismoDTO.getDocumentoIdentificacion()));
		// domicilio solicitante
		linea.append(Utils.rpad(termalismoDTO.getDomicilioResidencia(), 50, ' '));
		// telefono solicitante
		linea.append(Utils.lpad(termalismoDTO.getTelefono(), 10, '0'));
		// localidad solicitante
		linea.append(Utils.rpad(termalismoDTO.getLocalidadResidencia(), 41, ' '));
		// CP solicitante
		linea.append(Utils.lpad(termalismoDTO.getCodigoPostalResidencia(), 5, '0'));
		// clave provincia domicilio
		linea.append(Utils.lpad(termalismoDTO.getProvinciaResidencia(), 2, '0'));
		// apellidos y nombre conyuge
		linea.append(Utils.buildName(termalismoDTO.getNombrec(), termalismoDTO.getApe1c(), termalismoDTO.getApe2c()));
		// nif conyuge
		linea.append(buildNIF(termalismoDTO.getDnic()));
		// fecha nacimiento conyuge
		linea.append(buildDate(termalismoDTO.getFencdia(), termalismoDTO.getFencmes(), termalismoDTO.getFencanio()));
		// balnearios solicitados
		linea.append(buildBalneariosSolicitados());
		// plazas solicitadas
		linea.append(buildPlazasSolicitadas());
		// turnos solicitados
		linea.append(buildTurnosSolicitados());
		// clase pension (Z,R,X...) e importe. (distinto de cero. los dos
		// últimos dígitos son los decimales) PENSION SOLICITANTE 1
		linea.append(buildPension(termalismoDTO.getClasPenSol1(), termalismoDTO.getImpoPenSol1()));
		// clase pension (Z,R,X...) e importe. (distinto de cero. los dos
		// últimos dígitos son los decimales) PENSION SOLICITANTE 2
		linea.append(buildPension(termalismoDTO.getClasPenSol2(), termalismoDTO.getImpoPenSol2()));
		// clase pension (Z,R,X...) e importe. (distinto de cero. los dos
		// últimos dígitos son los decimales) PENSION SOLICITANTE 3
		linea.append(buildPension(null, null));

		// clase pension (Z,R,X...) e importe. (distinto de cero. los dos
		// últimos dígitos son los decimales) PENSION CONYUGE 1
		linea.append(buildPension(termalismoDTO.getClasPenCon1(), termalismoDTO.getImpoPenCon1()));
		// clase pension (Z,R,X...) e importe. (distinto de cero. los dos
		// últimos dígitos son los decimales) PENSION CONYUGE 2
		linea.append(buildPension(termalismoDTO.getClasPenCon2(), termalismoDTO.getImpoPenCon2()));
		// clase pension (Z,R,X...) e importe. (distinto de cero. los dos
		// últimos dígitos son los decimales) PENSION CONYUGE 3
		linea.append(buildPension(null, null));

		// otros ingresos
		linea.append(termalismoDTO.getOtrs() != null ? buildImport(termalismoDTO.getOtrs()) : Utils.lpad("", 6, '0'));
		// apellidos y nombre instancia relacionada
		linea.append(
				Utils.buildName(termalismoDTO.getApernom(), termalismoDTO.getAperape1(), termalismoDTO.getAperape2()));
		// nif instancia relacionada (no obligatorio)
		linea.append(buildNIF(termalismoDTO.getDnir()));
		// requisitos solicitante
		linea.append(buildRequisitos(termalismoDTO.getTreqSol1()) + buildRequisitos(termalismoDTO.getTreqSol2())
				+ buildRequisitos(termalismoDTO.getTreqSol3()));
		// expediente relacionado
		linea.append(!Utils.isEmpty(termalismoDTO.getExpr()) ? Utils.lpad(termalismoDTO.getExpr(), 12, '0')
				: Utils.lpad("", 12, '0'));

		////  CARLOS   ****DATOS SOLICITANTE
		// nº informe médico
		linea.append(buildInformeMedico(termalismoDTO.getInfm1(), GlobalTerma.SOLICITANTE));
		// tratamiento solicitante		
		linea.append(buildTratamiento(termalismoDTO.getTratSol1(),termalismoDTO.getTratSol2(), termalismoDTO.getTratSol3(), 
				termalismoDTO.getTratSol4(), termalismoDTO.getTratSol5(), termalismoDTO.getTratSol6()));
				
		// nº articulaciones afectadas
		linea.append(termalismoDTO.getNart1() != null ? Utils.rpad(termalismoDTO.getNart1().toString(), 1, ' ')
				: Utils.rpad("", 1, ' '));
		// nº articulaciones afectadas
		linea.append(termalismoDTO.getLate1() != null ? Utils.rpad(termalismoDTO.getLate1().toString(), 1, ' ')
				: Utils.rpad("", 1, ' '));
		// articulaciones afectadas
		linea.append(buildArticulacionesAfectadas(termalismoDTO.getArtiSol1(), termalismoDTO.getArtiSol2(),
				termalismoDTO.getArtiSol3(), termalismoDTO.getArtiSol4(), termalismoDTO.getArtiSol5(),
				termalismoDTO.getArtiSol6()));
		// tiene o padede: dificultad movimiento, dolor...
		linea.append(buildPadecimiento(termalismoDTO.getDefoSol1(), termalismoDTO.getDefoSol2(),
				termalismoDTO.getDefoSol3(), termalismoDTO.getDefoSol4()));
		// vias respiratorias afectadas
		linea.append(buildViasRespiratoriasAfectadas(termalismoDTO.getVresSolA(), termalismoDTO.getVresSolB()));
		// nivel de afectacion
		linea.append(buildNivelAfectacion(termalismoDTO.getImpoSol1(), termalismoDTO.getImpoSol2(),
				termalismoDTO.getImpoSol3(), termalismoDTO.getImpoSol4()));
		// nº regularizaciones último año
		linea.append(termalismoDTO.getNred1() != null ? Utils.lpad(termalismoDTO.getNred1().toString(), 1, '0')
				: Utils.lpad("", 1, '0'));
		// procesos agudos último año
		linea.append(termalismoDTO.getCdef1() != null ? Utils.lpad(termalismoDTO.getCdef1().toString(), 1, '0')
				: Utils.lpad("", 1, '0'));

		////  CARLOS   ****DATOS CONYUGE
		// nº informe médico
		linea.append(buildInformeMedico(termalismoDTO.getInfm2(), GlobalTerma.CONYUGE));
		// tratamiento conyuge
		linea.append(buildTratamiento(termalismoDTO.getTratCon1(),termalismoDTO.getTratCon2(), termalismoDTO.getTratCon3(), 
					termalismoDTO.getTratCon4(), termalismoDTO.getTratCon5(), termalismoDTO.getTratCon6()));
		// nº articulaciones afectadas
		linea.append(termalismoDTO.getNart2() != null ? Utils.rpad(termalismoDTO.getNart2().toString(), 1, ' ')
				: Utils.rpad("", 1, ' '));
		// nº articulaciones afectadas
		linea.append(termalismoDTO.getLate2() != null ? Utils.rpad(termalismoDTO.getLate2().toString(), 1, ' ')
				: Utils.rpad("", 1, ' '));
		// articulaciones afectadas
		linea.append(buildArticulacionesAfectadas(termalismoDTO.getArtiCon1(), termalismoDTO.getArtiCon2(),
				termalismoDTO.getArtiCon3(), termalismoDTO.getArtiCon4(), termalismoDTO.getArtiCon5(),
				termalismoDTO.getArtiCon6()));
		// tiene o padede: dificultad movimiento, dolor...
		linea.append(buildPadecimiento(termalismoDTO.getDefoCon1(), termalismoDTO.getDefoCon2(),
				termalismoDTO.getDefoCon3(), termalismoDTO.getDefoCon4()));
		// vias respiratorias afectadas
		linea.append(buildViasRespiratoriasAfectadas(termalismoDTO.getVresSolA(), termalismoDTO.getVresSolB()));
		// nivel de afectacion
		linea.append(buildNivelAfectacion(termalismoDTO.getImpoCon1(), termalismoDTO.getImpoCon2(),
				termalismoDTO.getImpoCon3(), termalismoDTO.getImpoCon4()));
		// nº regularizaciones último año
		linea.append(termalismoDTO.getNred2() != null ? Utils.lpad(termalismoDTO.getNred2().toString(), 1, '0')
				: Utils.lpad("", 1, '0'));
		// procesos agudos último año
		linea.append(termalismoDTO.getCdef2() != null ? Utils.lpad(termalismoDTO.getCdef2().toString(), 1, '0')
				: Utils.lpad("", 1, '0'));

		// hotel. Siempre viene 0
		linea.append(termalismoDTO.getHotel() != null ? Utils.lpad(termalismoDTO.getHotel().toString(), 1, '0')
				: Utils.lpad("", 1, '0'));
		// tipo familia numerosa
		linea.append(!Utils.isEmpty(termalismoDTO.getTfam()) ? Utils.rpad(termalismoDTO.getTfam(), 1, ' ')
				: Utils.rpad("", 1, ' '));
		// presentado libro familia numerosa. Siempre viene 0
		linea.append(termalismoDTO.getTlibro() != null ? Utils.lpad(termalismoDTO.getTlibro().toString(), 1, '0')
				: Utils.lpad("", 1, '0'));
		// CODIGO familia numerosa
		linea.append(!Utils.isEmpty(termalismoDTO.getCfam()) ? Utils.rpad(termalismoDTO.getCfam(), 24, ' ')
				: Utils.rpad("", 24, ' '));
		// email
		linea.append(!Utils.isEmpty(termalismoDTO.getEmail()) ? Utils.rpad(termalismoDTO.getEmail(), 65, ' ')
				: Utils.rpad("", 65, ' '));
		// fax
		linea.append(termalismoDTO.getFax() != null ? Utils.lpad(termalismoDTO.getFax().toString(), 10, '0')
				: Utils.lpad("", 10, '0'));
		// tfno 2
		linea.append(termalismoDTO.getTelefono2() != null ? Utils.lpad(termalismoDTO.getTelefono2().toString(), 10, '0')
				: Utils.lpad("", 10, '0'));
		// requisitos conyuge
		linea.append(buildRequisitos(termalismoDTO.getTreqCon1()) + buildRequisitos(termalismoDTO.getTreqCon2())
				+ buildRequisitos(termalismoDTO.getTreqCon3()));
		// sexo conyuge
		linea.append(!Utils.isEmpty(termalismoDTO.getSexoc()) ? Utils.rpad(termalismoDTO.getSexoc(), 1, ' ')
				: Utils.rpad("", 1, ' '));
		// tipo duracion turno (10 dias, 12 dias...)
		linea.append(buildTipoDuracionTurno());

		// consentimiento identid residencia
		linea.append(buildConsentimiento());
		// sin preferencia turno balneario
		linea.append(termalismoDTO.getSinPreferenciaTurnoBal() != null
				? Utils.lpad(termalismoDTO.getSinPreferenciaTurnoBal().toString(), 1, '0') : Utils.lpad("", 1, '0'));

		try {
			ficheroWriter.escribir(linea.toString());
		} catch (IOException e) {
			log.error("Error al intentar escribir en fichero " + e.getMessage());
			throw new SedeException(e.getMessage());
		}


**/
	}

	// obligatorio
	private String buildPension(String clasePension, BigDecimal importe) {
		if (clasePension != null && importe != null) {
			pension = clasePension.trim().substring(0, 1) + buildImport(importe);
		} else {
			pension = Utils.lpad(" ", 1, ' ') + Utils.lpad("0", 6, '0');
		}
		return pension;
	}

	private String buildDate(Integer day, Integer month, Integer year) {
		date = "";
		date = Utils.lpad(day.toString(), 2, '0');
		date += Utils.lpad(month.toString(), 2, '0');
		date += Utils.lpad(year.toString(), 4, '0');
		return date;
	}

	private String buildBalneariosSolicitados() {
		balnearios = "";

		// el código de balneario son 4 posiciones. SÓLO HAY QUE COGER LAS TRES
		// PRIMERAS
		balnearios = termalismoDTO.getBals1() != null ? Utils.lpad(termalismoDTO.getBals1().substring(0, 3), 3, '0')
				: Utils.lpad("", 3, '0');
		balnearios += termalismoDTO.getBals2() != null ? Utils.lpad(termalismoDTO.getBals2().substring(0, 3), 3, '0')
				: Utils.lpad("", 3, '0');
		balnearios += termalismoDTO.getBals3() != null ? Utils.lpad(termalismoDTO.getBals3().substring(0, 3), 3, '0')
				: Utils.lpad("", 3, '0');
		balnearios += termalismoDTO.getBals4() != null ? Utils.lpad(termalismoDTO.getBals4().substring(0, 3), 3, '0')
				: Utils.lpad("", 3, '0');
		return balnearios;
	}

	private String buildPlazasSolicitadas() {
		plazasSolicitadas = termalismoDTO.getPlazSol();
		if (!Utils.isEmpty(termalismoDTO.getPlazCony())) {
			plazasSolicitadas = termalismoDTO.getPlazCony();
		} else {
			if (!Utils.isEmpty(termalismoDTO.getPlazAmbos()))
				plazasSolicitadas = termalismoDTO.getPlazAmbos();
		}
		return plazasSolicitadas;
	}

	// si no se marca nada. se entiende que opta por el de 12 días
	private String buildTipoDuracionTurno() {
		tipoDuracionTurno = termalismoDTO.getTdt2();
		if (!Utils.isEmpty(termalismoDTO.getTdt1())) {
			tipoDuracionTurno = termalismoDTO.getTdt1();
		} else {
			if (!Utils.isEmpty(termalismoDTO.getTdt3()))
				tipoDuracionTurno = termalismoDTO.getTdt3();
		}
		return tipoDuracionTurno;
	}

	private String buildRequisitos(String requesito) {
		requisitos = GlobalTerma.ESPACIO_BLANCO;
		if (!Utils.isEmpty(requesito)) {
			if (requesito.equals(GlobalTerma.SI_VALOR))
				requisitos = GlobalTerma.SI;
			else
				requisitos = GlobalTerma.NO;
		}
		return requisitos;
	}

	// con tal de que haya consentido uno es suficiente
	private String buildConsentimiento() {
		consentimiento = GlobalTerma.CERO;
		if ((termalismoDTO.getAutorizaSol()!=null && termalismoDTO.getAutorizaSol().equals(true))
				|| (termalismoDTO.getAutorizaCon()!=null)
						&& termalismoDTO.getAutorizaCon().equals(true)) {
			consentimiento = GlobalTerma.UNO;
		}
		return consentimiento;
	}

	private String buildNivelAfectacion(String nivel1, String nivel2, String nivel3, String nivel4) {
		nivelAfectacion = GlobalTerma.VACIO;
		// INGRESO_HOSPITAL
		if (!Utils.isEmpty(nivel1)) {
			nivelAfectacion = nivel1;
		} else {
			nivelAfectacion += Utils.lpad("", 1, ' ');
		}
		// TOMAR_MAS_DE_DOS_MEDICAMENTOS_DIA
		if (!Utils.isEmpty(nivel2)) {
			nivelAfectacion += nivel2;
		} else {
			nivelAfectacion += Utils.lpad("", 1, ' ');
		}
		// MUCHOS_SINTOMAS
		if (!Utils.isEmpty(nivel3)) {
			nivelAfectacion += nivel3;
		} else {
			nivelAfectacion += Utils.lpad("", 1, ' ');
		}
		// OXIGENO_DIARIO
		if (!Utils.isEmpty(nivel4)) {
			nivelAfectacion += nivel4;
		} else {
			nivelAfectacion += Utils.lpad("", 1, ' ');
		}
		return nivelAfectacion;
	}

	private String buildViasRespiratoriasAfectadas(String nivel1, String nivel2) {
		viasRespAfectadas = GlobalTerma.VACIO;
		// ALTAS
		if (!Utils.isEmpty(nivel1)) {
			viasRespAfectadas = nivel1;
		} else {
			viasRespAfectadas += Utils.lpad("", 1, ' ');
		}
		// BAJAS
		if (!Utils.isEmpty(nivel2)) {
			viasRespAfectadas += nivel2;
		} else {
			viasRespAfectadas += Utils.lpad("", 1, ' ');
		}

		return viasRespAfectadas;
	}

	private String buildPadecimiento(String padec1, String padec2, String padec3, String padec4) {
		padecimiento = GlobalTerma.VACIO;
		// DIFICULTAD_MOVI
		if (!Utils.isEmpty(padec1)) {
			padecimiento = padec1;
		} else {
			padecimiento += Utils.lpad("", 1, ' ');
		}
		// DOLOR
		if (!Utils.isEmpty(padec2)) {
			padecimiento += padec2;
		} else {
			padecimiento += Utils.lpad("", 1, ' ');
		}
		// DEFORMIDAD
		if (!Utils.isEmpty(padec3)) {
			padecimiento += padec3;
		} else {
			padecimiento += Utils.lpad("", 1, ' ');
		}
		// RIGIDEZ
		if (!Utils.isEmpty(padec4)) {
			padecimiento += padec4;
		} else {
			padecimiento += Utils.lpad("", 1, ' ');
		}
		return padecimiento;
	}

	private String buildArticulacionesAfectadas(String arti1, String arti2, String arti3, String arti4, String arti5,
			String arti6) {
		articulacionesAfectadas = GlobalTerma.VACIO;
		// CADERA_RODILLA
		if (!Utils.isEmpty(arti1)) {
			articulacionesAfectadas = arti1;
		} else {
			articulacionesAfectadas += Utils.lpad("", 1, ' ');
		}
		// COLUMNA
		if (!Utils.isEmpty(arti2)) {
			articulacionesAfectadas += arti2;
		} else {
			articulacionesAfectadas += Utils.lpad("", 1, ' ');
		}
		// HOMBRO
		if (!Utils.isEmpty(arti3) && arti3.equals(GlobalTerma.SI_VALOR)) {
			articulacionesAfectadas += arti3;
		} else {
			articulacionesAfectadas += Utils.lpad("", 1, ' ');
		}
		// MUNECA_MANO
		if (!Utils.isEmpty(arti4)) {
			articulacionesAfectadas += arti4;
		} else {
			articulacionesAfectadas += Utils.lpad("", 1, ' ');
		}
		// CODO
		if (!Utils.isEmpty(arti5)) {
			articulacionesAfectadas += arti5;
		} else {
			articulacionesAfectadas += Utils.lpad("", 1, ' ');
		}
		// TOBILLO_PIE
		if (!Utils.isEmpty(arti6)) {
			articulacionesAfectadas += arti6;
		} else {
			articulacionesAfectadas += Utils.lpad("", 1, ' ');
		}
		return articulacionesAfectadas;
	}

	private String buildInformeMedico(Integer informeMedico, String persona) {
		informe = GlobalTerma.CERO;
		if (informeMedico != null) {
			if (persona.equals(GlobalTerma.SOLICITANTE)) {
				informe = GlobalTerma.UNO;
			} else {
				informe = GlobalTerma.DOS;
			}
		}
		return informe;
	}

	private String buildTurnosSolicitados() {
		turnosSolicitados = "";
		// el código de balneario son 4 posiciones. SÓLO HAY QUE COGER LAS TRES
		// PRIMERAS
		turnosSolicitados = termalismoDTO.getTurn1() != null ? Utils.lpad(termalismoDTO.getTurn1().toString(), 2, '0')
				: Utils.lpad("", 2, '0');
		turnosSolicitados += termalismoDTO.getTurn2() != null ? Utils.lpad(termalismoDTO.getTurn2().toString(), 2, '0')
				: Utils.lpad("", 2, '0');
		turnosSolicitados += termalismoDTO.getTurn3() != null ? Utils.lpad(termalismoDTO.getTurn3().toString(), 2, '0')
				: Utils.lpad("", 2, '0');
		turnosSolicitados += termalismoDTO.getTurn4() != null ? Utils.lpad(termalismoDTO.getTurn4().toString(), 2, '0')
				: Utils.lpad("", 2, '0');
		return turnosSolicitados;
	}

	
	private String buildTratamiento(String trat1, String trat2, String trat3, String trat4, String trat5, String trat6) {
		tratamientos = "";
		// pueden venir como máximo 2 tratamientos (reumatológico, respiratorio, digestivo...)
		if (trat1!=null && Utils.isEmpty(trat1))
			tratamientos = trat1.trim();
		if (trat2!=null && Utils.isEmpty(trat2))
			tratamientos = trat2.trim();
		if (trat3!=null && Utils.isEmpty(trat3))
			tratamientos = trat3.trim();
		if (trat4!=null && Utils.isEmpty(trat4))
			tratamientos = trat4.trim();
		if (trat5!=null && Utils.isEmpty(trat5))
			tratamientos = trat5.trim();
		if (trat6!=null && Utils.isEmpty(trat6))
			tratamientos = trat6.trim();
		//si sólo ha seleccionado un tratamiento se rellena a Z
		if (tratamientos.length()==1){
			tratamientos = tratamientos+"Z";
		}
		return tratamientos;
	}
	
	private String buildTratamientoCon() {
		tratamientos = "";
		// pueden venir como máximo 2 tratamientos (reumatológico, respiratorio, digestivo...)
		if (termalismoDTO.getTratCon1()!=null && Utils.isEmpty(termalismoDTO.getTratCon1()))
			tratamientos = termalismoDTO.getTratCon1().trim();
		if (termalismoDTO.getTratCon2()!=null && Utils.isEmpty(termalismoDTO.getTratCon2()))
			tratamientos = termalismoDTO.getTratCon2().trim();
		if (termalismoDTO.getTratCon3()!=null && Utils.isEmpty(termalismoDTO.getTratCon3()))
			tratamientos = termalismoDTO.getTratCon3().trim();
		if (termalismoDTO.getTratCon4()!=null && Utils.isEmpty(termalismoDTO.getTratCon4()))
			tratamientos = termalismoDTO.getTratCon4().trim();
		if (termalismoDTO.getTratCon5()!=null && Utils.isEmpty(termalismoDTO.getTratCon5()))
			tratamientos = termalismoDTO.getTratCon5().trim();
		if (termalismoDTO.getTratCon6()!=null && Utils.isEmpty(termalismoDTO.getTratCon6()))
			tratamientos = termalismoDTO.getTratCon6().trim();		
		return tratamientos;
	}
	
	private String buildImport(BigDecimal imp) {
		importe = "";
		////  CARLOS  termalismoDTO.getImpoPenSol1().setScale(2, RoundingMode.HALF_UP);
		////  CARLOS  fraction = termalismoDTO.getImpoPenSol1().remainder(BigDecimal.ONE);
		////  CARLOS  entero = termalismoDTO.getImpoPenSol1().subtract(fraction).setScale(0);
		importe = Utils.lpad(entero.toString(), 4, '0');
		importe += Utils.rpad(fraction.toString().substring(2, 3), 2, '0');

		return importe;
	}

	private String buildNIF(String dni) {
		return (!Utils.isEmpty(dni) ? Utils.lpad(dni, 9, '0') : Utils.lpad("", 9, '0'));
	}

	// private TermalismoDTO buildDtoPrueba(){
	// termalismoDTO=new TermalismoDTO();
	// termalismoDTO.setFisc(new Date());
	// termalismoDTO.setNreg(5544);
	// termalismoDTO.setApellido1("Perez");
	// termalismoDTO.setApellido2("");
	// termalismoDTO.setNombre("Mario");
	// termalismoDTO.setSexo("V");
	// termalismoDTO.setEciv("S");
	// termalismoDTO.setFenadia(02);
	// termalismoDTO.setFenames(8);
	// termalismoDTO.setFenaanio(1958);
	// termalismoDTO.setDocumentoIdentificacion("0485758J");
	// termalismoDTO.setDoml("Delicias, 4");
	// termalismoDTO.setTlfn(914575L);
	// termalismoDTO.setLocd("Madrid");
	// termalismoDTO.setCdpt(28540);
	// termalismoDTO.setPrvd("01");
	// termalismoDTO.setNombrec("");
	// termalismoDTO.setApe2c("");
	// termalismoDTO.setApe1c("Sanchez");
	// termalismoDTO.setDnic("55874C");
	// termalismoDTO.setFencdia(02);
	// termalismoDTO.setFencmes(12);
	// termalismoDTO.setFencanio(1950);
	// termalismoDTO.setBals1("001");
	// termalismoDTO.setBals2(null);
	// termalismoDTO.setBals3(null);
	// termalismoDTO.setBals4(null);
	// termalismoDTO.setPlazSol(SI_VALOR);
	// termalismoDTO.setPlazCony(null);
	// termalismoDTO.setPlazAmbos(null);
	// termalismoDTO.setTratSol1(SI_VALOR);
	// termalismoDTO.setTratSol2(NO_VALOR);
	// termalismoDTO.setTurn1(1);
	// termalismoDTO.setTurn2(11);
	// termalismoDTO.setTurn3(null);
	// termalismoDTO.setTurn4(null);
	// termalismoDTO.setClasPenSol1("Z");
	// termalismoDTO.setImpoPenSol1(new BigDecimal(120.14));
	// termalismoDTO.setClasPenCon1("M");
	// termalismoDTO.setImpoPenCon1(new BigDecimal(923));
	// termalismoDTO.setApernom("Sara");
	// termalismoDTO.setAperape1("SALAZAR");
	// termalismoDTO.setTreqSol1(SI_VALOR);
	// termalismoDTO.setInfm1(1);
	// termalismoDTO.setVresSolB(SI_VALOR);
	// termalismoDTO.setArtiSol1(SI_VALOR);
	// termalismoDTO.setArtiSol2(SI_VALOR);
	// termalismoDTO.setArtiSol3(NO_VALOR);
	// termalismoDTO.setArtiSol4(SI_VALOR);
	// termalismoDTO.setArtiSol6(SI_VALOR);
	// termalismoDTO.setImpoSol1(SI_VALOR);
	// termalismoDTO.setImpoSol2(SI_VALOR);
	// termalismoDTO.setImpoSol3(SI_VALOR);
	// termalismoDTO.setImpoSol4(SI_VALOR);
	//
	// termalismoDTO.setDefoCon1(SI_VALOR);
	// termalismoDTO.setDefoSol2(NO_VALOR);
	// termalismoDTO.setDefoCon2(NO_VALOR);
	//
	// termalismoDTO.setAutorizaCon(NO_VALOR);
	// termalismoDTO.setAutorizaSol(SI_VALOR);
	// termalismoDTO.setSinPreferenciaTurnoBal(0);
	// return termalismoDTO;
	// }

	
	private TermalismoDTO createTermalismoDtoInstance() {
		TermalismoDTO dto = new TermalismoDTO() {
			private static final long serialVersionUID = 1L;
		};
		dto.setCodigoSIA(TipoTramite.TERMALISMO.getSia());
		return dto;
	}
}
