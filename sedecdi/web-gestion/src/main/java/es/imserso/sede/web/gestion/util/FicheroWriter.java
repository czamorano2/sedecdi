package es.imserso.sede.web.gestion.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.inject.Inject;
import org.jboss.logging.Logger;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.util.exception.FileException;
import es.imserso.sede.util.exception.SedeException;



public class FicheroWriter {

	private String nombreFichero;
	private String rutaFichero;
	private String nombreCompleto;
	private String tipoEncoding;

	private File fichero;
	private OutputStreamWriter output = null;
	private OutputStream outputStream = null;

	private Calendar now = null;
	private BufferedWriter bw;
	private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'_'hh'h'mm'm'ss's'");

	@Inject
	private Logger log;


	@Inject
	PropertyComponent propertyComponent;

	
	private boolean closed;

	/**
	 * @param encoding
	 * @param rutaFichero
	 * @param nameFichero
	 * @param extension
	 * @throws HermesException
	 */
	/*
	 * Método que crea un fichero con el nombre y la extension pasados por
	 * parámetro añadiéndole al nombre la fecha y hora actuales para que no
	 * existan duplicados con el nombre de fichero Params: encodign : el tipo de
	 * encoding con el que se tratará el fichero nameFichero: ruta completa del
	 * fichero que se quiere crear (sin formato de fecha y hora ni extension)
	 * extension: extensión que se aplicará al fichero
	 */
	public void abrirFichero(String encoding, String rutaFichero, String nameFichero, String extension) throws SedeException {
		abrirFichero(encoding, rutaFichero, nameFichero, extension, true);
	}
	/**
	 * @param encoding
	 * @param rutaFichero
	 * @param nameFichero
	 * @param extension
	 * @param anadirSufijoDiaHora
	 * @throws HermesException
	 */
	public void abrirFichero(String encoding, String rutaFichero, String nameFichero, String extension, boolean anadirSufijoDiaHora) throws SedeException {
		try {
			log.info("abriendo fichero para escritura...");
			log.info("\tencoding: {0}", encoding == null ? "" : encoding,null);
			log.info("\tnameFichero: {0}", nameFichero == null ? "" : nameFichero,null);
			log.info("\textension: {0}", extension == null ? "" : encoding,null);
			log.info("\tbuffersize: {0}", propertyComponent.getDataOutputBufferSize(),null);
			now = Calendar.getInstance();
			if (anadirSufijoDiaHora) {
				this.nombreFichero = nameFichero + "_" + format.format(now.getTime());
			} else {
				this.nombreFichero = nameFichero;
			}
			if (extension != null && extension.trim().length()>0) {
				this.nombreFichero += "." + extension; 
			}
			this.rutaFichero = rutaFichero;
			nombreCompleto = this.rutaFichero + "/" + this.nombreFichero;
			log.info("\tnombreFichero: {0}", nombreCompleto == null ? "" : nombreCompleto,null);
			tipoEncoding = encoding;
			outputStream = new FileOutputStream(nombreCompleto);
			output = new OutputStreamWriter(outputStream, tipoEncoding);
			bw = new BufferedWriter(output, propertyComponent.getDataOutputBufferSize());
			this.closed = false;

		} catch (Exception e) {
			throw new SedeException("Error al intentar abrir el fichero "+nombreCompleto+": " + e.getMessage());
		}
		log.info("fichero abierto para escritura!");
	}

	/**
	 * @param line
	 * @throws IOException
	 */
	public void escribir(String line) throws IOException {
		bw.write(line);
		bw.newLine();
	}

	/**
	 * @throws FileException
	 */
	public void cerrarFichero() throws FileException {
		try {
			log.info("cerrando fichero para escritura...");
			if (bw != null && !closed) {
				bw.flush();
				bw.close();
				this.closed = true;
				log.info("fichero para escritura cerrado!");
			} else {
				log.warn("El fichero para escritura no se pudo cerrar porque no estaba abierto");
			}

		} catch (IOException e) {
			throw new FileException("Error al intentar cerrar el fichero: " + nombreFichero + e.getMessage());
		}
	}

	/**
	 * @throws FileException
	 */
	public void eliminaFichero() throws FileException {
		try {
			log.info("eliminando fichero {0} ...", nombreFichero,null);
			cerrarFichero();
			File file = new File(nombreCompleto);
			if (file.exists()) {
				file.delete();
				log.info("fichero " + nombreCompleto + " eliminado!");
			} else {
				throw new FileException("No se ha encontrado el fichero: " + nombreFichero);
			}

		} catch (Exception e) {
			throw new FileException("No se ha eliminado el fichero: " + nombreFichero);
		}
	}

	/**
	 * @param rutaFichero
	 * @throws FileException
	 */
	public void eliminaFichero(String rutaFichero) throws FileException {
		nombreFichero = rutaFichero;
		eliminaFichero();
	}

	/**
	 * @return nombre del fichero gestionado para escritura
	 */
	public String getNombreFichero() {
		return nombreFichero;
	}

	/**
	 * @return true si el fichero existe pero está vacío
	 * @throws FileException
	 */
	public boolean isVacio() throws FileException {
		try {
			fichero = new File(nombreCompleto);
			if (!fichero.exists())// si no existe el fichero
				return true;
			if (fichero.length() == 0)
				return true;
		} catch (Exception e) {
			throw new FileException("error al abrir el fichero: " + nombreFichero);
		}
		return false;

	}

	/**
	 * @return ruta del fichero gestionado para escritura
	 */
	public String getRutaFichero() {
		return rutaFichero;
	}

	/**
	 * @return nombre completo del fichero gestionado para escritura
	 */
	public String getNombreCompleto() {
		if (nombreCompleto != null) {
			nombreCompleto = nombreCompleto.replace("//", "/");
		}
		return nombreCompleto;
	}

	/**
	 * @return encoding del fichero gestionado para escritura
	 */
	public String getTipoEncoding() {
		return tipoEncoding;
	}


	/**
	 * @return true si el fichero gestionado para escritura está cerrado
	 */
	public boolean isClosed() {
		return closed;
	}
	/**
	 * @param closed
	 */
	public void setClosed(boolean closed) {
		this.closed = closed;
	}

}
