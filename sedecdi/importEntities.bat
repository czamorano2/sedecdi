@echo off
echo Se van a importar las entidades generadas desde el modelo UML
echo Si deseas cancelar pulse control C y no se habra hecho nada
pause
@echo on
del /Q .\ejb\src\main\java\es\imserso\sede\model\*.*
copy /Y E:\code\workspaces\indigo\hermes\sede-master\sede-jpa\src\main\java\es\imserso\sede\model\*.* .\ejb\src\main\java\es\imserso\sede\model