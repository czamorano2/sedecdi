package es.imserso.sedecdi.test;

import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.Test;

import es.imserso.sede.data.dto.solicitud.SolicitudDTO;

/**
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 * @version $Revision: 1 $
 */
public class SimpleClientTest {
	@Test
	public void testResponse() throws Exception {
		ResteasyClient client = null;
		Response response = null;
		try {
			client = new ResteasyClientBuilder().build();
			response = client
					.target("http://preregistroe/hermes/seam/resource/restv1/solicitudes/buscarPorDi/40865803R")
					.request().get();
			GenericType<List<SolicitudDTO>> genericType = new GenericType<List<SolicitudDTO>>() {
			};
			List<SolicitudDTO> list = response.readEntity(genericType);
			System.out.println(list.size());
		} catch (Exception e) {
			System.err.println(e.toString());
		} finally {
//			response.close();
//			client.close();
		}
	}
	// @Test
	// public void testCustomer() throws Exception
	// {
	// // fill out a query param and execute a get request
	// Client client = ClientBuilder.newClient();
	// WebTarget target = client.target("http://localhost:9095/customers");
	// try
	// {
	// // extract solicitudTurismoDTO directly expecting success
	// SolicitudDTO s = target.queryParam("name",
	// "Bill").request().get(SolicitudDTO.class);
	// Assert.assertEquals("XXXXXX", s.getExpediente());
	// }
	// finally
	// {
	// client.close();
	// }
	// }

	// @Test
	// public void testTemplate() throws Exception
	// {
	// // fill out a path param and execute a get request
	// Client client = ClientBuilder.newClient();
	// WebTarget target = client.target("http://localhost:9095/customers/{id}");
	// Response response = target.resolveTemplate("id", "12345").request().get();
	// try
	// {
	// Assert.assertEquals(200, response.getStatus());
	// SolicitudDTO s =
	// response.readEntity(SolicitudDTO.class);
	// Assert.assertEquals("XXXXXX", s.getExpediente());
	// }
	// finally
	// {
	// response.close();
	// client.close();
	// }
	// }

}
