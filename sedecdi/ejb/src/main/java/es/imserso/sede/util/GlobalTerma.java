package es.imserso.sede.util;

public class GlobalTerma extends Global{
	

	
	//plazas solicitadas
	public static final String PLAZA_SOLICITANTE = "A";
	public static final String PLAZA_CONYUGE = "B";
	public static final String PLAZA_AMBOS = "C";
	
	// tipo duracion turno
	public static final String DIEZ_NOCHES="A";
	public static final String DOCE_NOCHES="B";
	public static final String SIN_PREFERENCIA="Z";
	
	public static final String REUMATOLOGICO="A";
	public static final String RESPIRATORIO = "B";
	public static final String DIGESTIVO = "C";
	public static final String RENAL = "D";
	public static final String DERMATOLOGICO = "E";
	public static final String NEUROPSIQUICO = "F";
	
	// nivel afectacion
	public static final String INGRESO_HOSPITAL="A";
	public static final String TOMAR_MAS_DE_DOS_MEDICAMENTOS_DIA="B";
	public static final String MUCHOS_SINTOMAS="C";
	public static final String OXIGENO_DIARIO="D";

	// articulaciones afectadas
	public static final String CADERA_RODILLA="A";
	public static final String COLUMNA="B";
	public static final String HOMBRO ="C";
	public static final String MUNECA_MANO="D";
	public static final String CODO="E";
	public static final String TOBILLO_PIE="F";

	// tiene o padece
	public static final String DIFICULTAD_MOVI="A";
	public static final String DOLOR="B";
	public static final String DEFORMIDAD="C";
	public static final String RIGIDEZ="D";

	// vias respiratoria afectadas
	public static final String ALTAS="A";
	public static final String BAJAS="B";
	
	public static final String SI_VALOR = "Sí";
	public static final String NO_VALOR="No";
	
	public static final String ESPACIO_BLANCO = " ";	
	public static final String VACIO = "";

	public static final String SI = "S";
	public static final String NO = "N";

	public static final String SOLICITANTE = "S";
	public static final String CONYUGE = "C";

	public static final String CERO = "0";
	public static final String UNO = "1";
	public static final String DOS = "2";
	//turno sin preferencia
	public static final String TURNO_CUALQUIERA = "13";

	
}
