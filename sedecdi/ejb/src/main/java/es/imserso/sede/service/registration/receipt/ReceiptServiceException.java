package es.imserso.sede.service.registration.receipt;

import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Esta clase y sus subclases son una forma de {@code Exception} que indica que
 * no se ha podido realizar satisfactoriamente la operación relacionada con el
 * justificante.
 * 
 * @author 11825775
 *
 */
public class ReceiptServiceException extends SedeRuntimeException {

	private static final long serialVersionUID = -633698730787829166L;

	protected static final String DEFAULT_MESSAGE = "Error en validación";

	/**
	 * constructor por defecto
	 * 
	 * @see java.lang.Exception
	 */
	public ReceiptServiceException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 */
	public ReceiptServiceException(String message) {
		super(message);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 * @param cause
	 *            causa de la excepción
	 */
	public ReceiptServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 *            causa de la excepción
	 */
	public ReceiptServiceException(Throwable cause) {
		super(cause);
	}

}
