package es.imserso.sede.util.persistence.ucm;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.jboss.logging.Logger;

import es.imserso.sede.util.resources.qualifier.UCMDataSourceQ;

public class Utils {

	@Inject
	private Logger log;

	@Inject
	@UCMDataSourceQ
	private DataSource ucmDatasource;

	// public static void connect() {
	// try {
	// Context ctx = new InitialContext();
	// // Change these two lines to suit your environm ent.
	// DataSource ds = (DataSource) ctx.lookup("jdbc/Exam pleDS");
	// Connection conn = ds.getConnection("testuser", "testpwd");
	// Statement stmt = null; // Non-transactional statem ent
	// Statement stmtx = null; // Transactional statem ent
	// Properties dbProperties = new Properties();
	//
	// stmt = conn.createStatement(); // non-tx statem ent
	// } catch (NamingException | SQLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }

	public List<String> getTramites() throws SQLException {
		List<String> tramiteList = null;

		Statement stmt = null;
		String query = "select * from Estados";
		try {
			tramiteList = new ArrayList<String>();
			Connection con = ucmDatasource.getConnection();
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
//				long id = rs.getLong("id");
//				String tipo = rs.getString("tipo");
				String nombre = rs.getString("nombre");
				tramiteList.add(nombre);
			}
		} catch (SQLException e) {
			log.error("error al obtener los trámites del UCM: " + es.imserso.sede.util.Utils.getExceptionMessage(e));
			throw e;

		} finally {
			if (stmt != null) {
				try {
					stmt.close();

				} catch (SQLException e) {
					log.error("error al cerrar el statement: " + es.imserso.sede.util.Utils.getExceptionMessage(e));
				}
			}
		}
		return tramiteList;
	}

}
