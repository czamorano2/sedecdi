package es.imserso.sede.data;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.logging.Logger;

import es.imserso.sede.model.RegistroTramite;

@Stateless
public class RegistroTramiteRepository implements Serializable {

	private static final long serialVersionUID = 3424917643235540671L;

	@Inject
	Logger log;

	@PersistenceContext
	private EntityManager em;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void persist(RegistroTramite registroTramite) {
		log.debug("persistiendo apuntes del registro...");
		em.persist(registroTramite);
		em.flush();
		log.info("apuntes del registro persistidos!");
	}

}
