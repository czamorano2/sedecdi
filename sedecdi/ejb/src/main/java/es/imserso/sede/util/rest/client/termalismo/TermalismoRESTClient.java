package es.imserso.sede.util.rest.client.termalismo;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.jboss.logging.Logger;
import org.jboss.wise.core.client.InvocationResult;
import org.jboss.wise.core.client.WSDynamicClient;
import org.jboss.wise.core.client.WSMethod;
import org.jboss.wise.core.client.WebParameter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import es.imserso.sede.data.dto.rest.termalismo.BalnearioVOWS;
import es.imserso.sede.data.dto.rest.termalismo.BuscadorWS;
import es.imserso.sede.data.dto.rest.termalismo.ContenedorExpedienteVOWS;
import es.imserso.sede.data.dto.rest.termalismo.MantenimientoVOWS;
import es.imserso.sede.util.exception.SedecdiRestException;
import es.imserso.sede.util.rest.client.termalismo.TermalismoRestClientAux;

@ApplicationScoped
public class TermalismoRESTClient  extends TermalismoRestClientAux implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7011110327513862980L;

	@Inject
	private Logger log;

	private Class<?> paramType;

	
	private WSDynamicClient client;
	
	
	private static String WEB_SERVICE = "SolicitanteWebServiceImplService";	
	private static String PARAM_NAME_SOLICITUDES = "buscador";
	private static String PARAM_NAME_BALNEARIO = "comunidadAutonoma";
	private static String PARAM_METHOD_IDCOMUDIDAD = "setIdComunidadAutonoma";
	private static String PARAM_METHOD_NIF_SOLICITUD = "setNif";
	private static String PARAM_METHOD_NUM_EXPDTE_SOLICITUD = "setNumeroExpediente";
	private static String PARAM_METHOD_ANIO_SOLICITUD = "setAnyo";
	private static final String METHOD_CONEXIONTERMALISMO = "getTextoPrueba";
	private static final String METHOD_APLICACION_MANTENIMIENTO = "getAplicacionEnMantenimiento";
	private static final String METHOD_BALNEARIOS = "getListaBalneariosAltaExpediente";
	
	
	
	
	/*
	 * Método que devuelve los balnearios que coincidan con el id de la comunidad pasado por parámetro
	 */
	public List<BalnearioVOWS> getBalneariosPorComunidad(String metodo, Long idComunidad){
		String writeValueAsString=null;
		List<BalnearioVOWS> arrayList=null;
		try {
			WSMethod method = getClientMethod(metodo);
					
			Object paramObject = webParams(method, PARAM_NAME_BALNEARIO);
			Map<String, Object>  mapReq = new HashMap<String, Object>();
			mapReq = busquedaBalnearioPorComunidadId(mapReq, paramObject, idComunidad);
			
			InvocationResult invocation =  invoke(method, mapReq);
			
			arrayList = (ArrayList<BalnearioVOWS>)invocation.getResult().get(WSMethod.RESULT);
		
			ObjectMapper mapper = new ObjectMapper();

			//Object to JSON 
			writeValueAsString = mapper.writeValueAsString(arrayList);
			log.info("llamada CORRECTA al método "+metodo+" del WS: "+WEB_SERVICE);			
		} catch ( JsonProcessingException  e) {
			log.info("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
			throw new SedecdiRestException("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
		}		
		return arrayList;			
	}
	
	
	/*
	 * Método para comprobar la conexión con termalismo
	 */
	public Response getConexion(){
		WSMethod method = getClientMethod(METHOD_CONEXIONTERMALISMO);
		Map<String, Object> mapReq = new HashMap<String, Object>();
		mapReq.put("texto", "Hola");
		
		invoke(method, mapReq);
		ResponseBuilder response = Response.ok();
		client.close();
		return response.build();			
	}
	
	
	
	
	/*
	 * Método para comprobar la conexión con termalismo
	 */
//	public boolean getAplicacionMantenimiento1(){
//		boolean isMto = false;
//		WSMethod method = getClientMethod(METHOD_APLICACION_MANTENIMIENTO);
//		MantenimientoVOWS mto = new MantenimientoVOWS();
//		InvocationResult invoke = invoke(method,  new HashMap<String, Object>());
//		Class<?> classType = (Class<?>)invoke.getResult().get(WSMethod.TYPE_RESULT);
//		Object object = invoke.getResult().get(WSMethod.RESULT);
//		
//		ElementBuilder builder = ElementBuilderFactory.getElementBuilder().client(client).request(true).useDefautValuesForNullLeaves(true);  
//		Element element =builder.request(false).useDefautValuesForNullLeaves(false).buildTree(classType, mto.getClass().getName(), object, true);
//		
//		for (Iterator<? extends Element> it = element.getChildren();it.hasNext();  ){
//			Element e = it.next();
//			if (e.getName().equals("paradaAplicacion")){
//				isMto = Boolean.valueOf(e.getValue());
//			}
//			System.out.println(e.getName()+" : "+e.getValue());
//		}
//		
//		return isMto;
//			
//	}
	
	
	/*
	 * Método para comprobar la conexión con termalismo
	 */
	public boolean getAplicacionMantenimiento(){
		boolean isMto = false;
		try {
			
			WSMethod method = getClientMethod(METHOD_APLICACION_MANTENIMIENTO);
			
			InvocationResult invoke = invoke(method,  new HashMap<String, Object>());
			
			Object object = invoke.getResult().get(WSMethod.RESULT);
			
			ObjectMapper mapper = new ObjectMapper();	
			//Object to JSON 
			String writeValueAsString = mapper.writeValueAsString(object);
			//JSON to java
			MantenimientoVOWS obj = mapper.readValue(writeValueAsString, MantenimientoVOWS.class);
			isMto = obj.isParadaAplicacion();
			
		} catch (IOException e ) {
			throw new SedecdiRestException("Error en el método getApliacionMantenimiento: "+e.getMessage());
		
		} 
		
		return isMto;
			
	}
	
	/*
	 * Método que devuelve una lista de objetos ContenedorExpedienteVOWS que contendrá las solicitudes de termalismo que coincidan con 
	 * el númeroExpediente y anioConvocatoria recibidos como parámetro
	 */
	public List<ContenedorExpedienteVOWS> getExpedienteTermalismoPorExpAnio(String numeroExpediente, Integer anioConvocatoria){
		if (numeroExpediente == null || anioConvocatoria == null){
			log.info("Es obligatorio introducir numero expdte y año convocatoria");
			throw new SedecdiRestException("Es obligatorio introducir numero expdte y año convocatoria para realizar la búsqueda en termalismo");
		}
		return busquedaExpedienteTermalismoPorNifExpdteAnio(null, numeroExpediente, anioConvocatoria);
	}

	/*
	 * Método que devuelve una lista de objetos ContenedorExpedienteVOWS que contendrá las solicitudes de termalismo que coincidan con 
	 * el el nif recibido como parámetro
	 */
	public List<ContenedorExpedienteVOWS> getExpedienteTermalismoPorNif(String nif){
		if (nif == null){
			log.info("Es obligatorio introducir nif");
			throw new SedecdiRestException("Es obligatorio introducir nif para realizar la búsqueda en termalismo");
		}
		return busquedaExpedienteTermalismoPorNifExpdteAnio(nif, null, null);
	}
	
	
	private List<ContenedorExpedienteVOWS> busquedaExpedienteTermalismoPorNifExpdteAnio(String nif, String numeroExpediente, Integer anioConvocatoria){
		
		List<ContenedorExpedienteVOWS> listaContenedorExpedientes=null;
		WSMethod method =  getClientMethod("busquedaExpediente");
		//nombre del método a invocar
		
		//nombre del parámetro que recibe el método
		Object paramObject = webParams(method, PARAM_NAME_SOLICITUDES);
		Map<String, Object>  mapReq = new HashMap<String, Object>();
		if (nif !=null){
			mapReq = busquedaNif(mapReq, paramObject, nif);
		}else{
			mapReq = busquedaExpdteAnio(mapReq,paramObject, numeroExpediente, anioConvocatoria);
		}
		InvocationResult invocation = invoke(method, mapReq);
		listaContenedorExpedientes = (ArrayList<ContenedorExpedienteVOWS>)invocation.getResult().get(WSMethod.RESULT);
		
		log.info("llamada CORRECTA al método busquedaExpediente del WS: "+WEB_SERVICE);
		
		client.close();
		return listaContenedorExpedientes;				
	}
	
	


	protected Object webParams(WSMethod method, String nameParam) {
		WebParameter webParameter = method.getWebParams().get(nameParam);
		paramType = (Class<?>)webParameter.getType();
		Object paramObject;
		try {
			paramObject = paramType.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		return paramObject;
	}



	private Map<String, Object> busquedaExpdteAnio(Map<String, Object>mapReq, Object paramObject, String numeroExpediente, Integer anioConvocatoria) {
		try {
			paramType.getMethod(PARAM_METHOD_NUM_EXPDTE_SOLICITUD, String.class).invoke(paramObject, numeroExpediente);
			paramType.getMethod(PARAM_METHOD_ANIO_SOLICITUD, Integer.class).invoke(paramObject, anioConvocatoria);			
			mapReq.put(PARAM_NAME_SOLICITUDES, paramObject);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		return mapReq;
	}

	
	private Map<String, Object> busquedaNif(Map<String, Object>mapReq, Object paramObject, String nif) {
		BuscadorWS b = new BuscadorWS();		
		try {
			paramType.getMethod(PARAM_METHOD_NIF_SOLICITUD, String.class).invoke(paramObject, nif);
			mapReq = new HashMap<String, Object>();
			mapReq.put(PARAM_NAME_SOLICITUDES, paramObject);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		return mapReq;
	}
	
	private Map<String, Object> busquedaBalnearioPorComunidadId(Map<String, Object>mapReq, Object paramObject, Long idComunidad) {
		try {
			paramType.getMethod(PARAM_METHOD_IDCOMUDIDAD, Long.class).invoke(paramObject, idComunidad);
			mapReq = new HashMap<String, Object>();
			mapReq.put(PARAM_NAME_BALNEARIO, paramObject);
	} catch (Exception e) {
		log.error(e.getMessage());
		throw new SedecdiRestException(e);
	}

		return mapReq;
	}
	


	
	
	public String getNuevoNumeroExpediente(){
		String expediente=null;
		WSMethod method =  getClientMethod("getNuevoNumeroExpediente");
		InvocationResult invocation = invoke(method, new java.util.HashMap<String, Object>());
		expediente = (String)invocation.getResult().get(WSMethod.RESULT);		
		log.info("llamada CORRECTA al método getNuevoNumeroExpediente del WS: "+WEB_SERVICE);		
		//client.close();
		return expediente;				
	}
	
	public List<BalnearioVOWS> getBalneariosPorCCAA(Long idComunidad) {
		return (List<BalnearioVOWS>)getBalneariosPorComunidad(METHOD_BALNEARIOS, idComunidad );
	}
	
}

