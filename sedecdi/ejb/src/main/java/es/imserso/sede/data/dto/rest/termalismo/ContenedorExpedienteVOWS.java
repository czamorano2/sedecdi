
package es.imserso.sede.data.dto.rest.termalismo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para contenedorExpedienteVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="contenedorExpedienteVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="expediente" type="{http://solicitante.jaxws.termalismo.imserso.com/}expedienteVOWS" minOccurs="0"/&gt;
 *         &lt;element name="modificable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contenedorExpedienteVOWS", propOrder = {
    "expediente",
    "modificable"
})
public class ContenedorExpedienteVOWS {

    protected ExpedienteVOWS expediente;
    protected Boolean modificable;

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteVOWS }
     *     
     */
    public ExpedienteVOWS getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteVOWS }
     *     
     */
    public void setExpediente(ExpedienteVOWS value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad modificable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isModificable() {
        return modificable;
    }

    /**
     * Define el valor de la propiedad modificable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setModificable(Boolean value) {
        this.modificable = value;
    }

}
