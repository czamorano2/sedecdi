
package es.imserso.sede.data.dto.rest.termalismo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para diaDeSalidaVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="diaDeSalidaVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="duracionTurnoBalneario" type="{http://solicitante.jaxws.termalismo.imserso.com/}duracionTurnoBalnearioVOWS" minOccurs="0"/&gt;
 *         &lt;element name="fechaDeSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idDiaDeSalida" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="turnoBalneario" type="{http://solicitante.jaxws.termalismo.imserso.com/}turnoBalnearioVOWS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "diaDeSalidaVOWS", propOrder = {
    "duracionTurnoBalneario",
    "fechaDeSalida",
    "idDiaDeSalida",
    "turnoBalneario"
})
public class DiaDeSalidaVOWS {

    protected DuracionTurnoBalnearioVOWS duracionTurnoBalneario;
    protected String fechaDeSalida;
    protected Long idDiaDeSalida;
    protected TurnoBalnearioVOWS turnoBalneario;

    /**
     * Obtiene el valor de la propiedad duracionTurnoBalneario.
     * 
     * @return
     *     possible object is
     *     {@link DuracionTurnoBalnearioVOWS }
     *     
     */
    public DuracionTurnoBalnearioVOWS getDuracionTurnoBalneario() {
        return duracionTurnoBalneario;
    }

    /**
     * Define el valor de la propiedad duracionTurnoBalneario.
     * 
     * @param value
     *     allowed object is
     *     {@link DuracionTurnoBalnearioVOWS }
     *     
     */
    public void setDuracionTurnoBalneario(DuracionTurnoBalnearioVOWS value) {
        this.duracionTurnoBalneario = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaDeSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDeSalida() {
        return fechaDeSalida;
    }

    /**
     * Define el valor de la propiedad fechaDeSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDeSalida(String value) {
        this.fechaDeSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad idDiaDeSalida.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdDiaDeSalida() {
        return idDiaDeSalida;
    }

    /**
     * Define el valor de la propiedad idDiaDeSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdDiaDeSalida(Long value) {
        this.idDiaDeSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad turnoBalneario.
     * 
     * @return
     *     possible object is
     *     {@link TurnoBalnearioVOWS }
     *     
     */
    public TurnoBalnearioVOWS getTurnoBalneario() {
        return turnoBalneario;
    }

    /**
     * Define el valor de la propiedad turnoBalneario.
     * 
     * @param value
     *     allowed object is
     *     {@link TurnoBalnearioVOWS }
     *     
     */
    public void setTurnoBalneario(TurnoBalnearioVOWS value) {
        this.turnoBalneario = value;
    }

}
