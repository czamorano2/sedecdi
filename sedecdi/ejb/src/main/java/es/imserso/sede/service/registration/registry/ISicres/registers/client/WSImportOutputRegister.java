
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BookIdentification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="regNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="regDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sysDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Office" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Datas" type="{http://www.invesicres.org}WSParamOutputRegisterEx" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookIdentification",
    "regNumber",
    "regDate",
    "user",
    "sysDate",
    "office",
    "datas"
})
@XmlRootElement(name = "WSImportOutputRegister")
public class WSImportOutputRegister {

    @XmlElement(name = "BookIdentification")
    protected int bookIdentification;
    protected String regNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar regDate;
    protected String user;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sysDate;
    @XmlElement(name = "Office")
    protected String office;
    @XmlElement(name = "Datas")
    protected WSParamOutputRegisterEx datas;

    /**
     * Obtiene el valor de la propiedad bookIdentification.
     * 
     */
    public int getBookIdentification() {
        return bookIdentification;
    }

    /**
     * Define el valor de la propiedad bookIdentification.
     * 
     */
    public void setBookIdentification(int value) {
        this.bookIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad regNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegNumber() {
        return regNumber;
    }

    /**
     * Define el valor de la propiedad regNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegNumber(String value) {
        this.regNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad regDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegDate() {
        return regDate;
    }

    /**
     * Define el valor de la propiedad regDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegDate(XMLGregorianCalendar value) {
        this.regDate = value;
    }

    /**
     * Obtiene el valor de la propiedad user.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Define el valor de la propiedad user.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

    /**
     * Obtiene el valor de la propiedad sysDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSysDate() {
        return sysDate;
    }

    /**
     * Define el valor de la propiedad sysDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSysDate(XMLGregorianCalendar value) {
        this.sysDate = value;
    }

    /**
     * Obtiene el valor de la propiedad office.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffice() {
        return office;
    }

    /**
     * Define el valor de la propiedad office.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffice(String value) {
        this.office = value;
    }

    /**
     * Obtiene el valor de la propiedad datas.
     * 
     * @return
     *     possible object is
     *     {@link WSParamOutputRegisterEx }
     *     
     */
    public WSParamOutputRegisterEx getDatas() {
        return datas;
    }

    /**
     * Define el valor de la propiedad datas.
     * 
     * @param value
     *     allowed object is
     *     {@link WSParamOutputRegisterEx }
     *     
     */
    public void setDatas(WSParamOutputRegisterEx value) {
        this.datas = value;
    }

}
