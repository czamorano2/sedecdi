package es.imserso.sede.util.persistence.service;

import javax.inject.Inject;

import es.imserso.sede.config.PropertyComponent;

/**
 * Este componente se encarga de la gestión del contexto de persistencia y las
 * transacciones del script de un listado.
 * <p>
 * Cada script de listado tendrá asociada su propia instancia de este
 * componente.
 * 
 * @author 11825775
 * 
 * 
 */
public class ReportPersistenceManager extends AbstractPersistenceManager {

	private static final long serialVersionUID = -7639022760270721621L;

	@Inject
	protected PropertyComponent propertyComponent;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.inventario.util.persistence.service.AbstractPersistenceManager
	 * #getTransactionTimeoutSeconds()
	 */
	@Override
	public int getTransactionTimeoutSeconds() {
		return propertyComponent.getReportTransactionTimeoutSeconds();
	}

}
