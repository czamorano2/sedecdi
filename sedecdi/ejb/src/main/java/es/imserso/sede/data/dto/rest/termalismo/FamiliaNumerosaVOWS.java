package es.imserso.sede.data.dto.rest.termalismo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para familiaNumerosaVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="familiaNumerosaVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codFamilia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="presentadoLibroFamilia" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="tipoFamilia" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoFamiliaNumerosaVOWS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "familiaNumerosaVOWS", propOrder = {
    "codFamilia",
    "presentadoLibroFamilia",
    "tipoFamilia"
})
public class FamiliaNumerosaVOWS {

    protected String codFamilia;
    protected Boolean presentadoLibroFamilia;
    protected TipoFamiliaNumerosaVOWS tipoFamilia;

    /**
     * Obtiene el valor de la propiedad codFamilia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFamilia() {
        return codFamilia;
    }

    /**
     * Define el valor de la propiedad codFamilia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFamilia(String value) {
        this.codFamilia = value;
    }

    /**
     * Obtiene el valor de la propiedad presentadoLibroFamilia.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPresentadoLibroFamilia() {
        return presentadoLibroFamilia;
    }

    /**
     * Define el valor de la propiedad presentadoLibroFamilia.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPresentadoLibroFamilia(Boolean value) {
        this.presentadoLibroFamilia = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoFamilia.
     * 
     * @return
     *     possible object is
     *     {@link TipoFamiliaNumerosaVOWS }
     *     
     */
    public TipoFamiliaNumerosaVOWS getTipoFamilia() {
        return tipoFamilia;
    }

    /**
     * Define el valor de la propiedad tipoFamilia.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoFamiliaNumerosaVOWS }
     *     
     */
    public void setTipoFamilia(TipoFamiliaNumerosaVOWS value) {
        this.tipoFamilia = value;
    }

}
