package es.imserso.sede.data;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.CategoriaFamiliaNumerosaDTO;
import es.imserso.hermes.session.webservice.dto.ClasePensionDTO;
import es.imserso.hermes.session.webservice.dto.EstadoCivilDTO;
import es.imserso.hermes.session.webservice.dto.OpcionDTO;
import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.ProcedenciaPensionDTO;
import es.imserso.hermes.session.webservice.dto.ProvinciaDTO;
import es.imserso.hermes.session.webservice.dto.SexoDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.hermes.session.webservice.dto.TemporadaDTO;
import es.imserso.hermes.session.webservice.dto.TurnoDTO;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.service.cache.hermes.CrunchifyInMemoryCache;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.CategoriasFamiliaNumerosa;
import es.imserso.sede.util.rest.client.ClasesPension;
import es.imserso.sede.util.rest.client.Destinos;
import es.imserso.sede.util.rest.client.EstadosCiviles;
import es.imserso.sede.util.rest.client.Plazo;
import es.imserso.sede.util.rest.client.ProcedenciasPension;
import es.imserso.sede.util.rest.client.Provincias;
import es.imserso.sede.util.rest.client.RestClient;
import es.imserso.sede.util.rest.client.Sexos;

/**
 * Gestiona los datos que se obtienen de Hermes de forma remota.
 * <p>
 * Precisamente por obtenerse de forma remota y el coste que eso conlleva, este
 * componente optimiza los recursos utilizando una caché para aquellos datos que
 * apenas son modificados (provincias, estados civiles, etc)
 * 
 * @author 11825775
 *
 */
@ApplicationScoped
public class TurismoRemoteRepository {

	private static final Logger log = Logger.getLogger(TurismoRemoteRepository.class.getName());

	private static final int HOURS_1 = 3600;

	@Inject
	Instance<PropertyComponent> propertyComponentInstance;

	List<SimpleDTOI> lastEstadosCiviles;
	List<SimpleDTOI> lastProvincias;
	List<SimpleDTOI> lastCategoriasFamiliaNumerosa;
	List<SimpleDTOI> lastClasesPensiones;
	List<SimpleDTOI> lastOpciones;
	List<SimpleDTOI> lastProcedenciasPensiones;
	List<SimpleDTOI> lastSexos;
	PlazoDTO lastPlazoTurnoPorDefecto;
	TurnoDTO lastTurnoPorDefecto;
	TemporadaDTO lastTemporadaTurnoPorDefecto;

	CrunchifyInMemoryCache<CachesTurismo, List<SimpleDTOI>> hermesDtoLists;
	CrunchifyInMemoryCache<CachesTurismo, PlazoDTO> hermesDtoPlazoTurnoPorDefecto;
	CrunchifyInMemoryCache<CachesTurismo, TurnoDTO> hermesDtoTurnoPorDefecto;
	CrunchifyInMemoryCache<CachesTurismo, TemporadaDTO> hermesDtoTemporadaTurnoPorDefecto;

	@PostConstruct
	public void onCreate() {
		lastEstadosCiviles = new ArrayList<SimpleDTOI>();
		lastProvincias = new ArrayList<SimpleDTOI>();
		lastCategoriasFamiliaNumerosa = new ArrayList<SimpleDTOI>();
		lastClasesPensiones = new ArrayList<SimpleDTOI>();
		lastOpciones = new ArrayList<SimpleDTOI>();
		lastProcedenciasPensiones = new ArrayList<SimpleDTOI>();
		lastSexos = new ArrayList<SimpleDTOI>();

		initCaches();
	}

	/**
	 * Inicia/reinicia las cachés con la configuración actualizada.
	 */
	private void initCaches() {
		hermesDtoLists = new CrunchifyInMemoryCache<CachesTurismo, List<SimpleDTOI>>(70, 60, 10);
		hermesDtoPlazoTurnoPorDefecto = new CrunchifyInMemoryCache<CachesTurismo, PlazoDTO>(HOURS_1, HOURS_1, 20);
		hermesDtoTurnoPorDefecto = new CrunchifyInMemoryCache<CachesTurismo, TurnoDTO>(HOURS_1, HOURS_1, 10);
		hermesDtoTemporadaTurnoPorDefecto = new CrunchifyInMemoryCache<CachesTurismo, TemporadaDTO>(HOURS_1, HOURS_1,
				10);
	}

	/**
	 * Reinicia las cachés con la configuración actualizada.
	 * <p>
	 * Pensado para ser invocado tras modificar algún parámetro de la configuración
	 * de las cachés.
	 */
	public void reconfigure() {
		// TODO implementar cuando estén terminadas las configuraciones desde el panel
		// de administración
	}

	public List<SimpleDTOI> getEstadosCiviles() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.estados_civiles);
		if (list == null) {
			log.debug("la lista no está ya en la caché");

			if ((list = convert2DTOI(getEstadosCivilesDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Hermes, por lo usamos la última buena");
				hermesDtoLists.put(CachesTurismo.estados_civiles, lastEstadosCiviles);

			} else {
				log.debug("la lista se ha obtenido de hermes");
				hermesDtoLists.put(CachesTurismo.estados_civiles, list);
				lastEstadosCiviles.clear();
				lastEstadosCiviles.addAll(list);
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	private List<EstadoCivilDTO> getEstadosCivilesDTO() throws SedeException {
		return (List<EstadoCivilDTO>) ((EstadosCiviles) UtilsCDI.getBeanByReference(EstadosCiviles.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_ESTADOS_CIVILES)
				.execute();
	}

	public List<SimpleDTOI> getProvincias() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.provincias);
		if (list == null) {
			log.debug("la lista no está ya en la caché");

			if ((list = convert2DTOI(getProvinciasDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Hermes, por lo usamos la última buena");
				hermesDtoLists.put(CachesTurismo.provincias, lastProvincias);

			} else {
				log.debug("la lista se ha obtenido de hermes");
				hermesDtoLists.put(CachesTurismo.provincias, list);
				lastProvincias.clear();
				lastProvincias.addAll(list);
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	private List<ProvinciaDTO> getProvinciasDTO() throws SedeException {
		return (List<ProvinciaDTO>) ((Provincias) UtilsCDI.getBeanByReference(Provincias.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_PROVINCIAS)
				.execute();
	}

	public List<SimpleDTOI> getCategoriasFamiliaNumerosa() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.categorias_familia_numerosa);
		if (list == null) {
			log.debug("la lista no está ya en la caché");

			if ((list = convert2DTOI(getCategoriasFamiliaNumerosaDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Hermes, por lo usamos la última buena");
				hermesDtoLists.put(CachesTurismo.categorias_familia_numerosa, lastCategoriasFamiliaNumerosa);

			} else {
				log.debug("la lista se ha obtenido de hermes");
				hermesDtoLists.put(CachesTurismo.categorias_familia_numerosa, list);
				lastCategoriasFamiliaNumerosa.clear();
				lastCategoriasFamiliaNumerosa.addAll(list);
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	private List<CategoriaFamiliaNumerosaDTO> getCategoriasFamiliaNumerosaDTO() throws SedeException {
		return (List<CategoriaFamiliaNumerosaDTO>) ((CategoriasFamiliaNumerosa) UtilsCDI
				.getBeanByReference(CategoriasFamiliaNumerosa.class))
						.call(propertyComponentInstance.get().getTurismoResourcesURL()
								+ RestClient.URL_SUFFIX_CATEGORIAS_FAMILIA_NUMEROSA)
						.execute();
	}

	public List<SimpleDTOI> getClasesPensiones() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.clases_pensiones);
		if (list == null) {
			log.debug("la lista no está ya en la caché");

			if ((list = convert2DTOI(getClasesPensionesDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Hermes, por lo usamos la última buena");
				hermesDtoLists.put(CachesTurismo.clases_pensiones, lastClasesPensiones);

			} else {
				log.debug("la lista se ha obtenido de hermes");
				hermesDtoLists.put(CachesTurismo.clases_pensiones, list);
				lastClasesPensiones.clear();
				lastClasesPensiones.addAll(list);
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	private List<ClasePensionDTO> getClasesPensionesDTO() throws SedeException {
		return (List<ClasePensionDTO>) ((ClasesPension) UtilsCDI.getBeanByReference(ClasesPension.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_CLASES_PENSIONES)
				.execute();
	}

	public List<SimpleDTOI> getOpciones() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.destinos);
		if (list == null) {
			log.debug("la lista no está ya en la caché");

			if ((list = convert2DTOI(getOpcionesDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Hermes, por lo usamos la última buena");
				hermesDtoLists.put(CachesTurismo.destinos, lastOpciones);

			} else {
				log.debug("la lista se ha obtenido de hermes");
				hermesDtoLists.put(CachesTurismo.destinos, list);
				lastOpciones.clear();
				lastOpciones.addAll(list);
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	private List<OpcionDTO> getOpcionesDTO() throws SedeException {
		return ((List<OpcionDTO>) ((Destinos) UtilsCDI.getBeanByReference(Destinos.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_OPCIONES)
				.execute()).stream().sorted(Comparator.comparing(OpcionDTO::getOrden)).collect(Collectors.toList());
	}

	public List<SimpleDTOI> getProcedenciasPensiones() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.procedencias_pensiones);
		if (list == null) {
			log.debug("la lista no está ya en la caché");

			if ((list = convert2DTOI(getProcedenciasPensionesDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Hermes, por lo usamos la última buena");
				hermesDtoLists.put(CachesTurismo.procedencias_pensiones, lastProcedenciasPensiones);

			} else {
				log.debug("la lista se ha obtenido de hermes");
				hermesDtoLists.put(CachesTurismo.procedencias_pensiones, list);
				lastProcedenciasPensiones.clear();
				lastProcedenciasPensiones.addAll(list);
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	private List<ProcedenciaPensionDTO> getProcedenciasPensionesDTO() throws SedeException {
		return (List<ProcedenciaPensionDTO>) ((ProcedenciasPension) UtilsCDI
				.getBeanByReference(ProcedenciasPension.class))
						.call(propertyComponentInstance.get().getTurismoResourcesURL()
								+ RestClient.URL_SUFFIX_PROCEDENCIA_PENSIONES)
						.execute();
	}

	public List<SimpleDTOI> getSexos() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.sexos);
		if (list == null) {
			log.debug("la lista no está ya en la caché");

			if ((list = convert2DTOI(getSexosDTO())) == null) {
				log.debug("la lista no está en caché ni se puede obtener de Hermes, por lo usamos la última buena");
				hermesDtoLists.put(CachesTurismo.sexos, lastSexos);

			} else {
				log.debug("la lista se ha obtenido de hermes");
				hermesDtoLists.put(CachesTurismo.sexos, list);
				lastSexos.clear();
				lastSexos.addAll(list);
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	private List<SexoDTO> getSexosDTO() throws SedeException {
		return (List<SexoDTO>) ((Sexos) UtilsCDI.getBeanByReference(Sexos.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_SEXOS).execute();
	}

	public PlazoDTO getPlazoTurnoPorDefecto() throws SedeException {
		PlazoDTO plazo = hermesDtoPlazoTurnoPorDefecto.get(CachesTurismo.plazo);
		if (plazo == null) {
			log.debug("el plazo por defecto no está ya en la caché");

			if ((plazo = getPlazoDTO()) == null) {
				log.debug(
						"el plazo por defecto no está en caché ni se puede obtener de Hermes, por lo usamos la última buena");
				hermesDtoPlazoTurnoPorDefecto.put(CachesTurismo.plazo, lastPlazoTurnoPorDefecto);

			} else {
				log.debug("el plazo por defecto se ha obtenido de hermes");
				hermesDtoPlazoTurnoPorDefecto.put(CachesTurismo.plazo, plazo);
				lastPlazoTurnoPorDefecto = plazo;
			}
		}
		return plazo;
	}

	private PlazoDTO getPlazoDTO() throws SedeException {
		return (PlazoDTO) ((Plazo) UtilsCDI.getBeanByReference(Plazo.class)).call(
				propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_PLAZOTURNO_POR_DEFECTO)
				.execute();
	}

	private List<SimpleDTOI> convert2DTOI(List<?> list) {
		List<SimpleDTOI> dtoList = null;
		try {
			dtoList = new ArrayList<SimpleDTOI>();
			for (Object o : list) {
				SimpleDTO dto = new SimpleDTO();
				dto.refactorByReflection(o);
				dtoList.add(dto);
			}

		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
		}
		return dtoList;
	}

}
