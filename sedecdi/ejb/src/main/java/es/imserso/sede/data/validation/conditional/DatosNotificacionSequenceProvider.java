package es.imserso.sede.data.validation.conditional;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import es.imserso.sede.data.dto.direccion.DireccionNotificacionDTOI;
import es.imserso.sede.util.Utils;

public class DatosNotificacionSequenceProvider implements DefaultGroupSequenceProvider<DireccionNotificacionDTOI> {

	@Override
	public List<Class<?>> getValidationGroups(DireccionNotificacionDTOI dto) {

		List<Class<?>> sequence = new ArrayList<>();

		if (!Utils.isEmptyTrimmedAll(dto.getDomicilio(), dto.getLocalidad(), dto.getProvincia(), dto.getPais(),
				dto.getCodigoPostal())) {

			// alguno de los campos tiene valor, por lo que todos los demás del
			// grupo deben tener valor también
			sequence.add(ConditionalValidation.class);
		}

		// aplica las reglas de validación del grupo por defecto
		sequence.add(DireccionNotificacionDTOI.class);

		return sequence;
	}
}