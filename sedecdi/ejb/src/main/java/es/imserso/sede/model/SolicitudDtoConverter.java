package es.imserso.sede.model;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import es.imserso.sede.data.dto.solicitud.SolicitudDTO;
import es.imserso.sede.util.Utils;

/**
 * Converter JABX para materializar/desmaterializar el POJO
 * 
 * @author 11825775
 *
 */
public class SolicitudDtoConverter {

	private static final Logger log = Logger.getLogger(SolicitudDtoConverter.class.getName());

	public static String convertToXML(SolicitudDTO dto) {
		log.debug("convirtiendo SolicitudDTO en XML ...");

		if (dto == null) {
			log.debug("no hay DTO");
			return null;
		}

		String value = null;
		try {
			value = new XmlMapper().writeValueAsString(dto);

			log.debug(value);

		} catch (JsonProcessingException e) {
			log.errorv("error al convertir a XML: {0}", Utils.getExceptionMessage(e));
		}
		return value;
	}

	public static SolicitudDTO convertToDTO(String value) {
		log.debug("convirtiendo XML en SolicitudDTO ...");

		if (StringUtils.isBlank(value)) {
			log.debug("no hay XML");
			return null;
		}

		SolicitudDTO dto = null;
		try {
			dto = new XmlMapper().readValue(value, SolicitudDTO.class);

		} catch (IOException e) {
			log.errorv("error al convertir a XML: {0}", Utils.getExceptionMessage(e));
		}

		return dto;
	}

}
