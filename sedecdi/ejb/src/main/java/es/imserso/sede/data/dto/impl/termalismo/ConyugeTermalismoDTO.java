package es.imserso.sede.data.dto.impl.termalismo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.validation.constraints.NotNull;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;
import es.imserso.sede.data.dto.persona.ConyugeDTO;
import es.imserso.sede.data.dto.qualifier.ConyugeTermalismoQ;
import es.imserso.sede.service.monitor.termalismo.TermalismoQ;


/**
 * @author 11825775
 *
 */
@ConyugeTermalismoQ
@TermalismoQ
@Dependent
public class ConyugeTermalismoDTO extends ConyugeDTO implements ConyugeTermalismoDTOI {

	private static final long serialVersionUID = -601023291280096437L;


	private SimpleDTOI selectedSexo;
	private List<DatoEconomicoDTOI> datosEconomicos;
	private DatoEconomicoDTOI datoEconomico;

	@PostConstruct
	public void onCreate() {
		datosEconomicos = new ArrayList<DatoEconomicoDTOI>();
	}



	@Override
	public List<DatoEconomicoDTOI> getDatosEconomicos() {
		return datosEconomicos;
	}

	@Override
	public void setDatosEconomicosDTO(List<DatoEconomicoDTOI> datosEconomicos) {
		this.datosEconomicos = datosEconomicos;

	}

	@Override
	public void addDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico) {
		this.datosEconomicos.add(datoEconomico);
	}

	@Override
	public void deleteDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico) {
		this.datosEconomicos.remove(datoEconomico);

	}

	@Override
	public SimpleDTOI getSelectedSexo() {
		return selectedSexo;
	}

	@Override
	public void setSelectedSexo(SimpleDTOI sexo) {
		this.selectedSexo = sexo;
		if (sexo!=null)
			setSexo(selectedSexo.getDescripcion());
	}

	public DatoEconomicoDTOI getDatoEconomico() {
		return datoEconomico;
	}

	public void setDatoEconomico(DatoEconomicoDTOI datoEconomico) {
		this.datoEconomico = datoEconomico;
	}

}
