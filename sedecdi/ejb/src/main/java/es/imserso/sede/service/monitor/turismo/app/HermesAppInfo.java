package es.imserso.sede.service.monitor.turismo.app;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.service.monitor.AbstractAppInfo;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.service.monitor.turismo.TurismoQ;
import es.imserso.sede.service.monitor.turismo.service.ApplicationAccesibleInfo;
import es.imserso.sede.service.monitor.turismo.service.CartasAcreditacionDownloadableInfo;
import es.imserso.sede.service.monitor.turismo.service.CreateSolicitudInfo;
import es.imserso.sede.service.monitor.turismo.service.TurnoUpdatableInfo;
import es.imserso.sede.service.monitor.turismo.service.WebServicesActiveInfo;
import es.imserso.sede.util.Utils;

/**
 * Información sobre la aplicación Hermes.
 * 
 * @author 11825775
 *
 */
@TurismoQ
@Dependent
public class HermesAppInfo extends AbstractAppInfo {

	private static final long serialVersionUID = -5393854848432414095L;

	private static final Logger log = Logger.getLogger(HermesAppInfo.class.getName());

	public static final String HERMES_URL = "http://lbaplic/hermes";

	@Inject
	TurismoRepository turismoRepository;

	@Inject
	ApplicationAccesibleInfo applicationAccesibleInfo;

	@Inject
	WebServicesActiveInfo webServicesActiveInfo;

	@Inject
	TurnoUpdatableInfo turnoUpdatableInfo;

	@Inject
	CreateSolicitudInfo creteSolicitudInfo;

	@Inject
	CartasAcreditacionDownloadableInfo cartasAcreditacionDownloadableInfo;

	@PostConstruct
	public void onCreate() {
		services = new ArrayList<ServiceInfo>();
		services.add(applicationAccesibleInfo);
		services.add(webServicesActiveInfo);
		services.add(turnoUpdatableInfo);
		services.add(creteSolicitudInfo);
		services.add(cartasAcreditacionDownloadableInfo);

	}

	@Override
	public String getWebSiteURL() {
		return HERMES_URL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getName()
	 */
	@Override
	public String getName() {
		return "Hermes";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Turismo para personas mayores";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#webServicesActive()
	 */
	@Override
	public Boolean webServicesActive() {
		try {
			return turismoRepository.checkHermesIsAlive() && !anyCriticalServiceDisabled();

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getServices()
	 */
	@Override
	public List<ServiceInfo> getServices() {
		return services;
	}

}
