package es.imserso.sede.data.dto.email;

import org.hibernate.validator.constraints.Email;

import es.imserso.sede.data.validation.constraint.match.FieldMatch;

/**
 * Datos del correo electrónico de contacto
 * 
 * @author 11825775
 *
 */
@FieldMatch.List({
		@FieldMatch(first = "direccion", second = "direccionConfirm", message = "El correo electrónico no coincide con el correo electrónico de confirmación") })
public interface EmailDTOI {

	@Email(message="La dirección de correo electrónico no es válida")
	String getDireccion();

	@Email
	String getDireccionConfirm();

	void setDireccionConfirm(String email);

	void setDireccion(String email);
}
