package es.imserso.sede.data.dto.impl.termalismo;

import javax.enterprise.context.Dependent;

import es.imserso.sede.data.dto.direccion.DireccionDTO;
import es.imserso.sede.data.dto.direccion.DireccionDTOI;
import es.imserso.sede.data.dto.rest.termalismo.ProvinciaVOWS;
import es.imserso.sede.service.monitor.termalismo.TermalismoQ;

/**
 * @author 11825775
 *
 */
@TermalismoQ
@Dependent
public class DireccionTermalismoDTO extends DireccionDTO implements DireccionDTOI, DireccionTermalismoDTOI {

	private static final long serialVersionUID = -6830690215334470722L;

	private ProvinciaVOWS selectedProvincia;

	@Override
	public ProvinciaVOWS getSelectedProvincia() {
		return selectedProvincia;
	}

	@Override
	public void setSelectedProvincia(ProvinciaVOWS selectedProvincia) {
		this.selectedProvincia = selectedProvincia;
	}

}
