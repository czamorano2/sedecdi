package es.imserso.sede.data;

import java.lang.reflect.Member;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import es.imserso.sede.model.Tramite;

@RequestScoped
public class TramiteListProducer {

	@Inject
	private TramiteRepository tramiteRepository;

	private List<Tramite> tramites;

	// @Named provides access the return value via the EL variable name
	// "tramites" in the UI (e.g.,
	// Facelets or JSP view)
	@Produces
	@Named
	public List<Tramite> getTramites() {
		return tramites;
	}

	public void onMemberListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Member member) {
		retrieveAllTramitesOrderedByCodigoSIA();
	}

	@PostConstruct
	public void retrieveAllTramitesOrderedByCodigoSIA() {
		tramites = tramiteRepository.findAllOrderedByCodigoSIA();
	}
}
