package es.imserso.sede.data.dto.util;

import java.io.IOException;

import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * @author 11825775
 *
 */
public class IdentificadorRegistroWrapper {

	private static final Logger log = Logger.getLogger(IdentificadorRegistroWrapper.class.getName());

	/**
	 * construye una instancia de {@link IdentificadorRegistroDTO} a partir del
	 * objeto JSON
	 * 
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public IdentificadorRegistroDTO build(@NotNull String jsonObject) {
		try {
			return new ObjectMapper().readValue(jsonObject, IdentificadorRegistroDTO.class);

		} catch (IOException e) {
			log.error("Error al construir una instancia de IdentificadorRegistroDTO a partir del JSON: "
					+ Utils.getExceptionMessage(e));
			throw new SedeRuntimeException(e);
		}
	}

}