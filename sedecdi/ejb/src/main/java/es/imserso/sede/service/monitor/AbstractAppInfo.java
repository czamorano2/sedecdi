/**
 * 
 */
package es.imserso.sede.service.monitor;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * @author 11825775
 *
 */
public abstract class AbstractAppInfo implements AppInfo {

	private static final long serialVersionUID = -347500009932337444L;

	private static Logger log = Logger.getLogger(AbstractAppInfo.class);
	
	@Inject
	protected PropertyComponent propertyComponent;
	
	/**
	 * servicios web de este {@code AppInfo} que se monitorizan
	 */
	protected List<ServiceInfo> services;

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.monitor.AppInfo#webSiteActive()
	 */
	@Override
	public Boolean webSiteActive() {
		return respondingURL();
	}

	/**
	 * @return si alguno de los servicios críticos de la aplicación está
	 *         deshabilitado
	 */
	protected Boolean anyCriticalServiceDisabled() throws SedeException {
		for (ServiceInfo serviceInfo : getServices()) {
			if (serviceInfo.isCritical() && !serviceInfo.isEnabled()) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * @param url
	 *            URL de la web de la aplicación
	 * @return si esa URL tiene detrás algún servicio respondiendo
	 * @throws IOException
	 */
	protected Boolean respondingURL() {

		Boolean result = Boolean.FALSE;
		try {
			URL siteURL = new URL(getWebSiteURL());
			HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();

			int code = connection.getResponseCode();
			if (code == 200) {
				log.debugv("La web {0} está respondiendo", getWebSiteURL());
				result = Boolean.TRUE;
			} else {
				log.warnv("La web {0} NO está respondiendo", getWebSiteURL());
			}
		} catch (Exception e) {
			log.errorv("Error al comprobar si responde la URL {0}: {1} ", getWebSiteURL(),
					Utils.getExceptionMessage(e));
		}
		return result;
	}

}
