package es.imserso.sede.util.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class SedecdiRestException extends WebApplicationException {

	private static final long serialVersionUID = 7234161081937518927L;

	/**
	 * Construct a new instance with a blank message and default HTTP status
	 * code of 500.
	 */
	public SedecdiRestException() {
		super(Response.Status.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Construct a new instance with a blank message and default HTTP status
	 * code of 500.
	 *
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link #getMessage()} method).
	 * @since 2.0
	 */
	public SedecdiRestException(String message) {
		super(message, Response.Status.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Construct a new instance using the supplied response.
	 *
	 * @param response
	 *            the response that will be returned to the client, a value of
	 *            null will be replaced with an internal server error response
	 *            (status code 500).
	 */
	public SedecdiRestException(final Response response) {
		super(response);
	}

	/**
	 * Construct a new instance using the supplied response.
	 *
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link #getMessage()} method).
	 * @param response
	 *            the response that will be returned to the client, a value of
	 *            null will be replaced with an internal server error response
	 *            (status code 500).
	 * @since 2.0
	 */
	public SedecdiRestException(final String message, final Response response) {
		super(message, response);
	}

	/**
	 * Construct a new instance with a blank message and default HTTP status
	 * code of 500.
	 *
	 * @param cause
	 *            the underlying cause of the exception.
	 */
	public SedecdiRestException(final Throwable cause) {
		super(cause, Response.Status.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Construct a new instance with a blank message and default HTTP status
	 * code of 500.
	 *
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link #getMessage()} method).
	 * @param cause
	 *            the underlying cause of the exception.
	 * @since 2.0
	 */
	public SedecdiRestException(final String message, final Throwable cause) {
		super(message, cause, Response.Status.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Construct a new instance using the supplied response.
	 *
	 * @param response
	 *            the response that will be returned to the client, a value of
	 *            null will be replaced with an internal server error response
	 *            (status code 500).
	 * @param cause
	 *            the underlying cause of the exception.
	 */
	public SedecdiRestException(final Throwable cause, final Response response) {
		super(cause, response);
	}

	/**
	 * Construct a new instance using the supplied response.
	 *
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link #getMessage()} method).
	 * @param response
	 *            the response that will be returned to the client, a value of
	 *            null will be replaced with an internal server error response
	 *            (status code 500).
	 * @param cause
	 *            the underlying cause of the exception.
	 * @since 2.0
	 */
	public SedecdiRestException(final String message, final Throwable cause, final Response response) {
		super(message, cause, response);
	}

}