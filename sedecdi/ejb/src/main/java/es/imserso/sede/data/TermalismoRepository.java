package es.imserso.sede.data;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.jboss.logging.Logger;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.rest.termalismo.BalnearioVOWS;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.termalismo.TermalismoRESTClient;

/**
 * @author 11825775
 *
 */
/**
 * @author 11825775
 *
 */
@Stateless
public class TermalismoRepository {

	private static Logger log = Logger.getLogger(TermalismoRepository.class);

	@Inject
	PropertyComponent propertyComponent;

	@Inject
	TermalismoRemoteRepository termalismoRemoteRepository;

	@Inject
	TermalismoRESTClient termalismoRESTClient;

	public List<SimpleDTOI> getSexos() throws SedeException {
		return termalismoRemoteRepository.getSexos();
	}

	public List<SimpleDTOI> getEstadosCiviles() throws SedeException {
		return termalismoRemoteRepository.getEstadosCiviles();
	}

	public List<SimpleDTOI> getProvincias() throws SedeException {
		return termalismoRemoteRepository.getProvincias();
	}

	public List<SimpleDTOI> getTiposTurno() throws SedeException {
		return termalismoRemoteRepository.getTiposTurno();
	}

	 public List<SimpleDTOI> getCategoriasFamiliaNumerosa() throws SedeException {
	 return termalismoRemoteRepository.getTiposFamiliaNum();
	 }
	


	public String getNuevoNumeroExpediente() throws SedeException {
		return termalismoRESTClient.getNuevoNumeroExpediente();
	}

	
	public List<SimpleDTOI> getBalneariosPorCCAA(Long idComunidad) throws SedeException{
		return termalismoRemoteRepository.getBalneariosPorCCAA(idComunidad);
	}
	
	public List<SimpleDTOI> getComunidadesAutonomas() throws SedeException {
		return termalismoRemoteRepository.getComunidadesAutonomas();
	}
	public List<SimpleDTOI> getTiposPensiones() throws SedeException {
		return termalismoRemoteRepository.getTiposPension();
	}
}
