package es.imserso.sede.data;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.logging.Logger;

import es.imserso.sede.model.SolicitudPdf;
import es.imserso.sede.util.exception.SedeException;

public class SolicitudPdfRepository {
	
	private static final String HQL_FIND_BY_TRAMITE = "select s from SolicitudPdf s where s.tramite.codigoSIA = :codigoSIA";
	
	@Inject
	private Logger log;
	
	@Inject
	private EntityManager em;

	public SolicitudPdf findById(Long id) {
		return em.find(SolicitudPdf.class, id);
	}

	public SolicitudPdf findByNombre(String nombre) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SolicitudPdf> criteria = cb.createQuery(SolicitudPdf.class);
		Root<SolicitudPdf> solicitudPdf = criteria.from(SolicitudPdf.class);
		criteria.select(solicitudPdf).where(cb.equal(solicitudPdf.get("nombre"), nombre));
		return em.createQuery(criteria).getSingleResult();
	}
	
	/**
	 * @param codigoSIA código SIA del trámite
	 * @return plantilla pdf para la solicitud del trámite especificado
	 */
	public SolicitudPdf findByTramite(String codigoSIA) throws SedeException {
		List<SolicitudPdf> solicitudes = em.createQuery(HQL_FIND_BY_TRAMITE, SolicitudPdf.class).setParameter("codigoSIA", codigoSIA).getResultList();
		SolicitudPdf solicitudPdf = null;
		if (solicitudes.size() == 0) {
			log.warn("no existe ninguna plantilla pdf para el trámite " + codigoSIA);
			solicitudPdf = null;
			
		} else if (solicitudes.size() > 1) {
			throw new SedeException("existe más de una plantilla pdf para el trámite " + codigoSIA);
			
		} else if (solicitudes.size() == 1) {
			solicitudPdf = solicitudes.get(0);
		}  
		return solicitudPdf;
	}

	public List<SolicitudPdf> findAllOrderedByNombre() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SolicitudPdf> criteria = cb.createQuery(SolicitudPdf.class);
		Root<SolicitudPdf> solicitudPdf = criteria.from(SolicitudPdf.class);
		criteria.select(solicitudPdf).orderBy(cb.asc(solicitudPdf.get("nombre")));
		return em.createQuery(criteria).getResultList();
	}

}
