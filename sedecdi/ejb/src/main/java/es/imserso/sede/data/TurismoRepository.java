package es.imserso.sede.data;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraDTO;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.HttpMethod;
import es.imserso.sede.util.rest.client.RestClient;
import es.imserso.sede.util.rest.client.hermes.CartaAcreditacion;
import es.imserso.sede.util.rest.client.hermes.CreateSolicitud;
import es.imserso.sede.util.rest.client.hermes.EstadoSolicitud;
import es.imserso.sede.util.rest.client.hermes.IsEnabled;
import es.imserso.sede.util.rest.client.hermes.SolicitudUsuario;
import es.imserso.sede.util.rest.client.hermes.SolicitudesUsuario;
import es.imserso.sede.util.rest.client.hermes.UpdateSolicitud;

/**
 * @author 11825775
 *
 */
@Stateless
public class TurismoRepository {

	private static Logger log = Logger.getLogger(TurismoRepository.class);

	@Inject
	private EntityManager em;

	@Inject
	PropertyComponent propertyComponent;

	@Inject
	TurismoRemoteRepository turismoRemoteRepository;

	public List<SimpleDTOI> getProvincias() throws SedeException {
		return turismoRemoteRepository.getProvincias();
	}

	public List<SimpleDTOI> getEstadosCiviles() throws SedeException {
		return turismoRemoteRepository.getEstadosCiviles();
	}

	public List<SimpleDTOI> getCategoriasFamiliaNumerosa() throws SedeException {
		return turismoRemoteRepository.getCategoriasFamiliaNumerosa();
	}

	public List<SimpleDTOI> getClasesPensiones() throws SedeException {
		return turismoRemoteRepository.getClasesPensiones();
	}

	public List<SimpleDTOI> getOpciones() throws SedeException {
		return turismoRemoteRepository.getOpciones();
	}

	public List<SimpleDTOI> getProcedenciasPensiones() throws SedeException {
		return turismoRemoteRepository.getProcedenciasPensiones();
	}

	public List<SimpleDTOI> getSexos() throws SedeException {
		return turismoRemoteRepository.getSexos();
	}

	public PlazoDTO getPlazoTurnoPorDefecto() throws SedeException {
		return turismoRemoteRepository.getPlazoTurnoPorDefecto();
	}

	/**
	 * @return solicitudes de la sede que no se hayan sincronizado con Hermes
	 */
	public List<Solicitud> findAllHermesUnsynchronized() {
		return em.createNamedQuery("solicitud.unsynchronized", Solicitud.class)
				.setParameter("codigoSIA", TipoTramite.TURISMO.getSia())
				.setParameter("elapsedTime",
						Date.from(LocalDateTime.now().minusMinutes(5).atZone(ZoneId.systemDefault()).toInstant()))
				.getResultList();
	}

	/**
	 * @param dto
	 *            DTO con la estructura del JSON persistido en la base de datos (id
	 *            y número de orden)
	 * @return estado en Hermes de la solicitud correspondiente al identificador
	 *         especificado
	 * @throws SedeException
	 */
	public String getRemoteEstadoSolicitud(TurismoExpedienteAplicacionGestoraDTO dto) throws SedeException {
		return (String) ((EstadoSolicitud) UtilsCDI.getBeanByReference(EstadoSolicitud.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_GET_ESTADO_SOLICITUD_REST_PATH
						+ dto.getId())
				.execute();
	}

	/**
	 * @return Si los servicios REST de Hermes están escuchando
	 * @throws SedeException
	 */
	public Boolean checkHermesIsAlive() throws SedeException {
		try {
			return (Boolean) ((IsEnabled) UtilsCDI.getBeanByReference(IsEnabled.class))
					.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_IS_ALIVE_REST_PATH).execute();
		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			return Boolean.FALSE;
		}
	}

	/**
	 * @param solicitudId
	 *            identificador de la solicitud
	 * @return solicitud de Hermes correspondiente al identificador especificado
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO getRemoteSolicitudById(Long solicitudId) throws SedeException {
		return (SolicitudTurismoDTO) ((SolicitudUsuario) UtilsCDI.getBeanByReference(SolicitudUsuario.class)).call(
				propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_GET_SOLICITUD_REST_PATH + solicitudId)
				.execute();
	}

	/**
	 * @return indica si la carta de areditación está accesible en Hermes ya que, en
	 *         dentro de plazo, no está accesible hasta que se pase el proceso de
	 *         acreditación
	 * @throws SedeException
	 */
	public Boolean isCartaAcreditaciónAccesible() throws SedeException {
		return (Boolean) ((IsEnabled) UtilsCDI.getBeanByReference(IsEnabled.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_ACREDITACION_DESCARGABLE_REST_PATH)
				.execute();
	}

	/**
	 * @param solicitudId
	 *            identificador de la solicitud acreditada
	 * @return fichero pdf en formato array de bytes
	 * @throws SedeException
	 */
	public byte[] getCartaAcreditacion(Long solicitudId) throws SedeException {
		return (byte[]) ((CartaAcreditacion) UtilsCDI.getBeanByReference(CartaAcreditacion.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_GET_CARTA_ACREDITACION_REST_PATH
						+ solicitudId)
				.execute();
	}

	/**
	 * Obtiene las solicitudes cuyo solicitante o cónyuge tenga el documento de
	 * identificación especificado.
	 * 
	 * @param
	 * @return
	 * @throws SedeException
	 */
	@SuppressWarnings("unchecked")
	public List<SolicitudTurismoDTO> getRemoteSolicitudesByDI(String nif_cif) throws SedeException {
		return (List<SolicitudTurismoDTO>) ((SolicitudesUsuario) UtilsCDI.getBeanByReference(SolicitudesUsuario.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_GET_SOLICITUDES_BY_DI_REST_PATH
						+ nif_cif)
				.execute();

	}

	/**
	 * Actualiza una solicitud en Hermes con los datos del DTO especificado.
	 * 
	 * @param selectedSolicitud
	 * @throws SedeException
	 */
	public void updateRemoteSolicitud(SolicitudTurismoDTO selectedSolicitud) throws SedeException {
		log.info((String) ((UpdateSolicitud) UtilsCDI.getBeanByReference(UpdateSolicitud.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_UPDATE_SOLICITUD_REST_PATH,
						HttpMethod.PUT, selectedSolicitud)
				.execute());
	}

	/**
	 * Crea una solicitud en Hermes con los datos del DTO especificado.
	 * 
	 * @param solicitudTurismoDTO
	 * @return
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO createRemoteSolicitud(SolicitudTurismoDTO solicitudTurismoDTO) throws SedeException {
		return (SolicitudTurismoDTO) ((CreateSolicitud) UtilsCDI.getBeanByReference(CreateSolicitud.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_CREATE_SOLICITUD_REST_PATH,
						solicitudTurismoDTO)
				.execute();
	}

	/**
	 * @return si Hermes está desbloqueada, es decir, que no está bloqueada
	 * @throws SedeException
	 */
	public boolean isAplicacionDesbloqueada() throws SedeException {
		return (Boolean) ((IsEnabled) UtilsCDI.getBeanByReference(IsEnabled.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_IS_UNLOCKED_REST_PATH).execute();
	}

	/**
	 * @return si el turno por defecto de Hermes está abierto (es modificable)
	 * @throws SedeException
	 */
	public boolean isTurnoAbierto() throws SedeException {
		return (Boolean) ((IsEnabled) UtilsCDI.getBeanByReference(IsEnabled.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_IS_TURNO_OPEN_REST_PATH).execute();
	}

	/**
	 * @return si se puede dar de alta una solicitud en el plazo por defecto
	 * @throws SedeException
	 */
	public Boolean checkAccesibleAltaSolicitudPlazoPorDefecto() throws SedeException {
		return (Boolean) ((IsEnabled) UtilsCDI.getBeanByReference(IsEnabled.class))
				.call(propertyComponent.getTurismoResourcesURL()
						+ RestClient.HERMES_GET_ACCESIBLE_ALTA_SOLICITUD_PLAZO_POR_DEFECTO_REST_PATH)
				.execute();
	}

	/**
	 * @return si se puede dar de alta una solicitud en el plazo especificado
	 * @throws SedeException
	 */
	public Boolean checkAccesibleAltaSolicitudPlazo(Long plazoId) throws SedeException {
		return (Boolean) ((IsEnabled) UtilsCDI.getBeanByReference(IsEnabled.class))
				.call(propertyComponent.getTurismoResourcesURL()
						+ RestClient.HERMES_GET_ACCESIBLE_ALTA_SOLICITUD_PLAZO_REST_PATH + plazoId)
				.execute();
	}

}
