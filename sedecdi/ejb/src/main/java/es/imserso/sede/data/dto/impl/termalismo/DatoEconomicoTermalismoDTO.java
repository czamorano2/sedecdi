package es.imserso.sede.data.dto.impl.termalismo;

import org.apache.commons.lang.StringUtils;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTO;

/**
 * @author 11825775
 *
 */
public class DatoEconomicoTermalismoDTO extends DatoEconomicoDTO implements DatoEconomicoTermalismoDTOI {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2112266313284324858L;
	private SimpleDTOI tipoPension;

	@Override
	public SimpleDTOI getTipoPension() {
		return tipoPension;
	}

	public void setTipoPension(SimpleDTOI tipoPension) {
		this.tipoPension = tipoPension;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipoPension == null) ? 0 : tipoPension.hashCode());
		result = prime * result + ((importe == null) ? 0 : importe.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatoEconomicoTermalismoDTO other = (DatoEconomicoTermalismoDTO) obj;
		if (tipoPension == null) {
			if (other.getTipoPension() != null)
				return false;
		} else if (!tipoPension.equals(other.getTipoPension()))
			return false;
		if (importe == null) {
			if (other.getImporte() != null)
				return false;
		} else if (!importe.equals(other.getImporte()))
			return false;		
		return true;
	}

	@Override
	public boolean isValid() {
		boolean valid = true;
		validationFailedMessage = StringUtils.EMPTY;
		if (tipoPension == null || importe == null) {
			validationFailedMessage = "Debe introducir todos los datos de la prestación";
			valid = false;
		}
		return valid;
	}

}
