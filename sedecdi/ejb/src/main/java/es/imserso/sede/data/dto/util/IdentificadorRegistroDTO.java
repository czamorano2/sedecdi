package es.imserso.sede.data.dto.util;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Datos para identificar una solicitud en el registro electrónico.
 * 
 * @author 11825775
 *
 */
@XmlRootElement
public class IdentificadorRegistroDTO implements Serializable {

	private static final long serialVersionUID = 6543666718656266910L;
	
	private static final Logger log = Logger.getLogger(IdentificadorRegistroDTO.class);

	/**
	 * identificador del libro del registro
	 */
	private Integer bookIdentification;

	/**
	 * identificador del registro interno de ISicres (ej: 12)
	 */
	private Integer registerIdentification;
	/**
	 * número de registro (ej: 201620000000011)
	 */
	private String registerNumber;

	/**
	 * constructor vacío
	 */
	public IdentificadorRegistroDTO() {
	}

	/**
	 * @param bookIdentification
	 *            identificador del libro del registro
	 * @param registerIdentification
	 *            identificador del registro interno de ISicres (ej: 12)
	 * @param registerNumber
	 *            número de registro (ej: 201620000000011)
	 */
	public IdentificadorRegistroDTO(@NotNull Integer bookIdentification, @NotNull Integer registerIdentification,
			@NotNull String registerNumber) {
		setBookIdentification(bookIdentification);
		setRegisterIdentification(registerIdentification);
		setRegisterNumber(registerNumber);
	}

	/**
	 * @return the bookIdentification
	 */
	public Integer getBookIdentification() {
		return bookIdentification;
	}

	/**
	 * @param bookIdentification
	 *            the bookIdentification to set
	 */
	public void setBookIdentification(Integer bookIdentification) {
		this.bookIdentification = bookIdentification;
	}

	/**
	 * @return the registerIdentification
	 */
	public Integer getRegisterIdentification() {
		return registerIdentification;
	}

	/**
	 * @param registerIdentification
	 *            the registerIdentification to set
	 */
	public void setRegisterIdentification(Integer registerIdentification) {
		this.registerIdentification = registerIdentification;
	}

	/**
	 * @return the registerNumber
	 */
	public String getRegisterNumber() {
		return registerNumber;
	}

	/**
	 * @param registerNumber
	 *            the registerNumber to set
	 */
	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
	
	/**
	 * @return devuelve el objeto en formato JSON
	 * @throws JsonProcessingException
	 */
	@JsonIgnore
	public String getJSON()  {
		log.debug("obtenemos el JSON del DTO...");
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			log.error("error al obtener el JSON del DTO: " + Utils.getExceptionMessage(e));
			throw new SedeRuntimeException(e);
		}
			
	}

}
