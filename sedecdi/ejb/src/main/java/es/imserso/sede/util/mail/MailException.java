package es.imserso.sede.util.mail;

import es.imserso.sede.util.exception.SedeException;

public class MailException extends SedeException {

	private static final long serialVersionUID = 2725609829560649698L;

	public MailException() {
		super("Ocurrió una excepción al enviar o recibir un email");
	}

	public MailException(String message) {
		super(message);
	}

	public MailException(Exception e) {
		super(e.toString());
		this.setStackTrace(e.getStackTrace());
	}
}