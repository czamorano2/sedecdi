package es.imserso.sede.service.monitor.turismo.service;

import javax.inject.Inject;

import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.util.exception.SedeException;

/**
 * Información sobre la accesibilidad a los servicios REST de Hermes.
 * 
 * @author 11825775
 *
 */
public class HermesRestServicesInfo implements ServiceInfo {

	private static final long serialVersionUID = 2660021293794565597L;

	@Inject
	TurismoRepository turismoRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isCritical()
	 */
	@Override
	public Boolean isCritical() {
		return Boolean.TRUE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceName()
	 */
	@Override
	public String getServiceName() {
		return "Servicios REST de Hermes";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceDescription()
	 */
	@Override
	public String getServiceDescription() {
		return "Información sobre si los servicios REST de Hermes están activos";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isEnabled()
	 */
	@Override
	public Boolean isEnabled() {
		try {
			return turismoRepository.checkHermesIsAlive();
		} catch (SedeException e) {
			return Boolean.FALSE;
		}
	}

}
