package es.imserso.sede.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.logging.Logger;

import es.imserso.sede.config.ConfigValidator;
import es.imserso.sede.config.ConfigurationException;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.SedeRuntimeException;
import es.imserso.sede.util.exception.SedecdiRestException;

public class Utils {

	/**
	 * obtiene el valor decodificado de una cookie
	 * 
	 * @param cookieUsuario
	 *            cadena de texto que llega como valor de la cookie
	 * @return instancia de Usuario
	 */
	public static Usuario extractCookieData(String cookieUsuario) {
		Logger log = getLogger(es.imserso.sede.util.Utils.class);

		log.debug("obteniendo los datos de la cookie de usuario...");

		String decodedValue = StringUtils.newString(Base64.decodeBase64(cookieUsuario),
				java.nio.charset.StandardCharsets.UTF_8.toString());
		log.debug("decodedValue value: " + decodedValue);

		Usuario usuario = null;
		try {
			usuario = (Usuario) (JAXBContext.newInstance(Usuario.class).createUnmarshaller()
					.unmarshal(new StringReader(decodedValue)));
			log.debug("cookie de usuario decodificada!");
		} catch (JAXBException e) {
			log.error("error al decodificar la cookie con los datos del usuario: " + Utils.getExceptionMessage(e));
			throw new SedecdiRestException(
					"error al decodificar la cookie con los datos del usuario: " + Utils.getExceptionMessage(e),
					e.getCause());
		}
		return usuario;
	}

	/**
	 * Asegura que el nif del certificado del usuario coincide con uno de los nifs
	 * de la solicitud (solicitante o representante).
	 * 
	 * @param solicitud
	 * @param usuarioCertificado
	 * @throws SedeException
	 *             cuando no se encuentran coincidencias
	 */
	public static void checkMatchingNIFs(@NotNull Solicitud solicitud, @NotNull Usuario usuarioCertificado)
			throws SedeException {

		Logger log = getLogger(es.imserso.sede.util.Utils.class);

		log.debug(String.format("comprobando coincidencia nif solicitud (%s) con nif certificado usuario (%s) ...",
				solicitud.getDocumentoIdentificacionRepresentante() == null ? solicitud.getDocumentoIdentificacion()
						: solicitud.getDocumentoIdentificacion() + "/"
								+ solicitud.getDocumentoIdentificacionRepresentante(),
				usuarioCertificado.getNIF_CIF()));

		if (solicitud != null) {
			// nos aseguramos que coincidan los nifs

			// comprobamos si coincide con el del solicitante
			if (!solicitud.getDocumentoIdentificacion().trim()
					.equalsIgnoreCase(usuarioCertificado.getNIF_CIF().trim())) {
				log.warn("no coincide el nif de la cookie con el de la solicitud");

				String errmsg = null;

				// comprobamos si coincide con el del representante, si hubiera
				if (solicitud.getDocumentoIdentificacionRepresentante() != null) {
					if (!solicitud.getDocumentoIdentificacionRepresentante().trim()
							.equalsIgnoreCase(usuarioCertificado.getNIF_CIF().trim())) {

						errmsg = String.format(
								"no coincide el NIF del solicitante (%s) ni el NIF del representante (%s) con el NIF del certificado (%s)",
								solicitud.getDocumentoIdentificacion(),
								solicitud.getDocumentoIdentificacionRepresentante(), usuarioCertificado.getNIF_CIF());
					}
				} else {

					// la solicitud no tiene representante
					errmsg = String.format("no coincide el NIF del solicitante (%s) con el NIF del certificado (%s)",
							solicitud.getDocumentoIdentificacion(), usuarioCertificado.getNIF_CIF());
				}
				log.warn(errmsg);
				throw new SedeException(errmsg);
			}
		}
	}

	/**
	 * obtiene el mensaje de una excepción (del método getMessage() o, si es
	 * NullPointerException, del método toString())
	 * 
	 * @param e
	 *            excepción de la cual recoger el mensaje
	 * @return
	 */
	public static String getExceptionMessage(Exception e) {
		String exceptionMessage = null;
		if (e.getMessage() == null) {
			exceptionMessage = e.toString();
		} else {
			exceptionMessage = e.getMessage();
		}
		return exceptionMessage;
	}

	/**
	 * se llama cuando se ha detectado un error en la configuración al arrancar la
	 * aplicación
	 * 
	 * @param errmsg
	 *            error detectado
	 */
	public static void configurationErrorDetected(String errmsg) {
		getLogger(ConfigValidator.class)
				.error(box("se ha detectado un error en la configuración al arrancar la aplicación: " + errmsg));

		try {
			// FIXME implementar la acción a realizar cuando se detecte un error
			// en la configuración de la aplicación

		} catch (Exception e) {
			throw new ConfigurationException("no se ha podido configurar la aplicación");
		}
	}

	/**
	 * Envuelve una cadena en una caja para poder visualizarse mejor en la consola
	 * 
	 * @param msg
	 *            mensaje que se envolverá en la caja
	 * @return
	 */
	public static String box(String msg) {
		StringBuilder sb = new StringBuilder();

		sb.append("\n\n\n********************************************************************************\n");
		sb.append("********************************************************************************\n");
		sb.append("********************************************************************************\n");
		sb.append("*\n");
		sb.append("*\n");
		sb.append("*\tERROR: " + msg + "\n");
		sb.append("*\n");
		sb.append("*\n");
		sb.append("********************************************************************************\n");
		sb.append("********************************************************************************\n");
		sb.append("********************************************************************************\n\n\n");
		return sb.toString();
	}

	/**
	 * obtiene un LogProvider en runtime para poder sacar trazas por la consola
	 * 
	 * @return LogProvider
	 */
	public static Logger getLogger(Class<?> classz) {
		if (classz == null) {
			throw new SedeRuntimeException(
					"No se puede obtener un proveedor de log si no se especifica una clase válida");
		}
		return Logger.getLogger(classz);
	}

	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean isEmptyTrimmed(String str) {
		return str == null || isEmpty(str.trim());
	}

	/**
	 * @param values
	 * @return <code>true</code> si todos los valores cumplen
	 *         <code>isEmptyTrimmed</code>
	 */
	public static boolean isEmptyTrimmedAll(String... values) {
		for (String value : values) {
			if (isEmptyTrimmed(value)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Convierte un tiempo en milisegundos a un formato hh:mm:ss
	 * 
	 * @param millis
	 *            tiempo en milisegundos
	 * @return tiempo formateado
	 */
	public static String millisecondsToTimeFormat(@NotNull Long millis) {
		return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
	}

	/**
	 * @param milliseconds
	 *            que se para el hilo
	 */
	@SuppressWarnings("static-access")
	public static void sleep(long milliseconds) {
		try {
			Thread.currentThread().sleep(milliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Calcula el intervalo de iteraciones en función del valor máximo especificado
	 * (recordCount)
	 * 
	 * @param recordCount
	 *            valor máximo sobre el cual calcular los intervalos
	 * @return intervalo de iteraciones adecuado
	 */
	public static int getIterationsToNotifyReportProgressBar(long recordCount) {
		if (recordCount < 100) {
			return 1;
		} else if (recordCount < 1000) {
			return 10;
		} else if (recordCount < 10000) {
			return 100;
		}
		return 1000;
	}

	/**
	 * Returns the contents of the file in a byte array.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] getBytesFromFile(Path path) throws IOException {
		File file = path.toFile();
		InputStream is = new FileInputStream(file);

		// Get the size of the file
		long length = file.length();

		// You cannot create an array using a long type.
		// It needs to be an int type.
		// Before converting to an int type, check
		// to ensure that file is not larger than Integer.MAX_VALUE.
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}

		// Create the byte array to hold the data
		byte[] bytes = new byte[(int) length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			if (is != null) {
				is.close();
			}
			throw new IOException("Could not completely read file " + file.getName());
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;
	}

	/**
	 * Returns the contents of the file in a byte array.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] getBytesFromFile(File file) throws IOException {
		return IOUtils.toByteArray(new FileInputStream(file));
	}

	private static String spad;
	private static String spadFormat;
	private static final String whitespace = " ";

	/**
	 * pad with " " to the right to the given length (n)
	 * 
	 * rpad("abc", 5) returns 'abc '
	 * 
	 * @param s
	 *            String to pad
	 * @param n
	 *            length of whitespaces
	 * @return
	 */
	public static String rpad(String s, int n) {
		spadFormat = "%1$-" + n + "s";
		if (s == null) {
			s = String.format(spadFormat, whitespace);
		}
		if (s.length() == n) {
			spad = s;
		} else if (s.length() > n) {
			spad = s.substring(0, n);
		} else {
			spad = String.format(spadFormat, s);
		}
		return spad;
	}

	/**
	 * pad with " " to the left to the given length (n)
	 * 
	 * lpad("abc", 5) returns ' abc'
	 * 
	 * @param s
	 *            String to pad
	 * @param n
	 *            length of whitespaces
	 * @return
	 */
	public static String lpad(String s, int n) {
		spadFormat = "%1$" + n + "s";
		if (s == null) {
			s = String.format(spadFormat, whitespace);
		}
		if (s.length() == n) {
			spad = s;
		} else if (s.length() > n) {
			spad = s.substring(s.length() - n, s.length());
		} else {
			spad = String.format(spadFormat, s);
		}
		return spad;
	}

	/**
	 * pad with " " to the right to the given length (n)
	 * 
	 * rpad("abc", 5, '0') returns 'abc00'
	 * 
	 * @param s
	 *            String to pad
	 * @param n
	 *            length of extra characteres
	 * @param c
	 *            extra character
	 * @return
	 */
	public static String rpad(String s, int n, char c) {

		if (s == null) {
			s = "";
		}
		if (s.length() == n) {
			spad = s;
		} else if (s.length() > n) {
			spad = s.substring(0, n);
		} else {
			spadFormat = "%1$-" + (n - s.length()) + "s";
			spad = s.concat(String.format(spadFormat, "").replace(' ', c));
		}
		return spad;
	}

	/**
	 * pad with " " to the left to the given length (n)
	 * 
	 * lpad("abc", 5, '0') returns '00abc'
	 * 
	 * @param s
	 *            String to pad
	 * @param n
	 *            length of extra characteres
	 * @param c
	 *            extra character
	 * @return
	 */
	public static String lpad(String s, int n, char c) {
		if (s == null) {
			s = "";
		}
		if (s.length() == n) {
			spad = s;
		} else if (s.length() > n) {
			spad = s.substring(s.length() - n, s.length());
		} else {
			spadFormat = "%1$" + (n - s.length()) + "s";
			spad = String.format(spadFormat, "").replace(' ', c).concat(s);
		}
		return spad;
	}

	private static final String PATH_SEPARATOR = "/";

	/**
	 * @return separador por defecto del sistema de ficheros
	 */
	public static final String pathSeparator() {
		return PATH_SEPARATOR;
		// return FileSystems.getDefault().getSeparator();
	}

	/**
	 * método que indica si una clase es una javax.persistence.Entity
	 * 
	 * @param clazz
	 *            clase que puede o no ser una Entity
	 * @return true si en una Entity, false si no lo es
	 */
	public static boolean isEntity(final Class<?> clazz) {
		return isSerializable(clazz) && clazz.isAnnotationPresent(Entity.class);
	}

	public static boolean isSerializable(final Class<?> clazz) {
		if (clazz.getInterfaces() != null) {
			for (Class<?> subClz : clazz.getInterfaces()) {
				if (subClz == Serializable.class) {
					return true;
				}
			}
		}
		return (clazz.getSuperclass() != null) ? isSerializable(clazz.getSuperclass()) : false;
	}

	/**
	 * Extrae los campos y sus valores de un POJO.
	 * 
	 * @param dto
	 *            POJO del cual se extraerán los datos
	 * @return hashtable con los campos y sus valores
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public static Hashtable<String, String> extractClassFieldsToHashtable(Object dto)
			throws IllegalArgumentException, IllegalAccessException {
		String name;
		String value;
		Object o;

		Hashtable<String, String> hash = new Hashtable<String, String>();

		Class<?> classz = dto.getClass();
		Field[] fields;

		while (classz.getSuperclass() != null) {
			fields = classz.getDeclaredFields();
			for (Field f : fields) {
				f.setAccessible(true);

				name = f.getName();
				value = null;

				o = f.get(dto);
				if (o == null) {
					value = "";
				} else {
					if (o instanceof Date) {
						value = DateUtil.parseDateToString((Date) o);
					} else {
						value = o.toString();
					}
				}

				getLogger(Utils.class).debug("field name: " + name);
				getLogger(Utils.class).debug("field value: " + value);

				hash.put(name, value);
			}

			classz = classz.getSuperclass();
		}

		return hash;
	}

	/**
	 * Dado un hashtable, devuelve un dto con sus campos con valores
	 * 
	 * @throws SedeException
	 */
	public static SolicitudDTOI setValuesFromHashtable(SolicitudDTOI dto, Hashtable<String, String> hasth)
			throws IllegalArgumentException, IllegalAccessException, SedeException {
		String name;
		String nameHash;
		Object o = null;
		Class<?> classz = dto.getClass();
		Field[] fields;

		Iterator<String> itr;
		try {
			while (classz.getSuperclass() != null) {
				itr = null;

				fields = classz.getDeclaredFields();
				for (Field f : fields) {
					f.setAccessible(true);
					name = f.getName();
					itr = hasth.keySet().iterator();
					while (itr.hasNext()) {
						nameHash = itr.next();

						if (name.equals(nameHash)) {
							o = null;
							o = hasth.get(nameHash);
							if (o != null && !Utils.isEmpty(o.toString())) {
								if (f.getType() == Date.class)
									o = DateUtil.parseDate(o.toString());
								if (f.getType() == Integer.class)
									o = Integer.valueOf(o.toString());

								if (f.getType() == BigDecimal.class)
									o = BigDecimal.valueOf((Double.valueOf(o.toString())));

								if (f.getType() == Long.class)
									o = Long.valueOf(o.toString());
								if (f.getType() == String.class)
									o = o.toString();
							} else {
								o = null;
							}
							f.set(dto, o);
							break;
						}
					}
				}
				classz = classz.getSuperclass();
			}

		} catch (Exception e) {
			throw new SedeException(e.getMessage());
		}
		return dto;
	}

	/**
	 * Obtiene los bytes de un ImputStream.
	 * 
	 * @param is
	 *            InputStream
	 * @return array de bytes del InputStream
	 * @throws IOException
	 */
	public static byte[] getBytes(InputStream is) throws IOException {

		int len;
		int size = 1024;
		byte[] buf;

		if (is instanceof ByteArrayInputStream) {
			size = is.available();
			buf = new byte[size];
			len = is.read(buf, 0, size);
		} else {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			buf = new byte[size];
			while ((len = is.read(buf, 0, size)) != -1)
				bos.write(buf, 0, len);
			buf = bos.toByteArray();
		}
		return buf;
	}

	/**
	 * Acorta el nombre de un fichero respetando la extensión.
	 * 
	 * @param filename
	 * @param maxLemngth
	 * @return
	 */
	public static String shortenFileName(String filename, int maxLength) {
		String name = null;

		if (filename != null) {
			int totalLength = filename.length();
			int lastPeriodPos = filename.lastIndexOf('.');
			int extensionLength = totalLength - lastPeriodPos - 1;

			if (lastPeriodPos > 0) {
				String nameNoExtension = filename.substring(0, lastPeriodPos);
				String extension = filename.substring(lastPeriodPos);
				name = nameNoExtension.substring(0, maxLength - 1 - extensionLength) + extension;
			} else {
				name = filename.substring(0, maxLength - 1);
			}
		}

		return name;
	}

	public static String buildName(String nom, String ape1, String ape2) {
		String nombre = "";
		nombre = !Utils.isEmpty(ape1) ? ape1.trim() : "";
		nombre.length();
		nombre += (!Utils.isEmpty(ape2) && !Utils.isEmpty(nombre)) ? " " + ape2 : (!Utils.isEmpty(ape2) ? ape2 : "");
		nombre += (!Utils.isEmpty(nombre) && !Utils.isEmpty(nom)) ? ", " + nom : (!Utils.isEmpty(nom) ? nom : "");
		nombre = Utils.rpad(nombre, 105, ' ');
		return nombre;
	}

	/**
	 * Elimina todos los caracteres no numericos de un string
	 * 
	 * @param alphanumericString
	 * @return
	 */
	public static String numericCharacters(String alphanumericString) {
		String result = alphanumericString.replaceAll("[\\D]", "");
		getLogger(es.imserso.sede.util.Utils.class).info("numericCharacters=" + result);
		return result;
	}

}
