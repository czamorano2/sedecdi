package es.imserso.sede.config;

import java.io.File;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.util.Utils;

/**
 * <p>
 * Realiza un chequeo inicial que confirme que todo está en orden para poder
 * correr la aplicación correctamente.
 * </p>
 * <p>
 * En caso de configuración no válida lanza un evento para que éste se encargue
 * de lanzar una ConfigurationException
 * </p>
 * 
 * @author 11825775
 *
 */
@ApplicationScoped
public class ConfigValidator {

	private Logger logger = Logger.getLogger(this.getClass());

	@Inject
	private PropertyComponent propertyComponent;

	/**
	 * valida la configuración de la aplicación antes de arrancarla del todo
	 */
	public void validateApplicationConfiguration() {

		try {

			validatePaths();

			// comentado porque no se puede comprobar la bbdd porque el
			// entityManagerFactory aún no está cargado cuando carga esta
			// clase
			validateDatabase();

			validateConfigurationProperties();

		} catch (ConfigurationException e) {

			throw e;

		} catch (Exception e) {

			throw new ConfigurationException(e.toString());

		}

	}

	/**
	 * valida las rutas especificadas en application.properties
	 */
	private void validatePaths() {

		String msg;
		File f;

		f = new File(propertyComponent.getInputFolder());
		if (!f.exists() || !f.isDirectory()) {
			msg = "La propiedad application.data.input.folder de application.properties no es correcta: "
					+ propertyComponent.getInputFolder();
			throw new ConfigurationException(msg);
		}

		f = new File(propertyComponent.getOutputFolder());
		if (!f.exists() || !f.isDirectory()) {
			msg = "La propiedad application.data.output.folder de application.properties no es correcta: "
					+ propertyComponent.getOutputFolder();
			throw new ConfigurationException(msg);
		}

		f = new File(propertyComponent.getReportFolder());
		if (!f.exists() || !f.isDirectory()) {
			msg = "La propiedad application.data.report.folder de application.properties no es correcta: "
					+ propertyComponent.getReportFolder();
			throw new ConfigurationException(msg);
		}

		f = new File(propertyComponent.getTempFolder());
		if (!f.exists() || !f.isDirectory()) {
			msg = "La propiedad application.data.temp.folder de application.properties no es correcta: "
					+ propertyComponent.getTempFolder();
			throw new ConfigurationException(msg);
		}

		f = new File(propertyComponent.getTestFolder());
		if (!f.exists() || !f.isDirectory()) {
			msg = "La propiedad application.data.test.folder de application.properties no es correcta: "
					+ propertyComponent.getTestFolder();
			throw new ConfigurationException(msg);
		}

		f = new File(propertyComponent.getReportLogFolder());
		if (!f.exists() || !f.isDirectory()) {
			msg = "La propiedad application.report.logFolder de application.properties no es correcta: "
					+ propertyComponent.getReportLogFolder();
			throw new ConfigurationException(msg);
		}

		f = new File(propertyComponent.getReportOutputFolder());
		if (!f.exists() || !f.isDirectory()) {
			msg = "La propiedad application.conf.report.outputPath de application.properties no es correcta: "
					+ propertyComponent.getReportOutputFolder();
			throw new ConfigurationException(msg);
		}

	}

	/**
	 * comprueba que la estructura y los datos en la BBDD sean correctos
	 */
	protected void validateDatabase() {

		logger.info("validando base de datos...");

		try {
			//TODO añadir validaciones de base de datos al arrancar la aplicación
		} catch (Exception e) {
			Utils.configurationErrorDetected(Utils.getExceptionMessage(e));
		}

	}

	/**
	 * valida las propiedades del fichero application.properties
	 */
	protected void validateConfigurationProperties() {

		logger.debug("validando propiedades...");

		try {
			// TODO implementar

		} catch (Exception e) {
			Utils.configurationErrorDetected(Utils.getExceptionMessage(e));
		}

	}

}
