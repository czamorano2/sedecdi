package es.imserso.sede.data.dto.impl.termalismo;

import es.imserso.sede.data.dto.rest.termalismo.ProvinciaVOWS;

public interface DireccionTermalismoDTOI {

	ProvinciaVOWS getSelectedProvincia();

	void setSelectedProvincia(ProvinciaVOWS selectedProvincia);

}