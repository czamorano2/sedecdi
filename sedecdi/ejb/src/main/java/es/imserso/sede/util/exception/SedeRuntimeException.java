package es.imserso.sede.util.exception;

/**
 * Esta clase y sus subclases son una forma de {@code RuntimeException} que
 * pueden ser lanzadas durante la ejecucón normal de la aplicación por razones
 * imprevistas como:
 * <ul>
 * <li>cualquier error inesperado</li>
 * <li>un uso inapropiado de la clase</li>
 * <li>cualquier condición anómala que no sea necesario preveer por los
 * implementadores de la clase</li>
 * </ul>
 * 
 * @author 11825775
 *
 */
public class SedeRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 7085787348168593518L;

	protected static String DEFAULT_MESSAGE;

	public SedeRuntimeException() {
		super("Ocurrió una excepción inesperada!");
	}

	public SedeRuntimeException(String message) {
		super(message);
	}

	public SedeRuntimeException(Exception e) {
		super(e);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 * @param cause
	 *            causa de la excepción
	 */
	public SedeRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 *            causa de la excepción
	 */
	public SedeRuntimeException(Throwable cause) {
		super(cause);
	}

}
