package es.imserso.sede.util.rest.client.hermes;

import java.util.List;

import javax.ws.rs.core.GenericType;

import org.jboss.logging.Logger;

import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.AbstractRestClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * Obtiene la lista de elementos para mostrar en los componentes SelectOneMenu
 * 
 * @author 11825775
 *
 */
public class SelectOneMenuStringList extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -6981237771987853780L;

	private static final Logger log = Logger.getLogger(SelectOneMenuStringList.class.getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#manageResponse()
	 */
	@Override
	public Object execute() throws SedeException {
		List<String> list = null;

		try {
			log.debug("leyendo response ...");
			// List<String> listCar = new ObjectMapper().readValue(response, new
			// TypeReference<List<String>>(){});
			GenericType<List<String>> genericType = new GenericType<List<String>>() {
			};
			list = response.readEntity(genericType);

		} catch (Exception e) {
			String errmsg = String.format("Error al obtener las entidades auxiliares para los selectOneMenu: %s",
					Utils.getExceptionMessage(e));
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
		}

		return (Object) list;
	}

}
