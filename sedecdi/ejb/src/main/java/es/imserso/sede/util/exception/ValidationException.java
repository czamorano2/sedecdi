package es.imserso.sede.util.exception;

/**
 * Esta clase y sus subclases son una forma de {@code Exception} que indica
 * que no se ha podido superar satisfactoriamente una validación.
 * 
 * @author 11825775
 *
 */
public class ValidationException extends SedeRuntimeException {

	private static final long serialVersionUID = -2655537997819464085L;
	
	protected static final String DEFAULT_MESSAGE = "Error en validación";
	
	/**
	 * constructor por defecto
	 * 
	 * @see java.lang.Exception
	 */
	public ValidationException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 */
	public ValidationException(String message) {
		super(message);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 * @param cause
	 *            causa de la excepción
	 */
	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 *            causa de la excepción
	 */
	public ValidationException(Throwable cause) {
		super(cause);
	}

}
