
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para WSInputRegister complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="WSInputRegister">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.invesicres.org}WSOutputRegister">
 *       &lt;sequence>
 *         &lt;element name="OriginalNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginalDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="OriginalType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OriginalEntity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginalEntityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSInputRegister", propOrder = {
    "originalNumber",
    "originalDate",
    "originalType",
    "originalEntity",
    "originalEntityName"
})
public class WSInputRegister
    extends WSOutputRegister
{

    @XmlElement(name = "OriginalNumber")
    protected String originalNumber;
    @XmlElement(name = "OriginalDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar originalDate;
    @XmlElement(name = "OriginalType")
    protected Integer originalType;
    @XmlElement(name = "OriginalEntity")
    protected String originalEntity;
    @XmlElement(name = "OriginalEntityName")
    protected String originalEntityName;

    /**
     * Obtiene el valor de la propiedad originalNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalNumber() {
        return originalNumber;
    }

    /**
     * Define el valor de la propiedad originalNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalNumber(String value) {
        this.originalNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad originalDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOriginalDate() {
        return originalDate;
    }

    /**
     * Define el valor de la propiedad originalDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOriginalDate(XMLGregorianCalendar value) {
        this.originalDate = value;
    }

    /**
     * Obtiene el valor de la propiedad originalType.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOriginalType() {
        return originalType;
    }

    /**
     * Define el valor de la propiedad originalType.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOriginalType(Integer value) {
        this.originalType = value;
    }

    /**
     * Obtiene el valor de la propiedad originalEntity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalEntity() {
        return originalEntity;
    }

    /**
     * Define el valor de la propiedad originalEntity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalEntity(String value) {
        this.originalEntity = value;
    }

    /**
     * Obtiene el valor de la propiedad originalEntityName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalEntityName() {
        return originalEntityName;
    }

    /**
     * Define el valor de la propiedad originalEntityName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalEntityName(String value) {
        this.originalEntityName = value;
    }

}
