
package es.imserso.sede.service.registration.registry.ISicres.books.client;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * <p>Clase Java para Security complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Security"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UsernameToken" type="{http://schemas.xmlsoap.org/ws/2002/04/secext}UsernameTokenClass" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;anyAttribute processContents='skip' namespace='##other'/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Security", namespace = "http://schemas.xmlsoap.org/ws/2002/04/secext", propOrder = {
    "usernameToken"
})
public class Security {

    @XmlElement(name = "UsernameToken")
    protected UsernameTokenClass usernameToken;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Obtiene el valor de la propiedad usernameToken.
     * 
     * @return
     *     possible object is
     *     {@link UsernameTokenClass }
     *     
     */
    public UsernameTokenClass getUsernameToken() {
        return usernameToken;
    }

    /**
     * Define el valor de la propiedad usernameToken.
     * 
     * @param value
     *     allowed object is
     *     {@link UsernameTokenClass }
     *     
     */
    public void setUsernameToken(UsernameTokenClass value) {
        this.usernameToken = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
