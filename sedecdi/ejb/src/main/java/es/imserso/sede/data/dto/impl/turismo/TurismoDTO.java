package es.imserso.sede.data.dto.impl.turismo;

import java.io.Serializable;
import java.util.Hashtable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.sede.data.dto.datosEconomicos.DatosEconomicosDTOI;
import es.imserso.sede.data.dto.destino.DestinosDTOI;
import es.imserso.sede.data.dto.familiaNumerosa.FamiliaNumerosaDTOI;
import es.imserso.sede.data.dto.persona.HijoDTOI;
import es.imserso.sede.data.dto.qualifier.ConyugeQ;
import es.imserso.sede.data.dto.qualifier.HijoQ;
import es.imserso.sede.data.dto.qualifier.NotificacionQ;
import es.imserso.sede.data.dto.qualifier.SolicitanteQ;
import es.imserso.sede.data.dto.solicitud.SolicitudDTO;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraDTO;
import es.imserso.sede.data.dto.vinculada.SolicitudVinculadaDTOI;
import es.imserso.sede.service.monitor.turismo.TurismoQ;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Este DTO es utilizado por la aplicación para recopilar los datos necesarios
 * para persistir una solicitud en la Sede Electrónica.
 * <p>
 * Se persistirá en dos rpositorios:
 * <ul>
 * <li>en el registro de Invesicres con todos los campos del formulario que,
 * aunque no se persisten en la base de datos de la Sede Electrónica, son
 * enviados a Hermes contenidos en otro DTO: SolicitudTurismoDTO</li>
 * <li>en la base de datos de la Sede Electrónica</li> </ul
 * 
 * @author 11825775
 *
 */
@XmlRootElement
@Dependent
public class TurismoDTO extends SolicitudDTO implements SolicitudTurismoDTOI, Serializable {

	private static final long serialVersionUID = 2604073683614857649L;

	private Logger log = Logger.getLogger(TurismoDTO.class.getName());

	@Inject
	@SolicitanteQ
	@TurismoQ
	private SolicitanteTurismoDTOI solicitante;
	
	@Inject
	@ConyugeQ
	@TurismoQ
	private ConyugeTurismoDTOI conyuge;

	@Inject
	@HijoQ
	private HijoDTOI hijo;

	@Inject
	@NotificacionQ
	@TurismoQ
	private DireccionNotificacionTurismoDTOI direccionNotificacion;

	@Inject
	private FamiliaNumerosaDTOI familiaNumerosa;

	@Inject
	private DestinosDTOI destinos;

	@Inject
	private DatosEconomicosDTOI datosEconomicos;

	@Inject
	private SolicitudVinculadaDTOI solicitudVinculada;

	private Boolean denegarAutorizacionConsultas;
	private Long id;
	private PlazoDTO plazo;
	private TurismoExpedienteAplicacionGestoraDTO expedienteAplicacionGestora;
	private AcreditacionDTO acreditacion;

	@PostConstruct
	public void onCreate() {
		log.debug("PostConstruct de TurismoDTO");
	}

	/*
	 * 
	 * @see es.imserso.sede.data.dto.DTOInterface#getPdfFields()
	 */
	@Override
	public Hashtable<String, String> extractHashtable()  {
		try {
			return Utils.extractClassFieldsToHashtable(this);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			log.error("error al obtener la Hashtable con los campos del DTO de la solicitud: "
					+ Utils.getExceptionMessage(e));
			throw new SedeRuntimeException(e);
		}
	}

	@Override
	@XmlAttribute
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the denegarAutorizacionConsultas
	 */
	@Override
	public Boolean getDenegarAutorizacionConsultas() {
		return denegarAutorizacionConsultas;
	}

	/**
	 * @param denegarAutorizacionConsultas
	 *            the denegarAutorizacionConsultas to set
	 */
	@Override
	public void setDenegarAutorizacionConsultas(Boolean denegarAutorizacionConsultas) {
		this.denegarAutorizacionConsultas = denegarAutorizacionConsultas;
	}

	/**
	 * @return devuelve el objeto en formato JSON
	 * @throws JsonProcessingException
	 */
	@Override
	@JsonIgnore
	public String getJSON() throws JsonProcessingException {
		log.debug("obtenemos el JSON del DTO...");
		return new ObjectMapper().writeValueAsString(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getSolicitudEmail()
	 */
	@Override
	public String getSolicitudEmail() {
		String value = null;

		if (solicitante.getEmail() != null) {
			value = solicitante.getEmail().getDireccion();
		}

		if (getMedioNotificacion() != null) {
			value = getMedioNotificacion().getEmail().getDireccion();
		}
		return value;
	}

	@Override
	public SolicitanteTurismoDTOI getSolicitante() {
		return solicitante;
	}

	@Override
	public void setSolicitante(SolicitanteTurismoDTOI solicitante) {
		this.solicitante = solicitante;
	}

	@Override
	public ConyugeTurismoDTOI getConyuge() {
		return conyuge;
	}

	@Override
	public void setConyuge(ConyugeTurismoDTOI conyuge) {
		this.conyuge = conyuge;
	}

	@Override
	public HijoDTOI getHijo() {
		return hijo;
	}

	@Override
	public void setHijo(HijoDTOI hijo) {
		this.hijo = hijo;
	}

	@Override
	public DireccionNotificacionTurismoDTOI getDireccionNotificacion() {
		return direccionNotificacion;
	}

	@Override
	public void setDireccionNotificacion(DireccionNotificacionTurismoDTOI direccionNotificacion) {
		this.direccionNotificacion = direccionNotificacion;
	}

	@Override
	public FamiliaNumerosaDTOI getFamiliaNumerosa() {
		return familiaNumerosa;
	}

	@Override
	public void setFamiliaNumerosa(FamiliaNumerosaDTOI familiaNumerosa) {
		this.familiaNumerosa = familiaNumerosa;
	}

	@Override
	public DestinosDTOI getDestinos() {
		return destinos;
	}

	@Override
	public void setDestinos(DestinosDTOI destinos) {
		this.destinos = destinos;
	}

	@Override
	public DatosEconomicosDTOI getDatosEconomicos() {
		return datosEconomicos;
	}

	@Override
	public void setDatosEconomicos(DatosEconomicosDTOI datosEconomicos) {
		this.datosEconomicos = datosEconomicos;
	}

	@Override
	public TurismoExpedienteAplicacionGestoraDTO getExpedienteAplicacionGestora() {
		return expedienteAplicacionGestora;
	}

	@Override
	public void setExpedienteAplicacionGestora(TurismoExpedienteAplicacionGestoraDTO expedienteAplicacionGestora) {
		this.expedienteAplicacionGestora = expedienteAplicacionGestora;
	}

	@Override
	public PlazoDTO getPlazo() {
		return plazo;
	}

	@Override
	public void setPlazo(PlazoDTO plazo) {
		this.plazo = plazo;
	}

	@Override
	public AcreditacionDTO getAcreditacion() {
		return acreditacion;
	}

	@Override
	public void setAcreditacion(AcreditacionDTO acreditacion) {
		this.acreditacion = acreditacion;
	}

	@Override
	public boolean hasConyuge() {
		return conyuge != null && conyuge.getDocumentoIdentificacion() != null;
	}

	@Override
	public boolean hasHijo() {
		return hijo != null && hijo.getDocumentoIdentificacion() != null;
	}

	@Override
	public boolean hasVinculacion() {
		return solicitudVinculada != null && solicitudVinculada.getDocumentoIdentificacion() != null;
	}

	@Override
	public boolean hasFamiliaNumerosa() {
		return familiaNumerosa != null && familiaNumerosa.getNumeroCarnet() != null;
	}

	@Override
	public int datosEconomicosCount() {
		return datosEconomicos.count();
	}

	@Override
	public SolicitudVinculadaDTOI getSolicitudVinculada() {
		return solicitudVinculada;
	}

	@Override
	public void setSolicitudVinculada(SolicitudVinculadaDTOI solicitudVinculada) {
		this.solicitudVinculada = solicitudVinculada;
	}

}