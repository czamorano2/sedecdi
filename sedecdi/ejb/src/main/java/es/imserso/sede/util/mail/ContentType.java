package es.imserso.sede.util.mail;

public enum ContentType {
	TEXT("text/plain"),
	TEXT_HTML("text/html");
	
	String contentType;
	
	private ContentType(String contentType) {
        this.contentType = contentType;
    }
	
	public String getContentType() {
		return this.contentType;
	}
}
