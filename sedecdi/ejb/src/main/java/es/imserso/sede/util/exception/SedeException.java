package es.imserso.sede.util.exception;

/**
 * Esta clase y sus subclases son una forma de {@code Exception} que indica
 * problemas en la aplicación que deben ser controladas.
 * 
 * @author 11825775
 *
 */
public class SedeException extends Exception {

	private static final long serialVersionUID = 4400161588703872755L;

	protected static final String DEFAULT_MESSAGE = "Ocurrió una excepción en la aplicación";

	/**
	 * constructor por defecto
	 * 
	 * @see java.lang.Exception
	 */
	public SedeException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 */
	public SedeException(String message) {
		super(message);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 * @param cause
	 *            causa de la excepción
	 */
	public SedeException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 *            causa de la excepción
	 */
	public SedeException(Throwable cause) {
		super(cause);
	}
}
