package es.imserso.sede.service.monitor.firma.app;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.service.monitor.AbstractAppInfo;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.service.monitor.firma.FirmaQ;
import es.imserso.sede.service.monitor.firma.service.FirmaRestServicesInfo;
import es.imserso.sede.service.registration.receipt.ReceiptServiceI;
import es.imserso.sede.util.Utils;

/**
 * Información sobre los servicios web de la aplicación de firma electrónica.
 * 
 * @author 11825775
 *
 */
@FirmaQ
@Dependent
public class FirmaAppInfo extends AbstractAppInfo {

	private static final long serialVersionUID = -5393854848432414095L;

	private static final Logger log = Logger.getLogger(FirmaAppInfo.class.getName());

	@Inject
	ReceiptServiceI receiptService;

	@Inject
	FirmaRestServicesInfo firmaRestServicesInfo;

	@PostConstruct
	public void onCreate() {
		services = new ArrayList<ServiceInfo>();
//		services.add(firmaRestServicesInfo);
	}

	@Override
	public String getWebSiteURL() {
		// No tiene sitio web
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getName()
	 */
	@Override
	public String getName() {
		return "Firma Electrónica";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Servicio de firma electrónica de documentos";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#webServicesActive()
	 */
	@Override
	public Boolean webServicesActive() {
		try {
			return receiptService.isAlive() && !anyCriticalServiceDisabled();

		} catch (Exception e) {
			String errmsg = "Error al comprobar si los servicios web de firma electrónica están activos: "
					+ Utils.getExceptionMessage(e);
			log.error(errmsg);
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getServices()
	 */
	@Override
	public List<ServiceInfo> getServices() {
		return services;
	}

}
