package es.imserso.sede.data.dto.solicitud;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistradoI;
import es.imserso.sede.data.dto.medioNotificacion.MedioNotificacionDTOI;
import es.imserso.sede.data.dto.persona.SolicitanteDTOI;
import es.imserso.sede.data.dto.qualifier.SolicitanteQ;
import es.imserso.sede.model.Estado;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Datos comunes de la solicitud de un trámite.
 * 
 * @author 11825775
 *
 */
public abstract class SolicitudDTO implements SolicitudDTOI, Serializable {

	private static final long serialVersionUID = -4554728337651802542L;

	protected String codigoSIA;

	@Inject
	@SolicitanteQ
	protected SolicitanteDTOI solicitante;

	@Inject
	protected MedioNotificacionDTOI medioNotificacion;

	protected DocumentoRegistradoI documentoSolicitud;

	protected DocumentoRegistradoI justificanteSolicitud;

	protected List<DocumentoRegistradoI> attachedFiles = new ArrayList<DocumentoRegistradoI>();;

	/**
	 * ip de la máquina del usuario (obsoleto)
	 */
	private String ip;

	@Override
	public String getCodigoSIA() {
		return codigoSIA;
	}

	public void setCodigoSIA(String codigoSIA) {
		this.codigoSIA = codigoSIA;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getSolicitante()
	 */
	@Override
	public SolicitanteDTOI getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(SolicitanteDTOI solicitante) {
		this.solicitante = solicitante;
	}

	public MedioNotificacionDTOI getMedioNotificacion() {
		return medioNotificacion;
	}

	public void setMedioNotificacion(MedioNotificacionDTOI medioNotificacion) {
		this.medioNotificacion = medioNotificacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getDocumentoSolicitud()
	 */
	@Override
	public DocumentoRegistradoI getDocumentoSolicitud() {
		return documentoSolicitud;
	}

	public void setDocumentoSolicitud(DocumentoRegistradoI documentoSolicitud) {
		this.documentoSolicitud = documentoSolicitud;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getJustificanteSolicitud()
	 */
	@Override
	public DocumentoRegistradoI getJustificanteSolicitud() {
		return justificanteSolicitud;
	}

	public void setJustificanteSolicitud(DocumentoRegistradoI justificanteSolicitud) {
		this.justificanteSolicitud = justificanteSolicitud;
	}

	@Override
	public List<DocumentoRegistradoI> getAttachedFiles() {
		return this.attachedFiles;
	}

	@Override
	public void setAttachedFiles(List<DocumentoRegistradoI> attachedFiles) {
		this.attachedFiles = attachedFiles;
	}

	@Override
	public void addAttachedFile(DocumentoRegistradoI attachedFile) {
		this.attachedFiles.add(attachedFile);
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DTOInterface#extractNewSolicitud()
	 */
	@Override
	public Map<String, String> extractHashtable() {
		try {
			return Utils.extractClassFieldsToHashtable(this);
		} catch (IllegalAccessException e) {
			throw new SedeRuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DTOInterface#extractNewSolicitud()
	 */
	@Override
	public Solicitud extractNewSolicitud() throws SedeRuntimeException {
		Solicitud solicitud = new Solicitud();
		solicitud.setNombre(getSolicitante().getNombre());
		solicitud.setApellido1(getSolicitante().getApellido1());
		solicitud.setApellido2(getSolicitante().getApellido2());
		solicitud.setDocumentoIdentificacion(getSolicitante().getDocumentoIdentificacion());

		solicitud.setIp(getIp());
		
		solicitud.setEmail(getSolicitudEmail());

		solicitud.setFechaAlta(new Date(System.currentTimeMillis()));
		solicitud.setEstado(Estado.PENDIENTE);

		return solicitud;
	}

}
