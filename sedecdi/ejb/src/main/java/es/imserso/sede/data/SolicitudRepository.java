package es.imserso.sede.data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;

@Stateless
public class SolicitudRepository implements Serializable {

	private static final long serialVersionUID = 3424917643235540671L;

	private static final Logger log = Logger.getLogger(SolicitudRepository.class.getName());

	@Inject
	private EntityManager em;

	public Solicitud findById(Long id) {
		return em.find(Solicitud.class, id);
	}

	public Solicitud findByNumeroRegistro(String numeroRegistro) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Solicitud> criteria = cb.createQuery(Solicitud.class);
		Root<Solicitud> solicitud = criteria.from(Solicitud.class);
		criteria.select(solicitud).where(cb.equal(solicitud.get("numeroRegistro"), numeroRegistro));
		return em.createQuery(criteria).getSingleResult();
	}

	public List<Solicitud> findAllOrderedByNumeroRegistro() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Solicitud> criteria = cb.createQuery(Solicitud.class);
		Root<Solicitud> solicitud = criteria.from(Solicitud.class);
		criteria.select(solicitud).orderBy(cb.asc(solicitud.get("numeroRegistro")));
		return em.createQuery(criteria).getResultList();
	}

	/**
	 * @return solicitudes de la sede que no se hayan sincronizado con Hermes
	 */
	public List<Solicitud> findAllHermesUnsynchronized() {
		return em.createNamedQuery("solicitud.unsynchronized", Solicitud.class)
				.setParameter("codigoSIA", TipoTramite.TURISMO.getSia())
				.setParameter("elapsedTime",
						Date.from(LocalDateTime.now().minusMinutes(5).atZone(ZoneId.systemDefault()).toInstant()))
				.getResultList();
	}

	/**
	 * @param id
	 * @param expedienteAplicacionGestora
	 */
	@Transactional
	public void updateExpedienteAplicacionGestora(Long id, String expedienteAplicacionGestora) {
		Solicitud sol = em.find(Solicitud.class, id);
		sol.setExpedienteAplicacionGestora(expedienteAplicacionGestora);
		em.persist(sol);
		em.flush();
	}

	/**
	 * obtiene las solicitudes de cualquier trámite del usuario
	 * 
	 * @param nif
	 *            documento de identificación del usuario
	 * @return lista de solicitudes del usuario
	 */
	public List<Solicitud> getSolicitudesUsuario(String nif) {
		TypedQuery<Solicitud> typedQuery = em.createNamedQuery("solicitud.user.all", Solicitud.class);
		return typedQuery.setParameter("nif", nif).getResultList();
	}

	/**
	 * @param id
	 *            identificador de la solicitud
	 * @return solicitud correspondiente al id specificado
	 */
	public Solicitud getSolicitudUsuario(@NotNull Long id) {
		return em.find(Solicitud.class, id);
	}

	/**
	 * @param solicitud
	 *            instancia a persistir
	 * @return id de la solicitud persistida
	 */
	public Solicitud save(Solicitud solicitud) {
		log.debug("persistiendo la solicitud en la base de datos ...");
		solicitud = em.merge(solicitud);
		em.persist(solicitud);
		em.flush();

		log.info("solicitud persistida en la base de datos");

		return solicitud;
	}

	public void update(Solicitud solicitud) {
		log.debug("actualizando la solicitud en la base de datos ...");
		solicitud = em.merge(solicitud);
		em.flush();

		log.info("solicitud actualizada en la base de datos");

	}
}
