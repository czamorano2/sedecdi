package es.imserso.sede.util.rest.client;

import java.util.List;

import javax.ws.rs.core.GenericType;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.OpcionDTO;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * Obtiene la lista de provincias
 * 
 * @author 11825775
 *
 */
public class Destinos extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -6981237771982343780L;

	private static final Logger log = Logger.getLogger(Destinos.class.getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#manageResponse()
	 */
	@Override
	public Object execute() throws SedeException {
		List<OpcionDTO> list = null;

		try {
			log.debug("leyendo response ...");
			GenericType<List<OpcionDTO>> genericType = new GenericType<List<OpcionDTO>>() {
			};
			list = response.readEntity(genericType);

		} catch (Exception e) {
			String errmsg = String.format("Error al obtener los destinos: %s", Utils.getExceptionMessage(e));
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
		}

		return (Object) list;
	}

}
