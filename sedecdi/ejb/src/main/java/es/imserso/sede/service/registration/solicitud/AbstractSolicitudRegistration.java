package es.imserso.sede.service.registration.solicitud;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.jboss.logging.Logger;

import es.imserso.sede.data.TramiteRepository;
import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.service.registration.event.RegistrationPhase;
import es.imserso.sede.service.registration.event.RegistrationPhaseEvent;
import es.imserso.sede.service.registration.event.RegistrationResult;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.ValidationException;

/**
 * Clase que extenderá cualquier implementación de SolicitudRegistrationI para
 * registrar una solicitud.
 * <p>
 * Invoca secuencialmente a las distintas fases necesarias para el registro de
 * una solicitud.
 * <p>
 * Cada implementación de esta clase sobreescribirá las fases que necesite para
 * registrar la solicitud.
 * <p>
 * Es muy aconsejable que los métodos sobreescritos de las distintas fases
 * implementen la llamada super.{metodo}() para que se lancen los eventos
 * correspondientes.
 * <p>
 * 
 * 
 * @author 11825775
 *
 */
public class AbstractSolicitudRegistration implements SolicitudRegistrationI {

	protected static final String SOLICITUD_VALIDADA_CORRECTAMENTE = "Solicitud validada correctamente!";
	protected static final String SOLICITUD_CONVERTIDA_A_PDF = "Solicitud convertida a pdf!";
	protected static final String SOLICITUD_DADA_DE_ALTA_EN_REGISTRO = "La solicitud ha sido dada de alta en el registro!";
	protected static final String JUSTIFICANTE_GENERADO = "justificante generado!";
	protected static final String JUSTIFICANTE_FIRMADO = "justificante firmado!";
	protected static final String JUSTIFICANTE_REGISTRADO = "justificante registrado!";

	@Inject
	private Logger log;

	@Inject
	protected Event<RegistrationPhaseEvent> registrationPhaseEvent;

	@Inject
	protected Event<EventMessage> registrationEvent;

	@Inject
	protected Event<RegistrationResult> registrationResultEvent;

	@Inject
	protected TramiteRepository tramiteRepository;

	/**
	 * Instancia del dto con la que vamos a trabajar.
	 */
	protected SolicitudDTOI dtoInstance;

	/**
	 * Instancia de Solicitud con la que vamos a trabajar creada a partir del dto.
	 */
	protected Solicitud solicitudInstance;

	/**
	 * pdf generado a partir de los datos del dto
	 */
	protected byte[] solicitudPdf;

	/**
	 * Justificante generado
	 */
	protected byte[] receipt;

	/**
	 * Justificante generado y firmado
	 */
	protected byte[] signedReceipt;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.SolicitudRegistrationI#persist()
	 */
	@Override
	public Solicitud register(@NotNull SolicitudDTOI dto) throws ValidationException, RegistrationException {
		log.debug("se va a registrar la solicitud...");

		try {
			// asignamos la solicitud a la instancia que vamos a manejar en la clase
			dtoInstance = dto;

			executePhases();

			log.info("solicitud registrada satisfactoriamente!");

			registrationResultEvent.fire(new RegistrationResult(RegistrationResult.Result.RESULT_OK));

		} catch (Exception e) {
			String errmsg = String.format("Error durante el registro de la solicitud: %s",
					Utils.getExceptionMessage(e));
			registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_VALIDATION, errmsg));
			// propagamos la excepción
			throw e;
		}

		return solicitudInstance;
	}

	/**
	 * Ejecuta cada fase del registro de una solicitud en la Sede Electrónica
	 * 
	 * @throws ValidationException
	 *             si no ha pasado las validaciones
	 * @throws RegistrationException
	 *             si ha ocurrido cualquier problema al registrar la solicitud en la
	 *             Sede
	 */
	protected void executePhases() throws RegistrationException, ValidationException {
		beforeValidation();
		validate();
		afterValidation();

		// convertimos la solicitud en pdf
		beforeConvertToPdf();
		convertToPdf();
		afterConvertToPdf();

		// persistimos la solicitud en la BBDD
		beforePersistOnBBDD();
		persistOnBBDD();
		afterPersistOnBBDD();

		// persistimos la solicitud en el registro
		beforePersistOnRegister();
		persistOnRegistry();
		afterPersistOnRegister();

		// generamos el justificante
		generateReceipt();

		// firmamos el justificante
		signReceipt();

		// persistimos el justificante en el registro
		beforePersistReceiptOnRegister();
		persistReceiptOnRegister();
		afterPersistReceiptOnRegister();

		// actualizamos la solicitud en la BBDD
		beforeUpdateOnBBDD();
		updateOnBBDD();
		afterUpdateOnBBDD();

	}

	/**
	 * antes de validar la solicitud
	 * 
	 * @throws RegistrationException
	 */
	protected void beforeValidation() throws ValidationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_VALIDATION));
	}

	/**
	 * valida la solicitud
	 * 
	 * @throws RegistrationException
	 */
	protected void validate() throws ValidationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.VALIDATION));
	}

	/**
	 * tras validar la solicitud
	 * 
	 * @throws RegistrationException
	 */
	protected void afterValidation() throws ValidationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_VALIDATION));
	}

	/**
	 * Antes de convertir la solicitud a pdf
	 * 
	 * @throws RegistrationException
	 */
	protected void beforeConvertToPdf() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_CONVERT_TO_PDF));
	}

	/**
	 * Convierte la solicitud a pdf
	 * 
	 * @throws RegistrationException
	 */
	protected void convertToPdf() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.CONVERT_TO_PDF));
	}

	/**
	 * Después de convertir la solicitud a pdf
	 * 
	 * @throws RegistrationException
	 */
	protected void afterConvertToPdf() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_CONVERT_TO_PDF));
	}

	/**
	 * Antes de persistir la solicitud en la base de datos
	 * 
	 * @throws RegistrationException
	 */
	protected void beforePersistOnBBDD() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_PERSIST_ON_BBDD));
	}

	/**
	 * Persiste la solicitud en la base de datos
	 * 
	 * @throws RegistrationException
	 */
	protected void persistOnBBDD() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.PERSIST_ON_BBDD));
	}

	/**
	 * Después de persistir la solicitud en la base de datos
	 * 
	 * @throws RegistrationException
	 */
	protected void afterPersistOnBBDD() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_PERSIST_ON_BBDD));
	}

	/**
	 * Antes de dar de alta la solicitud en el registro electrónico
	 * 
	 * @throws RegistrationException
	 */
	protected void beforePersistOnRegister() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_PERSIST_ON_REGISTER));
	}

	/**
	 * Da de alta la solicitud en el registro electrónico
	 * 
	 * @throws RegistrationException
	 * @throws SedeException
	 */
	protected void persistOnRegistry() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.PERSIST_ON_REGISTER));
	}

	/**
	 * Después de dar de alta la solicitud en el registro electrónico
	 * 
	 * @throws RegistrationException
	 */
	protected void afterPersistOnRegister() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_PERSIST_ON_REGISTER));
	}

	/**
	 * Genera el Justificante.
	 * 
	 * @throws RegistrationException
	 */
	protected void generateReceipt() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.GENERATE_RECEIPT));
	}

	/**
	 * Firma el Justificante.
	 * 
	 * @throws RegistrationException
	 */
	protected void signReceipt() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.SIGN_RECEIPT));
	}

	/**
	 * Antes de guardar el Justificante en el registro electrónico.
	 * 
	 * @throws RegistrationException
	 */
	protected void beforePersistReceiptOnRegister() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_PERSIST_RECEIPT_ON_REGISTER));
	}

	/**
	 * Guarda el Justificante en el registro electrónico.
	 * 
	 * @throws RegistrationException
	 */
	protected void persistReceiptOnRegister() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.PERSIST_RECEIPT_ON_REGISTER));
	}

	/**
	 * Después de guardar el Justificante en el registro electrónico.
	 * 
	 * @throws RegistrationException
	 */
	protected void afterPersistReceiptOnRegister() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_PERSIST_RECEIPT_ON_REGISTER));
	}

	/**
	 * Antes de actualizar la solicitud en la base de datos
	 * 
	 * @throws RegistrationException
	 */
	protected void beforeUpdateOnBBDD() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_UPDATE_ON_BBDD));
	}

	/**
	 * Actualiza la solicitud en la base de datos
	 * 
	 * @throws RegistrationException
	 */
	protected void updateOnBBDD() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.UPDATE_ON_BBDD));
	}

	/**
	 * Después de actualizar la solicitud en la base de datos
	 * 
	 * @throws RegistrationException
	 */
	protected void afterUpdateOnBBDD() throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_UPDATE_ON_BBDD));
	}

	/**
	 * Después de actualizar la solicitud en la base de datos
	 * 
	 * @param sentMailTo
	 *            dirección a la que se ha enviado la confirmación
	 * @throws RegistrationException
	 */
	protected void afterUpdateOnBBDD(@Email String sentMailTo) throws RegistrationException {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_UPDATE_ON_BBDD, sentMailTo));
	}

}
