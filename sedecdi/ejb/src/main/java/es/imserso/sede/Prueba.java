package es.imserso.sede;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import es.imserso.hermes.session.webservice.dto.OpcionDTO;

public class Prueba {

	public static void main(String[] args) {
		OpcionDTO opcion1 = new OpcionDTO();
		opcion1.setId(1l);
		opcion1.setDescripcion("Opcion40");
		opcion1.setOrdenGrupo(4l);
		opcion1.setDescripcionGrupo("G4");
		opcion1.setOrdenOpcion(0l);
		opcion1.setDescripcionOpcion("O0");
		
		OpcionDTO opcion2 = new OpcionDTO();
		opcion2.setId(2l);
		opcion2.setDescripcion("Opcion12");
		opcion2.setOrdenGrupo(1l);
		opcion2.setDescripcionGrupo("G1");
		opcion2.setOrdenOpcion(2l);
		opcion2.setDescripcionOpcion("O2");
		
		OpcionDTO opcion3 = new OpcionDTO();
		opcion3.setId(3l);
		opcion3.setDescripcion("Opcion11");
		opcion3.setOrdenGrupo(1l);
		opcion3.setDescripcionGrupo("G1");
		opcion3.setOrdenOpcion(1l);
		opcion3.setDescripcionOpcion("O1");
		
		
		
		List<OpcionDTO> list = new ArrayList<OpcionDTO>();
		list.add(opcion1);
		list.add(opcion2);
		list.add(opcion3);
		
//		list.sort((o1, o2) -> o1.getOrdenGrupo.compareTo(o2.ordenGrupo));
		
		List<OpcionDTO> orderedList = list.stream().sorted(Comparator.comparing(OpcionDTO::getOrden)).collect(Collectors.toList());
		orderedList.forEach(e -> System.out.println("id:"+ e.getId()+", descripcion: "+e.getDescripcion()+", ordenGrupo:"+e.getOrdenGrupo()+", ordenOpcion:"+e.getOrdenOpcion()));
		
		

		
		

	}

}
