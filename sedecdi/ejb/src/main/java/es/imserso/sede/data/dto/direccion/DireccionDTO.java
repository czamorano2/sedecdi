package es.imserso.sede.data.dto.direccion;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;

import org.apache.commons.lang.StringUtils;

/**
 * Datos de la dirección de una persona (solicitante, representante,
 * notificación, ...)
 * 
 * @author 11825775
 *
 */
@Dependent
@Default
public class DireccionDTO implements DireccionDTOI, Serializable {

	private static final long serialVersionUID = -1748916668465487351L;

	protected String domicilio;

	protected String localidad;

	protected String provincia;

	protected String pais;

	protected String codigoPostal;

	@Override
	public String getDomicilio() {
		return domicilio;
	}

	@Override
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	@Override
	public String getLocalidad() {
		return localidad;
	}

	@Override
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	@Override
	public String getProvincia() {
		return provincia;
	}

	@Override
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	@Override
	public String getPais() {
		return pais;
	}

	@Override
	public void setPais(String pais) {
		this.pais = pais;
	}

	@Override
	public String getCodigoPostal() {
		return codigoPostal;
	}

	@Override
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.Dirty#isDirty()
	 */
	@Override
	public Boolean isDirty() {
		return StringUtils.isNotBlank(getDomicilio()) || StringUtils.isNotBlank(getLocalidad())
				|| StringUtils.isNotBlank(getProvincia()) || StringUtils.isNotBlank(getPais())
				|| StringUtils.isNotBlank(getCodigoPostal());
	}

}
