package es.imserso.sede.data.dto.rest.termalismo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para motivoEstadoVOWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="motivoEstadoVOWS"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="descMotCarta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idMotivoEstado" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="tipoMotivoEstado" type="{http://solicitante.jaxws.termalismo.imserso.com/}tipoMotivoEstadoVOWS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "motivoEstadoVOWS", propOrder = {
    "descMotCarta",
    "descripcion",
    "idMotivoEstado",
    "tipoMotivoEstado"
})
public class MotivoEstadoVOWS {

    protected String descMotCarta;
    protected String descripcion;
    protected Long idMotivoEstado;
    protected TipoMotivoEstadoVOWS tipoMotivoEstado;

    /**
     * Obtiene el valor de la propiedad descMotCarta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescMotCarta() {
        return descMotCarta;
    }

    /**
     * Define el valor de la propiedad descMotCarta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescMotCarta(String value) {
        this.descMotCarta = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Define el valor de la propiedad descripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad idMotivoEstado.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdMotivoEstado() {
        return idMotivoEstado;
    }

    /**
     * Define el valor de la propiedad idMotivoEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdMotivoEstado(Long value) {
        this.idMotivoEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoMotivoEstado.
     * 
     * @return
     *     possible object is
     *     {@link TipoMotivoEstadoVOWS }
     *     
     */
    public TipoMotivoEstadoVOWS getTipoMotivoEstado() {
        return tipoMotivoEstado;
    }

    /**
     * Define el valor de la propiedad tipoMotivoEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoMotivoEstadoVOWS }
     *     
     */
    public void setTipoMotivoEstado(TipoMotivoEstadoVOWS value) {
        this.tipoMotivoEstado = value;
    }

}
