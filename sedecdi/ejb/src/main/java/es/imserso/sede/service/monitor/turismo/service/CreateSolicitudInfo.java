package es.imserso.sede.service.monitor.turismo.service;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.util.exception.SedeException;

/**
 * Información sobre la accesibilidad a los servicios Web de Hermes
 * 
 * @author 11825775
 *
 */
@Dependent
public class CreateSolicitudInfo implements ServiceInfo {

	private static final long serialVersionUID = -3501092722567399456L;

	@Inject
	TurismoRepository turismoRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isRootService()
	 */
	@Override
	public Boolean isCritical() {
		return Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceName()
	 */
	@Override
	public String getServiceName() {
		return "Crear nueva solicitud";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceDescription()
	 */
	@Override
	public String getServiceDescription() {
		return "Información sobre si es posible crear una nueva solicitud";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isEnabled()
	 */
	@Override
	public Boolean isEnabled() {
		try {
			return turismoRepository.checkAccesibleAltaSolicitudPlazoPorDefecto();
		} catch (SedeException e) {
			return Boolean.FALSE;
		}
	}

}



