package es.imserso.sede.data.validation.nif;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import es.imserso.sede.util.exception.SedeRuntimeException;

public class NifNieValidator implements ConstraintValidator<NifNie, String> {

	private static final Pattern PATTERN_NIF = Pattern.compile("^[0-9]{8}[A-Z]$");
	private static final Pattern PATTERN_NIE = Pattern.compile("^[XYZ][0-9]{7}[A-Z]$");
	private static final String letters = "TRWAGMYFPDXBNJZSQVHLCKE";

	@Override
	public void initialize(NifNie constraintAnnotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return isNif(value) || isCif(value);
	}

	private boolean isNif(String value) {

		final Matcher matcher = PATTERN_NIF.matcher(value);

		if (!matcher.matches()) {
			return false;
		}

		final String dni = value.substring(0, 8);
		final String control = value.substring(8, 9);
		final int position = Integer.parseInt(dni) % 23;

		final String controlCalculated = letters.substring(position, position + 1);

		if (!control.equalsIgnoreCase(controlCalculated)) {
			return false;
		}
		return true;
	}

	private boolean isCif(String value) {
		final Matcher matcher = PATTERN_NIE.matcher(value);

		if (!matcher.matches()) {
			return false;
		}

		String dni = value.substring(1, 8);
		final String firstChar = value.substring(0, 1);
		if (firstChar.equalsIgnoreCase("X")) {
			dni = "0".concat(dni);
		} else if (firstChar.equalsIgnoreCase("Y")) {
			dni = "1".concat(dni);
		} else if (firstChar.equalsIgnoreCase("Z")) {
			dni = "2".concat(dni);
		} else {
			throw new SedeRuntimeException("La primera letra debe ser X, Y o Z");
		}

		final String control = value.substring(8, 9);
		final int position = Integer.parseInt(dni) % 23;

		final String controlCalculated = letters.substring(position, position + 1);

		if (!control.equalsIgnoreCase(controlCalculated)) {
			return false;
		}
		return true;
	}
}