package es.imserso.sede.service.monitor;

import java.io.Serializable;

import es.imserso.sede.util.exception.SedeException;

/**
 * Data Object con información acerca de si un servicio de una aplicación está o
 * no habilitado
 * 
 * @author 11825775
 *
 */
public interface ServiceInfo extends Serializable {

	/**
	 * @return si la disponibilidad de este servicio es crítica o no para determinar
	 *         si los servicios web de la aplicación se consideran activos
	 */
	Boolean isCritical();

	/**
	 * @return nombre del servicio (ej: Turismo)
	 */
	String getServiceName();

	/**
	 * @return descripción del servicio (ej: Vacaciones de personas mayores)
	 */
	String getServiceDescription();

	/**
	 * @return si el servicio está disponible
	 * @throws SedeException
	 */
	Boolean isEnabled();

}
