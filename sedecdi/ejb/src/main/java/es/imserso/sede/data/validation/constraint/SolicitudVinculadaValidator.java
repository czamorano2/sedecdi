package es.imserso.sede.data.validation.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.vinculada.SolicitudVinculadaDTOI;

public class SolicitudVinculadaValidator implements ConstraintValidator<SolicitudVinculada, SolicitudVinculadaDTOI> {

	private Logger log = Logger.getLogger(SolicitudVinculadaValidator.class.getName());

	@Override
	public void initialize(SolicitudVinculada constraintAnnotation) {
		log.trace("initializing...");

	}

	@Override
	public boolean isValid(SolicitudVinculadaDTOI value, ConstraintValidatorContext context) {

		boolean valid = true;

		if (!value.isDirty()) {
			log.debug("No hay datos para validar la solicitud vinculada, por lo que es válida");
			valid = true;

		} else {
			if (StringUtils.isNotBlank(value.getNombre()) || StringUtils.isNotBlank(value.getApellido1())
					|| StringUtils.isNotBlank(value.getApellido2())
					|| StringUtils.isNotBlank(value.getDocumentoIdentificacion())) {
				valid = false;
			}
		}

		return valid;
	}

}
