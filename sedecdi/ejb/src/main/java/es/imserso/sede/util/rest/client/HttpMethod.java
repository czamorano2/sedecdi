package es.imserso.sede.util.rest.client;

/**
 * Métodos de Http
 * 
 * @author 11825775
 *
 */
public enum HttpMethod {
	GET, POST, PUT, DELETE
}
