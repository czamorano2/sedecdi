package es.imserso.sede.data.dto.impl.appgestora;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraDTO;

/**
 * Interface para cualquier DTO de una aplicación gestora (Turismo, Termalismo, ...)
 * @author 11825775
 *
 */
public interface SolicitudAppGestoraDTOI extends SolicitudDTOI {
	
	TurismoExpedienteAplicacionGestoraDTO getExpedienteAplicacionGestora();
	void setExpedienteAplicacionGestora(TurismoExpedienteAplicacionGestoraDTO expedienteAplicacionGestora);
}
