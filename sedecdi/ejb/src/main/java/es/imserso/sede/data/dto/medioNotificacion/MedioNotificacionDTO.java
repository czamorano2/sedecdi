package es.imserso.sede.data.dto.medioNotificacion;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import es.imserso.sede.data.dto.email.EmailDTOI;
import es.imserso.sede.data.dto.qualifier.NotificacionQ;

/**
 * Interface para datos de notificación al usuario.
 * <p>
 * El email es obligatorio
 * 
 * @author 11825775
 *
 */
@Dependent
public class MedioNotificacionDTO implements MedioNotificacionDTOI, Serializable {

	private static final long serialVersionUID = -2769907269181912845L;
	
	@Inject
	@NotificacionQ
	protected EmailDTOI email;
	
	protected String dispositivoElectronico;

	/* (non-Javadoc)
	 * @see es.imserso.sede.data.dto.MedioNotificacionDTOI#getEmail()
	 */
	@Override
	public EmailDTOI getEmail() {
		return email;
	}
	
	public void setEmail(EmailDTOI email) {
		this.email = email;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.data.dto.MedioNotificacionDTOI#getDispositivoElectronico()
	 */
	@Override
	public String getDispositivoElectronico() {
		return dispositivoElectronico;
	}
	
	public void setDispositivoElectronico(String dispositivo) {
		this.dispositivoElectronico = dispositivo;
	}

}
