package es.imserso.sede.service.registration.registry.ISicres.books.bean;

import es.imserso.sede.service.registration.registry.bean.BookI;

public class Book implements BookI {

	protected String name;
	protected int id;
	protected int type;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.ISicres.books.bean.BookI#
	 * getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.ISicres.books.bean.BookI#
	 * setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.ISicres.books.bean.BookI#
	 * getId()
	 */
	@Override
	public int getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.ISicres.books.bean.BookI#
	 * setId(int)
	 */
	@Override
	public void setId(int id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.ISicres.books.bean.BookI#
	 * getType()
	 */
	@Override
	public int getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.ISicres.books.bean.BookI#
	 * setType(int)
	 */
	@Override
	public void setType(int type) {
		this.type = type;
	}

}
