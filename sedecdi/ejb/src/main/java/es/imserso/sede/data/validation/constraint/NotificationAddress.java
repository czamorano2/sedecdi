package es.imserso.sede.data.validation.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Documented
@Constraint(validatedBy = { NotificationAddressValidator.class })
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface NotificationAddress {
	
	String message() default "Deben rellenarse todos los campos de la dirección de notificación";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
