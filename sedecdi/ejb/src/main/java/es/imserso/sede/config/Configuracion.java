package es.imserso.sede.config;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.apache.commons.io.Charsets;
import org.jboss.logging.Logger;

import es.imserso.sede.util.Global;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

@ApplicationScoped
@Named
public class Configuracion {
	private String variableDeEntorno = "application_properties";
	private String ficheroPropiedadesAplicacion = "application.properties";
	private String ficheroPropiedadesGeneral = "configuracion_imserso_general.properties";

	private Logger logger = Logger.getLogger(this.getClass());

	private Properties propiedades = new Properties();

	private String pathAplicacion = Global.APP_CONFIGURATION_PATH;

	/**
	 * Configuración de FreeMarker para los templates de los emails
	 */
	private Configuration freeMarkerConfiguration;

	/**
	 * lista por consola las propiedades del sistema (incluyendo las de la JVM)
	 */
	private void displaySystemProperties() {
		System.getProperties().list(System.out);
	}

	public Configuracion() {

		displaySystemProperties();

		if (System.getProperty(getVariableDeEntorno()) == null) {
			throw new RuntimeException("No ha sido posible encontrar la variable de entorno " + variableDeEntorno
					+ ". Debe estar definida en los parámetros de la JVM. Ejemplo: -Dapplication_properties=/app/imserso");
		}

		if (System.getProperty(getVariableDeEntorno()) != null
				&& System.getProperty(getVariableDeEntorno()).trim().length() == 0) {
			throw new RuntimeException("El valor de la variable de entorno " + variableDeEntorno
					+ " está vacío. Debe definir la ruta en los parámetros de la JVM. Ejemplo: -Dapplication_properties=/app/imserso");
		}

		String pathCompletoPropiedadesGenerales = System.getProperty(getVariableDeEntorno()) + "/"
				+ ficheroPropiedadesGeneral;

		try {
			logger.debug(
					"Cargando las propiedades generales del fichero '" + pathCompletoPropiedadesGenerales + "' ...");
			propiedades.load(new BufferedReader(
					new InputStreamReader(new FileInputStream(pathCompletoPropiedadesGenerales), Charsets.UTF_8)));

		} catch (Exception e) {
			throw new RuntimeException("No ha sido posible cargar el archivo de propiedades " + "generales '"
					+ ficheroPropiedadesGeneral + "' en " + pathCompletoPropiedadesGenerales);
		}

		String pathCompletoPropiedadesAplicacion = System.getProperty(getVariableDeEntorno()) + "/"
				+ Global.APP_CONFIGURATION_PATH + "/config/" + ficheroPropiedadesAplicacion;

		try {
			propiedades.load(new BufferedInputStream(new FileInputStream(pathCompletoPropiedadesAplicacion)));
		} catch (Exception e) {
			throw new RuntimeException("No ha sido posible cargar el archivo de propiedades " + "de la aplicación '"
					+ ficheroPropiedadesAplicacion + "' en " + pathCompletoPropiedadesAplicacion);
		}

		initFreeMarkerConfiguration();
	}

	/**
	 * Inicializa la configuración de FreeMarker
	 */
	private void initFreeMarkerConfiguration() {
		logger.info("inicializando FreeMarker...");
		freeMarkerConfiguration = new Configuration(Configuration.VERSION_2_3_24);
		try {
			freeMarkerConfiguration.setDirectoryForTemplateLoading(new File(System.getProperty(getVariableDeEntorno())
					+ "/" + Global.APP_CONFIGURATION_PATH + "/sysin/templates"));

			freeMarkerConfiguration.setDefaultEncoding("UTF-8");
			freeMarkerConfiguration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			freeMarkerConfiguration.setLogTemplateExceptions(false);

		} catch (IOException e) {
			throw new RuntimeException("No ha sido posible inicializar FreeMarker: " + e.toString());
		}
		logger.info("FreeMarker inicializado!");
	}

	public String getVariableDeEntorno() {
		return variableDeEntorno;
	}

	public void setVariableDeEntorno(String variableDeEntorno) {
		this.variableDeEntorno = variableDeEntorno;
	}

	public String getFicheroPropertiesAplicacion() {
		return ficheroPropiedadesAplicacion;
	}

	public void setFicheroPropertiesAplicacion(String ficheroPropertiesAplicacion) {
		this.ficheroPropiedadesAplicacion = ficheroPropertiesAplicacion;
	}

	public String getFicheroPropiedadesGeneral() {
		return ficheroPropiedadesGeneral;
	}

	public void setFicheroPropiedadesGeneral(String ficheroPropertiesGeneral) {
		this.ficheroPropiedadesGeneral = ficheroPropertiesGeneral;
	}

	public String getPropiedad(String propiedad) {
		return propiedades.getProperty(propiedad);
	}

	/**
	 * @return the pathAplicacion
	 */
	public String getPathAplicacion() {
		return pathAplicacion;
	}

	/**
	 * @param pathAplicacion
	 *            the pathAplicacion to set
	 */
	public void setPathAplicacion(String pathAplicacion) {
		this.pathAplicacion = pathAplicacion;
	}

	public Configuration getFreeMarkerConfiguration() {
		return freeMarkerConfiguration;
	}

	public void setFreeMarkerConfiguration(Configuration freeMarkerConfiguration) {
		this.freeMarkerConfiguration = freeMarkerConfiguration;
	}

}
