package es.imserso.sede.data.dto.destino;

import java.io.Serializable;

import javax.enterprise.context.Dependent;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;

/**
 * Opciones de destino
 * 
 * @author 11825775
 *
 */
@Dependent
public class DestinosDTO implements DestinosDTOI, Serializable {

	private static final long serialVersionUID = 5383986000187338988L;

	private SimpleDTOI opcion1;
	private SimpleDTOI opcion2;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DestinosDTOI#getOpcion1()
	 */
	@Override
	public SimpleDTOI getOpcion1() {
		return opcion1;
	}

	@Override
	public void setOpcion1(SimpleDTOI opcion1) {
		this.opcion1 = opcion1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DestinosDTOI#getOpcion2()
	 */
	@Override
	public SimpleDTOI getOpcion2() {
		return opcion2;
	}

	@Override
	public void setOpcion2(SimpleDTOI opcion2) {
		this.opcion2 = opcion2;
	}

}
