package es.imserso.sede.data.dto.impl.termalismo;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;

/**
 * @author 11825775
 *
 */
public interface DatoEconomicoTermalismoDTOI extends DatoEconomicoDTOI {

	/**
	 * @return Clase de pensión (jubilación, enferedad, ...)
	 */
	SimpleDTOI getTipoPension();

	void setTipoPension(SimpleDTOI tipoPension);


}