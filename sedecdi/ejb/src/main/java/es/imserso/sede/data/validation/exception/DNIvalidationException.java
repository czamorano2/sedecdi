package es.imserso.sede.data.validation.exception;

public class DNIvalidationException extends Exception {
	
	private static final long serialVersionUID = -7848333767026928932L;
	
	//TODO externalizar el mensaje
	private static final String MSG = "DNI no válido"; 

	public DNIvalidationException() {
		super(MSG);
	}

	public DNIvalidationException(String message) {
		super(message);
	}

}

