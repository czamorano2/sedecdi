package es.imserso.sede.service.converter.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.BaseField;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.TextField;

import es.imserso.sede.service.converter.impl.PdfUtil.PassThroughFields;

/**
 * ATENCION: esto solamente debe usarse en desarrollo, nunca en producción.
 * <p>
 * Crea en un pdf campos que correspondan a las claves de un HashMap.
 * 
 * @author 11825775
 *
 */
// FIXME quitar para producción
public class CreateTemplate {

	public static final String DEST = "E:/_tmp/sedeTmpl.pdf";

	public static void createPdf(Set<String> fields) throws IOException, DocumentException {
		File f = new File(DEST);
		f.getParentFile().mkdirs();

		Document document = new Document();
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(f));
		document.open();
		String fieldName;
		float incrementY = 20f;
		float currentY = 10f;

		Iterator<String> it = fields.iterator();
		while (it.hasNext()) {
			fieldName = it.next();
			TextField title = new TextField(writer, new Rectangle(10, currentY, 100, currentY + 18), fieldName);
			writer.addAnnotation(title.getTextField());

			currentY += incrementY;
		}
		document.close();
	}

	public static void manipulatePdf(Hashtable<String, String> atributosDto, String plantillaOrigen)
			throws DocumentException, IOException {

		String dest = plantillaOrigen + "_Destino.pdf";
		plantillaOrigen += ".pdf";
		PdfReader reader = new PdfReader(plantillaOrigen);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
		float incrementY = 20f;
		float currentY = 10f;
		String fieldName = "";
		TextField campo;
		PdfFormField hiddenCampo;

		reader = new PdfReader(plantillaOrigen);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PdfStamper stamperOrigen = new PdfStamper(reader, baos);
		stamperOrigen.setFormFlattening(false);
		AcroFields acroFields = stamperOrigen.getAcroFields();
		Map<String, Item> fieldMap = acroFields.getFields();
		Set<String> keysPlantilla = fieldMap.keySet();

		// recorremos el hash
		Iterator<String> it = atributosDto.keySet().iterator();
		while (it.hasNext()) {

			fieldName = it.next();
			// si existe en la plantilla una variable con el mismo nombre
			// que el leído en el hash, se pasa al acrofield.
			if (!PassThroughFields.isInEnum(fieldName)) {
				if (!keysPlantilla.contains(fieldName)) {

					campo = new TextField(stamper.getWriter(), new Rectangle(10, currentY, 100, currentY + 18),
							fieldName);

					campo.setVisibility(BaseField.HIDDEN);
					hiddenCampo = campo.getTextField();

					stamper.addAnnotation(hiddenCampo, 1);
					currentY += incrementY;
				}
			}
		}
		stamper.close();
		stamperOrigen.close();
		reader.close();
	}

}
