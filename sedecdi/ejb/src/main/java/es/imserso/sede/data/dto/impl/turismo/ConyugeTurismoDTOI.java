package es.imserso.sede.data.dto.impl.turismo;

import java.util.List;

import javax.validation.constraints.NotNull;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;
import es.imserso.sede.data.dto.persona.ConyugeDTOI;

/**
 * @author 11825775
 *
 */
public interface ConyugeTurismoDTOI extends ConyugeDTOI {

	SimpleDTOI getSelectedProvincia();

	void setSelectedProvincia(@NotNull SimpleDTOI provincia);

	SimpleDTOI getSelectedEstadoCivil();

	void setSelectedEstadoCivil( SimpleDTOI estadoCivil);

	List<DatoEconomicoDTOI> getDatosEconomicos();

	void setDatosEconomicosDTO( List<DatoEconomicoDTOI> datosEconomicos);

	void addDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico);

	void deleteDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico);

	SimpleDTOI getSelectedSexo();

	void setSelectedSexo( SimpleDTOI sexo);

}
