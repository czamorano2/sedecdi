package es.imserso.sede.data.dto.datosEconomicos;

import java.io.Serializable;
import java.math.BigDecimal;

public abstract class DatoEconomicoDTO implements DatoEconomicoDTOI, Serializable {

	private static final long serialVersionUID = 5131190920181574171L;

	protected BigDecimal importe;

	protected String validationFailedMessage;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DatoEconomicoDTOI#getImporte()
	 */
	@Override
	public BigDecimal getImporte() {
		return importe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.data.dto.DatoEconomicoDTOI#setImporte(java.math.BigDecimal)
	 */
	@Override
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DatoEconomicoDTOI#getValidationFailedMessage()
	 */
	@Override
	public String getValidationFailedMessage() {
		return validationFailedMessage;
	}

}
