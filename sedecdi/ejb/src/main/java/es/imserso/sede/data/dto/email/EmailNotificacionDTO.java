package es.imserso.sede.data.dto.email;

import javax.enterprise.context.Dependent;

import es.imserso.sede.data.dto.qualifier.NotificacionQ;

/**
 * @author 11825775
 *
 */
@Dependent
@NotificacionQ
public class EmailNotificacionDTO extends EmailDTO implements EmailNotificacionDTOI {

	private static final long serialVersionUID = -2771791968792802404L;

}
