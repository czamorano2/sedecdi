
package es.imserso.sede.service.registration.registry.ISicres.books.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WSGetOutputBooksResult" type="{http://www.invesicres.org}ArrayOfWSBook" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsGetOutputBooksResult"
})
@XmlRootElement(name = "WSGetOutputBooksResponse")
public class WSGetOutputBooksResponse {

    @XmlElement(name = "WSGetOutputBooksResult")
    protected ArrayOfWSBook wsGetOutputBooksResult;

    /**
     * Obtiene el valor de la propiedad wsGetOutputBooksResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSBook }
     *     
     */
    public ArrayOfWSBook getWSGetOutputBooksResult() {
        return wsGetOutputBooksResult;
    }

    /**
     * Define el valor de la propiedad wsGetOutputBooksResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSBook }
     *     
     */
    public void setWSGetOutputBooksResult(ArrayOfWSBook value) {
        this.wsGetOutputBooksResult = value;
    }

}
