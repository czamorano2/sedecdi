package es.imserso.sede.util.mail;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.TipoDocumentoRegistrado;

public class MailEngine implements Serializable {

	private static final long serialVersionUID = -699866409893046594L;

	/**
	 * Resource for sending the email. The mail subsystem is defined in either
	 * standalone.xml or domain.xml in your respective configuration directory.
	 */
	@Resource(mappedName = "java:jboss/mail/Default")
	private Session mySession;

	@Inject
	private Logger log;

	/**
	 * Envía un email.
	 * 
	 * @param subject
	 * @param messageText
	 * @param from
	 * @param to
	 * @throws Exception
	 */
	public void send(@NotNull String subject, @NotNull String messageText, @NotNull String from, @NotNull String to)
			throws Exception {
		// Por defecto LATIN1 porque es la codificación por defecto de lotus
		// notes
		send(subject, messageText, from, to, ContentType.TEXT_HTML, StandardCharsets.ISO_8859_1);
	}

	/**
	 * Envía un email.
	 * 
	 * @param subject
	 * @param messageText
	 * @param from
	 * @param to
	 * @param contentType
	 * @param charset
	 * @throws Exception
	 */
	public void send(@NotNull String subject, @NotNull String messageText, @NotNull String from, @NotNull String to,
			@NotNull ContentType contentType, @NotNull Charset charset) throws Exception {

		log.debug("se va a enviar un email con el asunto '" + subject + "' y el texto '" + messageText + "'");

		// El email lo enviamos en LATIN1 porque es la codificación por defecto
		// de lotus notes
		ByteBuffer bfSubject = charset.encode(subject);
		ByteBuffer bfBody = charset.encode(messageText);
		String subjectLatin1 = new String(bfSubject.array(), StandardCharsets.ISO_8859_1);
		String bodyLatin1 = new String(bfBody.array(), StandardCharsets.ISO_8859_1);

		Message message = new MimeMessage(mySession);
		message.setFrom(new InternetAddress(from));
		Address toAddress = new InternetAddress(to);
		message.addRecipient(Message.RecipientType.TO, toAddress);
		message.setSubject(subjectLatin1);
		message.setContent(bodyLatin1, contentType.getContentType());

		Transport.send(message);

		log.debug("email enviado!");
	}

	/**
	 * Envía un email con la solicitud en pdf adjunta.
	 * 
	 * @param subject
	 * @param messageText
	 * @param from
	 * @param to
	 * @param solicitudPdf
	 * @param justificantePdf
	 * @throws Exception
	 */
	public void sendWithSolicitudPdfAttached(@NotNull String subject, @NotNull String messageText, @NotNull String from,
			@NotNull String to, @NotNull byte[] solicitudPdf, byte[] justificantePdf) throws Exception {
		// Por defecto LATIN1 porque es la codificación por defecto de lotus
		// notes
		sendWithSolicitudPdfAttached(subject, messageText, from, to, ContentType.TEXT_HTML, StandardCharsets.ISO_8859_1,
				solicitudPdf, justificantePdf);
	}

	/**
	 * Envía un email con la solicitud en pdf adjunta.
	 * 
	 * @param subject
	 * @param messageText
	 * @param from
	 * @param to
	 * @param contentType
	 * @param charset
	 * @param solicitudPdf
	 * @param justificantePdf
	 * @throws Exception
	 */
	public void sendWithSolicitudPdfAttached(@NotNull String subject, @NotNull String messageText, @NotNull String from,
			@NotNull String to, @NotNull ContentType contentType, @NotNull Charset charset, byte[] solicitudPdf, byte[] justificantePdf)
			throws Exception {

		log.debug("se va a enviar un email con el asunto '" + subject + "' y el texto '" + messageText + "'");

		// El email lo enviamos en LATIN1 porque es la codificación por defecto
		// de lotus notes
		ByteBuffer bfSubject = charset.encode(subject);
		ByteBuffer bfBody = charset.encode(messageText);
		String subjectLatin1 = new String(bfSubject.array(), StandardCharsets.ISO_8859_1);
		String bodyLatin1 = new String(bfBody.array(), StandardCharsets.ISO_8859_1);

		Message message = new MimeMessage(mySession);

		message.setFrom(new InternetAddress(from));
		Address toAddress = new InternetAddress(to);
		message.addRecipient(Message.RecipientType.TO, toAddress);
		message.setSubject(subjectLatin1);

		// creates body part for the message
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(bodyLatin1, contentType.getContentType());
		
		// create Multipart object and add MimeBodyPart objects to this object
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		if (solicitudPdf != null) {
			// creates body part for the attachment
			MimeBodyPart solicitudAttachPart = new MimeBodyPart();
			DataSource dataSrc = new ByteArrayDataSource(solicitudPdf, "application/pdf");
			solicitudAttachPart.setDataHandler(new DataHandler(dataSrc));
			solicitudAttachPart.setFileName(TipoDocumentoRegistrado.SOLICITUD.getNombreDocumentoPorDefecto() +".pdf");
			multipart.addBodyPart(solicitudAttachPart);
		}

		if (justificantePdf != null) {
			// creates body part for the attachment
			MimeBodyPart justificanteAttachPart = new MimeBodyPart();
			DataSource dataSrc = new ByteArrayDataSource(justificantePdf, "application/pdf");
			justificanteAttachPart.setDataHandler(new DataHandler(dataSrc));
			justificanteAttachPart.setFileName(TipoDocumentoRegistrado.JUSTIFICANTE.getNombreDocumentoPorDefecto() +".pdf");
			multipart.addBodyPart(justificanteAttachPart);
		}

		// sets the multipart as message's content
		message.setContent(multipart);

		Transport.send(message);

		log.debug("email enviado!");
	}

}
