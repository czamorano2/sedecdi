package es.imserso.sede.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import es.imserso.sede.model.JustificantePdf;
import es.imserso.sede.model.PlantillaEmail;
import es.imserso.sede.model.SolicitudPdf;
import es.imserso.sede.model.Tramite;

@Stateless
public class TramiteRepository {

	@Inject
	private EntityManager em;

	public Tramite findById(Long id) {
		return em.find(Tramite.class, id);
	}

	public boolean existsTramite(String codigoSIA) {
		return (em.createQuery("select t from Tramite t where t.codigoSIA = :codigoSIA", Tramite.class)
				.setParameter("codigoSIA", codigoSIA).getResultList().size() == 1);
	}

	public Tramite findBySIA(@NotNull String codigoSIA) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tramite> criteria = cb.createQuery(Tramite.class);
		Root<Tramite> tramite = criteria.from(Tramite.class);
		criteria.select(tramite).where(cb.equal(tramite.get("codigoSIA"), codigoSIA));
		return em.createQuery(criteria).getSingleResult();
	}

	public SolicitudPdf findSolicitudPdfBySIA(String codigoSIA) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tramite> criteria = cb.createQuery(Tramite.class);
		Root<Tramite> tramite = criteria.from(Tramite.class);
		criteria.select(tramite).where(cb.equal(tramite.get("codigoSIA"), codigoSIA));
		return em.createQuery(criteria).getSingleResult().getSolicitudPdf();
	}

	public JustificantePdf findJustificantePdfBySIA(String codigoSIA) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tramite> criteria = cb.createQuery(Tramite.class);
		Root<Tramite> tramite = criteria.from(Tramite.class);
		criteria.select(tramite).where(cb.equal(tramite.get("codigoSIA"), codigoSIA));
		return em.createQuery(criteria).getSingleResult().getJustificantePdf();
	}

	public PlantillaEmail findPlantillaEmailBySIA(String codigoSIA) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tramite> criteria = cb.createQuery(Tramite.class);
		Root<Tramite> tramite = criteria.from(Tramite.class);
		criteria.select(tramite).where(cb.equal(tramite.get("codigoSIA"), codigoSIA));
		return em.createQuery(criteria).getSingleResult().getPlantillaEmail();
	}

	public List<Tramite> findAllOrderedByCodigoSIA() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tramite> criteria = cb.createQuery(Tramite.class);
		Root<Tramite> tramite = criteria.from(Tramite.class);
		criteria.select(tramite).orderBy(cb.asc(tramite.get("codigoSIA")));
		return em.createQuery(criteria).getResultList();
	}
}
