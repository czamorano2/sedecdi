package es.imserso.sede.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import es.imserso.sede.data.dto.solicitud.SolicitudDTO;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.data.dto.util.IdentificadorRegistroWrapper;
import es.imserso.sede.util.DateUtil;

/**
 * Datos de la solicitud de un trámite
 *
 * @author 11825775
 *
 */
@Entity
@SequenceGenerator(name = "SEQ_SOLICITUD", initialValue = 1, allocationSize = 1, sequenceName = "SEC_SOLICITUD")
@Table(uniqueConstraints = { @UniqueConstraint(name = "IDX_NUMEROREGISTRO", columnNames = { "numeroRegistro" }) })
@XmlRootElement
public class Solicitud implements Serializable {

	private static final long serialVersionUID = 8825266721896659194L;

	public Solicitud() {
	}

	/**
	 * Clave primaria
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITUD")
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	protected Long id = null;

	@Transient
	private SolicitudDTO dto;

	// @OneToOne(orphanRemoval = true)
	// @MapsId
	// private SolicitudFormData dto;

	
	public SolicitudDTO getDto() {
		return this.dto;
	}

	public void setDto(SolicitudDTO dto) {
		this.dto = dto;
		this.setDtoXml(SolicitudDtoConverter.convertToXML(dto));
	}
	
	@Column(name = "dto", length = 10000)
	private String dtoXml;

	// @OneToOne(orphanRemoval = true)
	// @MapsId
	// private SolicitudFormData dto;

	public String getDtoXml() {
		return this.dtoXml;
	}

	public void setDtoXml(String dto) {
		this.dtoXml = dto;
		this.setDto(SolicitudDtoConverter.convertToDTO(dto));
	}

	/**
	 * Número de registro en iSicres (formado por oficina + ejercicio + numero).
	 * <p>
	 * Cuidado, no es el identificador interno del registro. Ese identificador es
	 * IdentificadorRegistro)
	 * <p>
	 * Cada ejercicio se crea un nuevo libro y se reinicia la numeración.
	 * <p>
	 * Ejemplo: <code>
	 * {"bookIdentification":83,"registerIdentification":16,"registerNumber":"201820000000017"}
	 * </code>
	 *
	 *
	 */
	protected String getNumeroRegistro() {
		return numeroRegistro;
	}

	protected void setNumeroRegistro(final String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	protected String numeroRegistro = null;

	/**
	 * POJO con los datos del identificador interno del registro en ISicres
	 * 
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@Transient
	public IdentificadorRegistroDTO getIdentificadorRegistro()
			throws JsonParseException, JsonMappingException, IOException {
		return new IdentificadorRegistroWrapper().build(getNumeroRegistro());
	}

	/**
	 * @throws JsonProcessingException
	 */
	public void setIdentificadorRegistroDTO(@NotNull IdentificadorRegistroDTO identificadorRegistro) {
		setNumeroRegistro(identificadorRegistro.getJSON());
	}

	/**
	 * fecha de alta de la solicitud en la base de datos, no en el registro
	 */
	@Temporal(value = TemporalType.TIMESTAMP)
	@NotNull(message = "la fecha de alta es obligatoria")
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(final Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	protected Date fechaAlta = null;

	/**
	 * nombre del solicitante
	 */
	@NotNull(message = "el nombre del solicitante es obligatorio")
	@Pattern(message = "no debe contener numeros", regexp = "[^0-9]*")
	@Size(min = 1, max = 50, message = "maximo 50 caracteres")
	@Column(length = 50)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(final String nombre) {
		this.nombre = StringUtils.upperCase(nombre);
	}

	protected String nombre = null;

	/**
	 * primer apellido del solicitante
	 */
	@NotNull(message = "el primer apellido del solicitante es obligatorio")
	@Pattern(message = "no debe contener números", regexp = "[^0-9]*")
	@Size(min = 1, max = 50, message = "maximo 50 caracteres")
	@Column(length = 50)
	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(final String apellido1) {
		this.apellido1 = StringUtils.upperCase(apellido1);
	}

	protected String apellido1 = null;

	/**
	 * segundo apellido del solicitante
	 */
	@Pattern(message = "no debe contener números", regexp = "[^0-9]*")
	@Size(min = 1, max = 50, message = "máximo 50 caracteres")
	@Column(length = 50)
	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(final String apellido2) {
		this.apellido2 = StringUtils.upperCase(apellido2);
	}

	protected String apellido2 = null;

	/**
	 * NIF/NIE del representante del solicitante
	 */
	@NotNull(message = "debe especificarse el documento de identificación del solicitante")
	@Size(min = 1, max = 15, message = "máximo 15 caracteres")
	@Column(length = 15)
	public String getDocumentoIdentificacion() {
		return documentoIdentificacion;
	}

	public void setDocumentoIdentificacion(final String documentoIdentificacion) {
		this.documentoIdentificacion = StringUtils.upperCase(documentoIdentificacion);
	}

	protected String documentoIdentificacion = null;

	/**
	 * trámite de la solicitud
	 */
	@ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	public Tramite getTramite() {
		return tramite;
	}

	public void setTramite(final Tramite tramite) {
		this.tramite = tramite;
	}

	protected Tramite tramite = null;

	/**
	 * Las observaciones internas serán exclusivamente para el uso del funcionario
	 * que gestione el estado de la solicitud, nunca serán visibles para el usuario
	 * final.
	 *
	 */
	public String getObservacionesInternas() {
		return observacionesInternas;
	}

	public void setObservacionesInternas(final String observacionesInternas) {
		this.observacionesInternas = observacionesInternas;
	}

	@Column(length = 5000, updatable = true)
	protected String observacionesInternas = null;

	/**
	 * histórico de los cambios de estado de la solicitud
	 *
	 *
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "solicitud", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<HistoricoEstadoSolicitud> getHistoricos() {
		if (this.historicos == null) {
			this.historicos = new ArrayList<HistoricoEstadoSolicitud>();
		}
		return historicos;
	}

	public void setHistoricos(final List<HistoricoEstadoSolicitud> historicos) {
		this.historicos = historicos;
	}

	public void addHistorico(HistoricoEstadoSolicitud historico) {
		if (historico == null) {
			return;
		}
		getHistoricos().add(historico);
		historico.setSolicitud(this);
	}

	public void removeHistorico(HistoricoEstadoSolicitud historico) {
		if (historico == null) {
			return;
		}
		getHistoricos().remove(historico);
		historico.setSolicitud(null);
	}

	public void removeAllHistoricos() {
		List<HistoricoEstadoSolicitud> remove = new ArrayList<HistoricoEstadoSolicitud>();
		remove.addAll(getHistoricos());
		for (HistoricoEstadoSolicitud element : remove) {
			removeHistorico(element);
		}
	}

	protected List<HistoricoEstadoSolicitud> historicos = null;

	/**
	 * The optimistic lock property.
	 */
	@Version
	public Long getOplock() {
		return oplock;
	}

	public void setOplock(final Long oplock) {
		this.oplock = oplock;
	}

	protected Long oplock = null;

	@NotNull(message = "el tipo de autenticación es obligatorio")
	public TipoAutenticacion getTipoAutenticacion() {
		return tipoAutenticacion;
	}

	public void setTipoAutenticacion(final TipoAutenticacion tipoAutenticacion) {
		this.tipoAutenticacion = tipoAutenticacion;
	}

	@Transient
	public boolean isSINAUTENTICACION() {
		return TipoAutenticacion.SIN_AUTENTICACION.equals(tipoAutenticacion);
	}

	@Transient
	public boolean isCLAVECERTIFICADO() {
		return TipoAutenticacion.CLAVE_CERTIFICADO.equals(tipoAutenticacion);
	}

	@Transient
	public boolean isCLAVEPIN24() {
		return TipoAutenticacion.CLAVE_PIN24.equals(tipoAutenticacion);
	}

	@Transient
	public boolean isCLAVEPERMANENTE() {
		return TipoAutenticacion.CLAVE_PERMANENTE.equals(tipoAutenticacion);
	}

	@Transient
	public boolean isCLAVECIUDADANOUE() {
		return TipoAutenticacion.CLAVE_CIUDADANO_UE.equals(tipoAutenticacion);
	}

	protected TipoAutenticacion tipoAutenticacion = null;

	@NotNull(message = "debe especificarse el estado")
	@Column(length = 50)
	public Estado getEstado() {
		return estado;
	}

	public void setEstado(final Estado estado) {
		this.estado = estado;
	}

	@Transient
	public boolean isPENDIENTE() {
		return Estado.PENDIENTE.equals(estado);
	}

	@Transient
	public boolean isAPROBADA() {
		return Estado.APROBADA.equals(estado);
	}

	@Transient
	public boolean isRECHAZADA() {
		return Estado.RECHAZADA.equals(estado);
	}

	@Transient
	public boolean isCANCELADA() {
		return Estado.CANCELADA.equals(estado);
	}

	@Transient
	public boolean isTRAMITANDOSE() {
		return Estado.TRAMITANDOSE.equals(estado);
	}

	@Transient
	public boolean isGESTIONADOPORAPLICACION() {
		return Estado.GESTIONADO_POR_APLICACION.equals(estado);
	}

	protected Estado estado = null;

	protected String estadoAsString;

	/**
	 * Devuelve el estado de la solicitud.
	 * <p>
	 * Si la solicitud es de turismo o termalismo se le asignará el valor que tenga
	 * en la aplicación gestora.
	 * <p>
	 * De lo contrario, se le asignará el valor de la descripción de la enumeración
	 * Estado.
	 * 
	 * @return
	 */
	@Transient
	public String getEstadoAsString() {
		String e = null;

		if (this.tramite != null && (this.tramite.isTurismo() || this.tramite.isTermalismo())
				&& this.estadoAsString != null) {
			e = this.estadoAsString;
		} else if (this.estado != null) {
			e = this.estado.getDescripcion();
		}

		return e;
	}

	public void setEstadoAsString(String estado) {
		this.estadoAsString = estado;
	}

	/**
	 * Cada solicitud tiene asignada una dirección de correo electrónico para
	 * posibles notificaciones
	 * <p>
	 * Puede ser del solicitante, del representante, del cónyuge, etc.
	 * 
	 * @return Dirección de correo electrónico asociado a la solicitud.
	 */
	@Email(message = "el formato del campo email no es correcto")
	@Column(length = 100)
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	protected String email = null;

	/**
	 * direccion IP del solicitante
	 */
	@Length(min = 0, max = 40, message = "La ip no debe exceder los 40 caracteres")
	@Column(length = 40)
	public String getIp() {
		return ip;
	}

	public void setIp(final String ip) {
		this.ip = ip;
	}

	protected String ip = null;

	/**
	 * Nombre del representante
	 *
	 */
	@Pattern(message = "no debe contener números", regexp = "[^0-9]*")
	@Size(min = 0, max = 50, message = "máximo 50 caracteres")
	@Column(length = 50)
	public String getNombreRepresentante() {
		return nombreRepresentante;
	}

	public void setNombreRepresentante(final String nombreRepresentante) {
		this.nombreRepresentante = StringUtils.upperCase(nombreRepresentante);
	}

	protected String nombreRepresentante = null;

	/**
	 * Primer apellido del representante
	 *
	 *
	 */
	@Pattern(message = "no debe contener números", regexp = "[^0-9]*")
	@Size(min = 0, max = 50, message = "máximo 50 caracteres")
	@Column(length = 50)
	public String getApellido1Representante() {
		return apellido1Representante;
	}

	public void setApellido1Representante(final String apellido1Representante) {
		this.apellido1Representante = StringUtils.upperCase(apellido1Representante);
	}

	protected String apellido1Representante = null;

	/**
	 * Segundo apellido del representante
	 *
	 *
	 */
	@Pattern(message = "no debe contener números", regexp = "[^0-9]*")
	@Size(min = 0, max = 50, message = "máximo 50 caracteres")
	@Column(length = 50)
	public String getApellido2Representante() {
		return apellido2Representante;
	}

	public void setApellido2Representante(final String apellido2Representante) {
		this.apellido2Representante = StringUtils.upperCase(apellido2Representante);
	}

	protected String apellido2Representante = null;

	@Transient
	public String getApellidos() {
		return Stream.of(this.apellido1, this.apellido2).filter(Objects::nonNull).collect(Collectors.joining(" "));
	}

	@Transient
	public String getNombreCompleto() {
		return Stream.of(this.nombre, getApellidos()).filter(Objects::nonNull).collect(Collectors.joining(" "));
	}

	/**
	 * NIF/NIE del representante
	 */
	@Size(min = 0, max = 15, message = "máximo 15 caracteres")
	@Column(length = 15)
	public String getDocumentoIdentificacionRepresentante() {
		return documentoIdentificacionRepresentante;
	}

	public void setDocumentoIdentificacionRepresentante(final String documentoIdentificacionRepresentante) {
		this.documentoIdentificacionRepresentante = StringUtils.upperCase(documentoIdentificacionRepresentante);
	}

	protected String documentoIdentificacionRepresentante = null;

	/**
	 * información de la solicitud almacenada en la aplicacion gestora del trámite
	 */
	@Column(length = 50)
	public String getExpedienteAplicacionGestora() {
		return expedienteAplicacionGestora;
	}

	public void setExpedienteAplicacionGestora(final String expedienteAplicacionGestora) {
		this.expedienteAplicacionGestora = expedienteAplicacionGestora;
	}

	protected String expedienteAplicacionGestora = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------

	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
		builder.append("id", getId());
		builder.append("numeroRegistro", getNumeroRegistro());
		builder.append("fechaAlta", getFechaAlta());
		builder.append("nombre", getNombre());
		builder.append("apellido1", getApellido1());
		builder.append("apellido2", getApellido2());
		builder.append("documentoIdentificacion", getDocumentoIdentificacion());
		builder.append("observacionesInternas", getObservacionesInternas());
		builder.append("oplock", getOplock());
		builder.append("tipoAutenticacion", getTipoAutenticacion());
		builder.append("estado", getEstado());
		builder.append("email", getEmail());
		builder.append("ip", getIp());
		builder.append("nombreRepresentante", getNombreRepresentante());
		builder.append("apellido1Representante", getApellido1Representante());
		builder.append("apellido2Representante", getApellido2Representante());
		builder.append("documentoIdentificacionRepresentante", getDocumentoIdentificacionRepresentante());
		builder.append("expedienteAplicacionGestora", getExpedienteAplicacionGestora());
		builder.append("tramite", getTramite());
		return builder.toString();
	}

	public Solicitud deepClone() throws Exception {
		Solicitud clone = (Solicitud) super.clone();
		clone.setId(null);

		clone.setHistoricos(null);
		for (HistoricoEstadoSolicitud kid : this.getHistoricos()) {
			clone.addHistorico(kid.deepClone());
		}
		return clone;
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Solicitud))
			return false;
		final Solicitud other = (Solicitud) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}

	/**
	 * NOT generated
	 * 
	 * @return hashtable con los campos necesarios para generar el justificante de
	 *         la solicitud
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public Hashtable<String, String> extractReceiptHash()  {
		Hashtable<String, String> hash = new Hashtable<String, String>();
		hash.put("nombreApellidos", this.getNombre() + " " + this.getApellido1() + " " + this.getApellido2());
		hash.put("nif", (this.getDocumentoIdentificacion() == null ? "" : this.getDocumentoIdentificacion()));
		hash.put("email", (this.getEmail() == null ? "" : this.getEmail()));
		hash.put("registro", (this.getNumeroRegistro() == null ? ""
				: new IdentificadorRegistroWrapper().build(this.getNumeroRegistro()).getRegisterNumber()));
		hash.put("fecha", (this.getFechaAlta() == null ? "" : DateUtil.parseDateToString((Date) this.getFechaAlta())));
		hash.put("hora", (this.getFechaAlta() == null ? "" : DateUtil.parseTime((Date) this.getFechaAlta())));
		hash.put("estado", (this.getEstado() == null ? "" : this.getEstado().toString()));

		return hash;

	}

}
