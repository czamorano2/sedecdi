package es.imserso.sede.service.registration.registry.ISicres.books;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.xml.namespace.QName;

import org.jboss.logging.Logger;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.service.registration.registry.ISicres.books.bean.Book;
import es.imserso.sede.service.registration.registry.ISicres.books.client.ISWebServiceBooksSoap;
import es.imserso.sede.service.registration.registry.ISicres.books.client.ISWebServiceBooksSoapImplService;
import es.imserso.sede.service.registration.registry.ISicres.books.client.Security;
import es.imserso.sede.service.registration.registry.ISicres.books.client.UsernameTokenClass;
import es.imserso.sede.service.registration.registry.ISicres.books.client.WSBook;
import es.imserso.sede.service.registration.registry.ISicres.books.client.WSGetInputBooks;
import es.imserso.sede.service.registration.registry.ISicres.books.client.WSGetInputBooksResponse;
import es.imserso.sede.service.registration.registry.bean.BookI;
import es.imserso.sede.util.Utils;

/**
 * Implementación para InveSicres.
 * 
 * @author 11825775
 *
 */
public class BookRegistryServiceImpl implements BookRegistryServiceI {

	private QName ISWEBSERVICEBOOKSSOAPIMPLSERVICE_QNAME;
	private URL ISWEBSERVICEBOOKSSOAPIMPLSERVICE_WSDL_LOCATION;

	@Inject
	private Logger log;

	@Inject
	private PropertyComponent propertyComponent;

	@Override
	public List<BookI> getInputBooks() throws RegistrationException {
		List<BookI> bookList = null;

		log.debug("se va a obtener la información de los libros de entrada del registro ...");

		ISWebServiceBooksSoap pService = getBooksService();

		WSGetInputBooks wsGetInputBooks = new WSGetInputBooks();

		UsernameTokenClass usernameToken = new UsernameTokenClass();
		usernameToken.setUsername(propertyComponent.getiSicresUsername());
		usernameToken.setPassword(propertyComponent.getiSicresPassword());
		Security security = new Security();
		security.setUsernameToken(usernameToken);

		WSGetInputBooksResponse wsGetInputBooksResponse = null;
		try {
			wsGetInputBooksResponse = pService.wsGetInputBooks(wsGetInputBooks, security);
			log.debug("se ha obtenido la información de los libros  del registro");

			bookList = new ArrayList<BookI>();
			for (WSBook book : wsGetInputBooksResponse.getWSGetInputBooksResult().getWSBook()) {
				Book b = new Book();
				b.setId(book.getId());
				b.setName(book.getName());
				b.setType(book.getType());
				bookList.add(b);
			}

		} catch (Exception e) {
			String errmsg = "error al obtener los libros de entrada del registro" + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new RegistrationException(errmsg);

		}

		return bookList;
	}

	private ISWebServiceBooksSoap getBooksService() throws RegistrationException {
		ISWebServiceBooksSoapImplService is = new ISWebServiceBooksSoapImplService(
				getISWebServiceBooksSoapWsdlLocation(), getISWebServiceBooksSoapQName());
		return is.getISWebServiceBooksSoapImplPort();
	}

	private QName getISWebServiceBooksSoapQName() {
		if (ISWEBSERVICEBOOKSSOAPIMPLSERVICE_QNAME == null) {
			ISWEBSERVICEBOOKSSOAPIMPLSERVICE_QNAME = new QName(propertyComponent.getiSicresBooksNamespaceURI(),
					propertyComponent.getiSicresBooksQNameLocalPart());
		}
		return ISWEBSERVICEBOOKSSOAPIMPLSERVICE_QNAME;
	}

	private URL getISWebServiceBooksSoapWsdlLocation() throws RegistrationException {
		if (ISWEBSERVICEBOOKSSOAPIMPLSERVICE_WSDL_LOCATION == null) {
			try {
				ISWEBSERVICEBOOKSSOAPIMPLSERVICE_WSDL_LOCATION = new URL(propertyComponent.getiSicresBooksWsdlUrl());
			} catch (MalformedURLException ex) {
				throw new RegistrationException("No se pudo obtener el fichero WSDL:" + Utils.getExceptionMessage(ex));
			}
		}
		return ISWEBSERVICEBOOKSSOAPIMPLSERVICE_WSDL_LOCATION;
	}

}
