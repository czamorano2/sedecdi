
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSLoadInputRegistersResult" type="{http://www.invesicres.org}WSInputRegistersResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsLoadInputRegistersResult"
})
@XmlRootElement(name = "WSLoadInputRegistersResponse")
public class WSLoadInputRegistersResponse {

    @XmlElement(name = "WSLoadInputRegistersResult")
    protected WSInputRegistersResponse wsLoadInputRegistersResult;

    /**
     * Obtiene el valor de la propiedad wsLoadInputRegistersResult.
     * 
     * @return
     *     possible object is
     *     {@link WSInputRegistersResponse }
     *     
     */
    public WSInputRegistersResponse getWSLoadInputRegistersResult() {
        return wsLoadInputRegistersResult;
    }

    /**
     * Define el valor de la propiedad wsLoadInputRegistersResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSInputRegistersResponse }
     *     
     */
    public void setWSLoadInputRegistersResult(WSInputRegistersResponse value) {
        this.wsLoadInputRegistersResult = value;
    }

}
