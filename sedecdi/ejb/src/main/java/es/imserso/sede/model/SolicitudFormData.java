package es.imserso.sede.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "SOLICITUD_DTO")
@XmlRootElement
public class SolicitudFormData {

	@Id
	private long id;

	/**
	 * Datos del DTO de la solicitud
	 */
	private String data;

	@Column(length = 10000)
	@NotNull
	@Size(max = 10000, message = "los datos del POJO no pueden superar los 10000 caracteres")
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
