package es.imserso.sede.util.persistence.service;

import es.imserso.sede.util.exception.SedeException;

/**
 * Excepción lanzada durante la ejecución de un proceso.
 * 
 * @author 11825775
 *
 */
public class ProcessException extends SedeException {

	private static final long serialVersionUID = 2711372153791960781L;

	protected static String DEFAULT_MESSAGE;

	public ProcessException() {
		super("Ocurrió una excepción inesperada!");
	}

	public ProcessException(String message) {
		super(message);
	}

	public ProcessException(Exception e) {
		super(e);
	}

}
