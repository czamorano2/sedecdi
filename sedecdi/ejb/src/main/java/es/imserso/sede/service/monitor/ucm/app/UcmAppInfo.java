package es.imserso.sede.service.monitor.ucm.app;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.service.monitor.AbstractAppInfo;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.service.monitor.turismo.service.ApplicationAccesibleInfo;
import es.imserso.sede.service.monitor.ucm.service.TramiteServicesEnabledInfo;
import es.imserso.sede.service.ucm.UCMService;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.resources.qualifier.UCMDataSourceQ;

/**
 * Información sobre los servicios web del UCM
 * 
 * @author 11825775
 *
 */
@UCMDataSourceQ
@Dependent
public class UcmAppInfo extends AbstractAppInfo {

	private static final long serialVersionUID = -5393826878432414095L;

	private static final Logger log = Logger.getLogger(UcmAppInfo.class.getName());

	@Inject
	UCMService ucmService;

	@Inject
	ApplicationAccesibleInfo applicationAccesibleInfo;

	@Inject
	TramiteServicesEnabledInfo tramiteServicesEnabledInfo;

	@PostConstruct
	public void onCreate() {
		services = new ArrayList<ServiceInfo>();
		services.add(tramiteServicesEnabledInfo);
	}

	@Override
	public String getWebSiteURL() {
		// FIXME sustituir URL
		return "http://lbaplic/???";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getName()
	 */
	@Override
	public String getName() {
		return "UCM";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Gestor de contenios de la Sede Electrónica";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#webServicesActive()
	 */
	@Override
	public Boolean webServicesActive() {
		try {
			return ucmService.isUcmAlive() && !anyCriticalServiceDisabled();

		} catch (Exception e) {
			String errmsg = "Error al comprobar si los servicios web del UCM están activos: "
					+ Utils.getExceptionMessage(e);
			log.error(errmsg);
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getServices()
	 */
	@Override
	public List<ServiceInfo> getServices() {
		return services;
	}

}
