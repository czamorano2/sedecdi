package es.imserso.sede.data.dto.datosEconomicos;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

/**
 * Datos de un dato económico de una pensión
 * 
 * @author 11825775
 *
 */
public interface DatoEconomicoDTOI {

	/**
	 * @return Importe de la pensión (en euros)
	 */
	BigDecimal getImporte();

	void setImporte(@NotNull BigDecimal importe);

	/**
	 * @return si es válido o no el componente
	 */
	boolean isValid();

	/**
	 * @return mensaje de error de validación
	 */
	String getValidationFailedMessage();

}
