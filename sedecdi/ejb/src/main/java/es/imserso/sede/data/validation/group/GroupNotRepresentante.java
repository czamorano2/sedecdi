package es.imserso.sede.data.validation.group;

/**
 * Grupo de validaciones que deben cumplirse solamente si no hay representante.
 * 
 * @author 11825775
 *
 */
public interface GroupNotRepresentante {

}
