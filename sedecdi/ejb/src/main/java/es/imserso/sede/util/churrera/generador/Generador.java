package es.imserso.sede.util.churrera.generador;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

/**
 * Utilidad para generar el código de la vista del formulario de un trámite de la Sede Electrónica 
 * 
 * @author 02858341
 *
 */
public class Generador {
	//Cambiar el nombre del fichero de entrada a partir del que se generará el formulario
	private static final String FILENAME_XML_ENTRADA = "turismo.xml";
	
	private static final String DIRECTORY_FOR_TEMPLATE_LOADING = "E:\\code\\proyectos\\configuracion\\sedecdi\\sysin\\churrera\\templates";
	private static final String DIRECTORY_FOR_XML_ENTRADA_LOADING = "E:\\code\\proyectos\\configuracion\\sedecdi\\sysin\\churrera\\xml_entrada";
	private static final String DIRECTORY_FOR_RESULT = "E:\\code\\proyectos\\configuracion\\sedecdi\\sysout\\churrera";

	private Configuration freeMarkerConfiguration;

	// Lo usaremos para escribir el fichero de salida
	BufferedWriter bw;
	
	String formName;
	String ngmodel;
	String columnSize;
	

	public static void main(String argv[]) {

		try {
			Generador generator = new Generador();
			generator.doIt();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doIt() {
		try {
			String rutaYNombreFicheroSalida=DIRECTORY_FOR_RESULT + "\\" + FILENAME_XML_ENTRADA + "_resultado";
			bw = new BufferedWriter(new FileWriter(rutaYNombreFicheroSalida));

			// Inicializamos FreeMarker
			initFreeMarkerConfiguration();

			//Cargamos el fichero xml de entrada
			File fXmlFile = new File(DIRECTORY_FOR_XML_ENTRADA_LOADING + "\\" + FILENAME_XML_ENTRADA);
//					"E:/code/workspaces/jbds10/wks1/sedecdi/sedecdi/ejb/src/main/java/es/imserso/sede/util/churrera/generador/ejemplo1.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			printAttributes(doc.getDocumentElement());
			
			if (!doc.getDocumentElement().getNodeName().equals("form")){
				throw new Exception("Se esperaba un nodo tipo form y se ha recibido:" + doc.getDocumentElement().getNodeName());
			}
			//Generamos el header y el inicio del formulario
			formName=doc.getDocumentElement().getAttribute("name"); //guardamos el nombre del formulario para usarlo posteriormente
			ngmodel=doc.getDocumentElement().getAttribute("ngmodel"); //guardamos el ngmodel para usarlo posteriormente
			generateElementCode(doc.getDocumentElement(), "HeaderFormBegin.ftlh");

			NodeList nList = doc.getElementsByTagName("fieldset");

			//recorremos los fieldset
			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (!nNode.getNodeName().equals("fieldset")){
					throw new Exception("Se esperaba un nodo tipo fieldset y se ha recibido:"+nNode.getNodeName());
				}

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element fieldsetElement = (Element) nNode;

					printAttributes(fieldsetElement);
					//Generamos el inicio de un fieldset
					generateElementCode(fieldsetElement, "FieldsetBegin.ftlh");					

					NodeList rowList = fieldsetElement.getElementsByTagName("row");
					
					//recorremos las filas
					for (int row = 0; row < rowList.getLength(); row++) {

						Node rowNode = rowList.item(row);

						System.out.println("\nCurrent Element :" + rowNode.getNodeName());		
						if (!rowNode.getNodeName().equals("row")){
							throw new Exception("Se esperaba un nodo tipo row y se ha recibido:"+rowNode.getNodeName());
						}						

						if (rowNode.getNodeType() == Node.ELEMENT_NODE) {

							Element rowElement = (Element) rowNode;
							
							printAttributes(rowElement);
							//Generamos el inicio de una row
							generateElementCode(rowElement, "RowBegin.ftlh");
							setColumnSize(rowElement); //Establecemos el tamaño de las columnas de la fila

							NodeList fieldList = rowElement.getElementsByTagName("field");
							
							//recorremos los campos
							for (int field = 0; field < fieldList.getLength(); field++) {

								Node fieldNode = fieldList.item(field);

								System.out.println("\nCurrent Element :" + fieldNode.getNodeName());
								if (!fieldNode.getNodeName().equals("field")){
									throw new Exception("Se esperaba un nodo tipo field y se ha recibido:"+fieldNode.getNodeName());
								}								

								if (fieldNode.getNodeType() == Node.ELEMENT_NODE) {

									Element fieldElement = (Element) fieldNode;
																	
									printAttributes(fieldElement);
									//Generamos un campo
									generateFieldCodeBegin(fieldElement);
									generateFieldShowErrorBegin(fieldElement);
//									generateFieldRequiredError(fieldElement);
//									generateElementCode(fieldElement, "FieldMaxLengthError.ftlh");
									generateFieldShowErrorEnd(fieldElement);
									generateFieldCodeEnd(fieldElement);
								}
							}
							//Generamos el final de una row
							generateElementCode(rowElement, "RowEnd.ftlh");	
						}
					}
					//Generamos el final de un fieldset
					generateElementCode(doc.getDocumentElement(), "FieldsetEnd.ftlh");

				}
			}
			generateElementCode(doc.getDocumentElement(), "HeaderFormEnd.ftlh");
			bw.close();
			System.out.println("**********************************************************************************");
			System.out.println("Se ha generado el fichero:" + rutaYNombreFicheroSalida);
			System.out.println("**********************************************************************************");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void printAttributes(Element element) {
		System.out.println();
		System.out.println("NodeName=" + element.getNodeName());
		System.out.println("name : " + element.getAttribute("name"));
		System.out.println("label : " + element.getAttribute("label"));
		System.out.println("type : " + element.getAttribute("type"));
		System.out.println("required : " + element.getAttribute("required"));
		System.out.println("maxlength : " + element.getAttribute("maxlength"));
		System.out.println("options : " + element.getAttribute("options"));
		System.out.println("value : " + element.getAttribute("value"));
		System.out.println("columns : " + element.getAttribute("columns"));
		System.out.println("legend : " + element.getAttribute("legend"));
		System.out.println("formName :" + element.getAttribute("name"));
		System.out.println("formHeader :" + element.getAttribute("header"));
		System.out.println("formNgModel :" + element.getAttribute("ngmodel"));		
		System.out.println("----------------------------");	
	}

	/**
	 * Guarda la etiqueta para el tamaño de las columnas (el tamaño por el número de columnas siempre debe ser 12)
	 * @param rowElement
	 */
	private void setColumnSize(Element rowElement) {		 
		switch(rowElement.getAttribute("columns")){
		case "1":
			columnSize="col-md-12";
			break;	
		case "2":
			columnSize="col-md-6";
			break;
		case "3":
			columnSize="col-md-4";
			break;
		case "4":
			columnSize="col-md-3";
			break;
		case "6":
			columnSize="col-md-2";
			break;			
		default:
			columnSize="col-md-1"; //Si no es múltiplo de 12 asignamos un uno de modo que cabrán 12 columnas			
		}			
	}

	/**
	 * Graba en el fichero el código html correspondiente al elemento recibido según la plantilla recibida
	 * @param element
	 * @param template
	 */
	private void generateElementCode(Element element, String template) {
		Writer out = new StringWriter();
		try {
			/* Get the template */
			Template temp = freeMarkerConfiguration.getTemplate(template);

			/* Merge data-model with template */
			temp.process(prepareMap(element), out);
			
			//Escribimos en el fichero de salida
			bw.write(out.toString());
			bw.newLine();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Se ha producido un error al procesar el template");
		}

	}

	/**
	 * Genera el inicio del campo según sea su tipo
	 * @param element
	 */
	private void generateFieldCodeBegin(Element fieldElement){
		String templateFileName = "";
		switch(fieldElement.getAttribute("type")){
		case "text":
			templateFileName="FieldTextBegin.ftlh";
			break;
		case "date":
			templateFileName="FieldDateBegin.ftlh";
			break;
		case "select":
			templateFileName="FieldSelectBegin.ftlh";
			break;
		case "checkbox":
			templateFileName="FieldCheckboxBegin.ftlh";
			break;
		default:
			throw new RuntimeException("Se ha encontrado un type no reconocido:" + fieldElement.getAttribute("type"));
		}
		generateElementCode(fieldElement, templateFileName);
	}

	
	/**
	 * Genera el inicio de show de error según sea el tipo del campo
	 * @param element
	 */
	private void generateFieldShowErrorBegin(Element fieldElement){
		String templateFileName = "";
		switch(fieldElement.getAttribute("type")){
		case "date":
			templateFileName="FieldShowErrorDateBegin.ftlh";
			break;
		default:
			templateFileName="FieldShowErrorGeneralBegin.ftlh";
			break;
		}
		generateElementCode(fieldElement, templateFileName);
	}
	
	/**
	 * Genera el error required según sea el tipo del campo
	 * @param element
	 */
	@SuppressWarnings("unused")
	private void generateFieldRequiredError(Element fieldElement){
		String templateFileName = "";
		switch(fieldElement.getAttribute("type")){
		case "date":
			templateFileName="FieldRequiredErrorDate.ftlh";
			break;
		default:
			templateFileName="FieldRequiredErrorGeneral.ftlh";
			break;
		}
		generateElementCode(fieldElement, templateFileName);
	}
	
	/**
	 * Genera el final del show error según sea el tipo del campo
	 * @param element
	 */
	private void generateFieldShowErrorEnd(Element fieldElement){
		String templateFileName = "";
		switch(fieldElement.getAttribute("type")){
		case "date":
			templateFileName="FieldShowErrorDateEnd.ftlh";
			break;
		default:
			templateFileName="FieldShowErrorGeneralEnd.ftlh";
			break;
		}
		generateElementCode(fieldElement, templateFileName);
	}		

	/**
	 * Genera el final del field error según sea el tipo del campo
	 * @param element
	 */
	private void generateFieldCodeEnd(Element fieldElement){
		String templateFileName = "";
		switch(fieldElement.getAttribute("type")){
		case "date":
			templateFileName="FieldDateEnd.ftlh";
			break;
		case "select":
			templateFileName="FieldSelectEnd.ftlh";
			break;
		default:
			templateFileName="FieldGeneralEnd.ftlh";
			break;
		}
		generateElementCode(fieldElement, templateFileName);
	}	
	
	/**
	 * Inicializa la configuración de FreeMarker
	 */
	private void initFreeMarkerConfiguration() {
		freeMarkerConfiguration = new Configuration(Configuration.VERSION_2_3_24);
		try {
			freeMarkerConfiguration.setDirectoryForTemplateLoading(new File(DIRECTORY_FOR_TEMPLATE_LOADING));
			freeMarkerConfiguration.setDefaultEncoding("UTF-8");
			freeMarkerConfiguration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			freeMarkerConfiguration.setLogTemplateExceptions(false);
		} catch (IOException e) {
			throw new RuntimeException("No ha sido posible inicializar FreeMarker: " + e.toString());
		}

	}

	private Map<String, String> prepareMap(Element element){
		Map<String, String> root = new HashMap<String, String>();
		root.put("formName", formName);
		root.put("ngmodel", ngmodel);		
		root.put("columnSize", columnSize);
		root.put("header", element.getAttribute("header"));
		root.put("name", element.getAttribute("name"));
		root.put("legend", element.getAttribute("legend"));
		root.put("columns", element.getAttribute("columns"));
		root.put("label", element.getAttribute("label"));
		root.put("type", element.getAttribute("type"));
		if (element.getAttribute("required").equals("1")){
			root.put("required", "required");
		}else{
			root.put("required", "required");
		}
		root.put("maxlength", element.getAttribute("maxlength"));
		root.put("options", element.getAttribute("options"));
		return root;
	}

}
