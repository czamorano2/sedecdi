package es.imserso.sede.data.dto.persona;

import es.imserso.sede.data.dto.qualifier.RepresentanteQ;

@RepresentanteQ
public interface RepresentanteDTOI extends PersonaDTOI {

}
