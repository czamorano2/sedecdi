package es.imserso.sede.service.registration.registry.ISicres.books;

import java.util.List;

import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.service.registration.registry.bean.BookI;

/**
 * Interfaz que ofrece las funcionalidades del registro presencial.
 * 
 * @author 11825775
 *
 */
/**
 * @author 11825775
 *
 */
public interface BookRegistryServiceI {

	/**
	 * Obtiene todos los libros de entrada
	 * 
	 * @return
	 * @throws RegistrationException
	 */
	List<BookI> getInputBooks() throws RegistrationException;

}
