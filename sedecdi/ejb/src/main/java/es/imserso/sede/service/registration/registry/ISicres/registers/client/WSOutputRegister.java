
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para WSOutputRegister complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="WSOutputRegister">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.invesicres.org}WSRegister">
 *       &lt;sequence>
 *         &lt;element name="Sender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Destination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DestinationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MatterType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MatterTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Matter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Persons" type="{http://www.invesicres.org}ArrayOfWSPerson" minOccurs="0"/>
 *         &lt;element name="Documents" type="{http://www.invesicres.org}ArrayOfWSDocument" minOccurs="0"/>
 *         &lt;element name="AddFields" type="{http://www.invesicres.org}ArrayOfWSAddField" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSOutputRegister", propOrder = {
    "sender",
    "senderName",
    "destination",
    "destinationName",
    "transportType",
    "transportNumber",
    "matterType",
    "matterTypeName",
    "matter",
    "persons",
    "documents",
    "addFields"
})
@XmlSeeAlso({
    WSInputRegister.class
})
public class WSOutputRegister
    extends WSRegister
{

    @XmlElement(name = "Sender")
    protected String sender;
    @XmlElement(name = "SenderName")
    protected String senderName;
    @XmlElement(name = "Destination")
    protected String destination;
    @XmlElement(name = "DestinationName")
    protected String destinationName;
    @XmlElement(name = "TransportType")
    protected String transportType;
    @XmlElement(name = "TransportNumber")
    protected String transportNumber;
    @XmlElement(name = "MatterType")
    protected String matterType;
    @XmlElement(name = "MatterTypeName")
    protected String matterTypeName;
    @XmlElement(name = "Matter")
    protected String matter;
    @XmlElement(name = "Persons")
    protected ArrayOfWSPerson persons;
    @XmlElement(name = "Documents")
    protected ArrayOfWSDocument documents;
    @XmlElement(name = "AddFields")
    protected ArrayOfWSAddField addFields;

    /**
     * Obtiene el valor de la propiedad sender.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSender() {
        return sender;
    }

    /**
     * Define el valor de la propiedad sender.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSender(String value) {
        this.sender = value;
    }

    /**
     * Obtiene el valor de la propiedad senderName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * Define el valor de la propiedad senderName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderName(String value) {
        this.senderName = value;
    }

    /**
     * Obtiene el valor de la propiedad destination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Define el valor de la propiedad destination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationName() {
        return destinationName;
    }

    /**
     * Define el valor de la propiedad destinationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationName(String value) {
        this.destinationName = value;
    }

    /**
     * Obtiene el valor de la propiedad transportType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportType() {
        return transportType;
    }

    /**
     * Define el valor de la propiedad transportType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportType(String value) {
        this.transportType = value;
    }

    /**
     * Obtiene el valor de la propiedad transportNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportNumber() {
        return transportNumber;
    }

    /**
     * Define el valor de la propiedad transportNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportNumber(String value) {
        this.transportNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad matterType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatterType() {
        return matterType;
    }

    /**
     * Define el valor de la propiedad matterType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatterType(String value) {
        this.matterType = value;
    }

    /**
     * Obtiene el valor de la propiedad matterTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatterTypeName() {
        return matterTypeName;
    }

    /**
     * Define el valor de la propiedad matterTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatterTypeName(String value) {
        this.matterTypeName = value;
    }

    /**
     * Obtiene el valor de la propiedad matter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatter() {
        return matter;
    }

    /**
     * Define el valor de la propiedad matter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatter(String value) {
        this.matter = value;
    }

    /**
     * Obtiene el valor de la propiedad persons.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSPerson }
     *     
     */
    public ArrayOfWSPerson getPersons() {
        return persons;
    }

    /**
     * Define el valor de la propiedad persons.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSPerson }
     *     
     */
    public void setPersons(ArrayOfWSPerson value) {
        this.persons = value;
    }

    /**
     * Obtiene el valor de la propiedad documents.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSDocument }
     *     
     */
    public ArrayOfWSDocument getDocuments() {
        return documents;
    }

    /**
     * Define el valor de la propiedad documents.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSDocument }
     *     
     */
    public void setDocuments(ArrayOfWSDocument value) {
        this.documents = value;
    }

    /**
     * Obtiene el valor de la propiedad addFields.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSAddField }
     *     
     */
    public ArrayOfWSAddField getAddFields() {
        return addFields;
    }

    /**
     * Define el valor de la propiedad addFields.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSAddField }
     *     
     */
    public void setAddFields(ArrayOfWSAddField value) {
        this.addFields = value;
    }

}
