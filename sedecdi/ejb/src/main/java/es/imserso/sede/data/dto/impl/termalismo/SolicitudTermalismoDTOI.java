package es.imserso.sede.data.dto.impl.termalismo;

import com.fasterxml.jackson.core.JsonProcessingException;

import es.imserso.sede.data.dto.datosEconomicos.DatosEconomicosDTOI;
import es.imserso.sede.data.dto.familiaNumerosa.FamiliaNumerosaDTOI;
import es.imserso.sede.data.dto.impl.appgestora.SolicitudAppGestoraDTOI;
import es.imserso.sede.data.dto.vinculada.SolicitudVinculadaDTOI;


/**
 * @author 11825775
 *
 */
public interface SolicitudTermalismoDTOI extends SolicitudAppGestoraDTOI {

	Long getId();

	void setId(Long id);

	SolicitanteTermalismoDTOI getSolicitante();

	void setSolicitante(SolicitanteTermalismoDTOI solicitante);

	
	DireccionNotificacionTermalismoDTOI getDireccionNotificacion();

	void setDireccionNotificacion(DireccionNotificacionTermalismoDTOI direccionNotificacion);

	SolicitudVinculadaDTOI getSolicitudVinculada();

	void setSolicitudVinculada(SolicitudVinculadaDTOI solicitudVinculada);

	FamiliaNumerosaDTOI getFamiliaNumerosa();

	void setFamiliaNumerosa(FamiliaNumerosaDTOI familiaNumerosa);

	DatosEconomicosDTOI getDatosEconomicos();

	void setDatosEconomicos(DatosEconomicosDTOI datosEconomicos);

	int datosEconomicosCount();

	Boolean getDenegarAutorizacionConsultas();

	void setDenegarAutorizacionConsultas(Boolean denegarAutorizacionConsultas);

	String getJSON() throws JsonProcessingException;

	boolean hasConyuge();

	boolean hasVinculacion();

	boolean hasFamiliaNumerosa();

}
