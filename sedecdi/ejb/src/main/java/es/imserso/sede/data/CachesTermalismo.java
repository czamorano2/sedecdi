package es.imserso.sede.data;

/**
 * Nombres de cachés de datos auxiliares de Hermes para las vistas de turismo.
 * 
 * @author 11825775
 *
 */
public enum CachesTermalismo {
	estadosCiviles, sexos, provincias, tiposTurno, comunidadesAutonomas, tiposPension, tiposFamiliaNum

}
