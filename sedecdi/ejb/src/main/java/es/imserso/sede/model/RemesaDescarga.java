package es.imserso.sede.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import es.imserso.sede.util.exception.SedeException;

/**
 * Entidad que contiene las descargas que se han hecho de las solicitudes de un trámite
 *
 * @author 11825775
 */
@Entity
@SequenceGenerator(name = "SEQ_REMESA_DESCARGA", initialValue = 1, allocationSize = 1, sequenceName = "SEC_REMESA_DESCARGA")
public class RemesaDescarga implements Serializable, Cloneable {

	private static final long serialVersionUID = -7080064389440986318L;

	public RemesaDescarga() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REMESA_DESCARGA")
	public Long getId() {
		return id;
	}


	public void setId(final Long id) {
		this.id = id;
	}


	private Long id = null;

	/**
	 * fecha a partir de la cual se seleccionaron las solicitudes de la remesa
	 */
	@Temporal(value = TemporalType.DATE)
	@Past
	public Date getFechaDesde() {
		return fechaDesde;
	}


	public void setFechaDesde(final Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}


	private Date fechaDesde = null;

	/**
	 * fecha hasta la cual se seleccionaron las solicitudes de la remesa
	 */
	@Temporal(value = TemporalType.DATE)
	@NotNull
	@Past
	public Date getFechaHasta() {
		return fechaHasta;
	}


	public void setFechaHasta(final Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}


	private Date fechaHasta = null;

	/**
	 * fecha en que se realizó la descarga de las solicitudes del trámite
	 */
	@Temporal(value = TemporalType.TIMESTAMP)
	@NotNull
	@Column(nullable = false, updatable = false)
	public Date getFechaDescarga() {
		return fechaDescarga;
	}


	public void setFechaDescarga(final Date fechaDescarga) {
		this.fechaDescarga = fechaDescarga;
	}


	private Date fechaDescarga = null;

	/**
	 * número de solicitudes de la remesa
	 */
	@NotNull
	public String getNumeroSolicitudes() {
		return numeroSolicitudes;
	}


	public void setNumeroSolicitudes(final String numeroSolicitudes) {
		this.numeroSolicitudes = numeroSolicitudes;
	}


	private String numeroSolicitudes = null;

	/**
	 * trámite al cual pertenecen las solicitudes de la remesa
	 */
	@ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	public Tramite getTramite() {
		return tramite;
	}


	public void setTramite(final Tramite tramite) {
		this.tramite = tramite;
	}


	private Tramite tramite = null;

	/** 
	 * valida las fechas desde y hasta antes de insertar o modificar en base de datos 
	 * 
	 * @throws Exception 
	 **/
	@PrePersist
	@PreUpdate
	public void validate() throws Exception {
		if (getFechaDesde() != null && getFechaHasta() != null)
			if (getFechaDesde().after(getFechaHasta())) {
				throw new SedeException("RemesaDescarga: La fecha desde no puede ser posterior a la fecha Hasta");
			}
	}

	// ------------------------------------------
	// Utils
	// ------------------------------------------


	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
		builder.append("id", getId());
		builder.append("fechaDesde", getFechaDesde());
		builder.append("fechaHasta", getFechaHasta());
		builder.append("fechaDescarga", getFechaDescarga());
		builder.append("numeroSolicitudes", getNumeroSolicitudes());
		builder.append("tramite", getTramite());
		return builder.toString();
	}


	public RemesaDescarga deepClone() throws Exception {
		RemesaDescarga clone = (RemesaDescarga) super.clone();
		clone.setId(null);
		return clone;
	}


	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof RemesaDescarga))
			return false;
		final RemesaDescarga other = (RemesaDescarga) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}
}
