package es.imserso.sede.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.validation.constraints.NotNull;

public class DateUtil {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat sdfNoSeparator = new SimpleDateFormat("ddMMyyyy");

	private static SimpleDateFormat sdfFechaHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	static SimpleDateFormat sdfGuiones = new SimpleDateFormat("yyyy-MM-dd");

	private static SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");

	// private static SimpleDateFormat sdfFechaHora24 = new
	// SimpleDateFormat("dd/mm/yyyy HH:mm:ss");

	/**
	 * @param ddMMyyyy Fecha en formato dd/MM/yyyy
	 * @return
	 * @throws ParseException
	 */
	public static Date parseDate(String ddMMyyyy) throws ParseException {
		sdf.setLenient(false);
		return sdf.parse(ddMMyyyy);
	}
	
	/**
	 * 
	 * @param yyyyMMdd Fecha en formato yyyy-MM-dd
	 * @return
	 * @throws ParseException
	 */
	public static Date parseDateGuiones(String yyyyMMdd) throws ParseException {
		sdfGuiones.setLenient(false);
		return sdfGuiones.parse(yyyyMMdd);
	}	

	/**
	 * @param ddMMyyyy Fecha en formato dd/MM/yyyy HH:mm:ss
	 * @return
	 * @throws ParseException
	 */	
	public static Date parseDateHour(String ddMMyyyyHH24MMss) throws ParseException {
		sdfFechaHora.setLenient(false);
		return sdfFechaHora.parse(ddMMyyyyHH24MMss);
	}

	/**
	 * @param ddMMyyyy Fecha en formato HH:mm:ss
	 * @return
	 * @throws ParseException
	 */
	public static Date parseHour(String HH24MMss) throws ParseException {
		sdfHora.setLenient(false);
		return sdfHora.parse(HH24MMss);
	}

	/**
	 * @param fecha
	 * @return formato dd/MM/yyyy
	 */
	public static String parseDateToString(Date fecha) {
		return fecha == null ? "" : sdf.format(fecha);
	}

	/**
	 * @param fecha
	 * @return formato ddMMyyyy
	 */
	public static String parseDateToStringNoSeparator(Date fecha) {
		return fecha == null ? "" : sdfNoSeparator.format(fecha);
	}

	/**
	 * @param fecha
	 * @return formato dd/MM/yyyy HH:mm:ss
	 */
	public static String parseDateAndTime(Date fecha) {
		return fecha == null ? "" : sdfFechaHora.format(fecha);
	}

	/**
	 * @param fecha
	 * @return formato HH:mm:ss
	 */
	public static String parseTime(Date fecha) {
		return fecha == null ? "" : sdfHora.format(fecha);
	}

	/**
	 * @param fecha
	 * @return formato dd/MM/yyyy HH:mm:ss
	 */
	public static String parseDateAndTime(Long fecha) {
		return fecha == null ? "" : sdfFechaHora.format(fecha);
	}

	/**
	 * @param date
	 * @return año de la fecha especificada
	 */
	public static int getYear(@NotNull Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}

	/**
	 * @param date
	 * @return mes de la fecha especificada
	 */
	public static int getMonth(@NotNull Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return cal.get(Calendar.MONTH) + 1; // To shift range from 0-11 to 1-12;
	}

	/**
	 * @param year
	 *            año a partir del cual construir la fecha
	 * @return fecha del 1 de enero del año especificado
	 */
	public static Date createNewDateFromYear(int year) {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.SECOND, 1);
		return cal.getTime();

	}

	/**
	 * Comprueba si una fecha es del 31 de diciembre.
	 * 
	 * @param date
	 * @return {@code true} si la fecha es del día 31 de diciembre
	 */
	public static boolean isDecember31(@NotNull Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(Calendar.DAY_OF_MONTH) == 31) && (cal.get(Calendar.MONTH) == Calendar.DECEMBER);
	}
	
	/**
	 * Devuelve la fecha oficial de la Sede
	 * 
	 * @return
	 */
	public static Date getOfficialDate(){
		return new Date();
	}
	
	/*
	 * Añadimos 1 sg en lugar de 0 pq si se utiliza este formato para asiganarlo
	 * a una fecha de una clase del modelo, al ir a grabar esta fecha se llama a
	 * Utils.setDefaultTime(fechaDesde) que, si se encuentra con un formato
	 * 00:00:00 lo transforma a 10:00:00. (Solución tomada por el problema de
	 * fechas en el WAS de producción)
	 */
	public static Date preparaFechaDesde(Date fecha) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(fecha);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date preparaFechaHasta(Date fecha) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(fecha);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 99);
		return cal.getTime();
	}
	
	/**
	 * @param fecha
	 * @return formato ddMMyyyy
	 */
	public static String parseDateGuiones(Date fecha) {
		return fecha == null ? "" : sdfGuiones.format(fecha);
	}

}
