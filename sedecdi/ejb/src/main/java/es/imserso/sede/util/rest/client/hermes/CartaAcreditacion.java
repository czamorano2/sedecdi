/**
 * 
 */
package es.imserso.sede.util.rest.client.hermes;

import javax.ws.rs.core.GenericType;

import org.jboss.logging.Logger;

import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.AbstractRestClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * Obtiene la carta de acreditación especificada como parámetro (en {link AbstractRestClient})
 * 
 * @author 11825775
 *
 */
public class CartaAcreditacion extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -6981232141724853780L;

	private static final Logger log = Logger.getLogger(CartaAcreditacion.class.getName()); 

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#manageResponse()
	 */
	@Override
	public Object execute() throws SedeException {
		byte[] bytes = null;

		try {

			log.debug("leyendo response ...");
			GenericType<byte[]> genericType = new GenericType<byte[]>() {
			};
			bytes = response.readEntity(genericType);

			if (bytes != null) {
				log.debugv("se han recibido {0} bytes como carta de acreditación", bytes.length);
			} else {
				log.warn("el valor devuelto es nulo!!");
			}

		} catch (Exception e) {
			String errmsg = "error al obtener carta de acreditación de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
		}

		return (Object) bytes;
	}

}
