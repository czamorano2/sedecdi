package es.imserso.sede.data.dto.impl.turismo;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;

public interface DireccionTurismoDTOI {

	SimpleDTOI getSelectedProvincia();

	void setSelectedProvincia(SimpleDTOI selectedProvincia);

}