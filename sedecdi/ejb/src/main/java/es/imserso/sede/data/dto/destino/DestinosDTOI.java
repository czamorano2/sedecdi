package es.imserso.sede.data.dto.destino;

import javax.validation.constraints.NotNull;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;

/**
 * Opciones de destino
 * 
 * @author 11825775
 *
 */
public interface DestinosDTOI {

	/**
	 * @return Primera opción de destino
	 */
	@NotNull(message = "Debe seleccionar al menos un destino")
	SimpleDTOI getOpcion1();

	void setOpcion1(@NotNull SimpleDTOI opcion);

	/**
	 * @return Segunda opción de destino
	 */
	SimpleDTOI getOpcion2();

	void setOpcion2(@NotNull SimpleDTOI opcion);

}
