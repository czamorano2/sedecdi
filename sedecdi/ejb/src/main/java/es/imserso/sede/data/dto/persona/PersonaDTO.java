package es.imserso.sede.data.dto.persona;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

import es.imserso.sede.data.dto.direccion.DireccionDTOI;
import es.imserso.sede.data.dto.email.EmailDTOI;
import es.imserso.sede.data.dto.impl.appgestora.DireccionAplicacionGestoraDTOI;
import es.imserso.sede.data.dto.qualifier.AplicacionGestoraQ;
import es.imserso.sede.data.validation.nif.NifNie;

/**
 * DTO con los datos correspondientes a la entidad Solicitud que deben obtenerse
 * del usuario final.
 * 
 * @author 11825775
 *
 */
@Dependent
abstract public class PersonaDTO implements PersonaDTOI, Serializable {

	private static final long serialVersionUID = 3638272595731219160L;

	protected String codigoSIA;
	protected String nombre;
	protected String apellido1 = "";
	protected String apellido2;
	protected String documentoIdentificacion;
	protected Date fechaNacimiento;
	protected String sexo;
	protected String estadoCivil;
	protected String telefono;
	protected String telefono2;

	@Inject
	@AplicacionGestoraQ
	protected DireccionAplicacionGestoraDTOI direccion;

	@Inject
	@Default
	protected EmailDTOI email;

	@Override
	public String getNombre() {
		return nombre;
	}

	@Override
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String getApellido1() {
		return apellido1;
	}

	@Override
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	@Override
	public String getApellido2() {
		return apellido2;
	}

	@Override
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	@Override
	@NifNie
	public String getDocumentoIdentificacion() {
		return documentoIdentificacion;
	}

	@Override
	public void setDocumentoIdentificacion(String documentoIdentificacion) {
		this.documentoIdentificacion = documentoIdentificacion;
	}

	@Override
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	@Override
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Override
	public String getSexo() {
		return sexo;
	}

	@Override
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	@Override
	public String getEstadoCivil() {
		return estadoCivil;
	}

	@Override
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	@Override
	public String getTelefono() {
		return telefono;
	}

	@Override
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Override
	public String getTelefono2() {
		return telefono2;
	}

	@Override
	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	@Override
	public DireccionDTOI getDireccion() {
		return direccion;
	}

	@Override
	public EmailDTOI getEmail() {
		return email;
	}

	@Override
	public void setEmail(EmailDTOI email) {
		this.email = email;
	}
}
