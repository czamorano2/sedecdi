package es.imserso.sede.data.dto.datosEconomicos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.Dependent;

/**
 * Datos económicos de una solicitud
 * 
 * @author 11825775
 *
 */
@Dependent
public class DatosEconomicosDTO implements DatosEconomicosDTOI, Serializable {

	private static final long serialVersionUID = 4037348618653748445L;

	private List<DatoEconomicoDTOI> datosEconomicosSolicitante;
	private List<DatoEconomicoDTOI> datosEconomicosConyuge;

	public DatosEconomicosDTO() {
		datosEconomicosSolicitante = new ArrayList<DatoEconomicoDTOI>();
		datosEconomicosConyuge = new ArrayList<DatoEconomicoDTOI>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.data.dto.DatosEconomicosDTOI#getDatosEconomicosSolicitante()
	 */
	public List<DatoEconomicoDTOI> getDatosEconomicosSolicitante() {
		return datosEconomicosSolicitante;
	}

	public void setDatosEconomicosSolicitante(List<DatoEconomicoDTOI> datosEconomicosSolicitante) {
		this.datosEconomicosSolicitante = datosEconomicosSolicitante;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DatosEconomicosDTOI#getDatosEconomicosConyuge()
	 */
	public List<DatoEconomicoDTOI> getDatosEconomicosConyuge() {
		return datosEconomicosConyuge;
	}

	public void setDatosEconomicosConyuge(List<DatoEconomicoDTOI> datosEconomicosConyuge) {
		this.datosEconomicosConyuge = datosEconomicosConyuge;
	}

	@Override
	public int count() {
		return datosEconomicosSolicitante.size() + datosEconomicosConyuge.size();
	}

}
