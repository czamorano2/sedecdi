package es.imserso.sede.data.validation.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.direccion.DireccionDTOI;

public class NotificationAddressValidator implements ConstraintValidator<NotificationAddress, DireccionDTOI> {

	private Logger log = Logger.getLogger(NotificationAddressValidator.class);

	@Override
	public void initialize(NotificationAddress constraintAnnotation) {
		log.trace("initializing...");

	}

	@Override
	public boolean isValid(DireccionDTOI value, ConstraintValidatorContext context) {

		int blankCounter = 0;
		boolean valid = true;

		if (StringUtils.isBlank(value.getCodigoPostal())) {
			blankCounter++;
			log.debug("NOT BLANK: CodigoPostal");
		}
		if (StringUtils.isBlank(value.getDomicilio())) {
			blankCounter++;
			log.debug("NOT BLANK: Domicilio");
		}
		if (StringUtils.isBlank(value.getLocalidad())) {
			blankCounter++;
			log.debug("NOT BLANK: Localidad");
		}
		if (StringUtils.isBlank(value.getPais())) {
			blankCounter++;
			log.debug("NOT BLANK: Pais");
		}
		if (StringUtils.isBlank(value.getProvincia())) {
			blankCounter++;
			log.debug("NOT BLANK: Provincia");
		}

		if (blankCounter > 0 && blankCounter < 5) {
			log.warn("Tienen valor " + blankCounter + " de los 5 campos de la dirección");
			valid = false;
		}
		return valid;
	}

}
