package es.imserso.sede.data.validation.exception;

public class NIFvalidationException extends Exception {
	
	private static final long serialVersionUID = -7008553404243858856L;

	//TODO externalizar el mensaje
	private static final String msg = "DNI no válido"; 

	public NIFvalidationException() {
		super(msg);
	}

	public NIFvalidationException(String message) {
		super(message);
	}

}
