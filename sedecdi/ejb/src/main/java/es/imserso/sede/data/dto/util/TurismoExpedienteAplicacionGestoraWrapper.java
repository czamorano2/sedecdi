package es.imserso.sede.data.dto.util;

import java.io.IOException;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author 11825775
 *
 */
public class TurismoExpedienteAplicacionGestoraWrapper {

	/**
	 * construye una instancia de {@link TurismoExpedienteAplicacionGestoraDTO}
	 * a partir del objeto JSON
	 * 
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public TurismoExpedienteAplicacionGestoraDTO build(@NotNull String jsonObject)
			throws JsonParseException, JsonMappingException, IOException {
		if (jsonObject == null) {
			return null;
		}
		return new ObjectMapper().readValue(jsonObject, TurismoExpedienteAplicacionGestoraDTO.class);
	}

}
