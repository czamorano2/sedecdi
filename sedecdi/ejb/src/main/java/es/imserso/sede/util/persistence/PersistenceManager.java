package es.imserso.sede.util.persistence;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.jboss.logging.Logger;

/**
 * Este componente se encarga de gestionar el acceso a la capa de datos de la
 * aplicación.
 * 
 * @author 11825775
 *
 */
@Stateful
@SessionScoped
public class PersistenceManager {

	@Inject
	protected Logger log;

	@PersistenceContext(unitName = "primary")
	private EntityManager entityManager;

	protected Session session;

	/**
	 * Session de hibernate para ser utilizado por los componentes de la
	 * aplicación.
	 * <p>
	 * Filtra los datos que dependen de Area para que solamente se carguen los
	 * relacionados con el área actual ({@code currentArea})
	 * 
	 * @return
	 */
	@Produces
	public Session getHibernateSession() {
		if (session == null) {
			session = ((Session) entityManager.unwrap(Session.class));
		}

		return session;
	}

	/**
	 * EntityManager para ser utilizado por los componentes de la aplicación.
	 * <p>
	 * Filtra los datos que dependen de Area para que solamente se carguen los
	 * relacionados con el área actual ({@code currentArea})
	 * 
	 * @return
	 */
	@Produces
	@ImsersoEntityManager
	public EntityManager getHibernateEntityManager() {
		log.debug("flushMode: " + ((Session) entityManager.getDelegate()).getFlushMode());
		if (!((Session) entityManager.getDelegate()).getFlushMode().equals(FlushMode.MANUAL)) {
			log.error("El flushMode del entityManager debería ser MANUAK!");
		}
		return (EntityManager) entityManager;
	}

}
