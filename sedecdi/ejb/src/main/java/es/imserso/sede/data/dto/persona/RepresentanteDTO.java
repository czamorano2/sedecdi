package es.imserso.sede.data.dto.persona;

import javax.enterprise.context.Dependent;

import es.imserso.sede.data.dto.qualifier.RepresentanteQ;

/**
 * @author 11825775
 *
 */
@Dependent
@RepresentanteQ
public class RepresentanteDTO extends PersonaDTO implements RepresentanteDTOI {

	private static final long serialVersionUID = -2107115407924773518L;

}
