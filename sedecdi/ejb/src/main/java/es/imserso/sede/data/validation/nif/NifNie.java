package es.imserso.sede.data.validation.nif;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = NifNieValidator.class)
@Documented
public @interface NifNie {

	String message() default "Formato de NIE/NIE incorrecto";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
