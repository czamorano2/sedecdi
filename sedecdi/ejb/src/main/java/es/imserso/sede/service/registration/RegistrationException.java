package es.imserso.sede.service.registration;

import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Esta clase y sus subclases son una forma de {@code Exception} que indica que
 * no se ha podido realizar satisfactoriamente el registro de la solicitud.
 * 
 * @author 11825775
 *
 */
public class RegistrationException extends SedeRuntimeException {

	private static final long serialVersionUID = -632342514246067702L;

	protected static final String DEFAULT_MESSAGE = "Error en validación";

	/**
	 * constructor por defecto
	 * 
	 * @see java.lang.Exception
	 */
	public RegistrationException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 */
	public RegistrationException(String message) {
		super(message);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 * @param cause
	 *            causa de la excepción
	 */
	public RegistrationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 *            causa de la excepción
	 */
	public RegistrationException(Throwable cause) {
		super(cause);
	}

}
