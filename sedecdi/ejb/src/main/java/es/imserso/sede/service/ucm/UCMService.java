package es.imserso.sede.service.ucm;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.TramiteUCM;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Global;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.SedeRuntimeException;
import es.imserso.sede.util.rest.client.ucm.UCMrestClient;

/**
 * Proporciona todo lo relacionado con UCM
 * 
 * @author 11825775
 *
 */
@Stateless
public class UCMService {

	@Inject
	Logger log;

	@Inject
	UCMrestClient ucmRestClient;
	
	@Inject
	Event<EventMessage> eventMessage;
	
	/**
	 * @return si responden los servicios web del UCM o no
	 * @throws SedeException 
	 */
	public boolean isUcmAlive() throws SedeException {
		return ucmRestClient.checkUCMisAlive();
	}

	public TramiteUCM getTramiteUCM(String codigoSIA) throws SedeException {

		if (codigoSIA == null) {
			String errmsg = "El código SIA es obligatorio para obtener los datos de un trámite"; 
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			log.error(errmsg);
			throw new SedeException(errmsg);
		}

		//FIXME obtener trámite del UCM (descomentando estas líneas)
//		log.debug("obteniendo información del trámite con código SIA " + codigoSIA);
//		return ucmRestClient.getDatosTramite(codigoSIA);
		
		
		return mockTramiteUCM(codigoSIA);
	}
	/**
	 * De momento, hasta que funcione el servicio web de UCM, devolvemos el trámite
	 * a piñón
	 * 
	 * @param codigoSIA
	 * @return
	 */
	private TramiteUCM mockTramiteUCM(String codigoSIA) {
		log.info("En mockTramiteUCM obteniendo información del trámite con código SIA " + codigoSIA);
		TramiteUCM tramiteUCM = new TramiteUCM();
		if (codigoSIA.equals(Global.CODIGO_SIA_TURISMO)) {
			tramiteUCM.setSIA(codigoSIA);
			tramiteUCM.setDenominacion("Programa de Turismo del Imserso");
			tramiteUCM.setPermanente(false);
			tramiteUCM.setCerradotemporalmente(false);
			tramiteUCM.setFechaapertura(new GregorianCalendar(2017, 03, 01).getTime());
			tramiteUCM.setFechacierre(new GregorianCalendar(2018, 05, 31).getTime());
			tramiteUCM.setExigeautenticacionclave(false);
			tramiteUCM.setObjeto("solicitud de vacaciones");

		} else if (codigoSIA.equals("022670")) {
			tramiteUCM.setSIA(codigoSIA);
			tramiteUCM.setDenominacion("termalismo");
			tramiteUCM.setPermanente(false);
			tramiteUCM.setCerradotemporalmente(false);
			tramiteUCM.setFechaapertura(new GregorianCalendar(2016, 02, 01).getTime());
			tramiteUCM.setFechacierre(new GregorianCalendar(2018, 06, 30).getTime());
			tramiteUCM.setExigeautenticacionclave(true);
			tramiteUCM.setObjeto("solicitud de termalismo");
		} else if (codigoSIA.equals(Global.CODIGO_SIA_PROPOSITO_GENERAL)) {
			tramiteUCM.setSIA(codigoSIA);
			tramiteUCM.setDenominacion("propósito general");
			tramiteUCM.setPermanente(false);
			tramiteUCM.setCerradotemporalmente(false);
			tramiteUCM.setFechaapertura(new GregorianCalendar(2017, 02, 03).getTime());
			tramiteUCM.setFechacierre(new GregorianCalendar(2019, 11, 27).getTime());
			tramiteUCM.setExigeautenticacionclave(true);
			tramiteUCM.setObjeto("solicitud de propósito general");
		} else if (codigoSIA.equals("025804")) {
			tramiteUCM.setSIA(codigoSIA);
			tramiteUCM
					.setDenominacion("Centros de Recuperación de personas con discapacidad física (CRMF) del Imserso");
			tramiteUCM.setPermanente(false);
			tramiteUCM.setCerradotemporalmente(false);
			tramiteUCM.setFechaapertura(new GregorianCalendar(2016, 02, 03).getTime());
			tramiteUCM.setFechacierre(new GregorianCalendar(2035, 06, 27).getTime());
			tramiteUCM.setExigeautenticacionclave(true);
			tramiteUCM.setObjeto("Solicitud para Centros de Recuperación de personas con discapacidad física");
		}
		return tramiteUCM;
	}

	/**
	 * Valida que el trámite no esté cerrado temporalmente y que estemos en fecha
	 * 
	 * @return
	 * @throws SedeException
	 *             no debe ser ValidationException porque no debe volver a la vista
	 *             del formulario a mostrar los mensajes de error, sino que debe ir
	 *             a una vista de error.
	 */
	public void validateProcedureTerms(String codigoSIA) {
		TramiteUCM tramiteUCM;
		try {
			tramiteUCM = getTramiteUCM(codigoSIA);

			if (tramiteUCM.isCerradotemporalmente()) {
				throw new SedeRuntimeException("El trámite está cerrado temporalmente");
			}

			Date now = new Date(System.currentTimeMillis());
			if (tramiteUCM.getFechaapertura().after(now)) {
				throw new SedeRuntimeException("El trámite aún no se ha abierto");
			}

			if (tramiteUCM.getFechacierre().before(now)) {
				throw new SedeRuntimeException("El trámite ya está cerrado");
			}
		} catch (SedeException e) {
			log.errorv("error al validar el estado del trámite: {0}", Utils.getExceptionMessage(e));
		}
	}
}
