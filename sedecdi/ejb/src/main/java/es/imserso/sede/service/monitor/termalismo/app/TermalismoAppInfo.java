package es.imserso.sede.service.monitor.termalismo.app;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.service.monitor.AbstractAppInfo;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.service.monitor.termalismo.TermalismoQ;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.rest.client.termalismo.TermalismoRESTClient;

/**
 * Información sobre la aplicación Hermes.
 * 
 * @author 11825775
 *
 */
@TermalismoQ
@Dependent
public class TermalismoAppInfo extends AbstractAppInfo {

	private static final long serialVersionUID = -5393854848432414095L;

	private static final Logger log = Logger.getLogger(TermalismoAppInfo.class.getName());

	@Inject
	TermalismoRESTClient termalismoRESTClient;

	// FIXME asignar URL para la web de termalismo
	public static final String TERMALISMO_WEB_APP_URL = "http://www.imserso.es/imserso_01/envejecimiento_activo/termalismo/index.htm";

	@PostConstruct
	public void onCreate() {
		services = new ArrayList<ServiceInfo>();
		// añadir servicios {@link ServiceInfo} si se implementa alguno

	}

	@Override
	public String getWebSiteURL() {
		return TERMALISMO_WEB_APP_URL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getName()
	 */
	@Override
	public String getName() {
		return "Termalismo";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Termalismo para personas mayores";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#webServicesActive()
	 */
	@Override
	public Boolean webServicesActive() {
		try {
			return (termalismoRESTClient.getConexion().getStatus() == 200) && !anyCriticalServiceDisabled();
			
		} catch (Exception e) {
			String errmsg = "Error al comprobar si los servicios web de termalismo están activos: "
					+ Utils.getExceptionMessage(e);
			log.error(errmsg);
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getServices()
	 */
	@Override
	public List<ServiceInfo> getServices() {
		return services;
	}

}
