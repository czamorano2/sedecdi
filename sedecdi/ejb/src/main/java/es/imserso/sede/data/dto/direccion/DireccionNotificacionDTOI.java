package es.imserso.sede.data.dto.direccion;

import es.imserso.sede.data.dto.qualifier.NotificacionQ;
import es.imserso.sede.data.validation.constraint.NotificationAddress;

/**
 * Reliza la validación específica para una dirección de notificación
 * @author 11825775
 *
 */
@NotificationAddress
@NotificacionQ
public interface DireccionNotificacionDTOI extends DireccionDTOI {

}
