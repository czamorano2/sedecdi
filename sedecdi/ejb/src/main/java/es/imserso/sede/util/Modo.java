package es.imserso.sede.util;

import java.io.Serializable;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

@Named
@ConversationScoped
public class Modo implements Serializable {

	private static final long serialVersionUID = 4698513479512280449L;

	private String acceso;

	public String getAcceso() {
		return acceso;
	}

	public void setAcceso(String acceso) {
		if (acceso != null && !acceso.equals("alta") && !acceso.equals("edicion") && !acceso.equals("consulta")) {
			throw new IllegalArgumentException("Se ha recibido un tipo de acceso incorrecto: " + acceso
					+ " Sólo son válidos: alta, edicion y consulta.");

		}
		this.acceso = acceso;
	}

	@Override
	public String toString() {
		return "Acceso: " + acceso;
	}

}
