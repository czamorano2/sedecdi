package es.imserso.sede.data.dto.email;

import java.io.Serializable;

import javax.enterprise.context.Dependent;

/**
 * Datos del correo electrónico de contacto
 * 
 * @author 11825775
 *
 */
@Dependent
public class EmailDTO implements EmailDTOI, Serializable {

	private static final long serialVersionUID = 3900399241999034946L;

	protected String direccion;

	protected String direccionConfirm;

	@Override
	public String getDireccion() {
		return direccion;
	}

	@Override
	public String getDireccionConfirm() {
		return direccionConfirm;
	}

	@Override
	public void setDireccion(String email) {
		this.direccion = email;
	}

	@Override
	public void setDireccionConfirm(String email) {
		this.direccionConfirm = email;
	}

}
