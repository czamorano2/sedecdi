package es.imserso.sede.service.registration.solicitud;

import java.io.Serializable;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.model.Solicitud;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentosRegistradosManager;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentosRegistradosService;
import es.imserso.sede.util.exception.SedeException;

public class SolicitudService implements Serializable {

	private static final long serialVersionUID = -1589312629169924467L;

	@Inject
	Logger log;

	@Inject
	DocumentosRegistradosService documentosAdjuntosService;

	public DocumentosRegistradosManager getDocumentosSolicitud(Long idSolicitud) throws SedeException {
		return documentosAdjuntosService.getDocumentosAdjuntos(idSolicitud);
	}

	public DocumentosRegistradosManager getDocumentosSolicitud(Solicitud solicitud) throws SedeException {
		return documentosAdjuntosService.getDocumentosAdjuntos(solicitud);
	}

}
