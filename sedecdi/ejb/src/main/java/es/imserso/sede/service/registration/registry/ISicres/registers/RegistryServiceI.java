package es.imserso.sede.service.registration.registry.ISicres.registers;

import java.util.List;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistradoI;
import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.service.registration.registry.bean.InputRegisterI;
import es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI;

/**
 * Interfaz que ofrece las funcionalidades del registro presencial.
 * 
 * @author 11825775
 *
 */
public interface RegistryServiceI {

	/**
	 * Registra una solicitud
	 * 
	 * @param bookIdentification
	 *            identificador del libro de entrada
	 * 
	 * @param adjuntos
	 *            lista de ficheros adjuntos a la solicitud
	 * @param solicitudPdf
	 *            pdf generado a partir de los datos del dto
	 * @param solicitudId
	 *            id de la solicitud
	 * @param codigoSIA
	 *            código SIA del trámite de la solicitud
	 * @return datos del expediente registrado
	 * @throws RegistrationException
	 */
	public InputRegisterResponseI registerNewSolicitud(int bookIdentification, List<DocumentoRegistradoI> adjuntos,
			byte[] solicitudPdf, long solicitudId, String codigoSIA) throws RegistrationException;

	/**
	 * Permite la recuperación de la información de un registro de entrada a
	 * partir de su identificador interno.
	 * 
	 * @param bookIdentification
	 *            identificador del libro de entrada
	 * 
	 * @param registrationNumber
	 *            número de registro (si, por ejemplo, el número en el registro
	 *            es 201620000000015, hay que poner 15)
	 * @return objeto con los datos del registro de entrada recuperado.
	 */
	public InputRegisterI getInputRegister(int bookIdentification, int registrationNumber) throws RegistrationException;

	/**
	 * Permite recuperar un objeto documental anexado a un registro de entrada o
	 * de salida.
	 * 
	 * @param bookIdentification
	 *            identificador del libro de entrada
	 * 
	 * @param registrationNumber
	 *            número de registro (si, por ejemplo, el número en el registro
	 *            es 201620000000015, hay que poner 15)
	 * @param documentIndex
	 *            índice del documento (normalmente 1)
	 * @param pageIndex
	 *            índice del documento adjunto (de 1 a n)
	 * @return array de bytes que componen el fichero adjunto devuelto
	 */
	public byte[] getAttachedDocument(int bookIdentification, int registrationNumber, int documentIndex, int pageIndex)
			throws RegistrationException;

	/**
	 * Adjunta un documento a la solicitud.
	 * 
	 * @param bookIdentification
	 *            identificador del libro de entrada
	 * 
	 * @param documentName
	 *            nombre que se le asignará al documento en el registro
	 * @param fileName
	 *            nombre de fichero que se le asignará al documento en el
	 *            registro
	 * @param registrationNumber
	 *            número de registro de la solicitud
	 * @param document
	 *            array de bytes conteniendo el documento
	 * @throws RegistrationException
	 */
	void addDocumentToSolicitud(int bookIdentification, String documentName, String fileName, int registrationNumber,
			byte[] document) throws RegistrationException;

}
