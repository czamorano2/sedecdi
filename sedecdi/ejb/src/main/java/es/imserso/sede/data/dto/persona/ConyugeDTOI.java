package es.imserso.sede.data.dto.persona;

import es.imserso.sede.data.dto.qualifier.ConyugeQ;

@ConyugeQ
public interface ConyugeDTOI extends PersonaDTOI {

}
