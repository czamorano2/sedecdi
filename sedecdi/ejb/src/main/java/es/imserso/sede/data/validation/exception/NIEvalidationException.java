package es.imserso.sede.data.validation.exception;

public class NIEvalidationException extends Exception {
	
	private static final long serialVersionUID = -4392803132738680876L;
	
	//TODO externalizar el mensaje
	private static final String msg = "NIE no válido"; 

	public NIEvalidationException() {
		super(msg);
	}

	public NIEvalidationException(String message) {
		super(message);
	}

}
