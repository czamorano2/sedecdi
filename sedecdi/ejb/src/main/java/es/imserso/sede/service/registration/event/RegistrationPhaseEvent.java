package es.imserso.sede.service.registration.event;

import java.io.Serializable;

public class RegistrationPhaseEvent implements Serializable {

	private static final long serialVersionUID = 2254994227045777186L;

	private RegistrationPhase registrationPhase;
	private String eventData;
	
	/**
	 * Constructor por defecto
	 */
	public RegistrationPhaseEvent() {
		super();
	}
	
	/**
	 * Constructor con la fase del evento
	 * 
	 * @param registrationPhase
	 */
	public RegistrationPhaseEvent(RegistrationPhase registrationPhase) {
		super();
		this.registrationPhase = registrationPhase;
	}
	
	/**
	 * Constructor con la fase del evento y los datos asociados al evento
	 * 
	 * @param registrationPhase
	 * @param eventData
	 */
	public RegistrationPhaseEvent(RegistrationPhase registrationPhase, String eventData) {
		super();
		this.registrationPhase = registrationPhase;
		this.eventData = eventData;
	}

	public RegistrationPhase getRegistrationPhase() {
		return registrationPhase;
	}

	public void setRegistrationPhase(RegistrationPhase registrationPhase) {
		this.registrationPhase = registrationPhase;
	}

	public String getEventData() {
		return eventData;
	}

	public void setEventData(String eventData) {
		this.eventData = eventData;
	}

}
