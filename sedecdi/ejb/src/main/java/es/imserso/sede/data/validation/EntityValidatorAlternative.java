package es.imserso.sede.data.validation;

import javax.enterprise.inject.Alternative;

import es.imserso.sede.util.exception.ValidationException;

@Alternative
public class EntityValidatorAlternative implements EntityValidator {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.inventario.data.validation.EntityValidator#validate(java.lang
	 * .Object)
	 */
	@Override
	public void validate(Object o, PersistenceAction action)
			throws ValidationException {
		// no hace nada porque en el test de creación de histórico no debe
		// validar
		System.out.println("alternate");

	}

}
