package es.imserso.sede.data.dto.impl.turismo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.validation.constraints.NotNull;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.sede.data.dto.datosEconomicos.DatoEconomicoDTOI;
import es.imserso.sede.data.dto.persona.ConyugeDTO;
import es.imserso.sede.data.dto.qualifier.ConyugeQ;
import es.imserso.sede.service.monitor.turismo.TurismoQ;

/**
 * @author 11825775
 *
 */
@ConyugeQ
@TurismoQ
@Dependent
public class ConyugeTurismoDTO extends ConyugeDTO implements ConyugeTurismoDTOI {

	private static final long serialVersionUID = -601023291280096437L;

	private SimpleDTOI selectedProvincia;
	private SimpleDTOI selectedEstadoCivil;
	private SimpleDTOI selectedSexo;
	private List<DatoEconomicoDTOI> datosEconomicos;
	private DatoEconomicoDTOI datoEconomico;

	@PostConstruct
	public void onCreate() {
		datosEconomicos = new ArrayList<DatoEconomicoDTOI>();
	}

	@Override
	public SimpleDTOI getSelectedProvincia() {
		return selectedProvincia;
	}

	@Override
	public void setSelectedProvincia(SimpleDTOI provincia) {
		this.selectedProvincia = provincia;
		direccion.setProvincia(selectedProvincia.getDescripcion());

	}

	@Override
	public SimpleDTOI getSelectedEstadoCivil() {
		return selectedEstadoCivil;
	}

	@Override
	public void setSelectedEstadoCivil(SimpleDTOI estadoCivil) {
		this.selectedEstadoCivil = estadoCivil;
		setEstadoCivil(selectedEstadoCivil.getDescripcion());
	}

	@Override
	public List<DatoEconomicoDTOI> getDatosEconomicos() {
		return datosEconomicos;
	}

	@Override
	public void setDatosEconomicosDTO(List<DatoEconomicoDTOI> datosEconomicos) {
		this.datosEconomicos = datosEconomicos;

	}

	@Override
	public void addDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico) {
		this.datosEconomicos.add(datoEconomico);
	}

	@Override
	public void deleteDatoEconomico(@NotNull DatoEconomicoDTOI datoEconomico) {
		this.datosEconomicos.remove(datoEconomico);

	}

	@Override
	public SimpleDTOI getSelectedSexo() {
		return selectedSexo;
	}

	@Override
	public void setSelectedSexo(SimpleDTOI sexo) {
		this.selectedSexo = sexo;
		setSexo(selectedSexo.getDescripcion());
	}

	public DatoEconomicoDTOI getDatoEconomico() {
		return datoEconomico;
	}

	public void setDatoEconomico(DatoEconomicoDTOI datoEconomico) {
		this.datoEconomico = datoEconomico;
	}

}
