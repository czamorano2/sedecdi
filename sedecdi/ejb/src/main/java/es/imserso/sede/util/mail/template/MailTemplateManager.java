package es.imserso.sede.util.mail.template;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.config.Configuracion;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.exception.SedeException;
import freemarker.cache.StringTemplateLoader;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

/**
 * Gestiona los templates de los emails combinándolos con información de una
 * solicitud.
 * 
 * @author 11825775
 * 
 */
@Model
public class MailTemplateManager {
	
	@Inject
	private Logger log;
	
	@Inject private Configuracion configuracion;
	
	/**
	 * @return mensaje construído a partir de un template y datos de la solicitud
	 */
	/**
	 * @param dto solicitud con los datos que puede necesitar el template
	 * @param template plantilla del mensaje
	 * @return mensaje construído a partir de un template y datos de la solicitud
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws MalformedTemplateNameException 
	 * @throws TemplateNotFoundException 
	 */
	public String buildMessage(Solicitud dto, String template) throws SedeException {
		
		log.debug("construyendo mensaje a partir de un template y los datos de una solicitud...");
			try {
				Configuration cfg = configuracion.getFreeMarkerConfiguration();
				
				StringTemplateLoader stringLoader = new StringTemplateLoader();
				stringLoader.putTemplate("template", template);
				
				cfg.setTemplateLoader(stringLoader);
				
				/* Create a data-model */
				Map<Object, Object> root = new HashMap<Object, Object>();
				root.put("solicitud", dto);

				/* Get the template */
				Template temp = cfg.getTemplate("template");

				/* Merge data-model with template */
				Writer out = new StringWriter();
				
				temp.process(root, out);
				
				return out.toString();
				
			} catch (IOException | TemplateException e) {
				throw new SedeException(e.toString());
			} 
	}
}
