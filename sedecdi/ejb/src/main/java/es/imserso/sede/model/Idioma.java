/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package es.imserso.sede.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @todo add comment for javadoc
 *
 * @author 11825775
 *
 */
@Entity
@SequenceGenerator(name = "SEQ_IDIOMA", initialValue = 1, allocationSize = 1, sequenceName = "SEC_IDIOMA")
public class Idioma implements Serializable {
	
	private static final long serialVersionUID = 5346107487531174149L;

	public Idioma() {
	}

	/**
	 * ------------------------------------------ The primary key.
	 *
	 *
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_IDIOMA")
	public Long getId() {
		return id;
	}


	public void setId(final Long id) {
		this.id = id;
	}


	private Long id = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	public String getLocale() {
		return locale;
	}


	public void setLocale(final String locale) {
		this.locale = locale;
	}


	private String locale = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	public String getNombre() {
		return nombre;
	}


	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}


	private String nombre = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idioma", cascade = CascadeType.MERGE, orphanRemoval = false)
	public List<PlantillaPdf> getPlantillasPdf() {
		if (this.plantillasPdf == null) {
			this.plantillasPdf = new ArrayList<PlantillaPdf>();
		}
		return plantillasPdf;
	}


	public void setPlantillasPdf(final List<PlantillaPdf> plantillasPdf) {
		this.plantillasPdf = plantillasPdf;
	}

	/**
	 * Associate Idioma with PlantillaPdf
	 * 
	 *
	 */
	public void addPlantillasPdf(PlantillaPdf plantillasPdf) {
		if (plantillasPdf == null) {
			return;
		}
		getPlantillasPdf().add(plantillasPdf);
		plantillasPdf.setIdioma(this);
	}

	/**
	 * Unassociate Idioma from PlantillaPdf
	 * 
	 *
	 */
	public void removePlantillasPdf(PlantillaPdf plantillasPdf) {
		if (plantillasPdf == null) {
			return;
		}
		getPlantillasPdf().remove(plantillasPdf);
		plantillasPdf.setIdioma(null);
	}

	/**
	 *
	 */
	public void removeAllPlantillasPdf() {
		List<PlantillaPdf> remove = new ArrayList<PlantillaPdf>();
		remove.addAll(getPlantillasPdf());
		for (PlantillaPdf element : remove) {
			removePlantillasPdf(element);
		}
	}


	private List<PlantillaPdf> plantillasPdf = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idioma", cascade = CascadeType.MERGE, orphanRemoval = false)
	public List<PlantillaEmail> getPlantillasEmail() {
		if (this.plantillasEmail == null) {
			this.plantillasEmail = new ArrayList<PlantillaEmail>();
		}
		return plantillasEmail;
	}


	public void setPlantillasEmail(final List<PlantillaEmail> plantillasEmail) {
		this.plantillasEmail = plantillasEmail;
	}

	/**
	 * Associate Idioma with PlantillaEmail
	 * 
	 *
	 */
	public void addPlantillasEmail(PlantillaEmail plantillasEmail) {
		if (plantillasEmail == null) {
			return;
		}
		getPlantillasEmail().add(plantillasEmail);
		plantillasEmail.setIdioma(this);
	}

	/**
	 * Unassociate Idioma from PlantillaEmail
	 * 
	 *
	 */
	public void removePlantillasEmail(PlantillaEmail plantillasEmail) {
		if (plantillasEmail == null) {
			return;
		}
		getPlantillasEmail().remove(plantillasEmail);
		plantillasEmail.setIdioma(null);
	}

	/**
	 *
	 */
	public void removeAllPlantillasEmail() {
		List<PlantillaEmail> remove = new ArrayList<PlantillaEmail>();
		remove.addAll(getPlantillasEmail());
		for (PlantillaEmail element : remove) {
			removePlantillasEmail(element);
		}
	}


	private List<PlantillaEmail> plantillasEmail = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------

	/** @NOT generated */
	public String toString() {
		return getLocale() + " - " + getNombre();
	}


	public Idioma deepClone() throws Exception {
		Idioma clone = (Idioma) super.clone();
		clone.setId(null);

		clone.setPlantillasPdf(null);
		for (PlantillaPdf kid : this.getPlantillasPdf()) {
			clone.addPlantillasPdf(kid.deepClone());
		}

		clone.setPlantillasEmail(null);
		for (PlantillaEmail kid : this.getPlantillasEmail()) {
			clone.addPlantillasEmail(kid.deepClone());
		}
		return clone;
	}


	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Idioma))
			return false;
		final Idioma other = (Idioma) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}
}
