package es.imserso.sede.service.converter;

import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Esta clase y sus subclases son una forma de {@code Exception} que indica que
 * no se ha podido realizar satisfactoriamente la conversión.
 * 
 * @author 11825775
 *
 */
public class ConverterException extends SedeRuntimeException {

	private static final long serialVersionUID = -6687315308982044518L;

	protected static final String DEFAULT_MESSAGE = "Error en validación";

	/**
	 * constructor por defecto
	 * 
	 * @see java.lang.Exception
	 */
	public ConverterException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 */
	public ConverterException(String message) {
		super(message);
	}

	/**
	 * @param message
	 *            mensaje de la excepción
	 * @param cause
	 *            causa de la excepción
	 */
	public ConverterException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 *            causa de la excepción
	 */
	public ConverterException(Throwable cause) {
		super(cause);
	}

}
