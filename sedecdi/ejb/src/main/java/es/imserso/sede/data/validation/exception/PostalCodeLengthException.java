package es.imserso.sede.data.validation.exception;

public class PostalCodeLengthException extends ValidatorException {
	
	private static final long serialVersionUID = -677036450738333058L;
	
	//TODO externalizar el mensaje
	private static final String msg = "La longitud debe ser de 5 dígitos."; 

	public PostalCodeLengthException() {
		super(msg);
	}

	public PostalCodeLengthException(String message) {
		super(message);
	}

}
