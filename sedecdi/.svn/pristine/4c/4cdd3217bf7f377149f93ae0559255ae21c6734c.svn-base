package es.imserso.sede.web.model.validator;

import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;

import es.imserso.sede.data.dto.SolicitudDTOI;
import es.imserso.sede.data.dto.impl.PropositoGeneralDTO;
import es.imserso.sede.data.validation.group.GroupNotRepresentante;
import es.imserso.sede.data.validation.group.GroupRepresentante;
import es.imserso.sede.util.exception.ValidationException;

/**
 * Validador de {@link es.imserso.sede.data.dto.impl.PropositoGeneralDto}
 * 
 * @author 11825775
 *
 */
/**
 * @author 11825775
 *
 */
@Dependent
public class PropositoGeneralDtoValidator extends DtoValidator {

	private static final long serialVersionUID = 4348522065866245807L;

	public void validate(@NotNull SolicitudDTOI dto) throws ValidationException {
		validated = Boolean.TRUE;

		log.info("validando los datos del formuario ...");
		validateFormData((PropositoGeneralDTO) dto);

		log.info("validando fechas de tramitación...");
		UCMservice.validateProcedureTerms(dto.getCodigoSIA());

		validatedOK = Boolean.TRUE;
		validatedKO = Boolean.FALSE;

	}

	/**
	 * Ejecuta todas las validaciones necesarias para comprobar que los datos
	 * introcucidos son válidos
	 * 
	 * @return verdadero si pasa todas las validaciones
	 */
	private void validateFormData(@NotNull PropositoGeneralDTO dto) throws ValidationException {

		int constraintViolationCounter = 0;

		// validamos los campos del formulario
		Set<ConstraintViolation<PropositoGeneralDTO>> constraintViolations = validator
				.validate((PropositoGeneralDTO) dto);

		if (constraintViolations.size() > 0) {
			log.warn("se han encontrado " + constraintViolations.size() + " errores de validación en el formulario:");
			for (ConstraintViolation<PropositoGeneralDTO> violation : constraintViolations) {
				constraintViolationCounter++;
				log.warn(violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}

		}

		if (paramValues.isRepresentante()) {
			log.debug("validando campos cuando hay representante...");
			Set<ConstraintViolation<PropositoGeneralDTO>> constraintViolationsRepresentante = validator
					.validate((PropositoGeneralDTO) dto, GroupRepresentante.class);
			log.warn("se han encontrado " + constraintViolationsRepresentante.size()
					+ " errores de validación de representante en el formulario:");
			for (ConstraintViolation<PropositoGeneralDTO> violation : constraintViolationsRepresentante) {

				constraintViolationCounter++;
				log.warn(violation.getPropertyPath() + ": " + violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}

		} else {
			log.debug("validando campos cuando no hay representante...");
			Set<ConstraintViolation<PropositoGeneralDTO>> constraintViolationsNotRepresentante = validator
					.validate((PropositoGeneralDTO) dto, GroupNotRepresentante.class);
			log.warn("se han encontrado " + constraintViolationsNotRepresentante.size()
					+ " errores de validación de NO representante en el formulario:");
			for (ConstraintViolation<PropositoGeneralDTO> violation : constraintViolationsNotRepresentante) {

				constraintViolationCounter++;
				log.warn(violation.getPropertyPath() + ": " + violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}
		}

		if (constraintViolationCounter > 0) {
			if (FacesContext.getCurrentInstance().isValidationFailed()) {
				log.info("isValidationFailed = TRUE");
			}
			throw new ValidationException(String.format("Se han encontrado %d violaciones de reglas en el formulario",
					constraintViolationCounter));
		}

	}

	/**
	 * @return <code>true</code> si se ha realizado ya la validación del DTO
	 */
	public boolean alreadyValidated() {
		return validated;
	}

	/**
	 * @return <code>true</code> si ha pasado correctamente todas las
	 *         validaciones o <code>false</code> si aún no se ha validado o si
	 *         no ha pasado correctamente alguna validación
	 */
	public boolean isValid() {
		return validatedOK;
	}

	/**
	 * @return <code>true</code> si NO ha pasado correctamente todas las
	 *         validaciones o <code>false</code> si SI ha pasado correctamente
	 *         todas las validaciones
	 */
	public boolean isInvalid() {
		return validatedKO;
	}

}
