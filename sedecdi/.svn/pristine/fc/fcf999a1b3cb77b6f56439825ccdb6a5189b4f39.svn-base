package es.imserso.sede.util.mail;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import es.imserso.sede.config.PropertyComponent;

@Named
public class MailManager implements Serializable {

	private static final String NOREPLY = "noreply@imserso.es";

	private static final long serialVersionUID = -7987368820400284500L;

	@Inject
	private MailEngine mailEngine;

	@Inject
	private PropertyComponent propertyComponent;

	/**
	 * Método para enviar un email de incidencia al administrador.
	 * 
	 * @param subject
	 * @param messageText
	 * @throws Exception
	 */
	public void sendNotificationToAdmin(@NotNull String subject, @NotNull String messageText) throws Exception {

		mailEngine.send(subject, messageText, NOREPLY, propertyComponent.getEmailErrorAccount());
	}

	/**
	 * Método para enviar un email de incidencia al administrador.
	 * 
	 * @param subject
	 * @param messageText
	 * @param to
	 * @throws Exception
	 */
	public void send(@NotNull String subject, @NotNull String messageText, @NotNull String to) throws Exception {
		mailEngine.send(subject, messageText, NOREPLY, to);
	}

}
