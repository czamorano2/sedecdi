package es.imserso.sede.util.rest.client.ucm;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.TramiteUCM;
import es.imserso.sede.util.Global;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * Cliente REST para los servicios web del UCM.
 * 
 * @author 11825775
 *
 */
@Dependent
public class UCMrestClient implements Serializable {

	private static final long serialVersionUID = 7590992487120377239L;

	Logger log = Logger.getLogger(UCMrestClient.class);

	@Inject
	private PropertyComponent propertyComponent;

	/**
	 * @return true si UCM responde que está operativo
	 * @throws SedeException
	 */
	public boolean checkUCMisAlive() throws SedeException {
		boolean isAlive = false;

		// llamamos a un servicio REST de UCM que responde si está la aplicación
		// operativa
		log.debug("comprobando si UCM está operativo...");

		try {
			ResteasyClient client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getUcmResourcesURL() + Global.CODIGO_SIA_PROPOSITO_GENERAL;
			log.info("conectamos al servicio REST: " + url);
			ResteasyWebTarget target = client.target(url);

			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.info("Obtenemos el código de repuesta de UCM: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("ResponseMessageFromServer: " + responseMessageFromServer);

			if (isAlive = (responseCode == 200)) {
				log.info("UCM responde que está operativo");
			} else {
				log.warn("UCM no responde o responde que NO está operativo");
			}

		} catch (IllegalArgumentException | NullPointerException e) {
			String errmsg = "error al comprobar si UCM está operativo: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}

		return isAlive;
	}

	/**
	 * @param sia
	 *            código SIA del trámite
	 * @return datos del trámite
	 * @throws SedeException
	 */
	public TramiteUCM getDatosTramite(String sia) throws SedeException {
		log.debugv("obteniendo datos del trámite {0} ...", sia);
		TramiteUCM tramiteUCM = null;

		try {
			ResteasyClient client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getUcmResourcesURL() + sia;
			log.info("conectamos al servicio REST: " + url);
			ResteasyWebTarget target = client.target(url);

			target.request(MediaType.APPLICATION_JSON_TYPE);

			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.debug("código de repuesta de UCM: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de UCM: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			String resp = response.readEntity(String.class);
			log.debug(resp);

			if (resp != null) {
				tramiteUCM = new ObjectMapper().readValue(resp, TramiteUCM.class);
				log.info(String.format("se ha recibido correctamente la información del trámite: %s", tramiteUCM));
			} else {
				log.warn("se ha recibido un valor nulo!!!");
			}

		} catch (IllegalArgumentException | NullPointerException | SedeException | IOException e) {
			String errmsg = "error al comprobar si UCM está operativo: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}

		return tramiteUCM;
	}

}
