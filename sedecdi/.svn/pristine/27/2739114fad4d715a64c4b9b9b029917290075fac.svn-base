package es.imserso.sede.util.rest.client;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraDTO;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * Cliente REST para los servicios de Hermes.
 * 
 * @author 11825775
 *
 */
@Dependent
public class HermesRESTClient extends HermesRestClientAux implements Serializable {

	private static final long serialVersionUID = -3127948137115733320L;

	@Inject
	private Logger log;

	/**
	 * path del método del endpoint de Hermes para saber si la aplicación está
	 * operativa
	 */
	private static final String HERMES_IS_ALIVE_REST_PATH = "isalive";

	/**
	 * path del método del endpoint de Hermes para saber si la aplicación está
	 * bloqueada
	 */
	private static final String HERMES_IS_UNLOCKED_REST_PATH = "isunlocked";

	/**
	 * path del método del endpoint de Hermes para saber si el turno por defecto
	 * está abierto
	 */
	private static final String HERMES_IS_TURNO_OPEN_REST_PATH = "isTurnoOpen";

	/**
	 * path del método del endpoint de Hermes para obtener una solicitud por su id
	 */
	private static final String HERMES_GET_SOLICITUD_REST_PATH = "";

	/**
	 * path del método del endpoint de Hermes para obtener una solicitud por su id
	 */
	private static final String HERMES_GET_SOLICITUDES_BY_DI_REST_PATH = "buscarPorDi/";

	/**
	 * path del método del endpoint de Hermes para obtener el estado de una
	 * solicitud por su id
	 */
	private static final String HERMES_GET_ESTADO_SOLICITUD_REST_PATH = "estado/";

	/**
	 * path del método del endpoint de Hermes para crear una solicitud
	 */
	private static final String HERMES_CREATE_SOLICITUD_REST_PATH = "createsolicitud";

	/**
	 * path del método del endpoint de Hermes para modificar una solicitud
	 */
	private static final String HERMES_UPDATE_SOLICITUD_REST_PATH = "updateSolicitud";

	/**
	 * path del método del endpoint de Hermes para crear una solicitud
	 */
	private static final String HERMES_GET_CARTA_ACREDITACION_REST_PATH = "cartaacreditacion/";

	/**
	 * @return true si Hermes responde que está operativo
	 * @throws SedeException
	 */
	public boolean checkHermesIsAlive() throws SedeException {
		boolean isAlive = false;

		// llamamos a un servicio REST de Hermes que responde si está la aplicación
		// operativa
		log.debug("comprobando si Hermes está operativo...");

		try {
			ResteasyClient client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getTurismoResourcesURL() + HERMES_IS_ALIVE_REST_PATH;
			log.info("conectamos al servicio REST: " + url);
			ResteasyWebTarget target = client.target(url);

			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.info("Obtenemos el código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("ResponseMessageFromServer: " + responseMessageFromServer);

			if (isAlive = (responseCode == 200)) {
				log.info("Hermes responde que está operativo");
			} else {
				log.warn("Hermes no responde o responde que NO está operativo");
			}

		} catch (Exception e) {
			String errmsg = "error al comprobar si Hermes está operativo: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		}

		return isAlive;
	}

	/**
	 * @return si Hermes está desbloqueada, es decir, que no está bloqueada
	 * @throws SedeException
	 */
	public boolean checkAplicacionDesbloqueada() throws SedeException {
		boolean isUnlocked = false;

		log.debug("comprobando si Hermes está operativo...");

		try {
			ResteasyClient client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getTurismoResourcesURL() + HERMES_IS_UNLOCKED_REST_PATH;
			log.info("conectamos al servicio REST: " + url);
			ResteasyWebTarget target = client.target(url);

			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.info("Obtenemos el código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("ResponseMessageFromServer: " + responseMessageFromServer);

			if (isUnlocked = (responseCode == 200)) {
				log.info("Hermes responde que la aplicación está desbloqueada");
			} else {
				log.warn("Hermes no responde o responde que la aplicación está bloqueada");
			}

		} catch (Exception e) {
			String errmsg = "error al comprobar si Hermes está bloqueada: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		}

		return isUnlocked;
	}

	/**
	 * @return si el turno por defecto de Hermes está abierto (es modificable)
	 * @throws SedeException
	 */
	public boolean checkTurnoAbierto() throws SedeException {
		boolean isOpen = false;

		log.debug("comprobando si Hermes está operativo...");

		try {
			ResteasyClient client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getTurismoResourcesURL() + HERMES_IS_TURNO_OPEN_REST_PATH;
			log.info("conectamos al servicio REST: " + url);
			ResteasyWebTarget target = client.target(url);

			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.info("Obtenemos el código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("ResponseMessageFromServer: " + responseMessageFromServer);

			if (isOpen = (responseCode == 200)) {
				log.info("Hermes responde que el turno por defecto está abierto");
			} else {
				log.warn("Hermes no responde o responde que el turno por defecto está abierto");
			}

		} catch (Exception e) {
			String errmsg = "error al comprobar si el turno por defecto está abierto: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		}

		return isOpen;
	}

	/**
	 * Da de alta una solicitud en Hermes.
	 * 
	 * @return dto con la solicitud dada de alta en Hermes.
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO createRemoteSolicitud(SolicitudTurismoDTO dtoInstance) throws SedeException {
		SolicitudTurismoDTO outputDTO = null;

		log.debug("se va a dar de alta una solicitud en Hermes...");

		try {
			// Using the RESTEasy libraries, initiate a client request
			ResteasyClient client = new ResteasyClientBuilder().build();

			// Set url as target
			log.info("conectamos al servicio REST: " + propertyComponent.getTurismoResourcesURL()
					+ HERMES_CREATE_SOLICITUD_REST_PATH);
			ResteasyWebTarget target = client
					.target(propertyComponent.getTurismoResourcesURL() + HERMES_CREATE_SOLICITUD_REST_PATH);

			String jsonDTO = new ObjectMapper().writeValueAsString(dtoInstance);

			log.info(jsonDTO);

			Response response = target.request().post(Entity.entity(jsonDTO, MediaType.APPLICATION_JSON));

			// get response code
			int responseCode = response.getStatus();
			log.info("Obtenemos el código de repuesta de Hermes: " + responseCode);

			// get response message
			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("ResponseMessageFromServer: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				String errmsg = "alta solicitud en Hermes: " + responseMessageFromServer;
				log.error(errmsg);
				throw new SedeException("(HTTP error code : " + responseCode + ") " + errmsg);
			}

			// read response byte[]
			outputDTO = response.readEntity(SolicitudTurismoDTO.class);
			if (outputDTO != null) {
				log.debug(
						"se ha recibido correctamente el dto con la solicitud dada de alta con el número de registro: "
								+ outputDTO.getNumeroRegistro());
			} else {
				log.warn("el DTO recibido del alta de solicitud es nulo!!!");
			}

		} catch (Exception e) {
			String errmsg = "error al dar de alta remotamente una solicitud en Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		}

		return outputDTO;
	}

	/**
	 * Modifica una solicitud en Hermes.
	 * 
	 * @throws SedeException
	 */
	public void updateRemoteSolicitud(SolicitudTurismoDTO dtoInstance) throws SedeException {

		log.debug("se va a modificar una solicitud en Hermes...");

		try {
			// Using the RESTEasy libraries, initiate a client request
			ResteasyClient client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getTurismoResourcesURL() + HERMES_UPDATE_SOLICITUD_REST_PATH;

			// Set url as target
			log.info("conectando al servicio REST: " + url);
			ResteasyWebTarget target = client.target(url);

			String jsonDTO = new ObjectMapper().writeValueAsString(dtoInstance);

			log.info(jsonDTO);

			Response response = target.request().put(Entity.entity(jsonDTO, MediaType.APPLICATION_JSON));

			// get response code
			int responseCode = response.getStatus();
			log.info("Obtenemos el código de repuesta de Hermes: " + responseCode);

			String responseStatusMessage = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseStatusMessage);

			String responseMessageFromServer = "";
			try {
				responseMessageFromServer = response.readEntity(String.class);
				log.debug("mensaje detallado de respuesta de Hermes: " + responseMessageFromServer);
			} catch (Exception e) {
				log.warn(Utils.getExceptionMessage(e));
			}

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				String errmsg = "(HTTP error : " + responseCode + "-" + responseStatusMessage + ") "
						+ responseMessageFromServer;
				log.error(errmsg);
				throw new SedeException(errmsg);
			}

			log.info(responseMessageFromServer);

		} catch (Exception e) {
			String errmsg = "error al actualizar remotamente una solicitud en Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		}

	}

	/**
	 * @param identificadorSolicitud
	 *            datos necesarios para que Hermes pueda localizar una solicitud
	 * @return datos de una solicitud de turismo
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO getRemoteSolicitud(@NotNull TurismoExpedienteAplicacionGestoraDTO identificadorSolicitud)
			throws SedeException {

		if (identificadorSolicitud == null) {
			throw new SedeException("debe especificarse el identificador de la solicitud de turismo");
		}

		SolicitudTurismoDTO solicitudTurismoDTO = null;

		try {
			log.debug(String.format("vamos a buscar una solicitud en Hermes con los datos de identificación: %s",
					identificadorSolicitud.getJSON()));

			log.debug("inicializando cliente REST ...");
			ResteasyClient client = new ResteasyClientBuilder().build();

			log.debug("conectamos al servicio REST: " + propertyComponent.getTurismoResourcesURL()
					+ HERMES_GET_SOLICITUD_REST_PATH + identificadorSolicitud.getId());
			ResteasyWebTarget target = client
					.target(propertyComponent.getTurismoResourcesURL() + HERMES_GET_SOLICITUD_REST_PATH);

			log.debug(String.format("hacemos la llamada enviando como parámetro: %s ",
					identificadorSolicitud.toString()));

			Response response = target.request()
					.post(Entity.entity(identificadorSolicitud.getJSON(), MediaType.APPLICATION_JSON));

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			solicitudTurismoDTO = response.readEntity(SolicitudTurismoDTO.class);
			if (solicitudTurismoDTO != null) {
				log.info(String.format(
						"se ha recibido correctamente el dto con la solicitud dada de alta con el número de registro %s",
						solicitudTurismoDTO.getNumeroOrden()));
			} else {
				log.warn("el DTO recibido del alta de solicitud es nulo!!!");
			}

		} catch (Exception e) {
			String errmsg = "error al obtener solicitud de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		}

		return solicitudTurismoDTO;
	}

	/**
	 * @param identificadorSolicitud
	 *            datos necesarios para que Hermes pueda localizar una solicitud
	 * @return cadena de texto que especifica el estado de la solicitud en Hermes
	 * @throws SedeException
	 */
	public String getRemoteEstadoSolicitud(@NotNull TurismoExpedienteAplicacionGestoraDTO identificadorSolicitud)
			throws SedeException {

		if (identificadorSolicitud == null) {
			throw new SedeException("debe especificarse el identificador de la solicitud de turismo");
		}

		String estado = null;

		try {
			log.debug(String.format(
					"vamos a buscar el estado de una solicitud en Hermes con los datos de identificación: %s",
					identificadorSolicitud.getJSON()));

			String restUrl = propertyComponent.getTurismoResourcesURL() + HERMES_GET_ESTADO_SOLICITUD_REST_PATH
					+ identificadorSolicitud.getId();
			log.debug(String.format("hacemos la llamada a %s", restUrl));

			log.debug("inicializando cliente REST ...");
			ResteasyClient client = new ResteasyClientBuilder().build();

			ResteasyWebTarget target = client.target(restUrl);

			target.request(MediaType.APPLICATION_JSON_TYPE);

			log.debug("conectamos al servicio REST: " + restUrl);
			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			estado = response.readEntity(String.class);
			if (estado != null) {
				log.info(String.format("se ha recibido correctamente el estado de la solicitud: %s", estado));
			} else {
				log.warn("el estado de la solicitud recibido es nulo!!!");
			}

		} catch (Exception e) {
			String errmsg = "error al obtener solicitud de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		}

		return estado;
	}

	/**
	 * @param solicitudId
	 *            identificador de la solicitud en Hermes
	 * @return DTO con los datos de la solicitud obtenidos de Hermes
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO getRemoteSolicitud(@NotNull Long solicitudId) throws SedeException {

		SolicitudTurismoDTO dto = null;

		try {
			log.debugf("vamos a buscar en Hermes la solicitud con id=%d", solicitudId);

			String restUrl = propertyComponent.getTurismoResourcesURL() + HERMES_GET_SOLICITUD_REST_PATH + solicitudId;
			log.debugf("hacemos la llamada a %s  ", restUrl);

			log.debug("inicializando cliente REST ...");
			ResteasyClient client = new ResteasyClientBuilder().build();

			ResteasyWebTarget target = client.target(restUrl);

			target.request(MediaType.APPLICATION_JSON_TYPE);

			log.debug("conectamos al servicio REST: " + restUrl);
			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			dto = response.readEntity(SolicitudTurismoDTO.class);
			if (dto != null) {
				log.infof("se ha recibido correctamente la solicitud: %s", dto.toString());
			} else {
				log.warn("la solicitud recibido es nula!!!");
			}

		} catch (Exception e) {
			String errmsg = "error al obtener solicitud de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		}

		return dto;
	}

	/**
	 * @param documentoIdentificacion
	 *            NIF/NIE del solicitante o del cónyuge
	 * @return lista de solicitudes de turismo de la temporada actual cuyo
	 *         solicitante o cónyuge coincidan con el especificado por parámetro
	 * @throws SedeException
	 */
	public List<SolicitudTurismoDTO> getRemoteSolicitudesByDI(@NotNull String documentoIdentificacion)
			throws SedeException {

		if (documentoIdentificacion == null) {
			String errmsg = "debe especificarse el NIF/NIE del solicitante o del cónyuge";
			log.error(errmsg);
			throw new SedeException(errmsg);
		}

		List<SolicitudTurismoDTO> list = null;
		ResteasyClient client = null;
		Response response = null;

		try {
			log.debugv(
					"se va a solicitar a Hermes las solicitudes de la temporada actual de la persona con NIF/NIE: {0}",
					documentoIdentificacion);

			log.debug("inicializando cliente REST ...");
			client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getTurismoResourcesURL() + HERMES_GET_SOLICITUDES_BY_DI_REST_PATH
					+ documentoIdentificacion;
			log.debug("conectamos al servicio REST: " + url);

			response = client.target(url).request().get();

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			GenericType<List<SolicitudTurismoDTO>> genericType = new GenericType<List<SolicitudTurismoDTO>>() {
			};
			list = response.readEntity(genericType);
			// SolicitudesDTO solicitudesDTO = response.readEntity(SolicitudesDTO.class);
			// list = solicitudesDTO.getValores();

			if (list != null) {
				log.debugv("se han recibido {0} solicitudes para el NIF/NIE {1}", list.size(), documentoIdentificacion);
			} else {
				log.warn("la lista de solicitudes es nula!!!");
			}

		} catch (Exception e) {
			String errmsg = "error al obtener solicitudes de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);
		} finally {
			if (response != null) {
				response.close();
			}
			if (client != null) {
				client.close();
			}
		}

		return list;
	}

	/**
	 * @param solicitudId
	 *            identificador de la solicitud
	 * @return array de bytes que componen la carta de acreditación de la solicitud
	 *         especificada
	 * @throws SedeException
	 */
	public byte[] getCartaAcreditacionSolicitud(Long solicitudId) throws SedeException {
		if (solicitudId == null) {
			String errmsg = "debe especificarse el identificador de la solicitud";
			log.error(errmsg);
			throw new SedeException(errmsg);
		}

		byte[] bytes = null;
		ResteasyClient client = null;
		Response response = null;

		try {
			log.debugv("se va a solicitar a Hermes la carta de acreditación de la solicitud con id={0}", solicitudId);

			log.debug("inicializando cliente REST ...");
			client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getTurismoResourcesURL() + HERMES_GET_CARTA_ACREDITACION_REST_PATH
					+ solicitudId.longValue();
			log.debug("conectamos al servicio REST: " + url);

			response = client.target(url).request().get();

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseStatusMessage = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseStatusMessage);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				String responseMessageFromServer = "";
				try {
					responseMessageFromServer = response.readEntity(String.class);
					log.debug("mensaje detallado de respuesta de Hermes: " + responseMessageFromServer);
				} catch (Exception e) {
					log.warn(Utils.getExceptionMessage(e));
				}
				String errmsg = "(HTTP error : " + responseCode + "-" + responseStatusMessage + ") "
						+ responseMessageFromServer;
				log.error(errmsg);
				throw new SedeException(errmsg);
			}

			log.debug("leyendo response ...");
			GenericType<byte[]> genericType = new GenericType<byte[]>() {
			};
			bytes = response.readEntity(genericType);

			if (bytes != null) {
				log.debugv("se han recibido {0} bytes como carta de acreditación", bytes.length);
			} else {
				log.warn("el valor devuelto es nulo!!");
			}

		} catch (Exception e) {
			String errmsg = "error al obtener carta de acreditación de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
			if (client != null) {
				client.close();
			}
		}

		return bytes;
	}

}
