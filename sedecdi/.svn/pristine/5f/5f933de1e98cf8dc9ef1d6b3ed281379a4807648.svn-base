package es.imserso.sede.data.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.sede.data.validation.group.GroupNotRepresentante;
import es.imserso.sede.data.validation.nif.NifNie;
import es.imserso.sede.model.Estado;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * DTO con los datos correspondientes a la entidad Solicitud que deben obtenerse
 * del usuario final.
 * 
 * @author 11825775
 *
 */
public class PersonaInteresadaDTO implements es.imserso.sede.data.dto.PersonaInteresadaDTOI {

	@NotBlank(message = "el código SIA no puede estar vacío")
	protected String codigoSIA;

	@NotBlank(message = "el nombre de la persona interesada no puede estar vacío")
	protected String nombre;

	@NotBlank(message = "el primer apellido de la persona interesada no puede estar vacío")
	protected String apellido1;

	protected String apellido2;

	@NotBlank(message = "el documento de identificación de la persona interesada no puede estar vacío")
	@NifNie(message="El documento de identificación de la persona interesada no es válido")
	protected String documentoIdentificacion;

	@NotBlank(groups = GroupNotRepresentante.class, message = "el teléfono de la persona interesada no puede estar vacío")
	protected String telefono;

	@NotBlank(groups = GroupNotRepresentante.class, message = "el correo electrónico de la persona interesada no puede estar vacío")
	@Email(message="Debe especificarse email de confirmación de la persona interesada")
	protected String email;

	@NotBlank(groups = GroupNotRepresentante.class, message = "el correo electrónico de confirmación de la persona interesada no puede estar vacío")
	@Email(message="Debe especificarse email de confirmación de la persona interesada")
	@XmlTransient
	protected String transientEmailConfirm;

	protected List<DocumentoRegistrado> attachedFiles;

	public PersonaInteresadaDTO() {
		this.attachedFiles = new ArrayList<DocumentoRegistrado>();
	}

	@Override
	public String getCodigoSIA() {
		return this.codigoSIA;
	}

	@Override
	public void setCodigoSIA(String codigoSIA) {
		this.codigoSIA = codigoSIA;
	}

	@Override
	public String getNombre() {
		return nombre;
	}

	@Override
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String getApellido1() {
		return apellido1;
	}

	@Override
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	@Override
	public String getApellido2() {
		return apellido2;
	}

	@Override
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	@Override
	@NifNie
	public String getDocumentoIdentificacion() {
		return documentoIdentificacion;
	}

	@Override
	public void setDocumentoIdentificacion(String documentoIdentificacion) {
		this.documentoIdentificacion = documentoIdentificacion;
	}

	@Override
	public String nombreApellidos() {
		return this.getNombre() + " " + this.getApellido1() + " " + this.getApellido2();
	}

	@Override
	public String getEmail() {
		return this.email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public List<DocumentoRegistrado> getAttachedFiles() {
		return this.attachedFiles;
	}

	@Override
	public void setAttachedFiles(List<DocumentoRegistrado> attachedFiles) {
		this.attachedFiles = attachedFiles;
	}

	@Override
	public void addAttachedFile(DocumentoRegistrado attachedFile) {
		this.attachedFiles.add(attachedFile);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DTOInterface#extractNewSolicitud()
	 */
	@Override
	public Map<String, String> extractHashtable() throws IllegalAccessException {
		return Utils.extractClassFieldsToHashtable(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DTOInterface#extractNewSolicitud()
	 */
	@Override
	public Solicitud extractNewSolicitud() throws SedeException {
		Solicitud solicitud = new Solicitud();
		solicitud.setNombre(getNombre());
		solicitud.setApellido1(getApellido1());
		solicitud.setApellido2(getApellido2());
		solicitud.setDocumentoIdentificacion(getDocumentoIdentificacion());
		solicitud.setEmail(getEmail());

		solicitud.setFechaAlta(new Date(System.currentTimeMillis()));
		solicitud.setEstado(Estado.PENDIENTE);

		return solicitud;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.PersonaInteresadaDTOI#getEmailConfirm()
	 */
	@Override
	public String getTransientEmailConfirm() {
		return this.transientEmailConfirm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.data.dto.PersonaInteresadaDTOI#setEmailConfirm(java.lang.
	 * String)
	 */
	@Override
	public void setTransientEmailConfirm(String email) {
		this.transientEmailConfirm = email;

	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono
	 *            the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
