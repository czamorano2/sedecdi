<ui:composition
	template="/WEB-INF/templates/createMultipartLayout.xhtml"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:f="http://xmlns.jcp.org/jsf/core"
	xmlns:h="http://xmlns.jcp.org/jsf/html"
	xmlns:ui="http://xmlns.jcp.org/jsf/facelets"
	xmlns:c="http://xmlns.jcp.org/jsp/jstl/core"
	xmlns:fn="http://xmlns.jcp.org/jsp/jstl/functions"
	xmlns:p="http://primefaces.org/ui" xmlns:o="http://omnifaces.org/ui"
	xmlns:of="http://omnifaces.org/functions">

	<ui:define name="title">Imserso - Sede Electrónica</ui:define>

	<ui:define name="contentTitle">
		<h:outputText value="Alta de Solicitud de Turismo" styleClass="code" />
	</ui:define>

	<ui:define name="multipartForm">

		<p:fieldset legend="Representante" style="margin-bottom:20px"
			rendered="${genericoView.showRepresentante()}">

			<p:panelGrid columns="4"
				columnClasses="ui-grid-col-3,ui-grid-col-3,ui-grid-col-3,ui-grid-col-3"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="nombreRepresentante" value="Nombre" />
					<p:inputText id="nombreRepresentante" maxlength="50"
						style="text-transform: uppercase"
                        disabled="${genericoView.disableRepresentanteMainFields()}"
						value="${genericoView.dto.nombreRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="apellido1Representante" value="Primer apellido" />
					<p:inputText id="apellido1Representante" maxlength="50"
						style="text-transform: uppercase"
                        disabled="${genericoView.disableRepresentanteMainFields()}"
						value="${genericoView.dto.apellido1Representante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="apellido2Representante"
						value="Segundo apellido" />
					<p:inputText id="apellido2Representante" maxlength="50"
						style="text-transform: uppercase"
                        disabled="${genericoView.disableRepresentanteMainFields()}"
						value="${genericoView.dto.apellido2Representante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="documentoIdentificacionRepresentante"
						value="DNI/NIE" />
					<p:inputText id="documentoIdentificacionRepresentante"
						disabled="${genericoView.disableRepresentanteMainFields()}"
						style="text-transform: uppercase"
                        maxlength="9"
						value="${genericoView.dto.documentoIdentificacionRepresentante}" />
				</p:panelGrid>

			</p:panelGrid>


			<p:panelGrid columns="2" columnClasses="ui-grid-col-7, ui-grid-col-5"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="domicilioRepresentante" value="Domicilio" />
					<p:inputText id="domicilioRepresentante" maxlength="100"
						value="${genericoView.dto.domicilioRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="localidadRepresentante" value="Localidad" />
					<p:inputText id="localidadRepresentante" maxlength="50"
						style="text-transform: uppercase"
                        value="${genericoView.dto.localidadRepresentante}" />
				</p:panelGrid>

			</p:panelGrid>


			<p:panelGrid columns="6"
				columnClasses="ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="codigoPostalRepresentante"
						value="Código postal" />
					<p:inputText id="codigoPostalRepresentante" maxlength="10"
						value="${genericoView.dto.codigoPostalRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="provinciaRepresentante" value="Provincia" />
					<p:inputText id="provinciaRepresentante" maxlength="50"
						style="text-transform: uppercase"
                        value="${genericoView.dto.provinciaRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="paisRepresentante" value="País" />
					<p:inputText id="paisRepresentante" maxlength="50"
						style="text-transform: uppercase"
                        value="${genericoView.dto.paisRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="telefonoRepresentante" value="Teléfono" />
					<p:inputNumber id="telefonoRepresentante" thousandSeparator=""
						maxlength="11" decimalPlaces="0"
						value="${genericoView.dto.telefonoRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="emailRepresentante" value="Correo electrónico" />
					<p:inputText id="emailRepresentante" maxlength="50"
						value="${genericoView.dto.emailRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="emailConfirmRepresentante"
						value="Confirmación correo electrónico" />
					<p:inputText id="emailConfirmRepresentante" maxlength="50"
						value="${genericoView.dto.transientEmailConfirmationRepresentante}" />
				</p:panelGrid>

			</p:panelGrid>

		</p:fieldset>



		<p:fieldset legend="Persona interesada" style="margin-bottom:20px">

			<p:panelGrid columns="4"
				columnClasses="ui-grid-col-3,ui-grid-col-3,ui-grid-col-3,ui-grid-col-3"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="nombreSolicitante" value="Nombre" />
					<p:inputText id="nombreSolicitante" maxlength="50"
						disabled="${genericoView.disableSolicitanteMainFields()}"
						style="text-transform: uppercase"
						value="${genericoView.dto.nombre}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="apellido1Solicitante" value="Primer apellido" />
					<p:inputText id="apellido1Solicitante" maxlength="50"
						disabled="${genericoView.disableSolicitanteMainFields()}"
						style="text-transform: uppercase"
						value="${genericoView.dto.apellido1}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="apellido2Solicitante" value="Segundo apellido" />
					<p:inputText id="apellido2Solicitante" maxlength="50"
						disabled="${genericoView.disableSolicitanteMainFields()}"
						style="text-transform: uppercase"
						value="${genericoView.dto.apellido2}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="documentoIdentificacionSolicitante"
						value="DNI/NIE" />
					<p:inputText id="documentoIdentificacionSolicitante" maxlength="9"
						style="text-transform: uppercase"
						disabled="${genericoView.disableSolicitanteMainFields()}"
						value="${genericoView.dto.documentoIdentificacion}" />
				</p:panelGrid>

			</p:panelGrid>


			<p:panelGrid columns="2" columnClasses="ui-grid-col-6, ui-grid-col-6"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="domicilio" value="Domicilio" />
					<p:inputText id="domicilio" maxlength="100"
						value="${genericoView.dto.domicilioResidencia}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="localidad" value="Localidad" />
					<p:inputText id="localidad" maxlength="50"
						style="text-transform: uppercase"
						value="${genericoView.dto.localidadResidencia}" />
				</p:panelGrid>

			</p:panelGrid>


			<p:panelGrid columns="6"
				columnClasses="ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="codigoPostal" value="Código postal" />
					<p:inputText id="codigoPostal" maxlength="10"
						value="${genericoView.dto.codigoPostalResidencia}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="provincia" value="Provincia" />
					<p:inputText id="provincia" maxlength="50"
						style="text-transform: uppercase"
						value="${genericoView.dto.provinciaResidencia}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="pais" value="País" />
					<p:inputText id="pais" value="${genericoView.dto.paisResidencia}"
						style="text-transform: uppercase" maxlength="50" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="telefono" value="Teléfono"
						rendered="${!genericoView.showRepresentante()}" />
					<p:inputNumber id="telefono" thousandSeparator="" maxlength="11"
						decimalPlaces="0" rendered="${!genericoView.showRepresentante()}"
						value="${genericoView.dto.telefono}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="email" value="Correo electrónico"
						rendered="${!genericoView.showRepresentante()}" />
					<p:inputText id="email" value="${genericoView.dto.email}"
						maxlength="50" rendered="${!genericoView.showRepresentante()}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="emailConfirm"
						value="Confirmación correo electrónico"
						rendered="${!genericoView.showRepresentante()}" />
					<p:inputText id="emailConfirm"
						value="${genericoView.dto.transientEmailConfirm}" maxlength="50"
						rendered="${!genericoView.showRepresentante()}" />
				</p:panelGrid>

			</p:panelGrid>

		</p:fieldset>


		<p:fieldset legend="Ficheros adjuntos" style="margin-bottom:20px">
			<p:growl id="msg" />
			<h:panelGrid id="pnlAttach" columns="2" cellpadding="5">

				<p:fileUpload id="attachFile" value="#{genericoView.uploadedFile}"
					mode="advanced"
					fileUploadListener="#{genericoView.fileUploadListener}" auto="true"
					multiple="true" cancelLabel="Cancelar" label="Seleccione fichero"
					uploadLabel="Enviar"
					allowTypes="/(\.|\/)(gif|jpe?g|png|pdf|doc|xls|xlsx)$/"
					fileLimit="3" sizeLimit="2048000"
					invalidSizeMessage="El tamaño del fichero es demasiado grande"
					invalidFileMessage="Tipo de fichero no válido"
					fileLimitMessage="No se permite adjuntar más ficheros"
					update="filesDT" />

				<p:dataTable id="filesDT" var="item"
					value="#{genericoView.dto.attachedFiles}">

					<p:column headerText="nombre fichero">
						<h:outputText value="${item.nombreFichero}" />
					</p:column>

					<p:column headerText="nombre documento">
						<h:outputText value="#{item.nombreDocumento}" />
					</p:column>

				</p:dataTable>

			</h:panelGrid>

		</p:fieldset>

		<ui:include src="/WEB-INF/templates/createButtons.xhtml" />

	</ui:define>

	<ui:define name="cancelButton">
		<p:commandButton id="cmdCancel" value="Cancelar" ajax="false"
			action="#{genericoView.cancel}" />
	</ui:define>

	<ui:define name="saveButton">
		<p:commandButton id="cmdSave" value="Registrar la Solicitud"
			ajax="true" actionListener="#{genericoView.save}" process="@form"
			update="@form" />
	</ui:define>

	<ui:define name="defaultButton">
		<p:defaultCommand target="cmdSave" />
	</ui:define>

</ui:composition>





