package es.imserso.sede.web.service.registration.solicitud;

import java.io.IOException;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.TemporadaDTO;
import es.imserso.hermes.session.webservice.dto.TurnoDTO;
import es.imserso.sede.data.dto.impl.TurismoDTO;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.rest.client.HermesRESTClient;
import es.imserso.sede.web.util.route.ParamValues.Params;

@Dependent
class DtoTurismoFactory extends DtoFactory {

	private static final long serialVersionUID = -1724574087978368534L;

	private static final Logger log = Logger.getLogger(DtoFactory.class);

	@Inject
	HermesRESTClient hermesRESTClient;

	public TurismoDTO createDtoInstance() throws IOException {
		TurismoDTO dto = new TurismoDTO() {
			private static final long serialVersionUID = 1L;
		};

		try {

			dto.setCodigoSIA(paramValues.getParamValue(Params.sia).getValue());

			if (usuario != null) {
				dto.setNombre(usuario.getNombre());
				dto.setApellido1(usuario.getApellido1());
				dto.setApellido2(usuario.getApellido2());
				dto.setDocumentoIdentificacion(usuario.getNIF_CIF());
			}

			PlazoDTO plazoDTO = hermesRESTClient.getPlazoTurnoPorDefecto();
			TurnoDTO turnoDTO = hermesRESTClient.getTurnoPorDefecto();
			TemporadaDTO temporadaDTO = hermesRESTClient.getTemporadaTurnoPorDefecto();

			dto.setTemporadaAnho(temporadaDTO.getAnho());
			dto.setTemporadaDescripcion(temporadaDTO.getDescripcion());
			dto.setTurnoNumero(turnoDTO.getNumero());
			dto.setTurnoDescripcion(turnoDTO.getDescripcion());
			dto.setPlazoId(plazoDTO.getId());

		} catch (IOException e) {
			log.error("error al crear una instancia inicializada de TurisoDTO: " + Utils.getExceptionMessage(e));
			throw e;
		}

		return dto;
	}

}
