package es.imserso.sede.data.dto;

import java.math.BigDecimal;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;

/**
 * Datos de un dato económico de una pensión
 * 
 * @author 11825775
 *
 */
public interface DatoEconomicoDTOI {

	/**
	 * @return Clase de pensión (jubilación, enferedad, ...)
	 */
	SimpleDTOI getClasePension();

	/**
	 * @return Procedencia de la pensión
	 */
	SimpleDTOI getProcedenciaPension();

	/**
	 * @return Importe de la pensión (en euros)
	 */
	BigDecimal getImporte();

}
