package es.imserso.sede.service.converter.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;

import org.jboss.logging.Logger;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import es.imserso.sede.service.converter.ConverterException;
import es.imserso.sede.util.cdi.UtilsCDI;

public class PdfUtil {

	private static final Logger log = Logger.getLogger(PdfUtil.class);

	/**
	 * Genera un fichero pdf con los datos de la solicitud a partir de una
	 * plantilla.
	 * 
	 * @param data
	 *            datos que se quieren pasar al pdf
	 * @param plantillaPdf
	 *            plantilla pdf
	 * @return documento pdf binario generado
	 * @throws ConverterException
	 * @throws DocumentException
	 */
	public static byte[] generatePdf(Map<String, String> map, byte[] plantillaPdf) throws ConverterException {
		// plantilla que utilizaremos para rellenar el pdf

		ByteArrayOutputStream baos = null;
		log.info("Inicio generate pdf");
		try {
			PdfReader reader = new PdfReader(plantillaPdf);
			// Definimos baos para que el fichero creado con los datos rellenos
			// lo
			// deje en memoria y no lo grabe en disco
			baos = new ByteArrayOutputStream();

			// Creamos el stamper que contendrá el fichero
			PdfStamper stamper = new PdfStamper(reader, baos);
			stamper.setFormFlattening(false);
			stamper = asignaCampos(stamper, map);

			stamper.close();
			reader.close();
		} catch (IOException | DocumentException e) {
			log.error("No se ha podido generar el pdf: " + e.getMessage());
			throw new ConverterException(e.getMessage());
		}
		log.info("Pdf generado sin errores");
		return baos.toByteArray();
	}

	private static PdfStamper asignaCampos(PdfStamper stamper, Map<String, String> map) throws ConverterException {

		AcroFields acroFields = stamper.getAcroFields();
		Map<String, Item> fieldMap = acroFields.getFields();
		Set<String> keysPlantilla = fieldMap.keySet();
		String fieldName;
		String fieldValue = "";
		Iterator<?> iter;

		// Recorremos el hash
		Iterator<String> itr = map.keySet().iterator();
		try {
			while (itr.hasNext()) {

				fieldName = itr.next();
				iter = keysPlantilla.iterator();

				// miramos si el campo debe pasarse al pdf (no debe estar en la
				// enumeración de campos a obviar, ni empezar por 'emailConfirmation' o
				// 'this'
				if (!(PassThroughFields.isInEnum(fieldName) || fieldName.startsWith("emailConfirmation")
						|| fieldName.endsWith("Dto") || fieldName.startsWith("this"))) {

					// si existe en la plantilla una variable con el mismo
					// nombre
					// que el leído en el hash, se pasa al acrofield.

					if (keysPlantilla.contains(fieldName)) {
						fieldValue = map.get(fieldName);
						while (iter.hasNext()) {
							// si el campo del formulario está en el hashmap de
							// valores a rellenar en el pdf
							if (fieldName.equals((String) iter.next())) {
								// fieldValue=
								// buscarCampoEnFormulario(fieldName);
								acroFields.setField(fieldName, fieldValue);
								acroFields.setFieldProperty(fieldName, fieldValue, PdfFormField.FF_READ_ONLY, null);
							}
						}
					} else {

						if ("true".equals(((ServletContext) UtilsCDI.getBeanByContext(ServletContext.class))
								.getInitParameter("org.jboss.weld.development"))) {
							// ATENCION: esto solamente es para desarrollo,
							// nunca para producción
							CreateTemplate.createPdf(map.keySet());
						}

						log.error("No se ha encontrado el atributo " + fieldName + " en la plantilla");
						throw new ConverterException(
								"No se ha encontrado el atributo " + fieldName + " en la plantilla");
					}
				}
			}
		} catch (IOException | DocumentException e) {
			log.error("No se han podido asignar los atributos a la plantilla: " + e.getMessage());
			throw new ConverterException(e.getMessage());
		}
		return stamper;
	}

	/**
	 * Extrae los datos de la solicitud de un fichero pdf.
	 * 
	 * @param pdf
	 * @return datos del pdf en formato hash
	 * @throws ConverterException
	 */
	public static Hashtable<String, String> extractData(byte[] solicitudPdf) throws ConverterException {
		// pdf ya generado que vamos a utilizar para leer
		Hashtable<String, String> valores = null;
		log.info("Inicio extraer datos pdf");
		try {
			PdfReader reader;

			// reader = new
			// PdfReader("E:/code/plantillaPdf/CartaAcreditaditacionPrueba2.pdf");
			reader = new PdfReader(solicitudPdf);

			AcroFields acroFields = reader.getAcroFields();

			valores = obtenerValores(acroFields);
			reader.close();
		} catch (IOException e) {
			log.error("No se han podido extraer los valores del pdf: " + e.getMessage());
			throw new ConverterException(e.getMessage());
		}
		return valores;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Hashtable<String, String> obtenerValores(AcroFields acroFields) {
		Hashtable<String, String> valores = new Hashtable<>();
		Map<String, Item> fieldMap = acroFields.getFields();
		String fieldName;
		Entry<String, String> entry;

		Iterator<?> iter = fieldMap.entrySet().iterator();
		while (iter.hasNext()) {
			entry = (Entry) iter.next();
			fieldName = (String) entry.getKey();
			valores.put(fieldName, acroFields.getField(fieldName));
		}
		return valores;
	}

	/**
	 * Campos del DTO que debe pasar por alto el conversor.
	 * 
	 * @author 11825775
	 *
	 */
	public enum PassThroughFields {
		serialVersionUID, attachedFiles, log;

		public static boolean isInEnum(String fieldName) {
			for (PassThroughFields p : PassThroughFields.values()) {
				if (p.toString().equalsIgnoreCase(fieldName)) {
					return true;
				}
			}
			return false;
		}
	}

}
