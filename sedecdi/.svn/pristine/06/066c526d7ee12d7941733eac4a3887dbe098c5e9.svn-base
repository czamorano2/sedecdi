package es.imserso.sede.data.dto;

import org.hibernate.validator.constraints.NotBlank;

import es.imserso.sede.data.validation.group.GroupRepresentante;
import es.imserso.sede.data.validation.nif.NifNie;

public interface RepresentanteDTOI extends PersonaInteresadaDTOI {

	@NotBlank(groups = GroupRepresentante.class, message = "El nombre del representante no puede estar vacío")
	String getNombreRepresentante();

	@NotBlank(groups = GroupRepresentante.class, message = "El primer apellido del representante no puede estar vacío")
	String getApellido1Representante();

	@NotBlank(groups = GroupRepresentante.class, message = "El segundo apellido del representante no puede estar vacío")
	String getApellido2Representante();

	@NifNie(message = "El documento de identificación del representante no es válido")
	@NotBlank(groups = GroupRepresentante.class, message = "El documento de identificación del representante no puede estar vacío")
	String getDocumentoIdentificacionRepresentante();

	@NotBlank(groups = GroupRepresentante.class, message = "El teléfono del representante no puede estar vacío")
	String getTelefonoRepresentante();

	@NotBlank(groups = GroupRepresentante.class, message = "El domicilio del representante no puede estar vacío")
	String getDomicilioRepresentante();

	@NotBlank(groups = GroupRepresentante.class, message = "La localidad del representante no puede estar vacío")
	String getLocalidadRepresentante();

	@NotBlank(groups = GroupRepresentante.class, message = "La provincia del representante no puede estar vacía")
	String getProvinciaRepresentante();

	@NotBlank(groups = GroupRepresentante.class, message = "El código postal del representante no puede estar vacío")
	String getCodigoPostalRepresentante();

	@NotBlank(groups = GroupRepresentante.class, message = "El país del representante no puede estar vacío")
	String getPaisRepresentante();

}
