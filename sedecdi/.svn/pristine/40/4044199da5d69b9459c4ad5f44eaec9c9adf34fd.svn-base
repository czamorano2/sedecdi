package es.imserso.sede.web.view.alta;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.hermes.session.webservice.dto.TemporadaDTO;
import es.imserso.hermes.session.webservice.dto.TurnoDTO;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.data.dto.PersonaDTOI;
import es.imserso.sede.data.dto.impl.TurismoDTO;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.impl.turismo.TurismoSolicitudRegistrationQ;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Bean de respaldo para el dto de genérico
 * 
 * @author 11825775
 *
 */
@Named(value = "solicitudTurismoView")
@ViewScoped
@Secure
public class SolicitudTurismoView extends AbstractSolicitudView implements Serializable {

	private static final long serialVersionUID = 6441965433909918407L;

	@Inject
	Logger log;

	@Inject
	@TurismoSolicitudRegistrationQ
	private SolicitudRegistrationI turismoSolicitudRegistration;

	@Inject
	TurismoRepository turismoRepository;

	PlazoDTO plazoDTO;
	TurnoDTO turnoDTO;
	TemporadaDTO temporadaDTO;

	private List<SimpleDTOI> provincias;
	private List<SimpleDTOI> estadosCiviles;
	private List<SimpleDTOI> categoriasFamiliaNumerosa;
	private List<SimpleDTOI> clasesPensiones;
	private List<SimpleDTOI> procedenciasPension;
	private List<SimpleDTOI> opciones;
	private List<String> sexos;

	@PostConstruct
	public void onCreate() {

		try {
			
			super.init();
			log.debugv("onCreate!");
			
			log.debug("obteniendo plazo...");
			plazoDTO = turismoRepository.getPlazoTurnoPorDefecto();
			log.debug("obteniendo turno...");
			turnoDTO = plazoDTO.getTurno();
			log.debug("obteniendo temporada...");
			temporadaDTO = turnoDTO.getTemporada();

			log.debug("obteniendo datos auxiliares...");
			provincias = turismoRepository.getProvincias();
			estadosCiviles = turismoRepository.getEstadosCiviles();
			categoriasFamiliaNumerosa = turismoRepository.getCategoriasFamiliaNumerosa();
			clasesPensiones = turismoRepository.getClasesPensiones();
			procedenciasPension = turismoRepository.getProcedenciasPensiones();
			opciones = turismoRepository.getOpciones();
			sexos = turismoRepository.getSexos();

			// crea la entidad para la solicitud
			setDto(dtoFactory.createTurismoDtoInstance());

			tramite = tramiteRepository.findBySIA(dto.getCodigoSIA());

		} catch (Exception e) {
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.TURISMO,
					"No se pudo crear la instacia del DTO", Utils.getExceptionMessage(e));
		}

	}

	/**
	 * Registra la solicitud en la Sede Electrónica
	 */
	public void save() {
		save(turismoSolicitudRegistration, dto);
	}

	public List<SimpleDTOI> getProvincias() {
		return provincias;
	}

	public List<SimpleDTOI> getEstadosCiviles() {
		return estadosCiviles;
	}

	public List<SimpleDTOI> getCategoriasFamiliaNumerosa() {
		return categoriasFamiliaNumerosa;
	}

	public List<SimpleDTOI> getClasesPensiones() {
		return clasesPensiones;
	}

	public List<SimpleDTOI> getProcedenciasPensiones() {
		return procedenciasPension;
	}

	public List<SimpleDTOI> getOpciones() {
		return opciones;
	}

	public List<String> getSexos() {
		return sexos;
	}

	public PlazoDTO getPlazoTurnoPorDefecto() {
		return plazoDTO;
	}

	public TurnoDTO getTurnoPorDefecto() {
		return turnoDTO;
	}

	public TemporadaDTO getTemporadaTurnoPorDefecto() {
		return temporadaDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.view.SolicitudView#setDTO(es.imserso.sede.data.dto.
	 * PersonaInteresadaDTOI)
	 */
	@Override
	public void setDto(PersonaDTOI personaInteresadaDTO) {
		dto = personaInteresadaDTO;
	}

	public TurismoDTO getDto() {
		return (TurismoDTO) dto;
	}

}