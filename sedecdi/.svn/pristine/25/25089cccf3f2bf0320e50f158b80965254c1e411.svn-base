package es.imserso.sede.process.scheduled.sync;

import java.io.IOException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.hermes.session.webservice.dto.TemporadaDTO;
import es.imserso.hermes.session.webservice.dto.TurnoDTO;
import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.dto.util.DTOUtils;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraDTO;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.service.converter.impl.PdfUtil;
import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.service.registration.registry.ISicres.registers.RegistryServiceI;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSPage;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSPage;
import es.imserso.sede.service.registration.registry.bean.InputRegisterI;
import es.imserso.sede.service.registration.solicitud.SolicitudService;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentosRegistradosManager;
import es.imserso.sede.service.registration.solicitud.documentos.TipoDocumentoRegistrado;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.HermesRESTClient;

/**
 * Esta clase se encarga de sincronizar las solicitudes de la sede con las de
 * Hermes.
 * <p>
 * Busca las solicitudes de la sede que no existan aún en hermes y las da de
 * alta.
 * <p>
 * El criterio para buscar las solicitudes de la sede que no existan aún en
 * hermes es:
 * <ul>
 * <li>que el campo expedienteAplicacionGestora sea nulo</li>
 * <li>que hayan pasado más de 5 minutos desde la fecha de alta de la solicitud
 * </li>
 * </ul>
 * <p>
 * Normalmente esta clase será invocada en el arranque desde un EJB vinculado al
 * servicio Timer del contenedor.
 * 
 * @author 11825775
 *
 */
@Named
public class HermesSolicitudSynchronizer {

	@Inject
	private Logger log;

	@Inject
	private HermesRESTClient hermesRESTClient;

	@Inject
	private RegistryServiceI registryService;

	@Inject
	private SolicitudRepository solicitudRepository;

	@Inject
	SolicitudService solicitudService;

	/**
	 * sincroniza las solicitudes de la sede con las de Hermes cada 5 minutos Es
	 * invocado desde el StartupManager
	 */
	public void synchonize() {
		doSynchonize();
	}

	public void doSynchonize() {
		log.debug("obteniendo las solicitudes de turismo en la sede que no existan en Hermes...");
		List<Solicitud> list = solicitudRepository.findAllHermesUnsynchronized();
		if (list.size() > 0) {
			log.debug("se han encontrado " + list.size() + " de turismo solicitudes para sincronizar");

			// damos de alta en hermes las solicitudes obtenidas
			for (Solicitud solicitud : list) {

				try {
					DocumentosRegistradosManager adjuntos = solicitudService.getDocumentosSolicitud(solicitud);

					log.info("obtenemos el DTO...");
					Hashtable<String, String> solicitudTurismoHash = PdfUtil
							.extractData(adjuntos.getDocumentoSolicitud().getFichero());

					SolicitudTurismoDTO solicitudTurismoDTO = (SolicitudTurismoDTO) DTOUtils
							.getSolicitudTurismoDTO(solicitudTurismoHash, new SolicitudTurismoDTO());

					log.info(solicitudTurismoDTO.getCodigoSIA());
					if (solicitudTurismoDTO.getPlazoId() == null) {
						continue;
					}
					SolicitudTurismoDTO responseSolicitudTurismoDTO = hermesRESTClient
							.createRemoteSolicitud(solicitudTurismoDTO);

					if (responseSolicitudTurismoDTO != null) {
						log.info("responseSolicitudTurismoDTO OK!!");

						// actualizamos el campo expedienteAplicacionGestora
						// de
						// la solicitud
						TurismoExpedienteAplicacionGestoraDTO turismoExpedienteAplicacionGestoraDTO = new TurismoExpedienteAplicacionGestoraDTO(
								responseSolicitudTurismoDTO.getId(), responseSolicitudTurismoDTO.getNumeroOrden());
						// esta parte contiene el dto
						ObjectMapper mapper = new ObjectMapper();

						// JSON from String to Object
						String jsonInString = mapper.writeValueAsString(turismoExpedienteAplicacionGestoraDTO);
						solicitudRepository.updateExpedienteAplicacionGestora(solicitud.getId(), jsonInString);

						log.info("Se ha sincronizado la solicitud con numero de registro (ISicres) "
								+ solicitud.getIdentificadorRegistro().getRegisterNumber());

					} else {
						log.warn("responseSolicitudTurismoDTO KO!!");
					}

				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SedeException
						| ParseException | IOException e) {

					log.error("no se pudo sincronizar la solicitud con id=" + solicitud.getId() + ": "
							+ Utils.getExceptionMessage(e));
				}

			}

		} else {
			log.debug("no se han encontrado solicitudes de turismo para sincronizar");
		}

	}

	/**
	 * @param bookIdentification
	 *            identificador del libro de entrada
	 * 
	 * @param registrationNumber
	 *            número de registro (si, por ejemplo, el número en el registro es
	 *            201620000000015, hay que poner 15)
	 * @return posicion en la lista de documentos de la entrada donde se encuentra
	 *         el documento con el nombre 'formularioSolicitud'
	 * @throws RegistrationException
	 */
	private int getSolicitudDocumentPosition(int bookIdentification, int registrationNumber)
			throws RegistrationException {
		InputRegisterI inputRegister = registryService.getInputRegister(bookIdentification, registrationNumber);
		ArrayOfWSDocument documents = inputRegister.getDocuments();
		List<WSDocument> list = documents.getWSDocument();
		int i = 0;
		for (WSDocument document : list) {
			log.info(document.getName());
			i++;
			if (document.getName().equals(TipoDocumentoRegistrado.SOLICITUD.getNombreDocumento())) {
				log.info(
						"se ha encontrado en el registro el formulario con los datos de la solicitud con número de registro "
								+ registrationNumber);
				ArrayOfWSPage pageList = document.getPages();
				List<WSPage> pages = pageList.getWSPage();
				for (WSPage page : pages) {
					log.info(page.getName());
					return i;
				}

			}

		}
		return -1;
	}

}
