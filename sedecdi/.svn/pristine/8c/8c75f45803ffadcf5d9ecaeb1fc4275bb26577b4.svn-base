<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:o="http://omnifaces.org/ui"
	xmlns:of="http://omnifaces.org/functions"
	xmlns:p="http://primefaces.org/ui"
	xmlns:c="http://java.sun.com/jsp/jstl/core">


<h:head>
	<f:metadata>
		<f:attribute name="content" value="text/html; utf-8" />
	</f:metadata>
	<style type="text/css">
.ui-widget {
	font-size: 90%;
}

.ui-widget-overlay {
	opacity: 0.5;
}

.highlight1 {
	background: #ffe5e5 !important;
}

.highlight2 {
	background: #e9ffe2 !important;
	font-weight: bold;
}
</style>
</h:head>

<h:body>

	<p:messages id="msgs" autoUpdate="true" />

	<p:growl id="growlMsg" autoUpdate="true" showDetail="true"
		showSummary="true" globalOnly="true" life="5000" />

	<h:form id="form">

		<p:importEnum
			type="es.imserso.hermes.session.webservice.dto.TipoDocumentoRegistrado"
			var="tipoDocumentoRegistrado" allSuffix="ALL_ENUM_VALUES" />

		<p:panel id="pnlRoot"
			header="#{solicitudesTurismoUsuarioView.paramValues.consulta ? 'Consulta' : 'Modificación'} de expedientes de turismo para mayores de la temporada actual"
			style="margin-bottom:20px; text-align:center">

			<p:panelGrid columns="2" layout="grid"
				style="margin:20px; font-size:90%;">
				<h:outputText
					title="Documento de identificación del solicitante o del cónyuge"
					value="NIF/NIE: #{solicitudesTurismoUsuarioView.usuario.NIF_CIF}" />
				<h:outputText title="Nombre del solicitante o del cónyuge"
					value="Nombre: #{solicitudesTurismoUsuarioView.usuario.nombreCompleto}" />
			</p:panelGrid>


			<p:separator />


			<p:dataTable id="tbl" var="solicitud" liveResize="true"
				value="#{solicitudesTurismoUsuarioView.list}" rowIndexVar="rowIndex"
				style="width:50%; margin:20px; font-size:70%; text-align:center;"
				sortBy="#{solicitud.id}" sortOrder="descending"
				emptyMessage="No se han encontrado solicitudes" paginator="true"
				pageLinks="50"
				rowStyleClass="#{solicitud.estado eq 'ERRONEO' ? null : 'highlight2'}">

				<p:column headerText="Expediente" width="100">
					<p:commandLink update=":detailDlg"
						oncomplete="PF('editDialog').show()"
						title="#{solicitudesTurismoUsuarioView.paramValues.consulta ? 'consultar expediente' : 'editar expediente'}">
						<f:setPropertyActionListener value="#{solicitud}"
							target="#{solicitudesTurismoUsuarioView.selectedSolicitud}" />
						<h:outputText value="#{solicitud.numeroOrden}"
							title="#{solicitudesTurismoUsuarioView.paramValues.consulta ? 'consultar expediente' : 'editar expediente'}" />
					</p:commandLink>
				</p:column>

				<p:column headerText="Estado">
					<h:outputText value="#{solicitud.estado}"
						title="Estado del expediente" />
				</p:column>

				<p:column headerText="Acreditado">

					<p:commandLink ajax="false"
						rendered="#{solicitud.acreditada and solicitudesTurismoUsuarioView.habilitadaDescargaCartaAcreditacion}">
						<f:setPropertyActionListener value="#{solicitud}"
							target="#{solicitudesTurismoUsuarioView.selectedSolicitud}" />
						<p:fileDownload
							value="#{solicitudesTurismoUsuarioView.pdfCartaAcreditacion}" />
						<p:graphicImage library="forge" name="true.png"
							title="descargar la carta de acreditación" />
					</p:commandLink>

					<p:graphicImage library="forge" name="true.png"
						rendered="#{solicitud.acreditada and not solicitudesTurismoUsuarioView.habilitadaDescargaCartaAcreditacion}"
						title="el expediente está acreditado" />

					<p:graphicImage library="forge" name="false.png"
						rendered="#{not solicitud.acreditada}"
						title="el expediente no está acreditado" />
				</p:column>



			</p:dataTable>
		</p:panel>
	</h:form>



	<p:dialog
		header="Modificación de expedientes de turismo para mayores para la temporada actual"
		widgetVar="editDialog" id="detailDlg" modal="true" showEffect="fade"
		hideEffect="fade" resizable="false" positionType="absolute">

		<o:form id="formEdit" name="formEdit" method="post"
			includeRequestParams="true" acceptcharset="UTF-8" lang="es">

			<f:validateBean disabled="true">

				<p:outputPanel id="mainPanel">

					<p:fieldset legend="Datos de la persona solicitante"
						style="margin-bottom:20px">

						<p:panelGrid columns="3"
							columnClasses="ui-grid-col-4,ui-grid-col-4,ui-grid-col-4"
							layout="grid" styleClass="ui-fluid ui-noborder">

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<p:outputLabel for="apellido1" value="Primer Apellido" />
								<p:inputText id="apellido1" maxlength="35" widgetVar="ap1"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									label="Primer Apellido" style="text-transform: uppercase"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.apellido1}" />
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="apellido2" value="Segundo Apellido" />
								<p:inputText id="apellido2" maxlength="35"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									label="Segundo Apellido" style="text-transform: uppercase"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.apellido2}" />
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="nombre" value="Nombre" />
								<p:inputText id="nombre" maxlength="35" label="Nombre"
									style="text-transform: uppercase"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.nombre}" />
							</p:panelGrid>

						</p:panelGrid>


						<p:panelGrid columns="2"
							columnClasses="ui-grid-col-6,ui-grid-col-6" layout="grid"
							styleClass="ui-fluid ui-noborder">

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="solicitanteSexo" value="Sexo" />
								<p:selectOneMenu id="solicitanteSexo"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.sexo}">
									<f:selectItem itemLabel="" itemValue="#{null}"
										noSelectionOption="true" />
									<f:selectItems value="#{solicitudesTurismoUsuarioView.sexos}" />
								</p:selectOneMenu>
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="solicitanteEstadoCivil" value="Estado Civil" />
								<p:selectOneMenu id="solicitanteEstadoCivil"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.estadoCivilDto}"
									converter="simpleDtoConverter">
									<f:selectItem itemLabel="" itemValue="#{null}"
										noSelectionOption="true" />
									<f:selectItems
										value="#{solicitudesTurismoUsuarioView.estadosCiviles}"
										var="_var" itemLabel="#{_var.descripcion}" itemValue="#{_var}" />
								</p:selectOneMenu>
							</p:panelGrid>

						</p:panelGrid>




						<p:panelGrid columns="1" columnClasses="ui-grid-col-12"
							layout="grid" styleClass="ui-fluid ui-noborder">

							<o:outputLabel for="domicilio" value="Domicilio" />
							<p:inputText id="domicilio" maxlength="35"
								disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
								value="#{solicitudesTurismoUsuarioView.selectedSolicitud.domicilio}" />

						</p:panelGrid>



						<p:panelGrid columns="3"
							columnClasses="ui-grid-col-4,ui-grid-col-4,ui-grid-col-4"
							layout="grid" styleClass="ui-fluid ui-noborder">

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="localidad" value="Localidad" />
								<p:inputText id="localidad" maxlength="35"
									style="text-transform: uppercase"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.localidad}" />
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="codigoPostal" value="Código Postal" />
								<p:inputText id="codigoPostal" maxlength="5"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.codigoPostal}" />
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="provinciaId" value="Provincia" />
								<p:selectOneMenu id="provinciaId"
									disabled="#{solicitudesTurismoUsuarioView.selectedSolicitud.acreditada or solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.provinciaDto}"
									converter="simpleDtoConverter">
									<f:selectItem itemLabel="" itemValue="#{null}"
										noSelectionOption="true" />
									<f:selectItems
										value="#{solicitudesTurismoUsuarioView.provinciaList}"
										var="_var" itemLabel="#{_var.descripcion}" itemValue="#{_var}" />
								</p:selectOneMenu>
							</p:panelGrid>

						</p:panelGrid>


						<p:panelGrid columns="2"
							columnClasses="ui-grid-col-6,ui-grid-col-6" layout="grid"
							styleClass="ui-fluid ui-noborder">

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="telefono1" value="Teléfono 1" />
								<p:inputText id="telefono1" maxlength="35"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.telefono}" />
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="telefono2" value="Teléfono 2" />
								<p:inputText id="telefono2" maxlength="35"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.telefono2}" />
							</p:panelGrid>
						</p:panelGrid>

					</p:fieldset>





					<p:fieldset legend="Datos del cónyuge o pareja de hecho"
						style="margin-bottom:20px">

						<p:panelGrid columns="3"
							columnClasses="ui-grid-col-4,ui-grid-col-4,ui-grid-col-4"
							layout="grid" styleClass="ui-fluid ui-noborder">

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="conyugePrimerApellido"
									value="Primer Apellido" />
								<p:inputText id="conyugePrimerApellido" maxlength="35"
									style="text-transform: uppercase"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.conyugePrimerApellido}" />
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="conyugeSegundoApellido"
									value="Segundo Apellido" />
								<p:inputText id="conyugeSegundoApellido" maxlength="35"
									style="text-transform: uppercase"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.conyugeSegundoApellido}" />
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="conyugeNombre" value="Nombre" />
								<p:inputText id="conyugeNombre" maxlength="35"
									style="text-transform: uppercase"
									disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.conyugeNombre}" />
							</p:panelGrid>

						</p:panelGrid>
					</p:fieldset>



					<p:fieldset legend="Plazas solicitadas" style="margin-bottom:20px">

						<p:panelGrid columns="1" columnClasses="ui-grid-col-12"
							layout="grid" styleClass="ui-fluid ui-noborder">

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="peticion1OpcionId" value="Primer destino" />
								<p:selectOneMenu id="peticion1OpcionId"
									disabled="#{solicitudesTurismoUsuarioView.selectedSolicitud.acreditada  or solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.peticion1OpcionDto}"
									converter="simpleDtoConverter">
									<f:selectItem itemLabel="" itemValue="#{null}"
										noSelectionOption="true" />
									<f:selectItems
										value="#{solicitudesTurismoUsuarioView.opciones}" var="_var"
										itemLabel="#{_var.descripcion}" itemValue="#{_var}" />
								</p:selectOneMenu>
							</p:panelGrid>

							<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
								<o:outputLabel for="peticion2OpcionId" value="Segundo Destino" />
								<p:selectOneMenu id="peticion2OpcionId"
									disabled="#{solicitudesTurismoUsuarioView.selectedSolicitud.acreditada or solicitudesTurismoUsuarioView.modoConsulta}"
									value="#{solicitudesTurismoUsuarioView.selectedSolicitud.peticion2OpcionDto}"
									converter="simpleDtoConverter">
									<f:selectItem itemLabel="" itemValue="#{null}"
										noSelectionOption="true" />
									<f:selectItems
										value="#{solicitudesTurismoUsuarioView.opciones}" var="_var"
										itemLabel="#{_var.descripcion}" itemValue="#{_var}" />
								</p:selectOneMenu>
							</p:panelGrid>

						</p:panelGrid>

					</p:fieldset>


					<p:spacer />

					<p:panelGrid id="cmdPanel" columns="2"
						columnClasses="ui-grid-col-6,ui-grid-col-6" layout="grid"
						styleClass="ui-fluid ui-noborder" cellpadding="30">

						<p:commandButton id="cmdCancel" value="Cerrar" ajax="true"
							actionListener="#{solicitudesTurismoUsuarioView.cancel}"
							oncomplete="PF('editDialog').hide()" />

						<p:commandButton id="cmdSave" value="Modificar el expediente"
							ajax="true"
							disabled="#{solicitudesTurismoUsuarioView.modoConsulta}"
							actionListener="#{solicitudesTurismoUsuarioView.save}" />

					</p:panelGrid>


					<p:defaultCommand target="cmdSave" />


					<p:ajaxStatus onstart="PF('bui').show()"
						oncomplete="PF('bui').hide()" />

					<p:ajaxStatus onstart="PF('buicmd').show()"
						oncomplete="PF('buicmd').hide()" />

				</p:outputPanel>


			</f:validateBean>

			<p:blockUI block="mainPanel" widgetVar="bui">...<br />
				<p:graphicImage library="demo" name="/images/ajaxloadingbar.gif" />
			</p:blockUI>

			<p:blockUI block="cmdPanel" widgetVar="buicmd">Actualizando expediente<br />
				<p:graphicImage library="demo" name="/images/ajaxloadingbar.gif" />
			</p:blockUI>


		</o:form>
	</p:dialog>



</h:body>
</html>
