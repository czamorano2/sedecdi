package es.imserso.sede.web.auth.secure;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.TramiteUCM;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.UCMService;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;
import es.imserso.sede.util.resources.ResourceQ;
import es.imserso.sede.util.route.ParamValues;
import es.imserso.sede.util.route.ParamValues.Params;

/**
 * Comprueba que la petición esté autorizada debidamente.
 * 
 * @author 11825775
 *
 */
@Interceptor
@Secure
public class AuthenticatorInterceptor implements Serializable {

	private static final long serialVersionUID = -6604331159623501394L;

	private Logger log = Logger.getLogger(AuthenticatorInterceptor.class);

	/**
	 * Proporciona información acerca de los trámites, como si un trámite requiere
	 * autenticación o no.
	 */
	@Inject
	UCMService ucmService;

	@Inject
	Event<EventMessage> eventMessage;

	/**
	 * Datos del usuario obtenidos de la cookie
	 * <p>
	 * Si la petición no incluye la cookie <code>usuario</code> será null
	 */
	@Inject
	@ResourceQ
	Usuario usuario;

	/**
	 * Datos relevantes especificados en los parámetros
	 */
	@Inject
	ParamValues paramValues;

	@PostConstruct
	public void injectDependencies(InvocationContext ctx) throws Exception {

		try {
			log.info("AuthenticatorInterceptor ..........................................................");

			if (paramValues == null) {
				String errmsg = "No se han podido obtener los parámetros de la petición";
				eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
				throw new SedeRuntimeException(errmsg);
			}

			if (paramValues.isConsulta()) {
				// no necesita el parámetro SIA
				if (paramValues.isAuthenticationRequired()) {
					assertUserNotNull();
				}

			} else {

				if (!paramValues.getParamValue(Params.sia).isPresent()) {
					String errmsg = "No se encuentra el parámetro sia en la petición!";
					eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, String.valueOf(errmsg)));
					log.warn(errmsg);
					throw new SedeRuntimeException(errmsg);

				} else {

					TramiteUCM tramiteUCM = ucmService.getTramiteUCM(paramValues.getParamValue(Params.sia).getValue());
					if (tramiteUCM == null) {
						throw new SedeRuntimeException("No se han podido obtener del UCM los datos del trámite");
					}
					log.info("Trámite: " + tramiteUCM.getDenominacion());

					// si el UCM dice que requiere autenticación debe venir la cookie de usuario
					// si el UCM dice que NO requiere autenticación, da igual que venga o no cookie
					// de usuario
					if (tramiteUCM.isExigeautenticacionclave() && !paramValues.isAuthenticationRequired()) {
						String errmsg = "Conflicto de autenticación: el servicio web del UCM indica que se requiere autenticación, pero el parámetro aut de la URL indica lo contrario";
						eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
						log.error(errmsg);
						throw new SedeRuntimeException(errmsg);
					}

					if (tramiteUCM.isExigeautenticacionclave()) {
						log.info("El trámite requiere autenticación");
						assertUserNotNull();
					} else {
						log.info("AUTORIZADO: no requiere autenticación");
					}

					// TODO se podrían realizar más comprobaciones, como si está
					// entre las fechas de apertura y cierre, si está cerrado
					// temporalmente, ...

					if (tramiteUCM.isCerradotemporalmente()) {
						String errmsg = "El trámite está cerrado temporalmente";
						eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_WARN, errmsg));
						log.warn(errmsg);
						throw new SedeRuntimeException(errmsg);
					}

				}
			}

			log.info("proceed ...");
			ctx.proceed();

		} catch (Exception e) {
			String errmsg = "Se ha producido un error al comprobar si la petición está debidamente autorizada: "
					+ Utils.getExceptionMessage(e);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_WARN, errmsg));
			log.error(errmsg);
			throw new SedeRuntimeException(errmsg);
		}
	}

	private void assertUserNotNull() {
		if (usuario == null) {
			String errmsg = "se requiere autenticación y no viene la cookie de usuario";
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_WARN, errmsg));
			log.warn(errmsg);
			throw new SedeRuntimeException(errmsg);
		} else {
			log.debug(usuario.getNombreCompleto());
			log.info("AUTORIZADO: cookie usuario");
		}
	}

}
