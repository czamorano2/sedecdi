package es.imserso.sede.data;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.sede.data.dto.rest.termalismo.EstadoCivilVOWS;
import es.imserso.sede.data.dto.rest.termalismo.ProvinciaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.SexoVOWS;
import es.imserso.sede.service.cache.hermes.CrunchifyInMemoryCache;
import es.imserso.sede.util.rest.client.TermalismoRESTClient;

/**
 * Gestiona los datos que se obtienen de Termalismo de forma remota.
 * <p>
 * Precisamente por obtenerse de forma remota y el coste que eso conlleva, este
 * componente optimiza los recursos utilizando una caché para aquellos datos que
 * apenas son modificados (provincias, estados civiles, etc)
 * 
 *
 */
@ApplicationScoped
public class TermalismoRemoteRepository {

	private static final int HOURS_1 = 3600;

	@Inject
	Instance<TermalismoRESTClient> termalismoRestClientInstance;

	CrunchifyInMemoryCache<CachesTermalismo, List<EstadoCivilVOWS>> termalismoEstadosCiviles;
	CrunchifyInMemoryCache<CachesTermalismo, List<SexoVOWS>> termalismoSexos;
	CrunchifyInMemoryCache<CachesTermalismo, List<ProvinciaVOWS>> termalismoProvincias;

	@PostConstruct
	public void init() {
		termalismoEstadosCiviles = new CrunchifyInMemoryCache<CachesTermalismo, List<EstadoCivilVOWS>>(HOURS_1, HOURS_1,
				10);
		termalismoSexos = new CrunchifyInMemoryCache<CachesTermalismo, List<SexoVOWS>>(HOURS_1, HOURS_1, 10);
		termalismoProvincias = new CrunchifyInMemoryCache<CachesTermalismo, List<ProvinciaVOWS>>(HOURS_1, HOURS_1, 10);

	}

	public List<EstadoCivilVOWS> getEstadosCiviles() throws JsonParseException, JsonMappingException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {
		List<EstadoCivilVOWS> list = termalismoEstadosCiviles.get(CachesTermalismo.estados_civiles);
		if (list == null) {
			list = convertirDTOEstadoCivil(termalismoRestClientInstance.get().getEstadosCiviles());
			termalismoEstadosCiviles.put(CachesTermalismo.estados_civiles, list);
		}
		return list;
	}

	private List<EstadoCivilVOWS> convertirDTOEstadoCivil(List<EstadoCivilVOWS> list)
			throws JsonParseException, JsonMappingException, IOException {
		EstadoCivilVOWS estado = null;
		ObjectMapper mapper = new ObjectMapper();
		List<EstadoCivilVOWS> estados = new ArrayList<EstadoCivilVOWS>();
		for (Object o : list) {
			estado = mapper.readValue(mapper.writeValueAsString(o), EstadoCivilVOWS.class);
			estados.add(estado);
		}
		return estados;
	}

	public List<SexoVOWS> getSexos() throws JsonParseException, JsonMappingException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		List<SexoVOWS> list = termalismoSexos.get(CachesTermalismo.sexos);
		if (list == null) {
			list = convertirDTOSexo(termalismoRestClientInstance.get().getSexos());
			termalismoSexos.put(CachesTermalismo.sexos, list);
		}
		return list;
	}

	private List<SexoVOWS> convertirDTOSexo(List<SexoVOWS> list)
			throws JsonParseException, JsonMappingException, IOException {
		SexoVOWS estado = null;
		ObjectMapper mapper = new ObjectMapper();
		List<SexoVOWS> estados = new ArrayList<SexoVOWS>();
		for (Object o : list) {
			estado = mapper.readValue(mapper.writeValueAsString(o), SexoVOWS.class);
			estados.add(estado);
		}
		return estados;
	}

	public List<ProvinciaVOWS> getProvincias() throws JsonParseException, JsonMappingException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		List<ProvinciaVOWS> list = termalismoProvincias.get(CachesTermalismo.provincias);
		if (list == null) {
			list = convertirDTOProvincia(termalismoRestClientInstance.get().getProvincias());
			termalismoProvincias.put(CachesTermalismo.provincias, list);
		}
		return list;
	}

	private List<ProvinciaVOWS> convertirDTOProvincia(List<ProvinciaVOWS> list)
			throws JsonParseException, JsonMappingException, IOException {
		ProvinciaVOWS estado = null;
		ObjectMapper mapper = new ObjectMapper();
		List<ProvinciaVOWS> estados = new ArrayList<ProvinciaVOWS>();
		for (Object o : list) {
			estado = mapper.readValue(mapper.writeValueAsString(o), ProvinciaVOWS.class);
			estados.add(estado);
		}
		return estados;
	}

}
