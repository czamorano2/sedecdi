package es.imserso.sede.service.monitor.isicres.app;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import es.imserso.sede.service.monitor.AbstractAppInfo;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.service.monitor.isicres.ISicresQ;
import es.imserso.sede.service.registration.registry.ISicres.books.BookRegistryServiceImpl;
import es.imserso.sede.util.exception.SedeException;

/**
 * Información sobre el registro electrónico ISicres.
 * 
 * @author 11825775
 *
 */
@ISicresQ
@Dependent
public class ISicresAppInfo extends AbstractAppInfo {

	private static final long serialVersionUID = -5366854848432414095L;

	public static final String WEB_SITE_URL = "http://10.75.5.31/ISicres/default.jsp";

	@Inject
	BookRegistryServiceImpl bookRegistryService;

	@PostConstruct
	public void onCreate() {
		services = new ArrayList<ServiceInfo>();
	}

	@Override
	public String getWebSiteURL() {
		return WEB_SITE_URL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getName()
	 */
	@Override
	public String getName() {
		return "inveSicres";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Sistema de Registro Entrada/Salida";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#webServicesActive()
	 */
	@Override
	public Boolean webServicesActive() throws SedeException {
		return (!bookRegistryService.getInputBooks().isEmpty());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.AppInfo#getServices()
	 */
	@Override
	public List<ServiceInfo> getServices() {
		return services;
	}

}
