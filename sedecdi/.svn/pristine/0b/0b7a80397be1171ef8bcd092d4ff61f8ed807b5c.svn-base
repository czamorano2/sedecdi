package es.imserso.sede.web.view.consulta.turismo;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.resources.ResourceQ;
import es.imserso.sede.util.rest.client.HermesRESTClient;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.DtoFactory;
import es.imserso.sede.web.service.registration.solicitud.DtoFactoryQ;
import es.imserso.sede.web.util.route.ParamValues;

/**
 * Muestra las solicitudes del usuario de la temporada actual dadas de alta en
 * Hermes
 * 
 * @author 11825775
 *
 */
@Named("solicitudesTurismoUsuarioView")
@ViewScoped
@Secure
public class SolicitudesTurismoUsuarioView implements Serializable {

	private static final long serialVersionUID = 4669574966404541810L;

	@Inject
	Logger log;

	@Inject
	HermesRESTClient hermesRESTClient;

	// @Inject
	// SolicitudService solicitudService;

	@Inject
	@ResourceQ
	private Usuario usuario;

	@Inject
	private ParamValues paramValues;

	@Inject
	TurismoRepository turismoRepository;

	@Inject
	@DtoFactoryQ
	DtoFactory dtoFactory;

	private SolicitudTurismoDTO selectedSolicitud;

	private StreamedContent pdfCartaAcreditacion;
	private boolean habilitadaDescargaCartaAcreditacion;

	/**
	 * lista de solicitudes del usuario
	 */
	private List<SolicitudTurismoDTO> list;

	private List<SimpleDTOI> provinciaList;
	private List<SimpleDTOI> estadosCiviles;
	private List<SimpleDTOI> opciones;
	private List<String> sexos;

	@PostConstruct
	public void onCreate() {
		log.debug("PostConstruct...");
		loadLists();

	}

	/**
	 * carga la lista de solicitudes de turismo del usuario
	 */
	public void loadLists() {
		try {
			log.debugv("se van a obtener de Hermes las solicitudes del DI {0} ...", usuario.getNIF_CIF());
			list = hermesRESTClient.getRemoteSolicitudesByDI(usuario.getNIF_CIF());
			if (list == null) {
				String errmsg = "No se han podido obtener las solicitudes de turismo del documento  "
						+ usuario.getNIF_CIF();
				log.warn(errmsg);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Solicitudes de Turismo", errmsg));
			} else {
				log.infov("se han obtenido de Hermes {0} solicitudes del DI {1}", list.size(), usuario.getNIF_CIF());
			}

			setProvinciaList(turismoRepository.getProvincias());
			setEstadosCiviles(turismoRepository.getEstadosCiviles());
			setOpciones(turismoRepository.getOpciones());
			setSexos(turismoRepository.getSexos());

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Solicitudes de Turismo", errmsg));
		}
	}

	/**
	 * Actualiza la solicitud con las modificaciones realizadas por el usuario
	 */
	public void save() {
		log.info("guardando solicitud en Hermes...");

		// TODO actualizar solicitud en Hermes

		log.infov("solicitud {0} sido modificada satisfactoriamente", selectedSolicitud.getId());
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Modificación de expedientes de Turismo",
				"Su solicitud ha sido modificada satisfactoriamente");
		RequestContext.getCurrentInstance().showMessageInDialog(message);
	}

	public void cancel() {
		log.info("el usuario ha cancelado la modificación de la solicitud de turismo!");
	}

	public List<SolicitudTurismoDTO> getList() {
		return list == null ? new ArrayList<SolicitudTurismoDTO>() : list;
	}

	public void setList(List<SolicitudTurismoDTO> list) {
		this.list = list;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the selectedSolicitud
	 */
	public SolicitudTurismoDTO getSelectedSolicitud() {
		return selectedSolicitud;
	}

	/**
	 * @param selectedSolicitud
	 *            the selectedSolicitud to set
	 */
	public void setSelectedSolicitud(SolicitudTurismoDTO selectedSolicitud) {
		this.selectedSolicitud = selectedSolicitud;
	}

	public List<SimpleDTOI> getProvinciaList() {
		return provinciaList;
	}

	public void setProvinciaList(List<SimpleDTOI> provinciaList) {
		this.provinciaList = provinciaList;
	}

	public List<SimpleDTOI> getEstadosCiviles() {
		return estadosCiviles;
	}

	public void setEstadosCiviles(List<SimpleDTOI> estadosCiviles) {
		this.estadosCiviles = estadosCiviles;
	}

	public List<String> getSexos() {
		return sexos;
	}

	public void setSexos(List<String> sexos) {
		this.sexos = sexos;
	}

	public List<SimpleDTOI> getOpciones() {
		return opciones;
	}

	public void setOpciones(List<SimpleDTOI> opciones) {
		this.opciones = opciones;
	}

	public ParamValues getParamValues() {
		return paramValues;
	}

	public void setParamValues(ParamValues paramValues) {
		this.paramValues = paramValues;
	}

	public StreamedContent getPdfCartaAcreditacion() {
		this.pdfCartaAcreditacion = null;
		
		try {
			this.pdfCartaAcreditacion = new DefaultStreamedContent(
					new ByteArrayInputStream(turismoRepository.getCartaAcreditacion(selectedSolicitud.getId())));
			
		} catch (SedeException e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"No se pudo obtener la carta de acreditación", errmsg));
		}
		
		return pdfCartaAcreditacion;
	}

	public void setPdfCartaAcreditacion(StreamedContent pdfCartaAcreditacion) {
		this.pdfCartaAcreditacion = pdfCartaAcreditacion;
	}

	public boolean isHabilitadaDescargaCartaAcreditacion() {
		this.habilitadaDescargaCartaAcreditacion = turismoRepository.isCartaAcreditaciónAccesible();
		return habilitadaDescargaCartaAcreditacion;
	}

	public void setHabilitadaDescargaCartaAcreditacion(boolean habilitadaDescargaCartaAcreditacion) {
		this.habilitadaDescargaCartaAcreditacion = habilitadaDescargaCartaAcreditacion;
	}

}
