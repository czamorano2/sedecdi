package es.imserso.sede.web.view.consulta;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.registration.solicitud.SolicitudService;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentoRegistrado;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentosRegistradosManager;
import es.imserso.sede.service.registration.solicitud.documentos.TipoDocumentoRegistrado;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.resources.ResourceQ;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Muestra las solicitudes del usuario dadas de alta en la Sede Electrónica
 * 
 * @author 11825775
 *
 */
@Named("solicitudesUsuarioView")
@ViewScoped
@Secure
public class SolicitudesUsuarioView implements Serializable {

	private static final long serialVersionUID = 4669574960404541810L;

	@Inject
	Logger log;

	@Inject
	SolicitudRepository solicitudRepository;

	@Inject
	SolicitudService solicitudService;

	@Inject
	@ResourceQ
	private Usuario usuario;

	private Solicitud selectedSolicitud;
	private Solicitud selectedSolicitudFull;

	private DocumentosRegistradosManager adjuntos;

	private StreamedContent pdfSolicitud;
	private StreamedContent pdfSolicitudAdjuntada;
	private StreamedContent pdfJustificante;
	private List<StreamedContent> pdfPersonales;

	/**
	 * lista de solicitudes del usuario
	 */
	private List<Solicitud> list;

	@PostConstruct
	public void onCreate() {
		log.debug("PostConstruct...");
		search();
	}

	/**
	 * carga la lista de solicitudes del usuario
	 */
	public void search() {
		try {
			log.debug("searching...");
			list = solicitudRepository.getSolicitudesUsuario(usuario.getNIF_CIF());

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Solicitudes Sede Electrónica", errmsg));
		}
	}

	/**
	 * Obtiene una solicitud completa del repositorio
	 */
	public void loadDetail() {
		try {
			Long idSolicitud = selectedSolicitud.getId();
			selectedSolicitudFull = solicitudRepository.getSolicitudUsuario(idSolicitud);
			if (selectedSolicitudFull == null) {
				ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.PROPOSITO_GENERAL,
						"No se pudo recuperar la solicitud",
						"No se encuentra la solicitud con identificador " + idSolicitud);
			}
			this.adjuntos = solicitudService.getDocumentosSolicitud(selectedSolicitudFull);

			this.pdfSolicitud = new DefaultStreamedContent(
					new ByteArrayInputStream(this.adjuntos.getDocumentoSolicitud().getFichero()), "application/pdf",
					TipoDocumentoRegistrado.SOLICITUD.getNombreDocumento() + ".pdf");

			if (this.adjuntos.getDocumentoSolicitudAdjunta() != null) {
				this.pdfSolicitudAdjuntada = new DefaultStreamedContent(
						new ByteArrayInputStream(this.adjuntos.getDocumentoSolicitudAdjunta().getFichero()),
						"application/pdf", TipoDocumentoRegistrado.SOLICITUD_ADJUNTA.getNombreDocumento() + ".pdf");
			}
			this.pdfJustificante = new DefaultStreamedContent(
					new ByteArrayInputStream(this.adjuntos.getDocumentoJustificante().getFichero()), "application/pdf",
					TipoDocumentoRegistrado.JUSTIFICANTE.getNombreDocumento() + ".pdf");

			this.pdfPersonales = new ArrayList<StreamedContent>();
			for (DocumentoRegistrado doc : this.adjuntos.getDocumentosPersonales()) {
				this.pdfPersonales.add(new DefaultStreamedContent(new ByteArrayInputStream(doc.getFichero()),
						"application/pdf", TipoDocumentoRegistrado.PERSONAL.getNombreDocumento() + ".pdf"));
			}

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Detalle de Solicitud", errmsg));
		}
	}
	
	public void onRowSelect(SelectEvent event) {
		setSelectedSolicitud((Solicitud) event.getObject());
        FacesMessage msg = new FacesMessage("Solicitud Selected: ", ((Solicitud) event.getObject()).getId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
	public List<Solicitud> getList() {
		return list == null ? new ArrayList<Solicitud>() : list;
	}

	public void setList(List<Solicitud> list) {
		this.list = list;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Solicitud getSelectedSolicitud() {
		return selectedSolicitud;
	}

	public void setSelectedSolicitud(Solicitud selectedSolicitud) {
		this.selectedSolicitud = selectedSolicitud;
	}

	public DocumentosRegistradosManager getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(DocumentosRegistradosManager adjuntos) {
		this.adjuntos = adjuntos;
	}

	public StreamedContent getPdfSolicitud() {
		return pdfSolicitud;
	}

	public void setPdfSolicitud(StreamedContent pdfSolicitud) {
		this.pdfSolicitud = pdfSolicitud;
	}

	public StreamedContent getPdfSolicitudAdjuntada() {
		return pdfSolicitudAdjuntada;
	}

	public void setPdfSolicitudAdjuntada(StreamedContent pdfSolicitudAdjuntada) {
		this.pdfSolicitudAdjuntada = pdfSolicitudAdjuntada;
	}

	public StreamedContent getPdfJustificante() {
		return pdfJustificante;
	}

	public void setPdfJustificante(StreamedContent pdfJustificante) {
		this.pdfJustificante = pdfJustificante;
	}

	public List<StreamedContent> getPdfPersonales() {
		return pdfPersonales;
	}

	public void setPdfPersonales(List<StreamedContent> pdfPersonales) {
		this.pdfPersonales = pdfPersonales;
	}
	
	public StreamedContent getPdfPersonal(int index) {
		log.debugv("se ha solicitado el documento adjunto con índice {0}", index);
		return this.pdfPersonales.get(index);
	}

	public Solicitud getSelectedSolicitudFull() {
		return selectedSolicitudFull;
	}

	public void setSelectedSolicitudFull(Solicitud selectedSolicitudFull) {
		this.selectedSolicitudFull = selectedSolicitudFull;
	}

}
