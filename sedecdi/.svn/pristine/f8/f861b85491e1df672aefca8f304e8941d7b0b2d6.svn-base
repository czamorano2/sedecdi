package es.imserso.sede.web.service.registration.solicitud.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Hashtable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import es.imserso.sede.data.dto.PersonaInteresadaDTOI;
import es.imserso.sede.data.dto.RepresentanteDTOI;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.model.Estado;
import es.imserso.sede.model.TipoAutenticacion;
import es.imserso.sede.model.Tramite;
import es.imserso.sede.service.converter.impl.PdfUtil;
import es.imserso.sede.service.message.event.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.service.registration.event.RegistrationPhase;
import es.imserso.sede.service.registration.event.RegistrationPhaseEvent;
import es.imserso.sede.service.registration.receipt.ReceiptServiceI;
import es.imserso.sede.service.registration.registry.RegistryBookManager;
import es.imserso.sede.service.registration.registry.RegistryUtils;
import es.imserso.sede.service.registration.registry.ISicres.registers.RegistryServiceI;
import es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI;
import es.imserso.sede.service.registration.solicitud.AbstractSolicitudRegistration;
import es.imserso.sede.service.registration.solicitud.documentos.TipoDocumentoRegistrado;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.util.mail.MailEngine;
import es.imserso.sede.util.mail.MailManager;
import es.imserso.sede.util.mail.template.MailTemplateManager;

/**
 * Registra una solicitud
 * 
 * @author 11825775
 *
 */
@GenericRegistrationQ
public class SolicitudRegistration extends AbstractSolicitudRegistration implements Serializable {

	
	private static final long serialVersionUID = 3085932273929975558L;

	@Inject
	private Logger log;

	@Inject
	private RegistryBookManager registryBookManager;

	@Inject
	protected MailEngine mailEngine;

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	protected EntityManager pc;

	@Inject
	protected UserTransaction userTransaction;

	@Inject
	protected RegistryServiceI registryService;

	@Inject
	protected ReceiptServiceI receiptService;

	@Inject
	protected MailTemplateManager mailTemplateManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.solicitud.AbstractSolicitudRegistration#
	 * beforeValidation()
	 */
	@Override
	protected void beforeValidation() throws ValidationException {
		super.beforeValidation();

		// realizamos unas primeras validaciones
		if (dtoInstance.getCodigoSIA() == null) {
			String errmsg = "Debe especificarse el código SIA del trámite";
			registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_VALIDATION, errmsg));
			throw new ValidationException(errmsg);
		}

		if (!tramiteRepository.existsTramite(dtoInstance.getCodigoSIA())) {
			String errmsg = "El código SIA especificado no es válido: " + dtoInstance.getCodigoSIA();
			registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_VALIDATION, errmsg));
			throw new ValidationException(errmsg);
		}

		Tramite tramite = tramiteRepository.findBySIA(dtoInstance.getCodigoSIA());

		if (tramite.getPlantillaEmail() == null) {
			String errmsg = String.format("El trámite %s no tiene asociada ninguna plantilla de email! ",
					tramite.getNombre());
			registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_VALIDATION, errmsg));
			throw new ValidationException(errmsg);
		}

		if (tramite.getJustificantePdf() == null) {
			String errmsg = String.format("El trámite %s no tiene asociada ninguna plantilla de justificante! ",
					tramite.getNombre());
			registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.BEFORE_VALIDATION, errmsg));
			throw new ValidationException(errmsg);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.AbstractSolicitudRegistration#
	 * validateSolicitud()
	 */
	@Override
	protected void validate() throws ValidationException {
		super.validate();

		log.info("validando la solicitud ...");

		registrationEvent
				.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO, SOLICITUD_VALIDADA_CORRECTAMENTE));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.
	 * AbstractSolicitudRegistration#convertToPdf()
	 */
	@Override
	protected void convertToPdf() throws RegistrationException {
		super.convertToPdf();

		try {

			solicitudPdf = PdfUtil.generatePdf(dtoInstance.extractHashtable(),
					tramiteRepository.findSolicitudPdfBySIA(dtoInstance.getCodigoSIA()).getValor());

		} catch (IllegalArgumentException | IllegalAccessException | SedeException e) {
			throw new RegistrationException("error al convertir la solicitud a pdf: " + Utils.getExceptionMessage(e));
		}

		registrationEvent.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO, SOLICITUD_CONVERTIDA_A_PDF));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.AbstractSolicitudRegistration#
	 * persistOnBBDD()
	 */
	@Override
	protected void persistOnBBDD() throws RegistrationException {
		super.persistOnBBDD();

		try {
			solicitudInstance = dtoInstance.extractNewSolicitud();
			solicitudInstance.setTipoAutenticacion(TipoAutenticacion.CLAVE_CERTIFICADO); // FIXME
																							// esta
																							// información
																							// nos
																							// la
																							// debería
																							// de
																							// pasar
																							// el
																							// UCM
			solicitudInstance.setEstado(Estado.getEstadoInicial(dtoInstance.getCodigoSIA()));
			solicitudInstance.setTramite(tramiteRepository.findBySIA(dtoInstance.getCodigoSIA()));
			updateOnBBDD();

		} catch (IllegalStateException | SecurityException | SedeException e) {

			String errmsg = "error al persistir la solicitud en base de datos: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new RegistrationException(errmsg);
		}
		registrationEvent.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO,
				"La solicitud ha sido persistida en la bbdd con id=" + solicitudInstance.getId()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.AbstractSolicitudRegistration#
	 * persistOnRegister()
	 */
	@Override
	protected void persistOnRegistry() throws RegistrationException {
		super.persistOnRegistry();

		InputRegisterResponseI newRegisterResponse;
		try {
			newRegisterResponse = registryService.registerNewSolicitud(registryBookManager.getCurrentBook().getId(),
					dtoInstance.getAttachedFiles(), solicitudPdf, solicitudInstance.getId(),
					dtoInstance.getCodigoSIA());
			log.info(String.format("solicitud registrada con el identificador de registro %d y número de registro %s",
					newRegisterResponse.getFolderId(), newRegisterResponse.getNumber()));
			solicitudInstance.setIdentificadorRegistroDTO(new IdentificadorRegistroDTO(newRegisterResponse.getBookId(),
					newRegisterResponse.getFolderId(), newRegisterResponse.getNumber()));

			updateOnBBDD();

			registrationEvent
					.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO, SOLICITUD_DADA_DE_ALTA_EN_REGISTRO));
		} catch (SedeException | SecurityException | IllegalStateException | JsonProcessingException e) {
			throw new RegistrationException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.
	 * AbstractSolicitudRegistration#generateReceipt()
	 */
	@Override
	protected void generateReceipt() throws RegistrationException {
		super.generateReceipt();

		try {
			Hashtable<String, String> receiptHash = solicitudInstance.extractReceiptHash();

			// para el email tiene prioridad el del representante si lo hubiese
			if (dtoInstance instanceof RepresentanteDTOI
					&& ((RepresentanteDTOI) dtoInstance).getEmailRepresentante() != null) {
				receiptHash.put("email", ((RepresentanteDTOI) dtoInstance).getEmailRepresentante());
			} else {
				receiptHash.put("email", ((PersonaInteresadaDTOI) dtoInstance).getEmail());
			}

			receipt = PdfUtil.generatePdf(receiptHash, tramiteRepository
					.findBySIA(solicitudInstance.getTramite().getCodigoSIA()).getJustificantePdf().getValor());

		} catch (IllegalArgumentException | SedeException | IOException e) {
			throw new RegistrationException("error al generar el justificante: " + Utils.getExceptionMessage(e));
		}

		registrationEvent.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO, JUSTIFICANTE_GENERADO));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.
	 * AbstractSolicitudRegistration#signReceipt()
	 */
	@Override
	protected void signReceipt() throws RegistrationException {
		super.signReceipt();

		try {
			signedReceipt = receiptService.signReceipt(receipt);

		} catch (IllegalArgumentException | SedeException e) {
			throw new RegistrationException("error al firmar el justificante: " + Utils.getExceptionMessage(e));
		}

		registrationEvent.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO, JUSTIFICANTE_FIRMADO));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.
	 * AbstractSolicitudRegistration#persistReceiptOnRegister()
	 */
	@Override
	protected void persistReceiptOnRegister() throws RegistrationException {
		super.persistReceiptOnRegister();
		try {
			registryService.addDocumentToSolicitud(registryBookManager.getCurrentBook().getId(),
					TipoDocumentoRegistrado.JUSTIFICANTE.getNombreDocumento(),
					RegistryUtils.getNombreJustificante(solicitudInstance.getId(),
							solicitudInstance.getIdentificadorRegistro().getRegisterNumber()),
					solicitudInstance.getIdentificadorRegistro().getRegisterIdentification(), signedReceipt);
		} catch (SedeException | IOException e) {
			throw new RegistrationException(e);
		}

		registrationEvent.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO, JUSTIFICANTE_REGISTRADO));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.
	 * AbstractSolicitudRegistration#updateOnBBDD()
	 */
	@Override
	protected void updateOnBBDD() throws RegistrationException {
		super.updateOnBBDD();

		try {
			log.info("actualizando la solicitud en la base de datos ...");
			// actualizamos la solicitud en la bbdd
			userTransaction.begin();
			solicitudInstance = pc.merge(solicitudInstance);
			pc.persist(solicitudInstance);
			pc.flush();
			userTransaction.commit();
			log.info("solicitud actualizada en la base de datos");

		} catch (IllegalStateException | SecurityException | NotSupportedException | SystemException
				| HeuristicMixedException | HeuristicRollbackException | RollbackException e) {
			String errmsg = "error al actualizar la solicitud (" + solicitudInstance.getId() + ") en base de datos: "
					+ Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new RegistrationException(errmsg);
		}
		registrationEvent.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO,
				"La solicitud con id= " + solicitudInstance.getId() + " ha sido actualizada en la bbdd"));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.
	 * AbstractSolicitudRegistration#afterUpdateOnBBDD()
	 */
	@Override
	protected void afterUpdateOnBBDD() throws RegistrationException {
		// super.afterUpdateOnBBDD();

		if (solicitudInstance == null) {
			throw new RegistrationException("la instancia de la nueva solicitud no debería ser nula!");
		}

		if (solicitudInstance.getTramite() == null) {
			throw new RegistrationException("la instancia de la nueva solicitud debe pertenecer a un trámite ");
		}

		if (solicitudInstance.getTramite().getPlantillaEmail() == null) {
			throw new RegistrationException(String.format("el trámite %s no tiene asignada plantilla de email",
					solicitudInstance.getTramite().getNombre()));
		}

		// enviamos un email al usuario confirmando el alta de la solicitud
		try {
			if (solicitudInstance.getEmail() != null) {
				// obtenemos la plantilla
				String templateText = solicitudInstance.getTramite().getPlantillaEmail().getTexto();
				// construimos el mensaje
				String messageText = mailTemplateManager.buildMessage(solicitudInstance, templateText);

				// FIXME sacar a fichero de configuración
				mailEngine.sendWithSolicitudPdfAttached(MailManager.EMAIL_SUBJECT_IMSERSO_ALTA_DE_SOLICITUD, messageText, MailManager.NOREPLY,
						solicitudInstance.getEmailNotificacion(), solicitudPdf, receipt);

			} else {
				log.info("no se envía email al usuario porque no lo ha especificado");
			}

		} catch (Exception e) {
			registrationEvent.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR,
					"No se ha podido enviar el email al usuario "
							+ (dtoInstance.nombreApellidos() == null ? "" : dtoInstance.nombreApellidos()) + ": "
							+ Utils.getExceptionMessage(e)));
		} finally {
			registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.AFTER_UPDATE_ON_BBDD,
					solicitudInstance.getEmailNotificacion()));
		}

	}

}
