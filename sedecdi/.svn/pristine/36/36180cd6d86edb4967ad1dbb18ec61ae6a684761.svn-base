package es.imserso.sede.data;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.hermes.session.webservice.dto.TemporadaDTO;
import es.imserso.hermes.session.webservice.dto.TurnoDTO;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraDTO;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.CartaAcreditacionRestClient;
import es.imserso.sede.util.rest.client.HermesRESTClient;
import es.imserso.sede.util.rest.client.RestClient;

/**
 * @author 11825775
 *
 */
/**
 * @author 11825775
 *
 */
@Stateless
public class TurismoRepository {

	private static Logger log = Logger.getLogger(TurismoRepository.class);

	@Inject
	PropertyComponent propertyComponent;

	@Inject
	TurismoRemoteRepository turismoRemoteRepository;

	@Inject
	HermesRESTClient hermesRestClient;

	public boolean checkHermesIsAlive() {
		boolean considerHermesAlive = false;
		try {
			considerHermesAlive = hermesRestClient.checkHermesIsAlive();
			considerHermesAlive = true;

		} catch (SedeException e) {
			// consideramos que Hermes no está operativo, ya que no podemos asegurar que lo
			// esté
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			considerHermesAlive = false;
		}
		return considerHermesAlive;
	}

	public List<SimpleDTOI> getProvincias() throws JsonParseException, JsonMappingException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		return turismoRemoteRepository.getProvincias();
	}

	public List<SimpleDTOI> getEstadosCiviles() throws JsonParseException, JsonMappingException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		return turismoRemoteRepository.getEstadosCiviles();
	}

	public List<SimpleDTOI> getCategoriasFamiliaNumerosa() throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return turismoRemoteRepository.getCategoriasFamiliaNumerosa();
	}

	public List<SimpleDTOI> getClasesPensiones() throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return turismoRemoteRepository.getClasesPensiones();
	}

	public List<SimpleDTOI> getOpciones() throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return turismoRemoteRepository.getOpciones();
	}

	public List<SimpleDTOI> getProcedenciasPensiones() throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return turismoRemoteRepository.getProcedenciasPensiones();
	}

	public List<String> getSexos() throws JsonParseException, JsonMappingException, IOException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		return turismoRemoteRepository.getSexos();
	}

	public PlazoDTO getPlazoTurnoPorDefecto() throws JsonParseException, JsonMappingException, IOException {
		return turismoRemoteRepository.getPlazoTurnoPorDefecto();
	}

	public TurnoDTO getTurnoPorDefecto() throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return turismoRemoteRepository.getTurnoPorDefecto();
	}

	public TemporadaDTO getTemporadaTurnoPorDefecto() throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return turismoRemoteRepository.getTemporadaTurnoPorDefecto();
	}

	/**
	 * @param dto
	 *            DTO con la estructura del JSON persistido en la base de datos (id
	 *            y número de orden)
	 * @return estado en Hermes de la solicitud correspondiente al identificador
	 *         especificado
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SedeException
	 */
	public String getRemoteEstadoSolicitud(TurismoExpedienteAplicacionGestoraDTO dto)
			throws JsonParseException, JsonMappingException, IOException, SedeException {
		return hermesRestClient.getRemoteEstadoSolicitud(dto);
	}

	/**
	 * @param solicitudId
	 * @return solicitud de Hermes correspondiente al identificador especificado
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO getRemoteSolicitudById(Long solicitudId) throws SedeException {
		return hermesRestClient.getRemoteSolicitud(solicitudId);
	}

	/**
	 * @return indica si la carta de areditación está accesible en Hermes ya que, en
	 *         dentro de plazo, no está accesible hasta que se pase el proceso de
	 *         acreditación
	 */
	public Boolean isCartaAcreditaciónAccesible() {
		return turismoRemoteRepository.isCartaAcreditacionAccesible();
	}

	/**
	 * @param solicitudId
	 *            identificador de la solicitud acreditada
	 * @return fichero pdf en formato array de bytes
	 * @throws SedeException
	 * @throws JsonProcessingException
	 */
	public byte[] getCartaAcreditacion(Long solicitudId) throws SedeException, JsonProcessingException {
		return (byte[]) ((CartaAcreditacionRestClient) UtilsCDI.getBeanByReference(CartaAcreditacionRestClient.class))
				.call(propertyComponent.getTurismoResourcesURL() + RestClient.HERMES_GET_CARTA_ACREDITACION_REST_PATH
						+ solicitudId)
				.execute();
	}

	/**
	 * Obtiene las solicitudes cuyo solicitante o cónyuge tenga el documento de
	 * identificación especificado.
	 * 
	 * @param nif_CIF
	 * @return
	 * @throws SedeException
	 */
	public List<SolicitudTurismoDTO> getRemoteSolicitudesByDI(String nif_CIF) throws SedeException {
		return hermesRestClient.getRemoteSolicitudesByDI(nif_CIF);
	}

	/**
	 * Actualiza una solicitud en Hermes con los datos del DTO especificado.
	 * 
	 * @param selectedSolicitud
	 * @throws SedeException
	 */
	public void updateRemoteSolicitud(SolicitudTurismoDTO selectedSolicitud) throws SedeException {
		hermesRestClient.updateRemoteSolicitud(selectedSolicitud);
	}

	/**
	 * Crea una solicitud en Hermes con los datos del DTO especificado.
	 * 
	 * @param solicitudTurismoDTO
	 * @return
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO createRemoteSolicitud(SolicitudTurismoDTO solicitudTurismoDTO) throws SedeException {
		return hermesRestClient.createRemoteSolicitud(solicitudTurismoDTO);
	}
}
