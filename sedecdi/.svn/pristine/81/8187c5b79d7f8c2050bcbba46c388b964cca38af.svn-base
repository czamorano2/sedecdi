package es.imserso.sede.util.rest.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.specimpl.BuiltResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.hermes.session.webservice.dto.CategoriasFamiliaNumerosaDTO;
import es.imserso.hermes.session.webservice.dto.ClasePensionDTO;
import es.imserso.hermes.session.webservice.dto.ClasesPensionesDTO;
import es.imserso.hermes.session.webservice.dto.EstadosCivilesDTO;
import es.imserso.hermes.session.webservice.dto.OpcionDTO;
import es.imserso.hermes.session.webservice.dto.OpcionesDTO;
import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.ProcedenciasPensionesDTO;
import es.imserso.hermes.session.webservice.dto.ProvinciaDTO;
import es.imserso.hermes.session.webservice.dto.ProvinciasDTO;
import es.imserso.hermes.session.webservice.dto.SexosDTO;
import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.hermes.session.webservice.dto.TemporadaDTO;
import es.imserso.hermes.session.webservice.dto.TurnoDTO;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraDTO;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.cache.hermes.CrunchifyInMemoryCache;
import es.imserso.sede.service.cache.hermes.qualifier.HermesAuxListsCache;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.SedecdiRestException;

/**
 * Cliente REST para los servicios de Hermes.
 * 
 * @author 11825775
 *
 */
@Dependent
public class HermesRESTClient implements Serializable {

	private static final long serialVersionUID = -3127948137115733320L;

	private static final String URL_SUFFIX_PLAZOTURNO_POR_DEFECTO = "plazoturnopordefecto";
	private static final String URL_SUFFIX_TEMPORADATURNO_POR_DEFECTO = "temporadaturnopordefecto";
	private static final String URL_SUFFIX_TURNO_POR_DEFECTO = "turnopordefecto";
	private static final String URL_SUFFIX_PROVINCIAS = "provincias";
	private static final String URL_SUFFIX_ESTADOS_CIVILES = "estadosciviles";
	private static final String URL_SUFFIX_SEXOS = "sexos";
	private static final String URL_SUFFIX_OPCIONES = "opciones";
	private static final String URL_SUFFIX_CLASES_PENSIONES = "clasespensiones";
	private static final String URL_SUFFIX_PROCEDENCIA_PENSIONES = "procedenciaspensiones";
	private static final String URL_SUFFIX_CATEGORIAS_FAMILIA_NUMEROSA = "categoriasfamilianumerosa";

	/**
	 * path del método del endpoint de Hermes para saber si la aplicación está
	 * operativa
	 */
	private static final String HERMES_IS_ALIVE_REST_PATH = "isalive";

	/**
	 * path del método del endpoint de Hermes para obtener una solicitud por su id
	 */
	private static final String HERMES_GET_SOLICITUD_REST_PATH = "";

	/**
	 * path del método del endpoint de Hermes para obtener una solicitud por su id
	 */
	private static final String HERMES_GET_SOLICITUDES_BY_DI_REST_PATH = "buscarPorDi/";

	/**
	 * path del método del endpoint de Hermes para obtener el estado de una
	 * solicitud por su id
	 */
	private static final String HERMES_GET_ESTADO_SOLICITUD_REST_PATH = "estado/";

	/**
	 * path del método del endpoint de Hermes para crear una solicitud
	 */
	private static final String HERMES_CREATE_SOLICITUD_REST_PATH = "createsolicitud";

	@Inject
	private Logger log;

	@Inject
	private PropertyComponent propertyComponent;

	@Inject
	Event<EventMessage> eventMessage;

	@HermesAuxListsCache
	CrunchifyInMemoryCache<String, List<ProvinciaDTO>> hermesAuxListsCahe;

	/**
	 * @return true si Hermes responde que está operativo
	 * @throws SedeException
	 */
	public boolean checkHermesIsAlive() throws SedeException {
		boolean isAlive = false;

		// llamamos a un servicio REST de Hermes que responde si está la aplicación
		// operativa
		log.debug("comprobando si Hermes está operativo...");

		try {
			ResteasyClient client = new ResteasyClientBuilder().build();

			log.info("conectamos al servicio REST: " + propertyComponent.getTurismoResourcesURL()
					+ HERMES_CREATE_SOLICITUD_REST_PATH);
			ResteasyWebTarget target = client
					.target(propertyComponent.getTurismoResourcesURL() + HERMES_IS_ALIVE_REST_PATH);

			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.info("Obtenemos el código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("ResponseMessageFromServer: " + responseMessageFromServer);

			if (isAlive = (responseCode == 200)) {
				log.info("Hermes responde que está operativo");
			} else {
				log.warn("Hermes no responde o responde que NO está operativo");
			}

		} catch (IllegalArgumentException | NullPointerException e) {
			String errmsg = "error al comprobar si Hermes está operativo: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}

		return isAlive;
	}

	/**
	 * Da de alta una solicitud en Hermes.
	 * 
	 * @return dto con la solicitud dada de alta en Hermes.
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO createRemoteSolicitud(SolicitudTurismoDTO dtoInstance) throws SedeException {
		SolicitudTurismoDTO outputDTO = null;

		log.debug("vamos a dar de alta una solicitud en Hermes...");

		try {
			// Using the RESTEasy libraries, initiate a client request
			ResteasyClient client = new ResteasyClientBuilder().build();

			// Set url as target
			log.info("conectamos al servicio REST: " + propertyComponent.getTurismoResourcesURL()
					+ HERMES_CREATE_SOLICITUD_REST_PATH);
			ResteasyWebTarget target = client
					.target(propertyComponent.getTurismoResourcesURL() + HERMES_CREATE_SOLICITUD_REST_PATH);

			String jsonDTO = new ObjectMapper().writeValueAsString(dtoInstance);

			log.info(jsonDTO);

			Response response = target.request().post(Entity.entity(jsonDTO, MediaType.APPLICATION_JSON));

			// get response code
			int responseCode = response.getStatus();
			log.info("Obtenemos el código de repuesta de Hermes: " + responseCode);

			// get response message
			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("ResponseMessageFromServer: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				String errmsg = "alta solicitud en Hermes: " + responseMessageFromServer;
				eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
				log.error(errmsg);
				throw new SedeException("(HTTP error code : " + responseCode + ") " + errmsg);
			}

			// read response byte[]
			outputDTO = response.readEntity(SolicitudTurismoDTO.class);
			if (outputDTO != null) {
				log.debug(
						"se ha recibido correctamente el dto con la solicitud dada de alta con el número de registro: "
								+ outputDTO.getNumeroRegistro());
			} else {
				log.warn("el DTO recibido del alta de solicitud es nulo!!!");
			}

		} catch (IllegalArgumentException | NullPointerException | IOException | SedeException e) {
			String errmsg = "error al dar de alta remotamente una solicitud en Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}

		return outputDTO;
	}

	/**
	 * @param identificadorSolicitud
	 *            datos necesarios para que Hermes pueda localizar una solicitud
	 * @return datos de una solicitud de turismo
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO getRemoteSolicitud(@NotNull TurismoExpedienteAplicacionGestoraDTO identificadorSolicitud)
			throws SedeException {

		if (identificadorSolicitud == null) {
			throw new SedeException("debe especificarse el identificador de la solicitud de turismo");
		}

		SolicitudTurismoDTO solicitudTurismoDTO = null;

		try {
			log.debug(String.format("vamos a buscar una solicitud en Hermes con los datos de identificación: %s",
					identificadorSolicitud.getJSON()));

			log.debug("inicializando cliente REST ...");
			ResteasyClient client = new ResteasyClientBuilder().build();

			log.debug("conectamos al servicio REST: " + propertyComponent.getTurismoResourcesURL()
					+ HERMES_GET_SOLICITUD_REST_PATH + identificadorSolicitud.getId());
			ResteasyWebTarget target = client
					.target(propertyComponent.getTurismoResourcesURL() + HERMES_GET_SOLICITUD_REST_PATH);

			log.debug(String.format("hacemos la llamada enviando como parámetro: %s ",
					identificadorSolicitud.toString()));

			Response response = target.request()
					.post(Entity.entity(identificadorSolicitud.getJSON(), MediaType.APPLICATION_JSON));

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			solicitudTurismoDTO = response.readEntity(SolicitudTurismoDTO.class);
			if (solicitudTurismoDTO != null) {
				log.info(String.format(
						"se ha recibido correctamente el dto con la solicitud dada de alta con el número de registro %s",
						solicitudTurismoDTO.getNumeroOrden()));
			} else {
				log.warn("el DTO recibido del alta de solicitud es nulo!!!");
			}

		} catch (IllegalArgumentException | NullPointerException | IOException | SedeException e) {
			String errmsg = "error al obtener solicitud de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}

		return solicitudTurismoDTO;
	}

	/**
	 * @param identificadorSolicitud
	 *            datos necesarios para que Hermes pueda localizar una solicitud
	 * @return cadena de texto que especifica el estado de la solicitud en Hermes
	 * @throws SedeException
	 */
	public String getRemoteEstadoSolicitud(@NotNull TurismoExpedienteAplicacionGestoraDTO identificadorSolicitud)
			throws SedeException {

		if (identificadorSolicitud == null) {
			throw new SedeException("debe especificarse el identificador de la solicitud de turismo");
		}

		String estado = null;

		try {
			log.debug(String.format(
					"vamos a buscar el estado de una solicitud en Hermes con los datos de identificación: %s",
					identificadorSolicitud.getJSON()));

			String restUrl = propertyComponent.getTurismoResourcesURL() + HERMES_GET_ESTADO_SOLICITUD_REST_PATH
					+ identificadorSolicitud.getId();
			log.debug(String.format("hacemos la llamada a %s", restUrl));

			log.debug("inicializando cliente REST ...");
			ResteasyClient client = new ResteasyClientBuilder().build();

			ResteasyWebTarget target = client.target(restUrl);

			target.request(MediaType.APPLICATION_JSON_TYPE);

			log.debug("conectamos al servicio REST: " + restUrl);
			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			estado = response.readEntity(String.class);
			if (estado != null) {
				log.info(String.format("se ha recibido correctamente el estado de la solicitud: %s", estado));
			} else {
				log.warn("el estado de la solicitud recibido es nulo!!!");
			}

		} catch (IllegalArgumentException | NullPointerException | IOException e) {
			String errmsg = "error al obtener solicitud de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}

		return estado;
	}

	/**
	 * @param solicitudId
	 *            identificador de la solicitud en Hermes
	 * @return DTO con los datos de la solicitud obtenidos de Hermes
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO getRemoteSolicitud(@NotNull Long solicitudId) throws SedeException {

		SolicitudTurismoDTO dto = null;

		try {
			log.debugf("vamos a buscar el estado de una solicitud en Hermes con los datos de identificación: %d",
					solicitudId);

			String restUrl = propertyComponent.getTurismoResourcesURL() + HERMES_GET_SOLICITUD_REST_PATH + solicitudId;
			log.debugf("hacemos la llamada a %s", restUrl);

			log.debug("inicializando cliente REST ...");
			ResteasyClient client = new ResteasyClientBuilder().build();

			ResteasyWebTarget target = client.target(restUrl);

			target.request(MediaType.APPLICATION_JSON_TYPE);

			log.debug("conectamos al servicio REST: " + restUrl);
			Response response = target.request().get();

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			dto = response.readEntity(SolicitudTurismoDTO.class);
			if (dto != null) {
				log.infof("se ha recibido correctamente la solicitud: %s", dto.toString());
			} else {
				log.warn("la solicitud recibido es nula!!!");
			}

		} catch (IllegalArgumentException | NullPointerException e) {
			String errmsg = "error al obtener solicitud de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}

		return dto;
	}

	/**
	 * Devuelve listas de valores de Hermes en función de la url suministrada
	 * 
	 * @param urlRestHermes
	 *            url del servicio REST de Hermes
	 * @return response con entidad String conteniendo valores en formato JSON
	 */
	public Response getValores(String urlRestHermes) {
		if (urlRestHermes == null) {
			throw new SedecdiRestException("Debe especificarse una URL para obtener los valores de Hermes");
		}
		Response sedeResponse = null;
		Response hermesResponse = ClientBuilder.newClient().target(urlRestHermes).request().get();

		log.trace(urlRestHermes + " response code: " + hermesResponse.getStatusInfo().getStatusCode());

		if (hermesResponse.getStatusInfo().getStatusCode() < 200
				|| hermesResponse.getStatusInfo().getStatusCode() > 299) {
			// el response está en el rango de errores
			log.warn(urlRestHermes + " response code:" + hermesResponse.getStatusInfo().getStatusCode());
			sedeResponse = Response.fromResponse(hermesResponse).build();

		} else {
			// el response está en el rango de respuestas ok
			if (hermesResponse.getStatusInfo().getStatusCode() == 200) {
				// response OK
				String entityJsonContent = StringUtils.EMPTY;
				if (hermesResponse.hasEntity()) {
					entityJsonContent = hermesResponse.readEntity(String.class);
					log.trace(urlRestHermes + ": " + entityJsonContent);
					sedeResponse = Response.ok(entityJsonContent).build();
				} else {
					log.warn(urlRestHermes + ": no hay entity en el response");
					sedeResponse = Response.noContent().build();
				}

			} else {
				// response no OK pero tampoco error
				sedeResponse = Response.fromResponse(hermesResponse).build();
			}
		}

		return sedeResponse;
	}

	/**
	 * @param documentoIdentificacion
	 *            NIF/NIE del solicitante o del cónyuge
	 * @return lista de solicitudes de turismo de la temporada actual cuyo
	 *         solicitante o cónyuge coincidan con el especificado por parámetro
	 * @throws SedeException
	 */
	public List<SolicitudTurismoDTO> getRemoteSolicitudesByDI(@NotNull String documentoIdentificacion)
			throws SedeException {

		if (documentoIdentificacion == null) {
			String errmsg = "debe especificarse el NIF/NIE del solicitante o del cónyuge";
			log.error(errmsg);
			throw new SedeException(errmsg);
		}

		List<SolicitudTurismoDTO> list = null;
		ResteasyClient client = null;
		Response response = null;

		try {
			log.debugv(
					"se va a solicitar a Hermes las solicitudes de la temporada actual de la persona con NIF/NIE: {0}",
					documentoIdentificacion);

			log.debug("inicializando cliente REST ...");
			client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getTurismoResourcesURL() + HERMES_GET_SOLICITUDES_BY_DI_REST_PATH
					+ documentoIdentificacion;
			log.debug("conectamos al servicio REST: " + url);

			response = client.target(url).request().get();

			int responseCode = response.getStatus();
			log.debug("código de repuesta de Hermes: " + responseCode);

			String responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
			log.debug("mensaje de respuesta de Hermes: " + responseMessageFromServer);

			if (response.getStatus() < 200 || response.getStatus() > 299) {
				throw new SedeException("Failed with HTTP error code : " + responseCode);
			}

			log.debug("leyendo response ...");
			GenericType<List<SolicitudTurismoDTO>> genericType = new GenericType<List<SolicitudTurismoDTO>>() {
			};
			list = response.readEntity(genericType);
			// SolicitudesDTO solicitudesDTO = response.readEntity(SolicitudesDTO.class);
			// list = solicitudesDTO.getValores();

			if (list != null) {
				log.debugv("se han recibido {0} solicitudes para el NIF/NIE {1}", list.size(), documentoIdentificacion);
			} else {
				log.warn("la lista de solicitudes es nula!!!");
			}

		} catch (IllegalArgumentException | NullPointerException | SedeException e) {
			String errmsg = "error al obtener solicitud de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		} finally {
			if (response != null) {
				response.close();
			}
			if (client != null) {
				client.close();
			}
		}

		return list;
	}

	/**
	 * Devuelve el plazo del turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public PlazoDTO getPlazoTurnoPorDefecto() throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(
				(String) (((BuiltResponse) getValores(
						propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_PLAZOTURNO_POR_DEFECTO)).getEntity()),
				PlazoDTO.class);
	}

	/**
	 * Devuelve la temporada del turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public TemporadaDTO getTemporadaTurnoPorDefecto() throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue((String) (((BuiltResponse) getValores(
				propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_TEMPORADATURNO_POR_DEFECTO)).getEntity()),
				TemporadaDTO.class);
	}

	/**
	 * Devuelve el turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public TurnoDTO getTurnoPorDefecto() throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(
				(String) (((BuiltResponse) getValores(
						propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_TURNO_POR_DEFECTO)).getEntity()),
				TurnoDTO.class);
	}

	/**
	 * Devuelve las provincias del turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public List<ProvinciaDTO> getProvincias() throws JsonParseException, JsonMappingException, IOException {
		// List<ProvinciaDTO> list = hermesAuxListsCahe.get(URL_SUFFIX_PROVINCIAS);
		// if (list == null) {
		List<ProvinciaDTO> list = new ObjectMapper().readValue(
				(String) (((BuiltResponse) getValores(
						propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_PROVINCIAS)).getEntity()),
				ProvinciasDTO.class).getValores();
		// }
		return list;
	}

	/**
	 * Devuelve los estados civiles del turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public EstadosCivilesDTO getEstadosCiviles() throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(
				(String) (((BuiltResponse) getValores(
						propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_ESTADOS_CIVILES)).getEntity()),
				EstadosCivilesDTO.class);
	}

	/**
	 * Devuelve los sexos del turno por defecto de Hermes lh pugpuih puph uohpuh ij
	 * oho oih oìhoijpj pppppppppppppp jpj pokçòkpoj pjpojpojppuhho pijpj pijh
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SexosDTO getSexos() throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(
				(String) (((BuiltResponse) getValores(propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_SEXOS))
						.getEntity()),
				SexosDTO.class);
	}

	/**
	 * Devuelve las opciones del turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public List<OpcionDTO> getOpciones() throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(
				(String) (((BuiltResponse) getValores(propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_OPCIONES))
						.getEntity()),
				OpcionesDTO.class).getValores();
	}

	/**
	 * Devuelve las clases pensiones del turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public List<ClasePensionDTO> getClasesPensiones() throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(
				(String) (((BuiltResponse) getValores(
						propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_CLASES_PENSIONES)).getEntity()),
				ClasesPensionesDTO.class).getValores();
	}

	/**
	 * Devuelve las procedencias pensiones del turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public ProcedenciasPensionesDTO getProcedenciasPensiones()
			throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(
				(String) (((BuiltResponse) getValores(
						propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_PROCEDENCIA_PENSIONES)).getEntity()),
				ProcedenciasPensionesDTO.class);
	}

	/**
	 * Devuelve las categorias familia numerosa del turno por defecto de Hermes
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public CategoriasFamiliaNumerosaDTO getCategoriasFamiliaNumerosa()
			throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue((String) (((BuiltResponse) getValores(
				propertyComponent.getTurismoResourcesURL() + URL_SUFFIX_CATEGORIAS_FAMILIA_NUMEROSA)).getEntity()),
				CategoriasFamiliaNumerosaDTO.class);
	}

}
