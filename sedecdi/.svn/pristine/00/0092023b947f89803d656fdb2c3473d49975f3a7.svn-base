package es.imserso.sede.web.view.alta;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.event.Event;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.hermes.session.webservice.dto.TipoDocumentoRegistrado;
import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.TramiteRepository;
import es.imserso.sede.data.dto.SolicitudDTOI;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.impl.turismo.TurismoDTO;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.model.Tramite;
import es.imserso.sede.service.registration.event.RegistrationPhase;
import es.imserso.sede.service.registration.event.RegistrationPhaseEvent;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.util.resources.ResourceQ;
import es.imserso.sede.web.service.registration.solicitud.DtoFactory;
import es.imserso.sede.web.service.registration.solicitud.DtoFactoryQ;
import es.imserso.sede.web.service.registration.solicitud.RegTramiteBean;
import es.imserso.sede.web.util.route.ParamValues;
import es.imserso.sede.web.util.route.ParamValues.Params;
import es.imserso.sede.web.view.SolicitudView;
import es.imserso.sede.web.view.ViewUtils;

public abstract class AbstractSolicitudView implements SolicitudView {

	@Inject
	Logger log;

	/**
	 * Datos relevantes especificados en los parámetros
	 */
	@Inject
	protected ParamValues paramValues;

	protected boolean disabledSolicitanteMainFields;
	protected boolean disabledRepresentanteMainFields;
	protected boolean showRepresentante;

	/**
	 * Datos del usuario obtenidos de la cookie
	 * <p>
	 * Si la petición no incluye la cookie <code>usuario</code> será null
	 */
	@Inject
	@ResourceQ
	protected Usuario usuario;

	@Inject
	protected Event<RegistrationPhaseEvent> registrationPhaseEvent;

	@Inject
	protected SolicitudRepository solicitudRepository;

	@Inject
	protected TramiteRepository tramiteRepository;

	@Inject
	@DtoFactoryQ
	protected DtoFactory dtoFactory;

	protected UploadedFile uploadedFile;

	protected Tramite tramite;

	protected Solicitud solicitud;

	protected SolicitudDTOI dto;

	/**
	 * Al ser {@link javax.enterprise.context.Dependent}, su ciclo de vida es
	 * definido por el contexto de InjectionPoint, en este caso
	 * {@link javax.faces.view.ViewScoped}
	 * <p>
	 * Por tanto, recogerá información desde que se crea la vista hasta que finaliza
	 * {@link es.imserso.sede.service.registration.solicitud.AbstractSolicitudRegistration#register(es.imserso.sede.data.dto.PersonaDTOI)}
	 * 
	 */
	@Inject
	RegTramiteBean regTramiteBean;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.view.SolicitudView#initTramite()
	 */
	@Override
	@PostConstruct
	public void init() {
		registrationPhaseEvent.fire(new RegistrationPhaseEvent(RegistrationPhase.VIEW_LOAD));

		// inicializa el bean que guarda información de la operación de alta de
		// solicitud
		regTramiteBean.initialize();

		disabledRepresentanteMainFields = (StringUtils.isNotBlank(usuario.getNIF_CIF())
				&& paramValues.isRepresentante());
		disabledSolicitanteMainFields = (StringUtils.isNotBlank(usuario.getNIF_CIF())
				&& !paramValues.isRepresentante());
		showRepresentante = paramValues.isRepresentante();
	}

	/**
	 * Persiste la solicitud en el sistema
	 * <p>
	 * Lanza la ejecución de las fases de registro de la solicitud
	 * 
	 * @param solicitudRegistration
	 *            Implementación de registro de la solicitud
	 * @param dto
	 *            Datos de la solicitud
	 * @return outcome para ir a la vista de respuesta si todo ha ido bien, o a la
	 *         vista de error si no se ha podido finalizar correctamente el proceso
	 *         de registro.
	 */
	protected void save(SolicitudRegistrationI solicitudRegistration, SolicitudDTOI dto) {

		setSolicitud(null);
		String respuestaAltaRedirect = null;
		try {

			log.debugf("[{0}] guardando la solicitud a partir de los datos del formuario ...",
					paramValues.getParamValue(Params.sia).getValue());
			setSolicitud(solicitudRegistration.register(dto));
			log.info("solicitud registrada!");

			respuestaAltaRedirect = "/app/respuesta_alta?faces-redirect=true&idSolicitud=" + solicitud.getId();
			log.infov("redirigimos a la vista de respuesta de alta de solicitud: {0}", respuestaAltaRedirect);

			((ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler())
					.performNavigation(respuestaAltaRedirect);

		} catch (ValidationException e) {
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(),
					TipoTramite.getTipoTramite(paramValues.getParamValue(Params.sia).getValue()),
					"No se pudo dar de alta la solicitud", Utils.getExceptionMessage(e));
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("messages");

		} catch (Exception e) {
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(),
					TipoTramite.getTipoTramite(paramValues.getParamValue(Params.sia).getValue()),
					"No se pudo dar de alta la solicitud", Utils.getExceptionMessage(e));
			// redirigimos a la vista de error
			respuestaAltaRedirect = "error";
			((ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler())
					.performNavigation(respuestaAltaRedirect);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.view.SolicitudView#cancel()
	 */
	@Override
	public String cancel() {
		try {
			log.info("operación cancelada por el usuario!");
			regTramiteBean.endUnsuccessfully("operación cancelada por el usuario!");
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

		} catch (Exception e) {
			log.errorf("Error al cancelar el alta: %s", Utils.getExceptionMessage(e));
		}
		return "backToUCM";
	}

	public void fileUploadListener(FileUploadEvent e) {
		log.info("Añadiendo fichero adjunto...");

		uploadedFile = e.getFile();

		if (this.uploadedFile == null) {
			log.warn("No hay fichero para adjuntar");
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("form:messages");
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(),
					TipoTramite.getTipoTramite(paramValues.getParamValue(Params.sia).getValue()), "Ficheros Adjuntos",
					"No se ha recibido ningún fichero para adjuntar");
			return;
		}

		log.info("Uploaded File Name Is :: " + uploadedFile.getFileName() + " :: Uploaded File Size :: "
				+ uploadedFile.getSize());

		// asignamos los ficheros adjuntos al DTO
		DocumentoRegistrado attachedFile = new DocumentoRegistrado(TipoDocumentoRegistrado.PERSONAL.toString(),
				uploadedFile.getFileName(), uploadedFile.getContents());
		if (!dto.getAttachedFiles().contains(attachedFile)) {
			log.info("Añadiendo fichero adjunto...");
			dto.addAttachedFile(attachedFile);
		}
	}

	@PreDestroy
	public void onTerminate() {
		// aseguramos que se persisten los apuntes de la operación realizada
		if (!regTramiteBean.persisted()) {
			try {
				regTramiteBean.endUnsuccessfully("la solicitud no se ha registrado!");
			} catch (Exception e) {
				log.warnv("error al intentar, in extremis, persistir los apuntes del registro de la solicitud: %s",
						Utils.getExceptionMessage(e));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.view.SolicitudView#isDisabledSolicitanteMainFields()
	 */
	public boolean isDisabledSolicitanteMainFields() {
		return disabledSolicitanteMainFields;
	}

	public void setDisabledSolicitanteMainFields(boolean disableSolicitanteMainFields) {
		this.disabledSolicitanteMainFields = disableSolicitanteMainFields;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.web.view.SolicitudView#isDisabledRepresentanteMainFields()
	 */
	public boolean isDisabledRepresentanteMainFields() {
		return disabledRepresentanteMainFields;
	}

	public void setDisabledRepresentanteMainFields(boolean disableRepresentanteMainFields) {
		this.disabledRepresentanteMainFields = disableRepresentanteMainFields;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.view.SolicitudView#isShowRepresentante()
	 */
	public boolean isShowRepresentante() {
		return showRepresentante;
	}

	public void setShowRepresentante(boolean showRepresentante) {
		this.showRepresentante = showRepresentante;
	}

	public ParamValues getParamValues() {
		return paramValues;
	}

	public void setParamValues(ParamValues paramValues) {
		this.paramValues = paramValues;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public Tramite getTramite() {
		return tramite;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.view.SolicitudView#setDTO(es.imserso.sede.data.dto.
	 * PersonaInteresadaDTOI)
	 */
	@Override
	public void setDto(SolicitudDTOI solicitudDTO) {
		dto = solicitudDTO;
	}

	

}
