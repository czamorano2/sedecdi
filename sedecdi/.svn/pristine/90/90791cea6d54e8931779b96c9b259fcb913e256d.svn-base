package es.imserso.sede.web.view.alta;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.impl.PropositoGeneralDTO;
import es.imserso.sede.data.dto.qualifier.RepresentanteQ;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.impl.pg.PropositoGeneralSolicitudRegistrationQ;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Bean de respaldo para el dto de propósito general.
 * 
 * @author 11825775
 *
 */
@Named(value = "pgeneralView")
@ViewScoped
@Secure
public class SolicitudPropositoGeneralView extends AbstractSolicitudView implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	Logger log;

	@Inject
	@PropositoGeneralSolicitudRegistrationQ
	private SolicitudRegistrationI propositoGeneralSolicitudRegistration;

	@Inject
	private PropositoGeneralDTO dto;

	@PostConstruct
	public void onCreate() {
		try {
			super.init();

		} catch (Exception e) {
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.PROPOSITO_GENERAL,
					"No se pudo crear la instacia del DTO", Utils.getExceptionMessage(e));
		}
	}

	/**
	 * Registra la solicitud en la Sede Electrónica
	 */
	public void save() {
		save(propositoGeneralSolicitudRegistration, getDto());
	}

	public PropositoGeneralDTO getDto() {
		return (PropositoGeneralDTO) dto;
	}

}
