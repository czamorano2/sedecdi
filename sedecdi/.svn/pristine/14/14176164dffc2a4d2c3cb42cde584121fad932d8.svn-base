package es.imserso.sede.service.monitor.turismo.service;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.util.exception.SedeException;

/**
 * Información sobre si se pueden descargar las cartas de acreditación en
 * Hermes.
 * <p>
 * Las cartas de acreditación solamente pueden descargarse después de haber
 * ejecutado el proceso de acreditación, por lo que antes de ese momento no
 * podrán descargarse las cartas de acreditación
 * 
 * @author 11825775
 *
 */
@Dependent
public class CartasAcreditacionDownloadableInfo implements ServiceInfo {

	private static final long serialVersionUID = -3501092722567399456L;

	@Inject
	TurismoRepository turismoRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isCritical()
	 */
	@Override
	public Boolean isCritical() {
		return Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceName()
	 */
	@Override
	public String getServiceName() {
		return "Carta de acreditación descargable";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceDescription()
	 */
	@Override
	public String getServiceDescription() {
		return "Información sobre si se pueden descargar las cartas de acreditación";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isEnabled()
	 */
	@Override
	public Boolean isEnabled() throws SedeException {
		return turismoRepository.isCartaAcreditaciónAccesible();
	}

}
