package es.imserso.sede.data.dto.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Hashtable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;
import es.imserso.sede.data.dto.DireccionNotificacionDTOI;
import es.imserso.sede.data.dto.PersonaInteresadaDTO;
import es.imserso.sede.data.dto.PersonaInteresadaDTOI;
import es.imserso.sede.data.dto.PersonaInteresadaResidenciaDTOI;
import es.imserso.sede.data.validation.constraint.NotificationAddress;
import es.imserso.sede.data.validation.constraint.match.FieldMatch;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

@XmlRootElement
@NotificationAddress
@FieldMatch.List({		
		@FieldMatch(first = "email", second = "transientEmailConfirm", message = "El correo electrónico de la persona interesada no coincide con el correo electrónico de confirmación")

})
abstract public class TermalismoDTO extends PersonaInteresadaDTO implements PersonaInteresadaDTOI,
		PersonaInteresadaResidenciaDTOI,  DireccionNotificacionDTOI, Serializable {

	private static final long serialVersionUID = 3217214452142789721L;

	@NotBlank(message = "El domicilio de la persona interesada no puede estar vacío")
	protected String domicilioResidencia;
	@NotBlank(message = "La localidad de la persona interesada no puede estar vacío")
	protected String localidadResidencia;
	@NotBlank(message = "El código postal de la persona interesada no puede estar vacío")
	protected String codigoPostalResidencia;
	@NotBlank(message = "La provincia de la persona interesada no puede estar vacío")
	protected String provinciaResidencia;
	@NotBlank(message = "El país de la persona interesada no puede estar vacío")
	protected String paisResidencia;

	protected String domicilioNotificacion;
	protected String localidadNotificacion;
	protected String codigoPostalNotificacion;
	protected String provinciaNotificacion;
	protected String paisNotificacion;
	protected String dispositivoElectronico; 


	//codigo provincia residencia (la descripcion irá en provinciaResidencia)
	private String prvd;
	//codigo provincia notificacion (la descripcion irá en provinciaNotificacion)
	private String prvdNotif;

	
	//fecha registro DDMMAAA
	private Date fisc;
	
	//nº expediente
	private Integer nreg;
	
	//****datos personales solicitante****//
	//clave sexo
	//guardamos el código (no el id) y la descripción
	private String sexo;
	//sexo descripcion
	private String sexoDesc;
		
	//guardamos el código (no el id) y la descripción
	//clave estado civil
	private String eciv;
	//estado civil descripicion
	private String ecivDesc;
	

	//día fecha nacimiento
	private Integer fenadia;
	//mes fecha nacimiento
	private Integer fenames;
	//año fecha nacimiento
	private Integer fenaanio;
	
	
	//****datos personales conyuge****//
	//apellidos nombre 
	private String ape1c;
	

	private String ape2c;
	

	private String nombrec;
	//nif conyuge
	private String dnic;
	//dia fecha nacimiento
	private Integer fencdia;
	//mes fecha nacimiento 
	private Integer fencmes;
	//año fecha nacimiento 
	private Integer fencanio;

	//Comunidades autónomas donde se encuentran los balnearios
	//obligatorio para acceder a los balnearios
	private Long ccaa1;
	private String ccaa1Desc;
	private Long ccaa2;
	private String ccaa2Desc;
	private Long ccaa3;
	private String ccaa3Desc;
	private Long ccaa4;
	private String ccaa4Desc;
	
	
	//balnearios solicitados número max 4 por solicitud		
	//balneario orden 1
	//guardamos el código (no el id) y la descripción
	private String bals1;
	private String bals1Desc;
	//balneario orden 2
	private String bals2;
	private String bals2Desc;
	//balneario orden 3
	private String bals3;
	private String bals3Desc;
	//balneario orden 4
	private String bals4;
	private String bals4Desc;
	
	//plazas solicitadas
	//plaza solicitante
	private String  plazSol;
	
	//plaza conyuge
	private String  plazCony;
	//plaza ambos
	private String  plazAmbos;
	
	//turnos solicitados (meses en los que se desea disfrutar de plaza)
	//guardamos el código (no el id) y la descripción
	//mes orden 1 (valores posibles del 1 al 12) Desc: enero, febrero, marzo...
	@Min(value=1)
	@Max(value=12)
	private int turn1=0;	
	private String turnDesc1;
	//mes orden 2 (valores posibles del 1 al 12) Desc: enero, febrero, marzo...
	private int turn2=0;
	private String turnDesc2;
	//mes orden 3 (valores posibles del 1 al 12) Desc: enero, febrero, marzo...
	private int turn3=0;
	private String turnDesc3;
	//mes orden 4 (valores posibles del 1 al 12) Desc: enero, febrero, marzo...
	private int turn4=0;
	private String turnDesc4;
	
	
	//****PENSIONES SOLICITANTE (dos pensiones posibles)
	//guardamos el código (no el id) y la descripción
	//clase de pensión 1. Obligatorio
	private String clasPenSol1;
	//descripcion clase pension 1
	private String clasPenSol1Desc;
	//importe pension. Obligatorio. Distinto de 0. Los dos últimos dígitos son los decimales
	private BigDecimal impoPenSol1;
	//clase de pensión 1
	private String clasPenSol2;
	//descripcion clase pension 2
	private String clasPenSol2Desc;
	//importe pension 2
	private BigDecimal impoPenSol2;
	
	
	//****PENSIONES CONYUGE (dos pensiones posibles)
	//guardamos el código (no el id) y la descripción
	//clase de pensión 1
	private String clasPenCon1;
	//descripcion clase pension 1
	private String clasPenCon1Desc;
	//importe pension 
	private BigDecimal impoPenCon1;
	//clase de pensión 1
	private String clasPenCon2;
	//descripcion clase pension 2
	private String clasPenCon2Desc;
	//importe pension 2
	private BigDecimal impoPenCon2;
	
	//otros ingresos
	private BigDecimal otrs;
	
	//*********VINCULACION**********//
	//apellidos y nombre instancia relacionada
	//apellido1 instancia relacionada
	private String aperape1;
	//apellido2 instancia relacionada
	private String aperape2;
	//nombre instancia relacionada
	private String apernom;
	//nif instancia relacionada
	private String dnir;
	

	//expediente relacionado
	private String expr;
	///******DATOS SOLICITANTE****////
	//nºinforme médico
	private Integer infm1;
	
	//nº articulaciones afectadas (SOLICITANTE). SIEMPRE VALE 1
	private Integer nart1=1;
	//lateralidad afectada (SOLICITANTE). SIEMPRE VALE 2
	private Integer late1=2;
	
	

	
	///******DATOS CONYUGE****////
	//nºinforme médico
	private Integer infm2;
	
	//nº articulaciones afectadas (CONYUGE). SIEMPRE VALE 1
	private Integer nart2;
	//lateralidad afectada (CONYUGE). SIEMPRE VALE 2
	private Integer late2;
	//SIEMPRE TENDRÁ VALOR 0
	private Integer hotel=0;
	
	//tipo familia numerosa
	private String tfam;
	//descripcion tipo familia numerosa
	private String tfamDesc;	
	//presentado libro familia. SIEMPRE RELLENO CON 0
	private Integer tlibro=0;
	//numero carnet familia numerosa
	private String cfam;
	
	//fax
	private Long fax;
	//teléfono 2
	private String telefono2;
	
		
	//sexo cónyuge
	//guardamos el código (no el id) y la descripción
	private String sexoc;
	//sexo cónyuge descripcion
	private String sexocDesc;


	//tipo duración turno
	//12 dias
	private String tdt1;
	//10 dias
	private String tdt2;
	//Sin preferencia
	private String tdt3;

	
	
	
	
	
	//*******DECLARACION RESPONSABLE DEL SOLICITANTE*****
	private String repetirNombreSol;
	private String repetirDniSol;
	
	//requisitos solicitante:1.se vale por si mismo;2.alteración comportamiento;3.enf.infecto-contag. 
	//valores posibles SI NO
	private String treqSol1;
	private String treqSol2;
	private String treqSol3;
	
	private String enfermedad1;
	
	//tipo tratamiento
	//1.reumatológico
	private String tratSol1;
	//2.respiratorio
	private String tratSol2;
	//3.digestivo
	private String tratSol3;
	//4.renal
	private String tratSol4;
	//5.dermatológico
	private String tratSol5;
	//6.neuropsiquico
	private String tratSol6;
	
	//articulaciones afectadas
	//cadera
	private String artiSol1;
	//columna
	private String artiSol2;
	//hombro
	private String artiSol3;
	//muñeca
	private String artiSol4;
	//codo
	private String artiSol5;
	//tobillo
	private String artiSol6;
	
	//padece de..
	//dificultad para moverme
	private String defoSol1;
	//dolor
	private String defoSol2;
	//deformidad
	private String defoSol3;
	//rigidez
	private String defoSol4;
	
	//padece vias respiratorias altas
	private String vresSolA;
	
	//padece vias respiratorias bajas
	private String vresSolB;
	
	//nº recaídas últimos 12 meses SOLICITANTE (respiratorio)
	private Integer nred1;
	
	//nivel afectacion respiratorio
	//ingresos hospital
	private String impoSol1;
	//tomar + 2 medicamentos
	private String impoSol2;
	//muchos síntomas
	private String impoSol3;
	//tomar oxígeno
	private String impoSol4;
	
	//nº procesos agudos SOLICITANTE (recaídas últimos 12 meses) 
	private Integer cdef1;
	
	//localidad firma
	//private String localidadSolFirma;
	//dia firma
	//private String diaSolFirma;
	//mes firma
	//private String mesSolFirma;
	//año firma
	//private String anioSolFirma;
	//no autoriza confirmacion datos
	private Boolean autorizaSol;

	//*******DECLARACION RESPONSABLE DEL SOLICITANTE*****
	private String repetirNombreCon;
	private String repetirDniCon;
	
	//requisitos solicitante:1.se vale por si mismo;2.alteración comportamiento;3.enf.infecto-contag
	private String treqCon1;
	private String treqCon2;
	private String treqCon3;
	
	private String enfermedad2;
	
	//tipo tratamiento
	//1.reumatológico
	private String tratCon1;
	//2.respiratorio
	private String tratCon2;
	//3.digestivo
	private String tratCon3;
	//4.renal
	private String tratCon4;
	//5.dermatológico
	private String tratCon5;
	//6.neuropsiquico
	private String tratCon6;
	
	//articulaciones afectadas
	//cadera
	private String artiCon1;
	//columna
	private String artiCon2;
	//hombro
	private String artiCon3;
	//muñeca
	private String artiCon4;
	//codo
	private String artiCon5;
	//tobillo
	private String artiCon6;
	
	//padece de..
	//dificultad para moverme
	private String defoCon1;
	//dolor
	private String defoCon2;
	//deformidad
	private String defoCon3;
	//rigidez
	private String defoCon4;
	
	//padece vias respiratorias altas
	private String vresConA;
	
	//padece vias respiratorias bajas
	private String vresConB;
	
	//nº recaídas últimos 12 meses (respiratorio)
	private Integer nred2;
	
	//nivel afectacion respiratorio
	//ingresos hospital
	private String impoCon1;
	//tomar + 2 medicamentos
	private String impoCon2;
	//muchos síntomas
	private String impoCon3;
	//tomar oxígeno
	private String impoCon4;
	
	//no autoriza confirmacion datos
	private Boolean autorizaCon;
	
	//nº procesos agudos (recaídas últimos 12 meses) 
	private Integer cdef2;
	
	//SIEMPRE CON 0
	private Integer sinPreferenciaTurnoBal=0;
	
	protected TermalismoDTO() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.DTOInterface#extractHashtable()
	 */
	@Override
	public Hashtable<String, String> extractHashtable() throws IllegalAccessException {
		return Utils.extractClassFieldsToHashtable(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.PersonaInteresadaDTO#extractNewSolicitud()
	 */
	@Override
	public Solicitud extractNewSolicitud() throws SedeException {
		Solicitud solicitud = super.extractNewSolicitud();
		
		return solicitud;
	}

	

	/**
	 * @return the domicilioResidencia
	 */
	@Override
	public String getDomicilioResidencia() {
		return domicilioResidencia;
	}

	/**
	 * @param domicilioResidencia
	 *            the domicilioResidencia to set
	 */
	@Override
	public void setDomicilioResidencia(String domicilioResidencia) {
		this.domicilioResidencia = domicilioResidencia;
	}

	/**
	 * @return the localidadResidencia
	 */
	@Override
	public String getLocalidadResidencia() {
		return localidadResidencia;
	}

	/**
	 * @param localidadResidencia
	 *            the localidadResidencia to set
	 */
	@Override
	public void setLocalidadResidencia(String localidadResidencia) {
		this.localidadResidencia = localidadResidencia;
	}

	/**
	 * @return the codigoPostalResidencia
	 */
	@Override
	public String getCodigoPostalResidencia() {
		return codigoPostalResidencia;
	}

	/**
	 * @param codigoPostalResidencia
	 *            the codigoPostalResidencia to set
	 */
	@Override
	public void setCodigoPostalResidencia(String codigoPostalResidencia) {
		this.codigoPostalResidencia = codigoPostalResidencia;
	}

	/**
	 * @return the provinciaResidencia
	 */
	@Override
	public String getProvinciaResidencia() {
		return provinciaResidencia;
	}

	/**
	 * @param provinciaResidencia
	 *            the provinciaResidencia to set
	 */
	@Override
	public void setProvinciaResidencia(String provinciaResidencia) {
		this.provinciaResidencia = provinciaResidencia;
	}

	/**
	 * @return the paisResidencia
	 */
	@Override
	public String getPaisResidencia() {
		return paisResidencia;
	}

	/**
	 * @param paisResidencia
	 *            the paisResidencia to set
	 */
	@Override
	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	/**
	 * @return the domicilioNotificacion
	 */
	@Override()
	public String getDomicilioNotificacion() {
		return domicilioNotificacion;
	}

	/**
	 * @param domicilioNotificacion
	 *            the domicilioNotificacion to set
	 */
	@Override
	public void setDomicilioNotificacion(String domicilioNotificacion) {
		this.domicilioNotificacion = domicilioNotificacion;
	}

	/**
	 * @return the localidadNotificacion
	 */
	@Override
	public String getLocalidadNotificacion() {
		return localidadNotificacion;
	}

	/**
	 * @param localidadNotificacion
	 *            the localidadNotificacion to set
	 */
	@Override
	public void setLocalidadNotificacion(String localidadNotificacion) {
		this.localidadNotificacion = localidadNotificacion;
	}

	/**
	 * @return the codigoPostalNotificacion
	 */
	@Override
	public String getCodigoPostalNotificacion() {
		return codigoPostalNotificacion;
	}

	/**
	 * @param codigoPostalNotificacion
	 *            the codigoPostalNotificacion to set
	 */
	@Override
	public void setCodigoPostalNotificacion(String codigoPostalNotificacion) {
		this.codigoPostalNotificacion = codigoPostalNotificacion;
	}

	/**
	 * @return the provinciaNotificacion
	 */
	@Override
	public String getProvinciaNotificacion() {
		return provinciaNotificacion;
	}

	/**
	 * @param provinciaNotificacion
	 *            the provinciaNotificacion to set
	 */
	@Override
	public void setProvinciaNotificacion(String provinciaNotificacion) {
		this.provinciaNotificacion = provinciaNotificacion;
	}

	/**
	 * @return the paisNotificacion
	 */
	@Override
	public String getPaisNotificacion() {
		return paisNotificacion;
	}

	/**
	 * @param paisNotificacion
	 *            the paisNotificacion to set
	 */
	@Override
	public void setPaisNotificacion(String paisNotificacion) {
		this.paisNotificacion = paisNotificacion;
	}


	public Date getFisc() {
		return fisc;
	}

	public void setFisc(Date fisc) {
		this.fisc = fisc;
	}

	public Integer getNreg() {
		return nreg;
	}

	public void setNreg(Integer nreg) {
		this.nreg = nreg;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getSexoDesc() {
		return sexoDesc;
	}

	public void setSexoDesc(String sexoDesc) {
		this.sexoDesc = sexoDesc;
	}

	public String getEciv() {
		return eciv;
	}

	public void setEciv(String eciv) {
		this.eciv = eciv;
	}

	public String getEcivDesc() {
		return ecivDesc;
	}

	public void setEcivDesc(String ecivDesc) {
		this.ecivDesc = ecivDesc;
	}

	public Integer getFenadia() {
		return fenadia;
	}

	public void setFenadia(Integer fenadia) {
		this.fenadia = fenadia;
	}

	public Integer getFenames() {
		return fenames;
	}

	public void setFenames(Integer fenames) {
		this.fenames = fenames;
	}

	public Integer getFenaanio() {
		return fenaanio;
	}

	public void setFenaanio(Integer fenaanio) {
		this.fenaanio = fenaanio;
	}




	public String getNombrec() {
		return nombrec;
	}

	public void setNombrec(String nombrec) {
		this.nombrec = nombrec;
	}

	public Integer getFencdia() {
		return fencdia;
	}

	public void setFencdia(Integer fencdia) {
		this.fencdia = fencdia;
	}

	public Integer getFencmes() {
		return fencmes;
	}

	public void setFencmes(Integer fencmes) {
		this.fencmes = fencmes;
	}

	public Integer getFencanio() {
		return fencanio;
	}

	public void setFencanio(Integer fencanio) {
		this.fencanio = fencanio;
	}

	public String getPlazSol() {
		return plazSol;
	}

	public void setPlazSol(String plazSol) {
		this.plazSol = plazSol;
	}

	public String getPlazCony() {
		return plazCony;
	}

	public void setPlazCony(String plazCony) {
		this.plazCony = plazCony;
	}

	public String getPlazAmbos() {
		return plazAmbos;
	}

	public void setPlazAmbos(String plazAmbos) {
		this.plazAmbos = plazAmbos;
	}

	public Integer getTurn1() {
		return turn1;
	}

	public void setTurn1(Integer turn1) {
		this.turn1 = turn1;
	}

	public String getTurnDesc1() {
		return turnDesc1;
	}

	public void setTurnDesc1(String turnDesc1) {
		this.turnDesc1 = turnDesc1;
	}

	public Integer getTurn2() {
		return turn2;
	}

	public void setTurn2(Integer turn2) {
		this.turn2 = turn2;
	}

	public String getTurnDesc2() {
		return turnDesc2;
	}

	public void setTurnDesc2(String turnDesc2) {
		this.turnDesc2 = turnDesc2;
	}

	public Integer getTurn3() {
		return turn3;
	}

	public void setTurn3(Integer turn3) {
		this.turn3 = turn3;
	}

	public String getTurnDesc3() {
		return turnDesc3;
	}

	public void setTurnDesc3(String turnDesc3) {
		this.turnDesc3 = turnDesc3;
	}

	public Integer getTurn4() {
		return turn4;
	}

	public void setTurn4(Integer turn4) {
		this.turn4 = turn4;
	}

	public String getTurnDesc4() {
		return turnDesc4;
	}

	public void setTurnDesc4(String turnDesc4) {
		this.turnDesc4 = turnDesc4;
	}

	public BigDecimal getImpoPenSol1() {
		return impoPenSol1;
	}

	public void setImpoPenSol1(BigDecimal impoPenSol1) {
		this.impoPenSol1 = impoPenSol1;
	}

	public BigDecimal getImpoPenSol2() {
		return impoPenSol2;
	}

	public void setImpoPenSol2(BigDecimal impoPenSol2) {
		this.impoPenSol2 = impoPenSol2;
	}


	public BigDecimal getImpoPenCon1() {
		return impoPenCon1;
	}

	public void setImpoPenCon1(BigDecimal impoPenCon1) {
		this.impoPenCon1 = impoPenCon1;
	}

	public BigDecimal getImpoPenCon2() {
		return impoPenCon2;
	}

	public void setImpoPenCon2(BigDecimal impoPenCon2) {
		this.impoPenCon2 = impoPenCon2;
	}

	public BigDecimal getOtrs() {
		return otrs;
	}

	public void setOtrs(BigDecimal otrs) {
		this.otrs = otrs;
	}

	public String getExpr() {
		return expr;
	}

	public void setExpr(String expr) {
		this.expr = expr;
	}

	public Integer getInfm1() {
		return infm1;
	}

	public void setInfm1(Integer infm1) {
		this.infm1 = infm1;
	}

	public Integer getNart1() {
		return nart1;
	}

	public void setNart1(Integer nart1) {
		this.nart1 = nart1;
	}

	public Integer getLate1() {
		return late1;
	}

	public void setLate1(Integer late1) {
		this.late1 = late1;
	}

	public Integer getInfm2() {
		return infm2;
	}

	public void setInfm2(Integer infm2) {
		this.infm2 = infm2;
	}

	public Integer getNart2() {
		return nart2;
	}

	public void setNart2(Integer nart2) {
		this.nart2 = nart2;
	}

	public Integer getLate2() {
		return late2;
	}

	public void setLate2(Integer late2) {
		this.late2 = late2;
	}

	public Integer getHotel() {
		return hotel;
	}

	public void setHotel(Integer hotel) {
		this.hotel = hotel;
	}

	public String getTfam() {
		return tfam;
	}

	public void setTfam(String tfam) {
		this.tfam = tfam;
	}

	public String getTfamDesc() {
		return tfamDesc;
	}

	public void setTfamDesc(String tfamDesc) {
		this.tfamDesc = tfamDesc;
	}

	public Integer getTlibro() {
		return tlibro;
	}

	public void setTlibro(Integer tlibro) {
		this.tlibro = tlibro;
	}

	

	public Long getFax() {
		return fax;
	}

	public void setFax(Long fax) {
		this.fax = fax;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getSexoc() {
		return sexoc;
	}

	public void setSexoc(String sexoc) {
		this.sexoc = sexoc;
	}

	public String getSexocDesc() {
		return sexocDesc;
	}

	public void setSexocDesc(String sexocDesc) {
		this.sexocDesc = sexocDesc;
	}

	public String getTdt1() {
		return tdt1;
	}

	public void setTdt1(String tdt1) {
		this.tdt1 = tdt1;
	}

	public String getTdt2() {
		return tdt2;
	}

	public void setTdt2(String tdt2) {
		this.tdt2 = tdt2;
	}

	public String getTdt3() {
		return tdt3;
	}

	public void setTdt3(String tdt3) {
		this.tdt3 = tdt3;
	}


	public String getRepetirNombreSol() {
		return repetirNombreSol;
	}

	public void setRepetirNombreSol(String repetirNombreSol) {
		this.repetirNombreSol = repetirNombreSol;
	}

	public String getRepetirDniSol() {
		return repetirDniSol;
	}

	public void setRepetirDniSol(String repetirDniSol) {
		this.repetirDniSol = repetirDniSol;
	}

	public String getTreqSol1() {
		return treqSol1;
	}

	public void setTreqSol1(String treqSol1) {
		this.treqSol1 = treqSol1;
	}

	public String getTreqSol2() {
		return treqSol2;
	}

	public void setTreqSol2(String treqSol2) {
		this.treqSol2 = treqSol2;
	}

	public String getTreqSol3() {
		return treqSol3;
	}

	public void setTreqSol3(String treqSol3) {
		this.treqSol3 = treqSol3;
	}

	public String getEnfermedad1() {
		return enfermedad1;
	}

	public void setEnfermedad1(String enfermedad1) {
		this.enfermedad1 = enfermedad1;
	}

	public String getTratSol1() {
		return tratSol1;
	}

	public void setTratSol1(String tratSol1) {
		this.tratSol1 = tratSol1;
	}

	public String getTratSol2() {
		return tratSol2;
	}

	public void setTratSol2(String tratSol2) {
		this.tratSol2 = tratSol2;
	}

	public String getTratSol3() {
		return tratSol3;
	}

	public void setTratSol3(String tratSol3) {
		this.tratSol3 = tratSol3;
	}

	public String getTratSol4() {
		return tratSol4;
	}

	public void setTratSol4(String tratSol4) {
		this.tratSol4 = tratSol4;
	}

	public String getTratSol5() {
		return tratSol5;
	}

	public void setTratSol5(String tratSol5) {
		this.tratSol5 = tratSol5;
	}

	public String getTratSol6() {
		return tratSol6;
	}

	public void setTratSol6(String tratSol6) {
		this.tratSol6 = tratSol6;
	}

	public String getVresSolA() {
		return vresSolA;
	}

	public void setVresSolA(String vresSolA) {
		this.vresSolA = vresSolA;
	}

	public String getVresSolB() {
		return vresSolB;
	}

	public void setVresSolB(String vresSolB) {
		this.vresSolB = vresSolB;
	}

	public Integer getNred1() {
		return nred1;
	}

	public void setNred1(Integer nred1) {
		this.nred1 = nred1;
	}

	public String getImpoSol1() {
		return impoSol1;
	}

	public void setImpoSol1(String impoSol1) {
		this.impoSol1 = impoSol1;
	}

	public String getImpoSol2() {
		return impoSol2;
	}

	public void setImpoSol2(String impoSol2) {
		this.impoSol2 = impoSol2;
	}

	public String getImpoSol3() {
		return impoSol3;
	}

	public void setImpoSol3(String impoSol3) {
		this.impoSol3 = impoSol3;
	}

	public String getImpoSol4() {
		return impoSol4;
	}

	public void setImpoSol4(String impoSol4) {
		this.impoSol4 = impoSol4;
	}

//	public String getLocalidadSolFirma() {
//		return localidadSolFirma;
//	}
//
//	public void setLocalidadSolFirma(String localidadSolFirma) {
//		this.localidadSolFirma = localidadSolFirma;
//	}
//
//	public String getMesSolFirma() {
//		return mesSolFirma;
//	}
//
//	public void setMesSolFirma(String mesSolFirma) {
//		this.mesSolFirma = mesSolFirma;
//	}

	public String getRepetirNombreCon() {
		return repetirNombreCon;
	}

	public void setRepetirNombreCon(String repetirNombreCon) {
		this.repetirNombreCon = repetirNombreCon;
	}

	public String getRepetirDniCon() {
		return repetirDniCon;
	}

	public void setRepetirDniCon(String repetirDniCon) {
		this.repetirDniCon = repetirDniCon;
	}

	public String getTreqCon1() {
		return treqCon1;
	}

	public void setTreqCon1(String treqCon1) {
		this.treqCon1 = treqCon1;
	}

	public String getTreqCon2() {
		return treqCon2;
	}

	public void setTreqCon2(String treqCon2) {
		this.treqCon2 = treqCon2;
	}

	public String getTreqCon3() {
		return treqCon3;
	}

	public void setTreqCon3(String treqCon3) {
		this.treqCon3 = treqCon3;
	}

	public String getEnfermedad2() {
		return enfermedad2;
	}

	public void setEnfermedad2(String enfermedad2) {
		this.enfermedad2 = enfermedad2;
	}

	public String getTratCon1() {
		return tratCon1;
	}

	public void setTratCon1(String tratCon1) {
		this.tratCon1 = tratCon1;
	}

	public String getTratCon2() {
		return tratCon2;
	}

	public void setTratCon2(String tratCon2) {
		this.tratCon2 = tratCon2;
	}

	public String getTratCon3() {
		return tratCon3;
	}

	public void setTratCon3(String tratCon3) {
		this.tratCon3 = tratCon3;
	}

	public String getTratCon4() {
		return tratCon4;
	}

	public void setTratCon4(String tratCon4) {
		this.tratCon4 = tratCon4;
	}

	public String getTratCon5() {
		return tratCon5;
	}

	public void setTratCon5(String tratCon5) {
		this.tratCon5 = tratCon5;
	}

	public String getTratCon6() {
		return tratCon6;
	}

	public void setTratCon6(String tratCon6) {
		this.tratCon6 = tratCon6;
	}

	public String getVresConA() {
		return vresConA;
	}

	public void setVresConA(String vresConA) {
		this.vresConA = vresConA;
	}

	public String getVresConB() {
		return vresConB;
	}

	public void setVresConB(String vresConB) {
		this.vresConB = vresConB;
	}

	public Integer getNred2() {
		return nred2;
	}

	public void setNred2(Integer nred2) {
		this.nred2 = nred2;
	}

	public String getImpoCon1() {
		return impoCon1;
	}

	public void setImpoCon1(String impoCon1) {
		this.impoCon1 = impoCon1;
	}

	public String getImpoCon2() {
		return impoCon2;
	}

	public void setImpoCon2(String impoCon2) {
		this.impoCon2 = impoCon2;
	}

	public String getImpoCon3() {
		return impoCon3;
	}

	public void setImpoCon3(String impoCon3) {
		this.impoCon3 = impoCon3;
	}

	public String getImpoCon4() {
		return impoCon4;
	}

	public void setImpoCon4(String impoCon4) {
		this.impoCon4 = impoCon4;
	}

//	public String getLocalidadConFirma() {
//		return localidadConFirma;
//	}
//
//	public void setLocalidadConFirma(String localidadConFirma) {
//		this.localidadConFirma = localidadConFirma;
//	}
//
//	public String getMesConFirma() {
//		return mesConFirma;
//	}
//
//	public void setMesConFirma(String mesConFirma) {
//		this.mesConFirma = mesConFirma;
//	}

	public Integer getSinPreferenciaTurnoBal() {
		return sinPreferenciaTurnoBal;
	}

	public void setSinPreferenciaTurnoBal(Integer sinPreferenciaTurnoBal) {
		this.sinPreferenciaTurnoBal = sinPreferenciaTurnoBal;
	}
	public String getApe2c() {
		return ape2c;
	}

	public void setApe2c(String ape2c) {
		this.ape2c = ape2c;
	}

	public String getAperape1() {
		return aperape1;
	}

	public void setAperape1(String aperape1) {
		this.aperape1 = aperape1;
	}

	public String getAperape2() {
		return aperape2;
	}

	public void setAperape2(String aperape2) {
		this.aperape2 = aperape2;
	}

	public String getApernom() {
		return apernom;
	}

	public void setApernom(String apernom) {
		this.apernom = apernom;
	}

	public String getArtiSol1() {
		return artiSol1;
	}

	public void setArtiSol1(String artiSol1) {
		this.artiSol1 = artiSol1;
	}

	public String getArtiSol2() {
		return artiSol2;
	}

	public void setArtiSol2(String artiSol2) {
		this.artiSol2 = artiSol2;
	}

	public String getArtiSol3() {
		return artiSol3;
	}

	public void setArtiSol3(String artiSol3) {
		this.artiSol3 = artiSol3;
	}

	public String getArtiSol4() {
		return artiSol4;
	}

	public void setArtiSol4(String artiSol4) {
		this.artiSol4 = artiSol4;
	}

	public String getArtiSol5() {
		return artiSol5;
	}

	public void setArtiSol5(String artiSol5) {
		this.artiSol5 = artiSol5;
	}

	public String getArtiSol6() {
		return artiSol6;
	}

	public void setArtiSol6(String artiSol6) {
		this.artiSol6 = artiSol6;
	}

	public String getArtiCon1() {
		return artiCon1;
	}

	public void setArtiCon1(String artiCon1) {
		this.artiCon1 = artiCon1;
	}

	public String getArtiCon2() {
		return artiCon2;
	}

	public void setArtiCon2(String artiCon2) {
		this.artiCon2 = artiCon2;
	}

	public String getArtiCon3() {
		return artiCon3;
	}

	public void setArtiCon3(String artiCon3) {
		this.artiCon3 = artiCon3;
	}

	public String getArtiCon4() {
		return artiCon4;
	}

	public void setArtiCon4(String artiCon4) {
		this.artiCon4 = artiCon4;
	}

	public String getArtiCon5() {
		return artiCon5;
	}

	public void setArtiCon5(String artiCon5) {
		this.artiCon5 = artiCon5;
	}

	public String getArtiCon6() {
		return artiCon6;
	}

	public void setArtiCon6(String artiCon6) {
		this.artiCon6 = artiCon6;
	}

	public String getDnic() {
		return dnic;
	}

	public void setDnic(String dnic) {
		this.dnic = dnic;
	}

	public String getClasPenSol1() {
		return clasPenSol1;
	}

	public void setClasPenSol1(String clasPenSol1) {
		this.clasPenSol1 = clasPenSol1;
	}

	public String getClasPenSol1Desc() {
		return clasPenSol1Desc;
	}

	public void setClasPenSol1Desc(String clasPenSol1Desc) {
		this.clasPenSol1Desc = clasPenSol1Desc;
	}

	public String getClasPenSol2() {
		return clasPenSol2;
	}

	public void setClasPenSol2(String clasPenSol2) {
		this.clasPenSol2 = clasPenSol2;
	}

	public String getClasPenSol2Desc() {
		return clasPenSol2Desc;
	}

	public void setClasPenSol2Desc(String clasPenSol2Desc) {
		this.clasPenSol2Desc = clasPenSol2Desc;
	}

	public String getClasPenCon1() {
		return clasPenCon1;
	}

	public void setClasPenCon1(String clasPenCon1) {
		this.clasPenCon1 = clasPenCon1;
	}

	public String getClasPenCon1Desc() {
		return clasPenCon1Desc;
	}

	public void setClasPenCon1Desc(String clasPenCon1Desc) {
		this.clasPenCon1Desc = clasPenCon1Desc;
	}

	public String getClasPenCon2() {
		return clasPenCon2;
	}

	public void setClasPenCon2(String clasPenCon2) {
		this.clasPenCon2 = clasPenCon2;
	}

	public String getClasPenCon2Desc() {
		return clasPenCon2Desc;
	}

	public void setClasPenCon2Desc(String clasPenCon2Desc) {
		this.clasPenCon2Desc = clasPenCon2Desc;
	}

	public String getDnir() {
		return dnir;
	}

	public void setDnir(String dnir) {
		this.dnir = dnir;
	}

	public String getCfam() {
		return cfam;
	}

	public void setCfam(String cfam) {
		this.cfam = cfam;
	}


	public String getDefoSol1() {
		return defoSol1;
	}

	public void setDefoSol1(String defoSol1) {
		this.defoSol1 = defoSol1;
	}

	public String getDefoSol2() {
		return defoSol2;
	}

	public void setDefoSol2(String defoSol2) {
		this.defoSol2 = defoSol2;
	}

	public String getDefoSol3() {
		return defoSol3;
	}

	public void setDefoSol3(String defoSol3) {
		this.defoSol3 = defoSol3;
	}

	public String getDefoSol4() {
		return defoSol4;
	}

	public void setDefoSol4(String defoSol4) {
		this.defoSol4 = defoSol4;
	}

//	public String getDiaSolFirma() {
//		return diaSolFirma;
//	}
//
//	public void setDiaSolFirma(String diaSolFirma) {
//		this.diaSolFirma = diaSolFirma;
//	}

	public String getDefoCon1() {
		return defoCon1;
	}

	public void setDefoCon1(String defoCon1) {
		this.defoCon1 = defoCon1;
	}

	public String getDefoCon2() {
		return defoCon2;
	}

	public void setDefoCon2(String defoCon2) {
		this.defoCon2 = defoCon2;
	}

	public String getDefoCon3() {
		return defoCon3;
	}

	public void setDefoCon3(String defoCon3) {
		this.defoCon3 = defoCon3;
	}

	public String getDefoCon4() {
		return defoCon4;
	}

	public void setDefoCon4(String defoCon4) {
		this.defoCon4 = defoCon4;
	}

//	public String getDiaConFirma() {
//		return diaConFirma;
//	}
//
//	public void setDiaConFirma(String diaConFirma) {
//		this.diaConFirma = diaConFirma;
//	}

	public String getApe1c() {
		return ape1c;
	}

	public void setApe1c(String ape1c) {
		this.ape1c = ape1c;
	}

	public Long getCcaa1() {
		return ccaa1;
	}

	public void setCcaa1(Long ccaa1) {
		this.ccaa1 = ccaa1;
	}

	public String getCcaa1Desc() {
		return ccaa1Desc;
	}

	public void setCcaa1Desc(String ccaa1Desc) {
		this.ccaa1Desc = ccaa1Desc;
	}

	public Long getCcaa2() {
		return ccaa2;
	}

	public void setCcaa2(Long ccaa2) {
		this.ccaa2 = ccaa2;
	}

	public String getCcaa2Desc() {
		return ccaa2Desc;
	}

	public void setCcaa2Desc(String ccaa2Desc) {
		this.ccaa2Desc = ccaa2Desc;
	}

	public Long getCcaa3() {
		return ccaa3;
	}

	public void setCcaa3(Long ccaa3) {
		this.ccaa3 = ccaa3;
	}

	public String getCcaa3Desc() {
		return ccaa3Desc;
	}

	public void setCcaa3Desc(String ccaa3Desc) {
		this.ccaa3Desc = ccaa3Desc;
	}

	public Long getCcaa4() {
		return ccaa4;
	}

	public void setCcaa4(Long ccaa4) {
		this.ccaa4 = ccaa4;
	}

	public String getCcaa4Desc() {
		return ccaa4Desc;
	}

	public void setCcaa4Desc(String ccaa4Desc) {
		this.ccaa4Desc = ccaa4Desc;
	}

	public String getBals1() {
		return bals1;
	}

	public void setBals1(String bals1) {
		this.bals1 = bals1;
	}

	public String getBals1Desc() {
		return bals1Desc;
	}

	public void setBals1Desc(String bals1Desc) {
		this.bals1Desc = bals1Desc;
	}

	public String getBals2() {
		return bals2;
	}

	public void setBals2(String bals2) {
		this.bals2 = bals2;
	}

	public String getBals2Desc() {
		return bals2Desc;
	}

	public void setBals2Desc(String bals2Desc) {
		this.bals2Desc = bals2Desc;
	}

	public String getBals3() {
		return bals3;
	}

	public void setBals3(String bals3) {
		this.bals3 = bals3;
	}

	public String getBals3Desc() {
		return bals3Desc;
	}

	public void setBals3Desc(String bals3Desc) {
		this.bals3Desc = bals3Desc;
	}

	public String getBals4() {
		return bals4;
	}

	public void setBals4(String bals4) {
		this.bals4 = bals4;
	}

	public String getBals4Desc() {
		return bals4Desc;
	}

	public void setBals4Desc(String bals4Desc) {
		this.bals4Desc = bals4Desc;
	}

	public Integer getCdef1() {
		return cdef1;
	}

	public void setCdef1(Integer cdef1) {
		this.cdef1 = cdef1;
	}

	public Integer getCdef2() {
		return cdef2;
	}

	public void setCdef2(Integer cdef2) {
		this.cdef2 = cdef2;
	}
	public String getDispositivoElectronico() {
		return dispositivoElectronico;
	}

	public void setDispositivoElectronico(String dispositivoElectronico) {
		this.dispositivoElectronico = dispositivoElectronico;
	}
	public Boolean getAutorizaSol() {
		return autorizaSol;
	}

	public void setAutorizaSol(Boolean autorizaSol) {
		this.autorizaSol = autorizaSol;
	}
	public Boolean getAutorizaCon() {
		return autorizaCon;
	}

	public void setAutorizaCon(Boolean autorizaCon) {
		this.autorizaCon = autorizaCon;
	}
	public String getPrvd() {
		return prvd;
	}

	public void setPrvd(String prvd) {
		this.prvd = prvd;
	}

	public String getPrvdNotif() {
		return prvdNotif;
	}

	public void setPrvdNotif(String prvdNotif) {
		this.prvdNotif = prvdNotif;
	}
	
}
