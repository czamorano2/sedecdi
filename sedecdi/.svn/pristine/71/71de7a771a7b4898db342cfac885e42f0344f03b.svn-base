package es.imserso.sede.util.rest.client;

import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.hermes.CartaAcreditacionRestClient;

/**
 * Obtiene el plazo por defecto de Hermes
 * 
 * @author 11825775
 *
 */
public class Plazo extends AbstractRestClient implements RestClient {

	private static final long serialVersionUID = -6981232141724853780L;

	private static final Logger log = Logger.getLogger(CartaAcreditacionRestClient.class.getName());

	@Inject
	protected Event<EventMessage> eventMessage;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.util.rest.client.RestClient#manageResponse()
	 */
	@Override
	public Object execute() throws SedeException {
		PlazoDTO plazo = null;

		try {

			log.debug("leyendo response ...");
			plazo = response.readEntity(PlazoDTO.class);

		} catch (Exception e) {
			String errmsg = "error al obtener el plazo por defecto de Hermes: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			throw new SedeException(errmsg, e);

		} finally {
			if (response != null) {
				response.close();
			}
		}

		return (Object) plazo;
	}

}
