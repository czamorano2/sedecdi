package es.imserso.sede.config;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * <p>
 * Este componente, en el contexto APPLICATION, puede inyectarse en cualquier
 * componente y tomar de él información relevante para la aplicación como el
 * directorio de los ficheros de entrada y salida, los subdirectorios de errores
 * y remesas y otra información extraida del directorio de configuración de la
 * aplicación.
 * </p>
 * <p>
 * Se carga mediante el propertyConfigurator que se ejecuta en el arranque de la
 * aplicación.
 * </p>
 * <p>
 * Tras ejecutarse el propertyConfigurator se ejecutará el configValidator que
 * validará que toda la configuración sea correcta lanzando excepciones que
 * detendrían el arranque de la aplicación si hay algo mal.
 * </p>
 * 
 * @author 11825775
 *
 */
@ApplicationScoped
@Named("propertyComponent")
public class PropertyComponent {

	@Inject
	Configuracion configuracion;
	
	private String mailServer;
	private String pop3Host;
	private String pop3Port;
	private String smtpHost;
	private String smtpPort;
	
	private String iSicresUsername;
	private String iSicresPassword;
	
	//registers
	private String iSicresRegistersServiceName;
	private String iSicresRegistersQNameLocalPart;
	private String iSicresRegistersNamespaceURI;
	private String iSicresRegistersTargetNamespace;
	private String iSicresRegistersWsdlUrl;
	
	//books
		private String iSicresBooksServiceName;
		private String iSicresBooksQNameLocalPart;
		private String iSicresBooksNamespaceURI;
		private String iSicresBooksTargetNamespace;
		private String iSicresBooksWsdlUrl;
	
	
	private String iSicresSender;
	private String iSicresDestination;
	private String iSicresTransportType;
	private String iSicresTransportNumber;
	private String iSicresMatterType;
//	private String iSicresNombreDocumentoSolicitud;
	
	private String turismoResourcesURL;
	private String termalismoResourcesURL;
	private String tramiteUCMservice;
	private String ucmResourcesURL;
	

	/**
	 * lista de usuarios de la aplicación
	 */
	private List<String> userList;

	/**
	 * path donde apunta -Dapplication_properties
	 */
	private String configurationPath = null;

	/**
	 * path donde apunta -Dapplication_properties más el nombre de la aplicación
	 */
	private String configurationApplicationPath = null;

	/**
	 * ruta donde se encuentra el engine para la generacion de listados
	 */
	private String reportEngineHome;

	/**
	 * nombre JNDI para coger la transacción del contenedor
	 */
	private String userTransactionJndiName;

	/**
	 * nombre JNDI para el gestor de transacciones del contenedor
	 */
	private String transactionManagerJndiName;

	/**
	 * número de iteraciones necesarias para hacer el flush del contexto de
	 * persistencia
	 * <p>
	 * este número debe coincidir con el valor de la propiedad
	 * hibernate.jdbc.batch_size en persistence.xml
	 */
	private int iterationsToFlush;

	/**
	 * segundos que se mantiene la misma transacción para los procesos
	 */
	private int processTransactionTimeoutSeconds;

	/**
	 * segundos que se mantiene la misma transacción para los listados
	 */
	private int reportTransactionTimeoutSeconds;

	/**
	 * directorio de entrada de recursos de la aplicación (relativa a
	 * ${application_properties})
	 */
	private String inputFolder;

	/**
	 * directorio de salida de recursos de la aplicación (relativa a
	 * ${application_properties})
	 */
	private String outputFolder;

	/**
	 * directorio donde dejaremos los ficheros temporales que genere la
	 * aplicación (relativa a ${application_properties})
	 */
	private String tempFolder;

	/**
	 * directorio donde dejaremos los ficheros necesarios para los test
	 * (relativa a ${application_properties})
	 */
	private String testFolder;

	private String logFolder;

	/**
	 * directorio donde se dejan los logs de los informes
	 */
	private String reportLogFolder;

	/**
	 * ruta en la que se encontrarán las plantillas para generar report de todos
	 * los listados de la aplicacion
	 */
	private String reportFolder;

	/**
	 * tamaño del buffer de escritura en fichero (siempre multiplos de 1024)
	 */
	private int dataOutputBufferSize;

	private String rutaImagenLogoImserso;

	private String rutaHojaEstilosImserso;

	private String acceptedTypes;

	/**
	 * número de iteraciones necesarias para hacer el clear en los script que
	 * leen datos para los listados
	 */
	private int reportScriptIterations;

	/**
	 * número de iteraciones necesarias para lanzar el evento
	 * 'report.siguiente.iteration' que informa del número de registros
	 * procesados
	 */
	private int reportIterationsToNotifyReportProgressBar;

	/**
	 * ruta donde se dejarán los listados generados. A partir de aqui se creará
	 * el directorio ejercicio y cada usuario tendrá a su vez, su propio
	 * directorio (relativa a ${application.data.output.folder})
	 */
	private String reportOutputFolder;
	
	/**
	 * Cuenta de correo de los planificadores (donde se enviarán los correos de errores de la aplicación)
	 */
	private String emailPlanificadores;

	/**
	 * Cuenta de correo de sistemas (donde se enviarán los correos de errores de configuración, base de datos, etc)
	 */
	private String emailSistemas;
	
	/**
	 * url del servicio REST de firma de documentos
	 */
	private String urlDocumentSignatureService;
	
	/**
	 * url para comprobar si el servicio REST de firma de documentos está escuchando
	 */
	private String urlDocumentSignatureIsAlive;
	
	/**
	 * url para comprobar si los servicio REST de firma de documentos están funcionando
	 */
	private String urlDocumentSignatureServicesTest;

	/**
	 * @return the reportEngineHome
	 */
	public String getReportEngineHome() {
		return reportEngineHome;
	}

	/**
	 * @param reportEngineHome
	 *            the reportEngineHome to set
	 */
	public void setReportEngineHome(String reportEngineHome) {
		this.reportEngineHome = reportEngineHome;
	}

	public String getInputFolder() {
		return getConfigurationApplicationPath() + inputFolder;
	}

	public void setInputFolder(String inputFolder) {
		this.inputFolder = inputFolder;
	}

	public String getOutputFolder() {
		return getConfigurationApplicationPath() + outputFolder;
	}

	public void setOutputFolder(String outputFolder) {
		this.outputFolder = outputFolder;
	}

	public String getTempFolder() {
		return getConfigurationApplicationPath() + tempFolder;
	}

	public void setTempFolder(String tempFolder) {
		this.tempFolder = tempFolder;
	}

	public String getTestFolder() {
		return getConfigurationApplicationPath() + testFolder;
	}

	public void setTestFolder(String testFolder) {
		this.testFolder = testFolder;
	}

	public String getLogFolder() {
		return getConfigurationApplicationPath() + logFolder;
	}

	public void setLogFolder(String logFolder) {
		this.logFolder = logFolder;
	}

	public String getReportFolder() {
		return getInputFolder() + reportFolder;
	}

	public void setReportFolder(String reportFolder) {
		this.reportFolder = reportFolder;
	}

	public String getConfigurationPath() {
		if (configurationPath == null) {
			configurationPath = System.getProperty(configuracion.getVariableDeEntorno());
		}
		return configurationPath;
	}

	public void setConfigurationPath(String configurationPath) {
		this.configurationPath = configurationPath;
	}

	public String getConfigurationApplicationPath() {
		if (configurationApplicationPath == null) {
			configurationApplicationPath = getConfigurationPath() + "/" + configuracion.getPathAplicacion();
		}
		return configurationApplicationPath;
	}

	public void setConfigurationApplicationPath(String configurationApplicationPath) {
		this.configurationApplicationPath = configurationApplicationPath;
	}

	/**
	 * @return the userTransactionJndiName
	 */
	public String getUserTransactionJndiName() {
		return userTransactionJndiName;
	}

	/**
	 * @param userTransactionJndiName
	 *            the userTransactionJndiName to set
	 */
	public void setUserTransactionJndiName(String userTransactionJndiName) {
		this.userTransactionJndiName = userTransactionJndiName;
	}

	/**
	 * @return the transactionManagerJndiName
	 */
	public String getTransactionManagerJndiName() {
		return transactionManagerJndiName;
	}

	/**
	 * @param transactionManagerJndiName
	 *            the transactionManagerJndiName to set
	 */
	public void setTransactionManagerJndiName(String transactionManagerJndiName) {
		this.transactionManagerJndiName = transactionManagerJndiName;
	}

	/**
	 * @return the iterationsToFlush
	 */
	public int getIterationsToFlush() {
		return iterationsToFlush;
	}

	/**
	 * @param iterationsToFlush
	 *            the iterationsToFlush to set
	 */
	public void setIterationsToFlush(int iterationsToFlush) {
		this.iterationsToFlush = iterationsToFlush;
	}

	/**
	 * @return the processTransactionTimeoutSeconds
	 */
	public int getProcessTransactionTimeoutSeconds() {
		return processTransactionTimeoutSeconds;
	}

	/**
	 * @param processTransactionTimeoutSeconds
	 *            the processTransactionTimeoutSeconds to set
	 */
	public void setProcessTransactionTimeoutSeconds(int processTransactionTimeoutSeconds) {
		this.processTransactionTimeoutSeconds = processTransactionTimeoutSeconds;
	}

	/**
	 * @return the reportTransactionTimeoutSeconds
	 */
	public int getReportTransactionTimeoutSeconds() {
		return reportTransactionTimeoutSeconds;
	}

	/**
	 * @param reportTransactionTimeoutSeconds
	 *            the reportTransactionTimeoutSeconds to set
	 */
	public void setReportTransactionTimeoutSeconds(int reportTransactionTimeoutSeconds) {
		this.reportTransactionTimeoutSeconds = reportTransactionTimeoutSeconds;
	}

	/**
	 * @return the reportLogFolder
	 */
	public String getReportLogFolder() {
		return getConfigurationApplicationPath() + reportLogFolder;
	}

	/**
	 * @param reportLogFolder
	 *            the reportLogFolder to set
	 */
	public void setReportLogFolder(String reportLogFolder) {
		this.reportLogFolder = reportLogFolder;
	}

	/**
	 * @return the dataOutputBufferSize
	 */
	public int getDataOutputBufferSize() {
		return dataOutputBufferSize;
	}

	/**
	 * @param dataOutputBufferSize
	 *            the dataOutputBufferSize to set
	 */
	public void setDataOutputBufferSize(int dataOutputBufferSize) {
		this.dataOutputBufferSize = dataOutputBufferSize;
	}

	/**
	 * @return the rutaImagenLogoImserso
	 */
	public String getRutaImagenLogoImserso() {
		return rutaImagenLogoImserso;
	}

	/**
	 * @param rutaImagenLogoImserso
	 *            the rutaImagenLogoImserso to set
	 */
	public void setRutaImagenLogoImserso(String rutaImagenLogoImserso) {
		this.rutaImagenLogoImserso = rutaImagenLogoImserso;
	}

	/**
	 * @return the rutaHojaEstilosImserso
	 */
	public String getRutaHojaEstilosImserso() {
		return rutaHojaEstilosImserso;
	}

	/**
	 * @param rutaHojaEstilosImserso
	 *            the rutaHojaEstilosImserso to set
	 */
	public void setRutaHojaEstilosImserso(String rutaHojaEstilosImserso) {
		this.rutaHojaEstilosImserso = rutaHojaEstilosImserso;
	}

	/**
	 * @return the acceptedTypes
	 */
	public String getAcceptedTypes() {
		return acceptedTypes;
	}

	/**
	 * @param acceptedTypes
	 *            the acceptedTypes to set
	 */
	public void setAcceptedTypes(String acceptedTypes) {
		this.acceptedTypes = acceptedTypes;
	}

	/**
	 * @return the reportScriptIterations
	 */
	public int getReportScriptIterations() {
		return reportScriptIterations;
	}

	/**
	 * @param reportScriptIterations
	 *            the reportScriptIterations to set
	 */
	public void setReportScriptIterations(int reportScriptIterations) {
		this.reportScriptIterations = reportScriptIterations;
	}

	/**
	 * @return the reportIterationsToNotifyReportProgressBar
	 */
	public int getReportIterationsToNotifyReportProgressBar() {
		return reportIterationsToNotifyReportProgressBar;
	}

	/**
	 * @param reportIterationsToNotifyReportProgressBar
	 *            the reportIterationsToNotifyReportProgressBar to set
	 */
	public void setReportIterationsToNotifyReportProgressBar(int reportIterationsToNotifyReportProgressBar) {
		this.reportIterationsToNotifyReportProgressBar = reportIterationsToNotifyReportProgressBar;
	}

	public String getMailServer() {
		return mailServer;
	}

	public void setMailServer(String mailServer) {
		this.mailServer = mailServer;
	}

	public String getPop3Host() {
		return pop3Host;
	}

	public void setPop3Host(String pop3Host) {
		this.pop3Host = pop3Host;
	}

	public String getPop3Port() {
		return pop3Port;
	}

	public void setPop3Port(String pop3Port) {
		this.pop3Port = pop3Port;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	/**
	 * @return the reportOutputFolder
	 */
	public String getReportOutputFolder() {
		return getOutputFolder() + reportOutputFolder;
	}

	/**
	 * @param reportOutputFolder
	 *            the reportOutputFolder to set
	 */
	public void setReportOutputFolder(String reportOutputFolder) {
		this.reportOutputFolder = reportOutputFolder;
	}

	/**
	 * @return the userList
	 */
	public List<String> getUserList() {
		return userList;
	}

	/**
	 * @param userList
	 *            the userList to set
	 */
	public void setUserList(List<String> userList) {
		this.userList = userList;
	}

	public String getUrlDocumentSignatureService() {
		return urlDocumentSignatureService;
	}

	public void setUrlDocumentSignatureService(String urlDocumentSignatureService) {
		this.urlDocumentSignatureService = urlDocumentSignatureService;
	}

	public Configuracion getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(Configuracion configuracion) {
		this.configuracion = configuracion;
	}

	public String getiSicresRegistersServiceName() {
		return iSicresRegistersServiceName;
	}

	public void setiSicresRegistersServiceName(String iSicresRegistersServiceName) {
		this.iSicresRegistersServiceName = iSicresRegistersServiceName;
	}

	public String getiSicresRegistersTargetNamespace() {
		return iSicresRegistersTargetNamespace;
	}

	public void setiSicresRegistersTargetNamespace(String iSicresRegistersTargetNamespace) {
		this.iSicresRegistersTargetNamespace = iSicresRegistersTargetNamespace;
	}

	public String getiSicresRegistersWsdlUrl() {
		return "file:/" + this.getInputFolder() + iSicresRegistersWsdlUrl;
	}

	public void setiSicresRegistersWsdlUrl(String iSicresRegistersWsdlUrl) {
		this.iSicresRegistersWsdlUrl = iSicresRegistersWsdlUrl;
	}
	
	public String getiSicresRegistersQNameLocalPart() {
		return iSicresRegistersQNameLocalPart;
	}

	public void setiSicresRegistersQNameLocalPart(String iSicresRegistersQNameLocalPart) {
		this.iSicresRegistersQNameLocalPart = iSicresRegistersQNameLocalPart;
	}

	public String getiSicresRegistersNamespaceURI() {
		return iSicresRegistersNamespaceURI;
	}

	public void setiSicresRegistersNamespaceURI(String iSicresRegistersNamespaceURI) {
		this.iSicresRegistersNamespaceURI = iSicresRegistersNamespaceURI;
	}
	
	public String getiSicresBooksServiceName() {
		return iSicresBooksServiceName;
	}

	public void setiSicresBooksServiceName(String iSicresBooksServiceName) {
		this.iSicresBooksServiceName = iSicresBooksServiceName;
	}

	public String getiSicresBooksTargetNamespace() {
		return iSicresBooksTargetNamespace;
	}

	public void setiSicresBooksTargetNamespace(String iSicresBooksTargetNamespace) {
		this.iSicresBooksTargetNamespace = iSicresBooksTargetNamespace;
	}

	public String getiSicresBooksWsdlUrl() {
		return "file:/" + this.getInputFolder() + iSicresBooksWsdlUrl;
	}

	public void setiSicresBooksWsdlUrl(String iSicresBookWsdlUrl) {
		this.iSicresBooksWsdlUrl = iSicresBookWsdlUrl;
	}
	
	public String getiSicresBooksQNameLocalPart() {
		return iSicresBooksQNameLocalPart;
	}

	public void setiSicresBooksQNameLocalPart(String iSicresBooksQNameLocalPart) {
		this.iSicresBooksQNameLocalPart = iSicresBooksQNameLocalPart;
	}

	public String getiSicresBooksNamespaceURI() {
		return iSicresBooksNamespaceURI;
	}

	public void setiSicresBooksNamespaceURI(String iSicresBooksNamespaceURI) {
		this.iSicresBooksNamespaceURI = iSicresBooksNamespaceURI;
	}

	public String getiSicresUsername() {
		return iSicresUsername;
	}

	public void setiSicresUsername(String iSicresUsername) {
		this.iSicresUsername = iSicresUsername;
	}

	public String getiSicresPassword() {
		return iSicresPassword;
	}

	public void setiSicresPassword(String iSicresPassword) {
		this.iSicresPassword = iSicresPassword;
	}


//	public String getiSicresNombreDocumentoSolicitud() {
//		return iSicresNombreDocumentoSolicitud;
//	}

//	public void setiSicresNombreDocumentoSolicitud(String iSicresNombreDocumentoSolicitud) {
//		this.iSicresNombreDocumentoSolicitud = iSicresNombreDocumentoSolicitud;
//	}

	public String getTermalismoResourcesURL() {
		return termalismoResourcesURL;
	}

	public void setTermalismoResourcesURL(String termalismoResourcesURL) {
		this.termalismoResourcesURL = termalismoResourcesURL;
	}

	public String getTurismoResourcesURL() {
		return turismoResourcesURL;
	}

	public void setTurismoResourcesURL(String turismoResourcesURL) {
		this.turismoResourcesURL = turismoResourcesURL;
	}

	

	/**
	 * @return the iSicresSender
	 */
	public String getiSicresSender() {
		return iSicresSender;
	}

	/**
	 * @param iSicresSender the iSicresSender to set
	 */
	public void setiSicresSender(String iSicresSender) {
		this.iSicresSender = iSicresSender;
	}

	/**
	 * @return the iSicresDestination
	 */
	public String getiSicresDestination() {
		return iSicresDestination;
	}

	/**
	 * @param iSicresDestination the iSicresDestination to set
	 */
	public void setiSicresDestination(String iSicresDestination) {
		this.iSicresDestination = iSicresDestination;
	}

	/**
	 * @return the iSicresTransportType
	 */
	public String getiSicresTransportType() {
		return iSicresTransportType;
	}

	/**
	 * @param iSicresTransportType the iSicresTransportType to set
	 */
	public void setiSicresTransportType(String iSicresTransportType) {
		this.iSicresTransportType = iSicresTransportType;
	}

	/**
	 * @return the iSicresTransportNumber
	 */
	public String getiSicresTransportNumber() {
		return iSicresTransportNumber;
	}

	/**
	 * @param iSicresTransportNumber the iSicresTransportNumber to set
	 */
	public void setiSicresTransportNumber(String iSicresTransportNumber) {
		this.iSicresTransportNumber = iSicresTransportNumber;
	}

	/**
	 * @return the iSicresMatterType
	 */
	public String getiSicresMatterType() {
		return iSicresMatterType;
	}

	/**
	 * @param iSicresMatterType the iSicresMatterType to set
	 */
	public void setiSicresMatterType(String iSicresMatterType) {
		this.iSicresMatterType = iSicresMatterType;
	}

	public String getEmailPlanificadores() {
		return emailPlanificadores;
	}

	public void setEmailPlanificadores(String emailPlanificadores) {
		this.emailPlanificadores = emailPlanificadores;
	}

	public String getEmailSistemas() {
		return emailSistemas;
	}

	public void setEmailSistemas(String emailSistemas) {
		this.emailSistemas = emailSistemas;
	}

	public String getTramiteUCMservice() {
		return tramiteUCMservice;
	}

	public void setTramiteUCMservice(String tramiteUCMservice) {
		this.tramiteUCMservice = tramiteUCMservice;
	}

	public String getUcmResourcesURL() {
		return ucmResourcesURL;
	}

	public void setUcmResourcesURL(String ucmResourcesURL) {
		this.ucmResourcesURL = ucmResourcesURL;
	}

	public String getUrlDocumentSignatureIsAlive() {
		return urlDocumentSignatureIsAlive;
	}

	public void setUrlDocumentSignatureIsAlive(String urlDocumentSignatureIsAlive) {
		this.urlDocumentSignatureIsAlive = urlDocumentSignatureIsAlive;
	}

	public String getUrlDocumentSignatureServicesTest() {
		return urlDocumentSignatureServicesTest;
	}

	public void setUrlDocumentSignatureServicesTest(String urlDocumentSignatureServicesTest) {
		this.urlDocumentSignatureServicesTest = urlDocumentSignatureServicesTest;
	}

	

	
}
