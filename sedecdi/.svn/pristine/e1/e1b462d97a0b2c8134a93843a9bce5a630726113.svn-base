package es.imserso.sede.service.registration.solicitud;

import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.validation.constraints.NotNull;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.util.exception.ValidationException;

/**
 * Interfaz para registrar una solicitud en la Sede Electrónica
 * 
 * @author 11825775
 *
 */
@Stateful
@TransactionManagement(TransactionManagementType.BEAN)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@AccessTimeout(60000)
public interface SolicitudRegistrationI {

	/**
	 * Ejecuta cada una de las fases para registrar una solicitud en la Sede
	 * Electrónica.
	 * <p>
	 * Cualquier solicitud que se registre en la Sede debe hacerse a través de una
	 * implementación de esta interface
	 * 
	 * @param dto
	 *            Datos de la solicitud del trámite.
	 * @return Solicitud creada
	 * @throws ValidationException
	 *             si la solicitud no es válida
	 * @throws RegistrationException
	 *             si no se ha podido registrar correctamente la solicitud en la
	 *             Sede Electrónica
	 */
	public Solicitud register(@NotNull SolicitudDTOI dto) throws ValidationException, RegistrationException;

}
