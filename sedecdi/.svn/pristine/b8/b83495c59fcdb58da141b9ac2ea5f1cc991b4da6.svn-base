package es.imserso.sede.web.view.alta;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.TramiteRepository;
import es.imserso.sede.data.dto.AttachedFile;
import es.imserso.sede.data.dto.impl.GenericoDTO;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.model.Tramite;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.service.registration.solicitud.documentos.TipoDocumentoRegistrado;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.util.route.ParamValues.Params;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.DtoFactory;
import es.imserso.sede.web.service.registration.solicitud.DtoFactoryQ;
import es.imserso.sede.web.service.registration.solicitud.impl.generico.GenericoSolicitudRegistrationQ;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Bean de respaldo para el dto de genérico
 * 
 * @author 11825775
 *
 */
@Named(value = "genericoView")
@ViewScoped
@Secure
public class SolicitudGenericoView extends AbstractSolicitudView implements Serializable {

	private static final long serialVersionUID = 2248273270416839828L;

	@Inject
	Logger log;

	@Inject
	@GenericoSolicitudRegistrationQ
	private SolicitudRegistrationI genericoSolicitudRegistration;

	@Inject
	protected SolicitudRepository solicitudRepository;

	@Inject
	@DtoFactoryQ
	DtoFactory dtoFactory;

	@Inject
	protected TramiteRepository tramiteRepository;

	private GenericoDTO dto;

	private Solicitud solicitud;

	private UploadedFile uploadedFile;

	private Tramite tramite;

	@PostConstruct
	public void onCreate() {
		super.init();
		// crea la entidad para la solicitud
		dto = dtoFactory.createGenericoDtoInstance();
	}

	/**
	 * Persiste la solicitud en el sistema
	 * <p>
	 * Lanza la ejecución de las fases de registro de la solicitud
	 * 
	 * @return outcome para ir a la vista de respuesta si todo ha ido bien, o a la
	 *         vista de error si no se ha podido finalizar correctamente el proceso
	 *         de registro.
	 */
	public void save() {

		this.setSolicitud(null);
		String respuestaAltaRedirect = null;
		try {

			log.infof("[{0}] guardando la solicitud a partir de los datos del formuario ...",
					paramValues.getParamValue(Params.sia).getValue());
			this.setSolicitud(genericoSolicitudRegistration.register(dto));
			log.info("solicitud registrada!");

			respuestaAltaRedirect = "/app/respuesta_alta?faces-redirect=true&idSolicitud=" + this.solicitud.getId();
			log.infov("redirigimos a la vista de respuesta de alta de solicitud: {0}", respuestaAltaRedirect);

			((ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler())
					.performNavigation(respuestaAltaRedirect);

		} catch (ValidationException e) {
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("messages");

		} catch (Exception e) {

			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.GENERICO,
					"No se pudo dar de alta la solicitud", Utils.getExceptionMessage(e));
			// redirigimos a la vista de error
			respuestaAltaRedirect = "error";
			((ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler())
					.performNavigation(respuestaAltaRedirect);
		}

	}

	public void fileUploadListener(FileUploadEvent e) {
		log.info("Añadiendo fichero adjunto...");

		this.uploadedFile = e.getFile();

		if (this.uploadedFile == null) {
			log.warn("No hay fichero para adjuntar");
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("GenericoForm:messages");
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.GENERICO, "Ficheros Adjuntos",
					"No se ha recibido ningún fichero para adjuntar");
			return;
		}

		log.infov("Uploaded File Name Is :: {0} :: Uploaded File Size :: {1} ::", uploadedFile.getFileName(),
				uploadedFile.getSize());

		// asignamos los ficheros adjuntos al DTO
		AttachedFile attachedFile = new AttachedFile(TipoDocumentoRegistrado.PERSONAL.toString(),
				this.uploadedFile.getFileName(), this.uploadedFile.getContents());
		if (!this.dto.getAttachedFiles().contains(attachedFile)) {
			log.info("Añadiendo fichero adjunto...");
			this.dto.addAttachedFile(attachedFile);
		}

	}

	public GenericoDTO getDto() {
		return this.dto;
	}

	public void setDto(GenericoDTO _dto) {
		this.dto = _dto;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public Tramite getTramite() {
		if (tramite == null) {
			tramite = tramiteRepository.findBySIA(dto.getCodigoSIA());
			if (tramite == null)
				throw new SedeRuntimeException("No se ha encontrado trámite con el código SIA:" + dto.getCodigoSIA());
		}
		return tramite;
	}

}
