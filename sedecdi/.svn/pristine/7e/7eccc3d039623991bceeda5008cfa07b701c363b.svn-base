/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package es.imserso.sede.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @todo add comment for javadoc
 *
 * @author 11825775
 *
 */
@Entity
@SequenceGenerator(name = "SEQ_PLANTILLA_EMAIL", initialValue = 1, allocationSize = 1, sequenceName = "SEC_PLANTILLA_EMAIL")
public class PlantillaEmail implements Serializable, Cloneable {

	private static final long serialVersionUID = 1909458682305058093L;

	public PlantillaEmail() {
	}

	/**
	 * ------------------------------------------ The primary key.
	 *
	 *
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PLANTILLA_EMAIL")
	public Long getId() {
		return id;
	}


	public void setId(final Long id) {
		this.id = id;
	}


	private Long id = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	public String getTexto() {
		return texto;
	}


	public void setTexto(final String texto) {
		this.texto = texto;
	}


	private String texto = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	@ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	public Idioma getIdioma() {
		return idioma;
	}


	public void setIdioma(final Idioma idioma) {
		this.idioma = idioma;
	}


	private Idioma idioma = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "plantillaEmail", cascade = CascadeType.MERGE, orphanRemoval = false)
	public List<Tramite> getTramites() {
		if (this.tramites == null) {
			this.tramites = new ArrayList<Tramite>();
		}
		return tramites;
	}


	public void setTramites(final List<Tramite> tramites) {
		this.tramites = tramites;
	}

	/**
	 * Associate PlantillaEmail with Tramite
	 * 
	 *
	 */
	public void addTramite(Tramite tramite) {
		if (tramite == null) {
			return;
		}
		getTramites().add(tramite);
		tramite.setPlantillaEmail(this);
	}

	/**
	 * Unassociate PlantillaEmail from Tramite
	 * 
	 *
	 */
	public void removeTramite(Tramite tramite) {
		if (tramite == null) {
			return;
		}
		getTramites().remove(tramite);
		tramite.setPlantillaEmail(null);
	}

	/**
	 *
	 */
	public void removeAllTramites() {
		List<Tramite> remove = new ArrayList<Tramite>();
		remove.addAll(getTramites());
		for (Tramite element : remove) {
			removeTramite(element);
		}
	}


	private List<Tramite> tramites = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	@NotNull
	public String getNombre() {
		return nombre;
	}


	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}


	private String nombre = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	@NotNull
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}


	private String descripcion = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------

	/** @NOT generated */
	public String toString() {
		return getNombre() + " " + getDescripcion();
	}


	public PlantillaEmail deepClone() throws Exception {
		PlantillaEmail clone = (PlantillaEmail) super.clone();
		clone.setId(null);

		clone.setTramites(null);
		for (Tramite kid : this.getTramites()) {
			clone.addTramite(kid.deepClone());
		}
		return clone;
	}


	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PlantillaEmail))
			return false;
		final PlantillaEmail other = (PlantillaEmail) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}
}
