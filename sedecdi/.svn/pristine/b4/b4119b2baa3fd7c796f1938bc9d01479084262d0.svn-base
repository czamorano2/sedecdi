package es.imserso.sede.web.rest;

import java.io.StringReader;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.TramiteUCM;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.service.UCMService;
import es.imserso.sede.util.DateUtil;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.context.CookieUtils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.SedecdiRestException;

/**
 * Servicio rest que ofrece distintas utilidades.
 * 
 * @author 11825775
 *
 */
@RequestScoped
@Path("/utils")
public class UtilsEndpoint {

	private static final String ERROR_COOKIE_CADENA_A_DECODIFICAR_NULA = "error al decodificar la cookie con los datos del usuario: la cadena a decodificar es nula";

	private static final String ERROR_COOKIE_CADENA_A_DECODIFICAR_VACIA = "error al decodificar la cookie con los datos del usuario: la cadena a decodificar está vacía";

	private static final String UTF_8 = "UTF-8";

	@Inject
	private Logger log;

	@Inject
	CookieUtils cookieUtils;

	@Inject
	UCMService ucmService;

	/**
	 * @param base64EncodedCookie
	 *            cadena de texto en Base64 con los datos de la cookie
	 * @return
	 */
	@GET
	@Path("/decodeCookie")
	@Produces("application/json")
	public Usuario decodeCookie(@QueryParam("base64EncodedCookie") String base64EncodedCookie) {
		log.debug("decodificando cookie de usuario...");

		// validamos el parámetro antes de decodificar
		if (base64EncodedCookie == null) {
			String errmsg = ERROR_COOKIE_CADENA_A_DECODIFICAR_NULA;
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, Response.status(Status.NO_CONTENT).build());
		}
		if (base64EncodedCookie.trim().isEmpty()) {
			String errmsg = ERROR_COOKIE_CADENA_A_DECODIFICAR_VACIA;
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, Response.status(Status.LENGTH_REQUIRED).build());
		}

		String decodedValue = StringUtils.newString(Base64.decodeBase64(base64EncodedCookie), UTF_8);
		log.debug("decodedValue value: " + decodedValue);

		Usuario usuario = null;
		try {
			usuario = CookieUtils.decodeCookie(base64EncodedCookie);
		} catch (JAXBException e) {
			String errmsg = "error al decodificar la cookie con los datos del usuario: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, e.getCause(), Response.status(Status.INTERNAL_SERVER_ERROR).build());
		}

		return usuario;
	}

	/**
	 * Comprueba que exista una cookie llamada <code>usuario</code> y que
	 * 
	 * @return
	 */
	@GET
	@Path("/checkCookieUsuario")
	@Produces("application/json")
	public Usuario checkCookieUsuario(@CookieParam("usuario") String base64EncodedCookie) {
		log.debug("decodificando cookie de usuario...");

		Usuario usuario = null;

		// validamos el parámetro antes de decodificar
		if (base64EncodedCookie == null) {
			String errmsg = "error al decodificar la cookie con los datos del usuario: no hay cookie";
			log.error(errmsg);
			// throw new HttpResponseException(HttpStatusCode.BasdRequest);
			throw new SedecdiRestException(errmsg, Response.status(Status.NO_CONTENT).build());
		}
		if (base64EncodedCookie.trim().isEmpty()) {
			String errmsg = ERROR_COOKIE_CADENA_A_DECODIFICAR_VACIA;
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, Response.status(Status.LENGTH_REQUIRED).build());
		}

		String decodedValue = StringUtils.newString(Base64.decodeBase64(base64EncodedCookie), UTF_8);
		log.debug("decodedValue value: " + decodedValue);

		try {
			usuario = (Usuario) (JAXBContext.newInstance(Usuario.class).createUnmarshaller()
					.unmarshal(new StringReader(decodedValue)));
			log.debug("cookie de usuario decodificada!");
		} catch (JAXBException e) {
			String errmsg = "error al decodificar la cookie con los datos del usuario: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, e.getCause(), Response.status(Status.INTERNAL_SERVER_ERROR).build());
		}

		return usuario;
	}

	/**
	 * 
	 * @return
	 */
	@GET
	@Path("/tramiteUCM/{codigoSIA}")
	@Produces("application/json")
	public TramiteUCM tramiteUCM(@PathParam("codigoSIA") String codigoSIA) {

		if (codigoSIA == null) {
			// el código SIA es obligatorio
			throw new RuntimeException("el código SIA es obligatorio");
		}

		TramiteUCM tramiteUCM = null;
		try {
			log.debug("obteniendo información del trámite con código SIA " + codigoSIA);
			tramiteUCM = ucmService.getTramiteUCM(codigoSIA);
		} catch (SedeException e) {
			String errmsg = "error al obtener los datos del trámite: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, e.getCause(), Response.status(Status.INTERNAL_SERVER_ERROR).build());
		}

		return tramiteUCM;
	}

	/**
	 * Devuelve la hora oficial de la Sede
	 * 
	 * @return
	 */
	@GET
	@Path("/officialDate")
	@Produces("application/json")
	public Response officialDate() {
		Date officialDate = DateUtil.getOfficialDate();
		String result = "{\"date\":" + officialDate.getTime() + "}";
		return Response.status(200).entity(result).build();
	}

	/**
	 * De momento, hasta que funcione el servicio web de UCM, devolvemos el trámite
	 * a piñón
	 * 
	 * @param codigoSIA
	 * @return
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private TramiteUCM mockTramiteUCM(String codigoSIA) {
		log.info("En mockTramiteUCM obteniendo información del trámite con código SIA " + codigoSIA);
		TramiteUCM tramiteUCM = new TramiteUCM();
		if (codigoSIA.equals("994874")) {
			tramiteUCM.setSIA(codigoSIA);
			tramiteUCM.setDenominacion("Programa de Turismo del Imserso");
			tramiteUCM.setPermanente(false);
			tramiteUCM.setCerradotemporalmente(false);
			tramiteUCM.setFechaapertura(new GregorianCalendar(2016, 03, 01).getTime());
			tramiteUCM.setFechacierre(new GregorianCalendar(2016, 05, 31).getTime());
			tramiteUCM.setExigeautenticacionclave(true);
			tramiteUCM.setObjeto("solicitud de vacaciones");

		} else if (codigoSIA.equals("022670")) {
			tramiteUCM.setSIA(codigoSIA);
			tramiteUCM.setDenominacion("termalismo");
			tramiteUCM.setPermanente(false);
			tramiteUCM.setCerradotemporalmente(false);
			tramiteUCM.setFechaapertura(new GregorianCalendar(2016, 02, 01).getTime());
			tramiteUCM.setFechacierre(new GregorianCalendar(2016, 06, 30).getTime());
			tramiteUCM.setExigeautenticacionclave(true);
			tramiteUCM.setObjeto("solicitud de termalismo");
		} else if (codigoSIA.equals("999999")) {
			tramiteUCM.setSIA(codigoSIA);
			tramiteUCM.setDenominacion("propósito general");
			tramiteUCM.setPermanente(false);
			tramiteUCM.setCerradotemporalmente(false);
			tramiteUCM.setFechaapertura(new GregorianCalendar(2016, 02, 03).getTime());
			tramiteUCM.setFechacierre(new GregorianCalendar(2016, 06, 27).getTime());
			tramiteUCM.setExigeautenticacionclave(true);
			tramiteUCM.setObjeto("solicitud de propósito general");
		} else if (codigoSIA.equals("025804")) {
			tramiteUCM.setSIA(codigoSIA);
			tramiteUCM
					.setDenominacion("Centros de Recuperación de personas con discapacidad física (CRMF) del Imserso");
			tramiteUCM.setPermanente(false);
			tramiteUCM.setCerradotemporalmente(false);
			tramiteUCM.setFechaapertura(new GregorianCalendar(2016, 02, 03).getTime());
			tramiteUCM.setFechacierre(new GregorianCalendar(2016, 06, 27).getTime());
			tramiteUCM.setExigeautenticacionclave(true);
			tramiteUCM.setObjeto("Solicitud para Centros de Recuperación de personas con discapacidad física");
		}
		return tramiteUCM;
	}
}
