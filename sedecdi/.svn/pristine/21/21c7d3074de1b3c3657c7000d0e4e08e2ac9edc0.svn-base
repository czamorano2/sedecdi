package es.imserso.sede.data.dto;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistradoI;
import es.imserso.sede.data.dto.qualifier.RepresentanteQ;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.exception.SedeException;

/**
 * Datos comunes de la solicitud de un trámite.
 * 
 * @author 11825775
 *
 */
public interface SolicitudDTOI {

	@NotBlank(message = "el código SIA no puede estar vacío")
	String getCodigoSIA();

	void setCodigoSIA(@NotNull String codigoSIA);

	/**
	 * @return Representante del trámite (si lo tiene)
	 */
	@RepresentanteQ
	default RepresentanteDTOI getRepresentante() {
		return null;
	}

	default void setRepresentante(@NotNull RepresentanteDTOI representante) {}

	default boolean hasRepresentante() {
		return false;
	}

	/**
	 * @return Solicitante del trámite
	 */
	SolicitanteDTOI getSolicitante();

	void setSolicitante(@NotNull SolicitanteDTOI solicitante);

	default boolean hasSolicitante() {
		return true;
	}

	MedioNotificacionDTOI getMedioNotificacion();

	void setMedioNotificacion(MedioNotificacionDTOI medioNotificacion);

	/**
	 * @return Pdf con los datos registrados de la solicitud.
	 */
	DocumentoRegistradoI getDocumentoSolicitud();

	void setDocumentoSolicitud(DocumentoRegistradoI documento);

	/**
	 * @return Pdf del justificante del registro de la solicitud.
	 */
	DocumentoRegistradoI getJustificanteSolicitud();

	public void setJustificanteSolicitud(DocumentoRegistradoI documento);

	/**
	 * @return Documentos adjuntados a la solicitud
	 */
	List<DocumentoRegistradoI> getAttachedFiles();

	void setAttachedFiles(List<DocumentoRegistradoI> attachedFiles);

	void addAttachedFile(DocumentoRegistradoI attachedFile);

	/**
	 * Extrae los campos y sus valores de un dto.
	 * 
	 * @return hashtable con los campos y sus valores
	 */
	Map<String, String> extractHashtable() throws IllegalAccessException;

	/**
	 * Crea una nueva instancia de Solicitud a partir de los datos del dto.
	 * 
	 * @return nueva instancia de Solicitud
	 */
	Solicitud extractNewSolicitud() throws SedeException;

	/**
	 * La solicitud pued tener varias direcciones de correo electrónico
	 * (solicitante, representante, notificación, ...)
	 * <p>
	 * Una de ellas debe ser la principal, de forma que la aplicación sepa dónde
	 * enviar los correos electrónicos que no tienen un destinatario específico.
	 * <p>
	 * Por ejemplo, cuando se registra una solicitud, la aplicación envía un email
	 * confirmando el registro de la solicitud y adjuntando el justificante, el pdf
	 * de la solicitud, etc.
	 * 
	 * @return Dirección de correo electrónico principal donde se envierá la
	 *         confirmación de la solicitud
	 */
	@NotBlank(message = "La solicitud debe tener una dirección de correo electrónico principal")
	@Email(message = "La dirección de correo electrónico no es válida")
	String getSolicitudEmail();

}
