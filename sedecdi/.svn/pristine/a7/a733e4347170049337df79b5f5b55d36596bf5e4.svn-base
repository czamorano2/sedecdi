package es.imserso.sede.web.view.alta;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.dto.AttachedFile;
import es.imserso.sede.data.dto.impl.TermalismoDTO;
import es.imserso.sede.data.dto.rest.termalismo.BalnearioVOWS;
import es.imserso.sede.data.dto.rest.termalismo.ComunidadAutonomaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.EstadoCivilVOWS;
import es.imserso.sede.data.dto.rest.termalismo.ProvinciaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.SexoVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoFamiliaNumerosaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoPensionVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoTurnoVOWS;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.service.registration.solicitud.documentos.TipoDocumentoRegistrado;
import es.imserso.sede.util.GlobalTerma;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.rest.TermalismoEndpoint;
import es.imserso.sede.web.service.registration.solicitud.DtoFactory;
import es.imserso.sede.web.service.registration.solicitud.DtoFactoryQ;
import es.imserso.sede.web.service.registration.solicitud.RegTramiteBean;
import es.imserso.sede.web.service.registration.solicitud.impl.termalismo.TermalismoSolicitudRegistrationQ;
import es.imserso.sede.web.util.route.ParamValues.Params;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Bean de respaldo para el dto de propósito general.
 * 
 * @author 11825775
 *
 */
@Named(value = "termalismoView")
@ViewScoped
@Secure
public class SolicitudTermalismoView extends AbstractSolicitudView implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	Logger log;

	@Inject
	@TermalismoSolicitudRegistrationQ
	private SolicitudRegistrationI termalismoSolicitudRegistration;

	@Inject
	protected SolicitudRepository solicitudRepository;

	@Inject
	@DtoFactoryQ
	DtoFactory dtoFactory;

	private String quienSolicita;
	/**
	 * Al ser {@link javax.enterprise.context.Dependent}, su ciclo de vida es
	 * definido por el contexto de InjectionPoint, en este caso
	 * {@link javax.faces.view.ViewScoped}
	 * <p>
	 * Por tanto, recogerá información desde que se crea la vista hasta que finaliza
	 * {@link es.imserso.sede.service.registration.solicitud.AbstractSolicitudRegistration#register(es.imserso.sede.data.dto.PersonaInteresadaDTOI)}
	 * 
	 */
	@Inject
	RegTramiteBean regTramiteBean;

	private TermalismoDTO dto;

	private Solicitud solicitud;

	private UploadedFile uploadedFile;

	@Inject
	private TermalismoEndpoint termalismoEndpoint;

	private List<SexoVOWS> listaSexos;
	private List<EstadoCivilVOWS> listaEstadoCivil;
	private List<ProvinciaVOWS> listaProvincias;
	private List<TipoTurnoVOWS> listaTiposTurno;
	private List<TipoPensionVOWS> listaTiposPensiones;
	private List<TipoFamiliaNumerosaVOWS> listaFamiliaNumerosa;

	// Tratamientos: reumatológico, respiratorio, digestimo...
	// variables asociadas: solicitante -->tratSol1, tratSol2.... conyuge
	// -->tratCon1, tratCon2....
	private String[] selectedTratamientosSolicitante = new String[7];
	private String[] selectedTratamientosConyuge = new String[7];

	// Articulaciones afectadas: cadera, columna, hombro...
	// variables asociadas: solicitante -->artiSol1, artiSol2... conyuge
	// -->artiCon1, artiCon2...
	private String[] selectedArticAfectadasSolicitante;
	private String[] selectedArticAfectadasConyuge;
	// Tiene o padece: difcultades para moverse, dolor....
	// variables asociadas: defoSol1, defoSol2...
	private String[] selectedPadece;

	// Padece enfermades de: vias resp altas, bajas
	// variables asociadas solicitante --> vresSolA y vresSolB conyuge-->vresConA y
	// vresConB
	private String[] selectedViasSolicitante;
	private String[] selectedViasConyuge;

	// por los problemas tiene que: ingresos hospital medicación..
	// variables asociadas solicitante--> impoSol1, impoSol2... conyuge--> impoCon1,
	// impoCon2...
	private String[] selectedConsecuenciasSolicitante;
	private String[] selectedConsecuenciasConyuge;

	private String tipoTurno;
	private Integer[] selectedDays = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
			23, 24, 25, 26, 27, 28, 29, 30, 31 };
	private Integer[] selectedMonths = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

	@PostConstruct
	public void onCreate() {
		super.init();
		// crea la entidad para la solicitud
		dto = dtoFactory.createTermalismoDtoInstance();
		selectedTratamientosSolicitante[0] = "A";
		selectedTratamientosConyuge[0] = "A";

	}

	@PreDestroy
	public void onTerminate() {
		// aseguramos que se persisten los apuntes de la operación realizada
		if (!regTramiteBean.persisted()) {
			try {
				regTramiteBean.endUnsuccessfully("la solicitud no se ha registrado!");
			} catch (Exception e) {
				log.warnv("error al intentar, in extremis, persistir los apuntes del registro de la solicitud: %s",
						Utils.getExceptionMessage(e));
			}
		}
	}

	/**
	 * Persiste la solicitud en el sistema
	 * <p>
	 * Lanza la ejecución de las fases de registro de la solicitud
	 */
	public void save() {

		this.setSolicitud(null);
		String respuestaAltaRedirect = null;
		try {
			validandoCampos();
			log.infof("[%s] guardando la solicitud a partir de los datos del formuario ...",
					paramValues.getParamValue(Params.sia).getValue());
			dto.setNreg(Integer.valueOf(termalismoEndpoint.getNuevoNumeroExpediente()));
			// FIXME de momento pongo ESPAÑA
			dto.setPaisResidencia("ESPAÑA");
			this.setSolicitud(termalismoSolicitudRegistration.register(dto));
			log.info("solicitud registrada!");

			respuestaAltaRedirect = "/app/respuesta_alta?faces-redirect=true&idSolicitud=" + this.solicitud.getId();
			log.infov("redirigimos a la vista de respuesta de alta de solicitud: {0}", respuestaAltaRedirect);

			((ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler())
					.performNavigation(respuestaAltaRedirect);

		} catch (ValidationException e) {
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("termalismoForm:messages");
		} catch (Exception e) {

			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.TERMALISMO,
					"No se pudo dar de alta la solicitud", Utils.getExceptionMessage(e));
			// redirigimos a la vista de error
			respuestaAltaRedirect = "error";
			((ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler())
					.performNavigation(respuestaAltaRedirect);
		}

	}

	protected void validandoCampos() throws ValidationException {
		plazasSolicitadas();
		turnoSolicitado();
		tratamientoSolicitado(selectedTratamientosSolicitante, GlobalTerma.SOLICITANTE);
		// si ha seleccionado conyuge o ambos
		if (!Utils.isEmptyTrimmed(dto.getPlazCony()) || !Utils.isEmptyTrimmed(dto.getPlazAmbos()))
			tratamientoSolicitado(selectedTratamientosConyuge, GlobalTerma.CONYUGE);
	}

	/**
	 * Cancela el alta de la solicitud
	 */
	public String cancel() {
		try {
			log.info("operación cancelada por el usuario!");
			regTramiteBean.endUnsuccessfully("operación cancelada por el usuario!");
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

		} catch (Exception e) {
			log.errorv("Error al cancelar el alta: %s", Utils.getExceptionMessage(e));
		}
		return "backToUCM";
	}

	public void fileUploadListener(FileUploadEvent e) {
		log.info("Añadiendo fichero adjunto...");

		this.uploadedFile = e.getFile();

		if (this.uploadedFile == null) {
			log.warn("No hay fichero para adjuntar");
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("termalismoForm:messages");
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.TERMALISMO,
					"Ficheros Adjuntos", "No se ha recibido ningún fichero para adjuntar");
			return;
		}

		log.info("Uploaded File Name Is :: " + uploadedFile.getFileName() + " :: Uploaded File Size :: "
				+ uploadedFile.getSize());

		// asignamos los ficheros adjuntos al DTO
		AttachedFile attachedFile = new AttachedFile(TipoDocumentoRegistrado.PERSONAL.toString(),
				this.uploadedFile.getFileName(), this.uploadedFile.getContents());
		if (!this.dto.getAttachedFiles().contains(attachedFile)) {
			log.info("Añadiendo fichero adjunto...");
			this.dto.addAttachedFile(attachedFile);
		}
	}

	private void plazasSolicitadas() throws ValidationException {
		dto.setPlazSol("");
		dto.setPlazCony("");
		dto.setPlazAmbos("");
		if (quienSolicita == null) {
			throw new ValidationException("Debe seleccionar las plazas solicitadas");
		}
		switch (quienSolicita) {
		case GlobalTerma.PLAZA_SOLICITANTE:
			dto.setPlazSol(GlobalTerma.PLAZA_SOLICITANTE);
			break;
		case GlobalTerma.PLAZA_CONYUGE:
			dto.setPlazCony(GlobalTerma.PLAZA_CONYUGE);
			break;
		case GlobalTerma.PLAZA_AMBOS:
			dto.setPlazAmbos(GlobalTerma.PLAZA_AMBOS);
			break;
		}

	}

	private void turnoSolicitado() throws ValidationException {
		dto.setTdt1("");
		dto.setTdt2("");
		dto.setTdt3("");
		if (tipoTurno == null) {
			throw new ValidationException("Debe seleccionar un tipo de turno");
		}
		switch (tipoTurno) {
		case GlobalTerma.DOCE_NOCHES:
			dto.setTdt1(GlobalTerma.DOCE_NOCHES);
			break;
		case GlobalTerma.DIEZ_NOCHES:
			dto.setTdt2(GlobalTerma.DIEZ_NOCHES);
			break;
		case GlobalTerma.SIN_PREFERENCIA:
			dto.setTdt3(GlobalTerma.SIN_PREFERENCIA);
			break;
		}
	}

	// reumatológico, respiratorio...
	private void tratamientoSolicitado(String[] tratamientos, String persona) throws ValidationException {
		// reumatológico
		if (tratamientos == null || tratamientos.length == 0) {
			throw new ValidationException("Al menos debe elegir un tratamiento");
		}
		for (String trat : tratamientos) {
			if (trat != null) {
				switch (trat) {
				case GlobalTerma.REUMATOLOGICO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						dto.setTratSol1(GlobalTerma.REUMATOLOGICO);
					} else {
						dto.setTratCon1(GlobalTerma.REUMATOLOGICO);
					}
					break;
				case GlobalTerma.RESPIRATORIO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						dto.setTratSol2(GlobalTerma.RESPIRATORIO);
					} else {
						dto.setTratCon2(GlobalTerma.RESPIRATORIO);
					}
					break;
				case GlobalTerma.DIGESTIVO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						dto.setTratSol3(GlobalTerma.DIGESTIVO);
					} else {
						dto.setTratCon3(GlobalTerma.DIGESTIVO);
					}
					break;
				case GlobalTerma.RENAL:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						dto.setTratSol4(GlobalTerma.RENAL);
					} else {
						dto.setTratCon4(GlobalTerma.RENAL);
					}
					break;
				case GlobalTerma.DERMATOLOGICO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						dto.setTratSol5(GlobalTerma.DERMATOLOGICO);
					} else {
						dto.setTratCon5(GlobalTerma.DERMATOLOGICO);
					}
					break;
				case GlobalTerma.NEUROPSIQUICO:
					if (persona.equals(GlobalTerma.SOLICITANTE)) {
						dto.setTratSol6(GlobalTerma.NEUROPSIQUICO);
					} else {
						dto.setTratCon6(GlobalTerma.NEUROPSIQUICO);
					}
					break;
				}
			}

		}
	}

	public List<SexoVOWS> getSexos() {
		if (listaSexos == null)
			listaSexos = (List<SexoVOWS>) termalismoEndpoint.getSexos();
		return listaSexos;
	}

	public List<EstadoCivilVOWS> getEstadoCivil() {
		if (listaEstadoCivil == null)
			listaEstadoCivil = (List<EstadoCivilVOWS>) termalismoEndpoint.getEstadosCiviles();
		return listaEstadoCivil;
	}

	public List<ProvinciaVOWS> getProvincias() {
		if (listaProvincias == null)
			listaProvincias = (List<ProvinciaVOWS>) termalismoEndpoint.getProvincias();
		return listaProvincias;
	}

	public List<ComunidadAutonomaVOWS> getComunidadesAutonomas() {
		return (List<ComunidadAutonomaVOWS>) termalismoEndpoint.getComunidadesAutonomas();
	}

	public List<BalnearioVOWS> getBalneariosPorCCAA(Long idComunidad) {
		List<BalnearioVOWS> listaBalnearios = null;
		if (idComunidad != null) {
			listaBalnearios = (List<BalnearioVOWS>) termalismoEndpoint.getBalneariosPorCCAA(idComunidad);
		}
		return listaBalnearios;
	}

	public List<TipoTurnoVOWS> getListaTurnos() {
		if (listaTiposTurno == null)
			listaTiposTurno = (List<TipoTurnoVOWS>) termalismoEndpoint.getListaTurnos();
		return listaTiposTurno;
	}

	public List<TipoPensionVOWS> getTiposPensiones() {
		if (listaTiposPensiones == null)
			listaTiposPensiones = (List<TipoPensionVOWS>) termalismoEndpoint.getTiposPensiones();
		return listaTiposPensiones;
	}

	public List<TipoFamiliaNumerosaVOWS> getListaFamiliaNumerosa() {
		if (listaFamiliaNumerosa == null)
			listaFamiliaNumerosa = (List<TipoFamiliaNumerosaVOWS>) termalismoEndpoint.getTiposFamiliaNumerosa();
		return listaFamiliaNumerosa;
	}

	public void setListaFamiliaNumerosa(List<TipoFamiliaNumerosaVOWS> listaFamiliaNumerosa) {
		this.listaFamiliaNumerosa = listaFamiliaNumerosa;
	}

	public TermalismoDTO getDto() {
		return this.dto;
	}

	public void setDto(TermalismoDTO _dto) {
		this.dto = _dto;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getQuienSolicita() {
		return quienSolicita;
	}

	public void setQuienSolicita(String quienSolicita) {
		this.quienSolicita = quienSolicita;
	}

	public String getTipoTurno() {
		return tipoTurno;
	}

	public void setTipoTurno(String tipoTurno) {
		this.tipoTurno = tipoTurno;
	}

	public String[] getSelectedPadece() {
		return selectedPadece;
	}

	public void setSelectedPadece(String[] selectedPadece) {
		this.selectedPadece = selectedPadece;
	}

	public String[] getSelectedArticAfectadasSolicitante() {
		return selectedArticAfectadasSolicitante;
	}

	public void setSelectedArticAfectadasSolicitante(String[] selectedArticAfectadasSolicitante) {
		this.selectedArticAfectadasSolicitante = selectedArticAfectadasSolicitante;
	}

	public String[] getSelectedArticAfectadasConyuge() {
		return selectedArticAfectadasConyuge;
	}

	public void setSelectedArticAfectadasConyuge(String[] selectedArticAfectadasConyuge) {
		this.selectedArticAfectadasConyuge = selectedArticAfectadasConyuge;
	}

	public String[] getSelectedTratamientosSolicitante() {
		return selectedTratamientosSolicitante;
	}

	public void setSelectedTratamientosSolicitante(String[] selectedTratamientosSolicitante) {
		this.selectedTratamientosSolicitante = selectedTratamientosSolicitante;
	}

	public String[] getSelectedViasSolicitante() {
		return selectedViasSolicitante;
	}

	public void setSelectedViasSolicitante(String[] selectedViasSolicitante) {
		this.selectedViasSolicitante = selectedViasSolicitante;
	}

	public String[] getSelectedViasConyuge() {
		return selectedViasConyuge;
	}

	public void setSelectedViasConyuge(String[] selectedViasConyuge) {
		this.selectedViasConyuge = selectedViasConyuge;
	}

	public String[] getSelectedConsecuenciasSolicitante() {
		return selectedConsecuenciasSolicitante;
	}

	public void setSelectedConsecuenciasSolicitante(String[] selectedConsecuenciasSolicitante) {
		this.selectedConsecuenciasSolicitante = selectedConsecuenciasSolicitante;
	}

	public String[] getSelectedConsecuenciasConyuge() {
		return selectedConsecuenciasConyuge;
	}

	public void setSelectedConsecuenciasConyuge(String[] selectedConsecuenciasConyuge) {
		this.selectedConsecuenciasConyuge = selectedConsecuenciasConyuge;
	}

	public String[] getSelectedTratamientosConyuge() {
		return selectedTratamientosConyuge;
	}

	public void setSelectedTratamientosConyuge(String[] selectedTratamientosConyuge) {
		this.selectedTratamientosConyuge = selectedTratamientosConyuge;
	}

	public Integer[] getSelectedDays() {
		return selectedDays;
	}

	public void setSelectedDays(Integer[] selectedDays) {
		this.selectedDays = selectedDays;
	}

	public Integer[] getSelectedMonths() {
		return selectedMonths;
	}

	public void setSelectedMonths(Integer[] selectedMonths) {
		this.selectedMonths = selectedMonths;
	}
}
