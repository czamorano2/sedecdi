package es.imserso.sede.web.view.alta;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.omnifaces.cdi.Param;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.converter.impl.PdfUtil;
import es.imserso.sede.service.registration.solicitud.SolicitudService;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentoRegistrado;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentosRegistradosManager;
import es.imserso.sede.service.registration.solicitud.documentos.TipoDocumentoRegistrado;
import es.imserso.sede.util.Global;
import es.imserso.sede.util.Utils;
import es.imserso.sede.web.view.ViewUtils;

@Named
@ViewScoped
public class RespuestaAltaView implements Serializable {

	private static final long serialVersionUID = -153562163533069655L;

	@Inject
	Logger log;

	@Inject
	SolicitudRepository solicitudRepository;

	@Inject
	SolicitudService solicitudService;

	@Inject
	@Param
	Long idSolicitud;

	private Solicitud solicitud;
	private DocumentosRegistradosManager adjuntos;

	private StreamedContent pdfSolicitud;
	private StreamedContent pdfSolicitudAdjuntada;
	private StreamedContent pdfJustificante;
	private List<StreamedContent> pdfPersonales;

	private String nombreSolicitante;
	private String emailedTo;

	@PostConstruct
	public void onCreate() {
		log.debug("post construct ...");
		retrieve();
		setNombreSolicitante(this.solicitud.getNombreCompleto());
	}

	/**
	 * Obtiene una solicitud del repositorio
	 * <p>
	 * Se usa para asegurar que los datos de la solicitud que ofrecemos en la
	 * confirmación del alta están correctamente persistidos en el sistema
	 */
	public void retrieve() {
		try {
			solicitud = solicitudRepository.getSolicitudUsuario(idSolicitud);
			if (solicitud == null) {
				ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.PROPOSITO_GENERAL,
						"No se pudo recuperar la solicitud",
						"No se encuentra la solicitud con identificador " + idSolicitud);
			}
			this.adjuntos = solicitudService.getDocumentosSolicitud(solicitud);

			this.pdfSolicitud = new DefaultStreamedContent(
					new ByteArrayInputStream(this.adjuntos.getDocumentoSolicitud().getFichero()),
					Global.CONTENT_TYPE_APPLICATION_PDF,
					TipoDocumentoRegistrado.SOLICITUD.getNombreDocumento() + ".pdf");

			if (this.adjuntos.getDocumentoSolicitudAdjunta() != null) {
				this.pdfSolicitudAdjuntada = new DefaultStreamedContent(
						new ByteArrayInputStream(this.adjuntos.getDocumentoSolicitudAdjunta().getFichero()),
						Global.CONTENT_TYPE_APPLICATION_PDF,
						TipoDocumentoRegistrado.SOLICITUD_ADJUNTA.getNombreDocumento() + ".pdf");
			}
			this.pdfJustificante = new DefaultStreamedContent(
					new ByteArrayInputStream(this.adjuntos.getDocumentoJustificante().getFichero()),
					Global.CONTENT_TYPE_APPLICATION_PDF,
					TipoDocumentoRegistrado.JUSTIFICANTE.getNombreDocumento() + ".pdf");

			this.pdfPersonales = new ArrayList<StreamedContent>();
			for (DocumentoRegistrado doc : this.adjuntos.getDocumentosPersonales()) {
				this.pdfPersonales.add(new DefaultStreamedContent(new ByteArrayInputStream(doc.getFichero()),
						Global.CONTENT_TYPE_APPLICATION_PDF,
						TipoDocumentoRegistrado.PERSONAL.getNombreDocumento() + ".pdf"));
			}

			Hashtable<String, String> registeredSolicitudContent = PdfUtil
					.extractData(this.adjuntos.getDocumentoSolicitud().getFichero());
			setEmailedTo(registeredSolicitudContent.get("email"));

		} catch (Exception e) {
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.PROPOSITO_GENERAL,
					"No se pudo recuperar la solicitud", Utils.getExceptionMessage(e));
		}
	}

	public Solicitud getSolicitud() {
		return this.solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public DocumentosRegistradosManager getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(DocumentosRegistradosManager adjuntos) {
		this.adjuntos = adjuntos;
	}

	public StreamedContent getPdfSolicitud() {
		return pdfSolicitud;
	}

	public void setPdfSolicitud(StreamedContent pdfSolicitud) {
		this.pdfSolicitud = pdfSolicitud;
	}

	public StreamedContent getPdfJustificante() {
		return pdfJustificante;
	}

	public void setPdfJustificante(StreamedContent pdfJustificante) {
		this.pdfJustificante = pdfJustificante;
	}

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public StreamedContent getPdfSolicitudAdjuntada() {
		return pdfSolicitudAdjuntada;
	}

	public void setPdfSolicitudAdjuntada(StreamedContent pdfSolicitudAdjuntada) {
		this.pdfSolicitudAdjuntada = pdfSolicitudAdjuntada;
	}

	/**
	 * @return the emailedTo
	 */
	public String getEmailedTo() {
		return emailedTo;
	}

	/**
	 * @param emailedTo
	 *            the emailedTo to set
	 */
	public void setEmailedTo(String emailedTo) {
		this.emailedTo = emailedTo;
	}

	public List<StreamedContent> getPdfPersonales() {
		return pdfPersonales;
	}

	public void setPdfPersonales(List<StreamedContent> pdfPersonales) {
		this.pdfPersonales = pdfPersonales;
	}

	public StreamedContent getPdfPersonal(int index) {
		log.debugv("se ha solicitado el documento adjunto con índice {0}", index);
		return this.pdfPersonales.get(index);
	}

}
