package es.imserso.sede.data.dto.impl;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.sede.data.dto.RepresentanteDTOI;
import es.imserso.sede.data.dto.SolicitudDTO;

@XmlRootElement
abstract public class GenericoDTO extends SolicitudDTO implements Serializable {

	private static final long serialVersionUID = 3217214452142789721L;

	private static final Logger log = Logger.getLogger(PropositoGeneralDTO.class.getName());

	private RepresentanteDTOI representante;

	/**
	 * Solicitud en formato PDF que debe haber rellenado y firmado el usuario
	 */
	@NotNull(message = "Debe enviar la solicitud rellena y firmada")
	protected DocumentoRegistrado solicitudPdf;

	public DocumentoRegistrado getSolicitudPdf() {
		return solicitudPdf;
	}

	public void setSolicitudPdf(DocumentoRegistrado solicitudPdf) {
		this.solicitudPdf = solicitudPdf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitudDTOI#getSolicitudEmail()
	 */
	@NotNull
	@Override
	public String getSolicitudEmail() {
		String email = null;

		if (representante.getEmail() != null) {
			email = representante.getEmail().getDireccion();
		}

		if (medioNotificacion == null && medioNotificacion.getEmail() != null
				&& medioNotificacion.getEmail().getDireccion() != null) {
			email = medioNotificacion.getEmail().getDireccion();
		}

		log.debugv("Correo electrónico de la solicitud: {0}", email);
		return email;
	}

}
