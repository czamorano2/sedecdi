package es.imserso.sede.data.dto;

import java.util.List;
import java.util.Map;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.sede.data.validation.nif.NifNie;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.exception.SedeException;

public interface PersonaInteresadaDTOI {

	String getCodigoSIA();

	void setCodigoSIA(String codigoSIA);

	String getNombre();

	void setNombre(String nombre);

	String getApellido1();

	void setApellido1(String apellido1);

	String getApellido2();

	void setApellido2(String apellido2);

	@NifNie
	String getDocumentoIdentificacion();

	void setDocumentoIdentificacion(String documentoIdentificacion);

	String getTelefono();

	void setTelefono(String telefono);

	String getEmail();

	void setEmail(String email);

	List<DocumentoRegistrado> getAttachedFiles();

	void setAttachedFiles(List<DocumentoRegistrado> attachedFiles);

	void addAttachedFile(DocumentoRegistrado attachedFile);

	/**
	 * @return nombre y apellidos concatenados
	 */
	String nombreApellidos();

	/**
	 * Extrae los campos y sus valores de un dto.
	 * 
	 * @return hashtable con los campos y sus valores
	 */
	public Map<String, String> extractHashtable() throws IllegalAccessException;

	/**
	 * Crea una nueva instancia de Solicitud a partir de los datos del dto.
	 * 
	 * @return nueva instancia de Solicitud
	 */
	public Solicitud extractNewSolicitud() throws SedeException;

	String getTransientEmailConfirm();

	void setTransientEmailConfirm(String email);
}