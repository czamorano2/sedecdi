<ui:composition
	template="/WEB-INF/templates/createMultipartLayout.xhtml"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:f="http://xmlns.jcp.org/jsf/core"
	xmlns:h="http://xmlns.jcp.org/jsf/html"
	xmlns:ui="http://xmlns.jcp.org/jsf/facelets"
	xmlns:c="http://xmlns.jcp.org/jsp/jstl/core"
	xmlns:fn="http://xmlns.jcp.org/jsp/jstl/functions"
	xmlns:p="http://primefaces.org/ui" xmlns:o="http://omnifaces.org/ui"
	xmlns:of="http://omnifaces.org/functions">

	<ui:define name="title">Imserso - Sede Electrónica</ui:define>

	<ui:define name="contentTitle">
		<h:outputText value="Alta de Solicitud de Propósito General"
			styleClass="code" />
	</ui:define>

	<ui:define name="multipartForm">

		<p:fieldset legend="Representante" style="margin-bottom:20px"
			rendered="${pgeneralView.showRepresentante()}">

			<p:panelGrid columns="4"
				columnClasses="ui-grid-col-3,ui-grid-col-3,ui-grid-col-3,ui-grid-col-3"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="nombreRepresentante" value="Nombre" />
					<p:inputText id="nombreRepresentante" maxlength="50"
						disabled="${pgeneralView.disableRepresentanteMainFields()}"
						style="text-transform: uppercase"
						value="${pgeneralView.dto.nombreRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="apellido1Representante" value="Primer apellido" />
					<p:inputText id="apellido1Representante" maxlength="50"
						disabled="${pgeneralView.disableRepresentanteMainFields()}"
						style="text-transform: uppercase"
						value="${pgeneralView.dto.apellido1Representante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="apellido2Representante"
						value="Segundo apellido" />
					<p:inputText id="apellido2Representante" maxlength="50"
						disabled="${pgeneralView.disableRepresentanteMainFields()}"
						style="text-transform: uppercase"
						value="${pgeneralView.dto.apellido2Representante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="documentoIdentificacionRepresentante"
						value="DNI/NIE" />
					<p:inputText id="documentoIdentificacionRepresentante"
						disabled="${pgeneralView.disableRepresentanteMainFields()}"
						maxlength="9" style="text-transform: uppercase"
						value="${pgeneralView.dto.documentoIdentificacionRepresentante}" />
				</p:panelGrid>

			</p:panelGrid>

			<p:panelGrid columns="2" columnClasses="ui-grid-col-6, ui-grid-col-6"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="domicilioRepresentante" value="Domicilio" />
					<p:inputText id="domicilioRepresentante" maxlength="100"
						value="${pgeneralView.dto.domicilioRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="localidadRepresentante" value="Localidad" />
					<p:inputText id="localidadRepresentante" maxlength="50"
						style="text-transform: uppercase"
						value="${pgeneralView.dto.localidadRepresentante}" />
				</p:panelGrid>

			</p:panelGrid>

			<p:panelGrid columns="6"
				columnClasses="ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2,ui-grid-col-2"
				layout="grid" styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="codigoPostalRepresentante"
						value="Codigo postal" />
					<p:inputText id="codigoPostalRepresentante" maxlength="10"
						value="${pgeneralView.dto.codigoPostalRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="provinciaRepresentante" value="Provincia" />
					<p:inputText id="provinciaRepresentante" maxlength="50"
						style="text-transform: uppercase"
						value="${pgeneralView.dto.provinciaRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="paisRepresentante" value="País" />
					<p:inputText id="paisRepresentante" maxlength="50"
						style="text-transform: uppercase"
						value="${pgeneralView.dto.paisRepresentante}" />
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="telefonoRepresentante" value="Teléfono" />
					<p:inputNumber id="telefonoRepresentante" thousandSeparator=""
						maxlength="11" decimalPlaces="0"
						value="${pgeneralView.dto.telefonoRepresentante}" />
				</p:panelGrid>

			</p:panelGrid>
		</p:fieldset>



		<ui:include src="/WEB-INF/templates/personaInteresada.xhtml">
            <ui:param name="templatesBeanName" value="#{genericoView}" />
        </ui:include>
        
        

		<ui:include src="/WEB-INF/templates/notificacion.xhtml">
            <ui:param name="templatesBeanName" value="#{pgeneralView}" />
        </ui:include>



		<p:fieldset legend="Datos relativos a la solicitud"
			style="margin-bottom:20px">

			<p:panelGrid columns="1" columnClasses="ui-grid-col-12" layout="grid"
				styleClass="ui-fluid ui-noborder">

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="asunto" value="Asunto" />
					<p:inputTextarea id="asunto" counter="displayAsunto"
						counterTemplate="quedan {0} caracteres." rows="4" cols="100"
						maxlength="200" value="${pgeneralView.dto.asunto}" />
					<h:outputText id="displayAsunto" style="font-style: italic; color: grey; font-size: 80%"/>
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="expone" value="Expone" />
					<p:inputTextarea id="expone" counter="displayExpone"
						counterTemplate="quedan {0} caracteres." rows="4" cols="100"
						maxlength="2000" value="${pgeneralView.dto.expone}" />
					<h:outputText id="displayExpone" style="font-style: italic; color: grey; font-size: 80%"/>
				</p:panelGrid>

				<p:panelGrid columns="1" columnClasses="ui-grid-col-12">
					<o:outputLabel for="solicita" value="Solicita" />
					<p:inputTextarea id="solicita" counter="displaySolicita"
						counterTemplate="quedan {0} caracteres." rows="4" cols="100"
						maxlength="2000" value="${pgeneralView.dto.solicita}" />
					<h:outputText id="displaySolicita" style="font-style: italic; color: grey; font-size: 80%"/>
				</p:panelGrid>

			</p:panelGrid>

		</p:fieldset>

		<ui:include src="/WEB-INF/templates/adjuntos.xhtml">
            <ui:param name="templatesBeanName" value="#{pgeneralView}" />
        </ui:include>

		<ui:include src="/WEB-INF/templates/createButtons.xhtml">
            <ui:param name="templatesBeanName" value="#{pgeneralView}" />
        </ui:include>

	</ui:define>

</ui:composition>





