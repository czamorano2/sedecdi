package es.imserso.sede.config;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.process.scheduled.sync.HermesSolicitudSynchronizer;

/**
 * Este componente se inicia al arrancar la aplicación.
 * <p>
 * Gestiona las acciones a realizar al arrancar la aplicación.
 * 
 * @author 11825775
 */
@Startup
@Singleton
public class StartupManager {

	@Inject
	Logger log;

	@Inject
	PropertyConfigurator propertyConfigurator;

	@Inject
	ConfigValidator configValidator;
	
	@Inject
	HermesSolicitudSynchronizer hermesSolicitudSynchronizer;


	/**
	 * Se ejecuta al arrancar la aplicación.
	 */
	@PostConstruct
	public void init() {
		log.info("iniciando acciones tras el inicio de la aplicación...");

		propertyConfigurator.configure();
		configValidator.validateApplicationConfiguration();
		

		log.info("acciones tras el inicio de la aplicación finalizadas!");
	}
	
	/**
	 * sincroniza las solicitudes de la sede con las de Hermes cada 5 minutos
	 */
	@Schedule(minute = "*/2", hour = "*", persistent = false)
	public void synchonizeHermes() {
		hermesSolicitudSynchronizer.synchonize();
	}

}
