package es.imserso.sede.web.view.alta;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.omnifaces.util.Messages;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.hermes.session.webservice.dto.TemporadaDTO;
import es.imserso.hermes.session.webservice.dto.TipoDocumentoRegistrado;
import es.imserso.hermes.session.webservice.dto.TurnoDTO;
import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.TramiteRepository;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.data.dto.impl.TurismoDTO;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.model.Tramite;
import es.imserso.sede.service.registration.solicitud.SolicitudRegistrationI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.DtoFactory;
import es.imserso.sede.web.service.registration.solicitud.DtoFactoryQ;
import es.imserso.sede.web.service.registration.solicitud.impl.turismo.TurismoSolicitudRegistrationQ;
import es.imserso.sede.web.util.route.ParamValues.Params;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Bean de respaldo para el dto de genérico
 * 
 * @author 11825775
 *
 */
@Named(value = "solicitudTurismoView")
@ViewScoped
@Secure
public class SolicitudTurismoView extends AbstractSolicitudView implements Serializable {

	private static final long serialVersionUID = 6441965433909918407L;

	@Inject
	Logger log;

	@Inject
	@TurismoSolicitudRegistrationQ
	private SolicitudRegistrationI turismoSolicitudRegistration;

	@Inject
	protected SolicitudRepository solicitudRepository;

	@Inject
	@DtoFactoryQ
	DtoFactory dtoFactory;

	@Inject
	protected TramiteRepository tramiteRepository;

	@Inject
	TurismoRepository turismoRepository;

	private TurismoDTO dto;

	private Solicitud solicitud;

	private UploadedFile uploadedFile;

	Tramite tramite;

	PlazoDTO plazoDTO;
	TurnoDTO turnoDTO;
	TemporadaDTO temporadaDTO;

	List<SimpleDTOI> provinciaList;
	List<SimpleDTOI> estadosCiviles;
	List<SimpleDTOI> categoriasFamiliaNumerosa;
	List<SimpleDTOI> clasesPensiones;
	List<SimpleDTOI> procedenciasPension;
	List<SimpleDTOI> opciones;
	List<String> sexos;

	public void viewPreInvokeActionListener(ComponentSystemEvent event) {
		log.infov("invoked method viewPreInvokeActionListener! - {0}", event);
		Messages.addGlobalInfo("invoked method viewPreInvokeActionListener! - {0}", event);
	}

	public void viewPostInvokeActionListener(ComponentSystemEvent event) {
		log.infov("invoked method viewPreInvokeActionListener! - {0}", event);
		Messages.addGlobalInfo("invoked method viewPostInvokeActionListener! - {0}", event);
	}

	@PostConstruct
	public void onCreate() {
		super.init();
		log.infov("onCreate!");

		try {

			plazoDTO = turismoRepository.getPlazoTurnoPorDefecto();
			turnoDTO = turismoRepository.getTurnoPorDefecto();
			temporadaDTO = turismoRepository.getTemporadaTurnoPorDefecto();

			provinciaList = turismoRepository.getProvincias();
			estadosCiviles = turismoRepository.getEstadosCiviles();
			categoriasFamiliaNumerosa = turismoRepository.getCategoriasFamiliaNumerosa();
			clasesPensiones = turismoRepository.getClasesPensiones();
			procedenciasPension = turismoRepository.getProcedenciasPensiones();
			opciones = turismoRepository.getOpciones();
			sexos = turismoRepository.getSexos();

			// crea la entidad para la solicitud
			dto = dtoFactory.createTurismoDtoInstance();

			tramite = tramiteRepository.findBySIA(dto.getCodigoSIA());

		} catch (IOException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.TURISMO,
					"No se pudo crear la instacia del DTO", Utils.getExceptionMessage(e));
		}

	}

	/**
	 * Persiste la solicitud en el sistema
	 * <p>
	 * Lanza la ejecución de las fases de registro de la solicitud
	 * 
	 * @return outcome para ir a la vista de respuesta si todo ha ido bien, o a la
	 *         vista de error si no se ha podido finalizar correctamente el proceso
	 *         de registro.
	 */
	public void save() {

		this.setSolicitud(null);
		String respuestaAltaRedirect = null;
		try {

			log.infof("[{0}] guardando la solicitud a partir de los datos del formuario ...",
					paramValues.getParamValue(Params.sia).getValue());
			this.setSolicitud(turismoSolicitudRegistration.register(dto));
			log.info("solicitud registrada!");

			respuestaAltaRedirect = "/app/respuesta_alta?faces-redirect=true&idSolicitud=" + this.solicitud.getId();
			log.infov("redirigimos a la vista de respuesta de alta de solicitud: {0}", respuestaAltaRedirect);

			((ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler())
					.performNavigation(respuestaAltaRedirect);

		} catch (ValidationException e) {
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("messages");

		} catch (Exception e) {

			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.TURISMO,
					"No se pudo dar de alta la solicitud", Utils.getExceptionMessage(e));
			// redirigimos a la vista de error
			respuestaAltaRedirect = "error";
			((ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler())
					.performNavigation(respuestaAltaRedirect);
		}

	}

	public void fileUploadListener(FileUploadEvent e) {
		log.info("Añadiendo fichero adjunto...");

		this.uploadedFile = e.getFile();

		if (this.uploadedFile == null) {
			log.warn("No hay fichero para adjuntar");
			// scroll al panel de mensajes
			RequestContext.getCurrentInstance().scrollTo("PropositoGeneralForm:messages");
			// generamos el mensaje de error a mostrar al usuario
			ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.PROPOSITO_GENERAL,
					"Ficheros Adjuntos", "No se ha recibido ningún fichero para adjuntar");
			return;
		}

		log.info("Uploaded File Name Is :: " + uploadedFile.getFileName() + " :: Uploaded File Size :: "
				+ uploadedFile.getSize());

		// asignamos los ficheros adjuntos al DTO
		DocumentoRegistrado attachedFile = new DocumentoRegistrado(TipoDocumentoRegistrado.PERSONAL.toString(),
				this.uploadedFile.getFileName(), this.uploadedFile.getContents());
		if (!this.dto.getAttachedFiles().contains(attachedFile)) {
			log.info("Añadiendo fichero adjunto...");
			this.dto.addAttachedFile(attachedFile);
		}
	}

	public List<SimpleDTOI> getProvincias() {
		log.debugv("getProvincias invoked!");
		return provinciaList;
	}

	public List<SimpleDTOI> getEstadosCiviles() {
		log.debugv("getEstadosCiviles invoked!");
		return estadosCiviles;
	}

	public List<SimpleDTOI> getCategoriasFamiliaNumerosa() {
		log.debugv("getCategoriasFamiliaNumerosa invoked!");
		return categoriasFamiliaNumerosa;
	}

	public List<SimpleDTOI> getClasesPensiones() {
		log.debugv("getClasesPensiones invoked!");
		return clasesPensiones;
	}

	public List<SimpleDTOI> getProcedenciasPensiones() {
		log.debugv("getProcedenciasPensiones invoked!");
		return procedenciasPension;
	}

	public List<SimpleDTOI> getOpciones() {
		log.debugv("getOpciones invoked!");
		return opciones;
	}

	public List<String> getSexos() {
		log.debugv("getSexos invoked!");
		return sexos;
	}

	public PlazoDTO getPlazoTurnoPorDefecto() {
		log.debugv("getPlazoTurnoPorDefecto invoked!");
		return plazoDTO;
	}

	public TurnoDTO getTurnoPorDefecto() {
		log.debugv("getTurnoPorDefecto invoked!");
		return turnoDTO;
	}

	public TemporadaDTO getTemporadaTurnoPorDefecto() {
		log.debugv("getTemporadaTurnoPorDefecto invoked!");
		return temporadaDTO;
	}

	public TurismoDTO getDto() {
		return this.dto;
	}

	public void setDto(TurismoDTO _dto) {
		this.dto = _dto;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public Tramite getTramite() {
		return tramite;
	}

}