package es.imserso.sede.service.monitor;

import java.io.Serializable;
import java.util.List;

/**
 * Data Object con información acerca del estado de una aplicación remota
 * 
 * @author 11825775
 *
 */
public interface AppInfo extends Serializable {

	/**
	 * @return Nombre de la aplicación
	 */
	String getName();

	/**
	 * @return Descripción de la aplicación
	 */
	String getDescription();

	/**
	 * @return URL de la web de la aplicación
	 */
	String getWebSiteURL();

	/**
	 * @return si la URL de la web de la aplicación está respondiendo o no.
	 */
	Boolean webSiteActive();

	/**
	 * Indica si los servicios web de una aplicación remota pueden ser invocados o
	 * no.
	 * <p>
	 * Es posble que los servicios web de una aplicación no respondan (404) o que,
	 * aunque estén activos y respondan, alguno de esos servicios web indique que
	 * alguna funcionalidad crítica de esa aplicación no está disponible, por lo que
	 * se considerará que los servicios web de esa aplicación no están activos.
	 * 
	 * @return si los servicios web de una aplicación remota están activos o no
	 */
	Boolean webServicesActive();

	/**
	 * @return Servicios de la aplicación
	 */
	List<ServiceInfo> getServices();

}
