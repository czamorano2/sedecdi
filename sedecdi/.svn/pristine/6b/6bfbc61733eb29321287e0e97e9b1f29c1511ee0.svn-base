package es.imserso.sede.util.rest.client;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.jboss.logging.Logger;
import org.jboss.wise.core.client.InvocationResult;
import org.jboss.wise.core.client.WSDynamicClient;
import org.jboss.wise.core.client.WSMethod;
import org.jboss.wise.core.client.WebParameter;
import org.jboss.wise.core.client.builder.WSDynamicClientBuilder;
import org.jboss.wise.core.client.factories.WSDynamicClientFactory;
import org.jboss.wise.core.exception.InvocationException;
import org.jboss.wise.core.exception.MappingException;
import org.jboss.wise.core.exception.ResourceNotAvailableException;
import org.jboss.wise.core.exception.WiseRuntimeException;
import org.jboss.wise.core.exception.WiseWebServiceException;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.rest.termalismo.BalnearioVOWS;
import es.imserso.sede.data.dto.rest.termalismo.BuscadorWS;
import es.imserso.sede.data.dto.rest.termalismo.ComunidadAutonomaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.ContenedorExpedienteVOWS;
import es.imserso.sede.data.dto.rest.termalismo.EstadoCivilVOWS;
import es.imserso.sede.data.dto.rest.termalismo.MantenimientoVOWS;
import es.imserso.sede.data.dto.rest.termalismo.ProvinciaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.SexoVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoFamiliaNumerosaVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoPensionVOWS;
import es.imserso.sede.data.dto.rest.termalismo.TipoTurnoVOWS;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.util.exception.SedecdiRestException;

public class TermalismoRestClientAux {

	private static final String METHOD_PROVINCIAS = "getListaProvincias";
	private static final String METHOD_SEXOS = "getListaSexos";
	private static final String METHOD_COMUNIDADES = "getListaComunidadesAutonomas";
	private static final String METHOD_ESTADOSCIVILES = "getListaEstadosCiviles";
	private static final String METHOD_TIPOSPENSIONES = "getListaTiposPensiones";
	private static final String METHOD_BALNEARIOS = "getListaBalneariosAltaExpediente";
	private static final String METHOD_LISTATURNOS = "getListaTurnosAltaExpediente";
	private static final String METHOD_TIPOSDURACION = "getListaTiposDuracionTurnoAltaExpediente";
	private static final String METHOD_TIPOSFAMILIANUM = "getListaTiposFamiliaNumerosa";
	

	@Inject
	private Logger log;

	@Inject
	protected Event<EventMessage> eventMessage;
	
	private WSDynamicClient client;
	
	
	private Class<?> paramType;

	@Inject
	private PropertyComponent propertyComponent;
	
	private static String WEB_SERVICE = "SolicitanteWebServiceImplService";
	private static String PORT = "SolicitanteWebServiceImplPort";
	private static String PARAM_NAME_SOLICITUDES = "buscador";
	private static String PARAM_NAME_BALNEARIO = "comunidadAutonoma";
	private static String PARAM_METHOD_IDCOMUDIDAD = "setIdComunidadAutonoma";
	private static String PARAM_METHOD_NIF_SOLICITUD = "setNif";
	private static String PARAM_METHOD_NUM_EXPDTE_SOLICITUD = "setNumeroExpediente";
	private static String PARAM_METHOD_ANIO_SOLICITUD = "setAnyo";
	private static final String METHOD_CONEXIONTERMALISMO = "getTextoPrueba";
	private static final String METHOD_APLICACION_MANTENIMIENTO = "getAplicacionEnMantenimiento";
	
	
	public TermalismoRestClientAux() {
		super();
	}
	
	
	public Response getValoresOLD(String metodo, ArrayList<?> arrayList){
		String writeValueAsString=null;
		try {
			WSMethod method = getClientMethod(metodo);
					
			InvocationResult invocation =  invoke(method, new java.util.HashMap<String, Object>());
			
			arrayList = (ArrayList<?>)invocation.getResult().get(WSMethod.RESULT);
		
			ObjectMapper mapper = new ObjectMapper();

			//Object to JSON 
			writeValueAsString = mapper.writeValueAsString(arrayList);
			log.info("llamada CORRECTA al método "+metodo+" del WS: "+WEB_SERVICE);
			//client.close();
		} catch ( JsonProcessingException e) {
			log.info("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
			throw new SedecdiRestException("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
		}		
		return Response.ok().entity(writeValueAsString).build();			
	}


	/*
	 * Método que devuelve los balnearios que coincidan con el id de la comunidad pasado por parámetro
	 */
	public Response getBalneariosPorComunidadOLD(String metodo, Long idComunidad){
		String writeValueAsString=null;
		try {
			WSMethod method = getClientMethod(metodo);
					
			Object paramObject = webParams(method, PARAM_NAME_BALNEARIO);
			Map<String, Object>  mapReq = new HashMap<String, Object>();
			mapReq = busquedaBalnearioPorComunidadId(mapReq, paramObject, idComunidad);
			
			InvocationResult invocation =  invoke(method, mapReq);
			
			List<BalnearioVOWS> arrayList = (ArrayList<BalnearioVOWS>)invocation.getResult().get(WSMethod.RESULT);
		
			ObjectMapper mapper = new ObjectMapper();

			//Object to JSON 
			writeValueAsString = mapper.writeValueAsString(arrayList);
			log.info("llamada CORRECTA al método "+metodo+" del WS: "+WEB_SERVICE);
			client.close();
		} catch ( JsonProcessingException  e) {
			log.info("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
			throw new SedecdiRestException("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
		}		
		return Response.ok().entity(writeValueAsString).build();			
	}
	
	
	/*
	 * Método que devuelve los balnearios que coincidan con el id de la comunidad pasado por parámetro
	 */
	public List<BalnearioVOWS> getBalneariosPorComunidad(String metodo, Long idComunidad){
		String writeValueAsString=null;
		List<BalnearioVOWS> arrayList=null;
		try {
			WSMethod method = getClientMethod(metodo);
					
			Object paramObject = webParams(method, PARAM_NAME_BALNEARIO);
			Map<String, Object>  mapReq = new HashMap<String, Object>();
			mapReq = busquedaBalnearioPorComunidadId(mapReq, paramObject, idComunidad);
			
			InvocationResult invocation =  invoke(method, mapReq);
			
			arrayList = (ArrayList<BalnearioVOWS>)invocation.getResult().get(WSMethod.RESULT);
		
			ObjectMapper mapper = new ObjectMapper();

			//Object to JSON 
			writeValueAsString = mapper.writeValueAsString(arrayList);
			log.info("llamada CORRECTA al método "+metodo+" del WS: "+WEB_SERVICE);			
		} catch ( JsonProcessingException  e) {
			log.info("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
			throw new SedecdiRestException("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
		}		
		return arrayList;			
	}
	
	
	/*
	 * Método para comprobar la conexión con termalismo
	 */
	public Response getConexion(){
		WSMethod method = getClientMethod(METHOD_CONEXIONTERMALISMO);
		Map<String, Object> mapReq = new HashMap<String, Object>();
		mapReq.put("texto", "Hola");
		
		invoke(method, mapReq);
		ResponseBuilder response = Response.ok();
		client.close();
		return response.build();			
	}
	
	
	
	
	/*
	 * Método para comprobar la conexión con termalismo
	 */
//	public boolean getAplicacionMantenimiento1(){
//		boolean isMto = false;
//		WSMethod method = getClientMethod(METHOD_APLICACION_MANTENIMIENTO);
//		MantenimientoVOWS mto = new MantenimientoVOWS();
//		InvocationResult invoke = invoke(method,  new HashMap<String, Object>());
//		Class<?> classType = (Class<?>)invoke.getResult().get(WSMethod.TYPE_RESULT);
//		Object object = invoke.getResult().get(WSMethod.RESULT);
//		
//		ElementBuilder builder = ElementBuilderFactory.getElementBuilder().client(client).request(true).useDefautValuesForNullLeaves(true);  
//		Element element =builder.request(false).useDefautValuesForNullLeaves(false).buildTree(classType, mto.getClass().getName(), object, true);
//		
//		for (Iterator<? extends Element> it = element.getChildren();it.hasNext();  ){
//			Element e = it.next();
//			if (e.getName().equals("paradaAplicacion")){
//				isMto = Boolean.valueOf(e.getValue());
//			}
//			System.out.println(e.getName()+" : "+e.getValue());
//		}
//		
//		return isMto;
//			
//	}
	
	
	/*
	 * Método para comprobar la conexión con termalismo
	 */
	public boolean getAplicacionMantenimiento(){
		boolean isMto = false;
		try {
			
			WSMethod method = getClientMethod(METHOD_APLICACION_MANTENIMIENTO);
			
			InvocationResult invoke = invoke(method,  new HashMap<String, Object>());
			
			Object object = invoke.getResult().get(WSMethod.RESULT);
			
			ObjectMapper mapper = new ObjectMapper();	
			//Object to JSON 
			String writeValueAsString = mapper.writeValueAsString(object);
			//JSON to java
			MantenimientoVOWS obj = mapper.readValue(writeValueAsString, MantenimientoVOWS.class);
			isMto = obj.isParadaAplicacion();
			
		} catch (IOException e ) {
			throw new SedecdiRestException("Error en el método getApliacionMantenimiento: "+e.getMessage());
		
		} 
		
		return isMto;
			
	}
	
	/*
	 * Método que devuelve una lista de objetos ContenedorExpedienteVOWS que contendrá las solicitudes de termalismo que coincidan con 
	 * el númeroExpediente y anioConvocatoria recibidos como parámetro
	 */
	public List<ContenedorExpedienteVOWS> getExpedienteTermalismoPorExpAnio(String numeroExpediente, Integer anioConvocatoria){
		if (numeroExpediente == null || anioConvocatoria == null){
			log.info("Es obligatorio introducir numero expdte y año convocatoria");
			throw new SedecdiRestException("Es obligatorio introducir numero expdte y año convocatoria para realizar la búsqueda en termalismo");
		}
		return busquedaExpedienteTermalismoPorNifExpdteAnio(null, numeroExpediente, anioConvocatoria);
	}

	/*
	 * Método que devuelve una lista de objetos ContenedorExpedienteVOWS que contendrá las solicitudes de termalismo que coincidan con 
	 * el el nif recibido como parámetro
	 */
	public List<ContenedorExpedienteVOWS> getExpedienteTermalismoPorNif(String nif){
		if (nif == null){
			log.info("Es obligatorio introducir nif");
			throw new SedecdiRestException("Es obligatorio introducir nif para realizar la búsqueda en termalismo");
		}
		return busquedaExpedienteTermalismoPorNifExpdteAnio(nif, null, null);
	}
	
	
	private List<ContenedorExpedienteVOWS> busquedaExpedienteTermalismoPorNifExpdteAnio(String nif, String numeroExpediente, Integer anioConvocatoria){
		
		List<ContenedorExpedienteVOWS> listaContenedorExpedientes=null;
		WSMethod method =  getClientMethod("busquedaExpediente");
		//nombre del método a invocar
		
		//nombre del parámetro que recibe el método
		Object paramObject = webParams(method, PARAM_NAME_SOLICITUDES);
		Map<String, Object>  mapReq = new HashMap<String, Object>();
		if (nif !=null){
			mapReq = busquedaNif(mapReq, paramObject, nif);
		}else{
			mapReq = busquedaExpdteAnio(mapReq,paramObject, numeroExpediente, anioConvocatoria);
		}
		InvocationResult invocation = invoke(method, mapReq);
		listaContenedorExpedientes = (ArrayList<ContenedorExpedienteVOWS>)invocation.getResult().get(WSMethod.RESULT);
		
		log.info("llamada CORRECTA al método busquedaExpediente del WS: "+WEB_SERVICE);
		
		client.close();
		return listaContenedorExpedientes;				
	}
	
	


	protected InvocationResult invoke(WSMethod method,  Map<String, Object> mapReq) {
		try {
			return method.invoke(mapReq);
		} catch (IllegalArgumentException | WiseWebServiceException | InvocationException | MappingException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		
	}



	protected Object webParams(WSMethod method, String nameParam) {
		WebParameter webParameter = method.getWebParams().get(nameParam);
		paramType = (Class<?>)webParameter.getType();
		Object paramObject;
		try {
			paramObject = paramType.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		return paramObject;
	}
	



	private Map<String, Object> busquedaExpdteAnio(Map<String, Object>mapReq, Object paramObject, String numeroExpediente, Integer anioConvocatoria) {
		try {
			paramType.getMethod(PARAM_METHOD_NUM_EXPDTE_SOLICITUD, String.class).invoke(paramObject, numeroExpediente);
			paramType.getMethod(PARAM_METHOD_ANIO_SOLICITUD, Integer.class).invoke(paramObject, anioConvocatoria);			
			mapReq.put(PARAM_NAME_SOLICITUDES, paramObject);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		return mapReq;
	}

	
	private Map<String, Object> busquedaNif(Map<String, Object>mapReq, Object paramObject, String nif) {
		BuscadorWS b = new BuscadorWS();		
		try {
			paramType.getMethod(PARAM_METHOD_NIF_SOLICITUD, String.class).invoke(paramObject, nif);
			mapReq = new HashMap<String, Object>();
			mapReq.put(PARAM_NAME_SOLICITUDES, paramObject);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		return mapReq;
	}
	
	private Map<String, Object> busquedaBalnearioPorComunidadId(Map<String, Object>mapReq, Object paramObject, Long idComunidad) {
		try {
			paramType.getMethod(PARAM_METHOD_IDCOMUDIDAD, Long.class).invoke(paramObject, idComunidad);
			mapReq = new HashMap<String, Object>();
			mapReq.put(PARAM_NAME_BALNEARIO, paramObject);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		return mapReq;
	}
	
	protected WSMethod getClientMethod(String metodo)  {		
		WSMethod method;
		if (client==null){
			try {
				WSDynamicClientBuilder clientBuilder = WSDynamicClientFactory.getJAXWSClientBuilder();
				client = clientBuilder.tmpDir("target/temp/wise").verbose(true).keepSource(true). maxThreadPoolSize(20)
					    .wsdlURL(propertyComponent.getTermalismoResourcesURL()).build();
			} catch (ConnectException|WiseRuntimeException|IllegalStateException e) {
				log.error(e.getMessage());
				throw new SedecdiRestException(e);
			} 
		}
		try {
			method = client.getWSMethod(WEB_SERVICE, PORT, metodo);			
		} catch (WiseRuntimeException | IllegalStateException | ResourceNotAvailableException e) {
			log.error(e.getMessage());
			throw new SedecdiRestException(e);
		}
		return method;
		
	}

	

	private ArrayList<?> getValores(String metodo, ArrayList<?> arrayList){
		String writeValueAsString=null;
		try {
			WSMethod method = getClientMethod(metodo);
					
			InvocationResult invocation =  invoke(method, new java.util.HashMap<String, Object>());
			
			arrayList = (ArrayList<?>)invocation.getResult().get(WSMethod.RESULT);
			
			Response response = (Response)invocation.getResult().get(InvocationResult.RESPONSE);
		
			ObjectMapper mapper = new ObjectMapper();

			//Object to JSON 
			writeValueAsString = mapper.writeValueAsString(arrayList);
			
			
			log.info("llamada CORRECTA al método "+metodo+" del WS: "+WEB_SERVICE);
			//client.close();
		} catch ( JsonProcessingException e) {
			log.info("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
			throw new SedecdiRestException("Error en el método "+metodo+" del WS: "+WEB_SERVICE+e.getMessage());
		}		
		return arrayList;			
	}
	
	private String getNumeroExpediente(){
		String expediente=null;
		WSMethod method =  getClientMethod("getNuevoNumeroExpediente");
		InvocationResult invocation = invoke(method, new java.util.HashMap<String, Object>());
		expediente = (String)invocation.getResult().get(WSMethod.RESULT);		
		log.info("llamada CORRECTA al método getNuevoNumeroExpediente del WS: "+WEB_SERVICE);		
		//client.close();
		return expediente;				
	}

	/**
	 * Devuelve los estados civiles 
	 * 
	 */
	public List<EstadoCivilVOWS> getEstadosCiviles()  {
		return (List<EstadoCivilVOWS>)getValores(METHOD_ESTADOSCIVILES, new ArrayList<EstadoCivilVOWS>() );
	}

	public List<SexoVOWS> getSexos() {
		
		//termalismoResourceClient.getExpedienteTermalismoPorNif("33762550E");
		//termalismoResourceClient.getAplicacionMantenimiento();		
		
		return (List<SexoVOWS>)getValores(METHOD_SEXOS, new ArrayList<SexoVOWS>() );
	}


	
	public List<ProvinciaVOWS> getProvincias() {
		return (List<ProvinciaVOWS>)getValores(METHOD_PROVINCIAS, new ArrayList<ProvinciaVOWS>() );
	}
	
	public List<ComunidadAutonomaVOWS> getComunidadesAutonomas() {
		return (List<ComunidadAutonomaVOWS>)getValores(METHOD_COMUNIDADES, new ArrayList<ComunidadAutonomaVOWS>() );
	}
	
	public List<BalnearioVOWS> getBalneariosPorCCAA(Long idComunidad) {
		return (List<BalnearioVOWS>)getBalneariosPorComunidad(METHOD_BALNEARIOS, idComunidad );
	}
	
	
	
	public List<TipoTurnoVOWS> getListaTurnos() {
		return (List<TipoTurnoVOWS>)getValores(METHOD_LISTATURNOS, new ArrayList<TipoTurnoVOWS>());
	}
	
	public List<TipoPensionVOWS> getTiposPensiones() {
		return (List<TipoPensionVOWS>)getValores(METHOD_TIPOSPENSIONES, new ArrayList<TipoPensionVOWS>() );
	}
	
	public List<TipoFamiliaNumerosaVOWS> getTiposFamiliaNumerosa() {
		return (List<TipoFamiliaNumerosaVOWS>)getValores(METHOD_TIPOSFAMILIANUM, new ArrayList<TipoFamiliaNumerosaVOWS>());
	}
	
	public String getNuevoNumeroExpediente() {
		return (String)getNuevoNumeroExpediente();
	}
	



}