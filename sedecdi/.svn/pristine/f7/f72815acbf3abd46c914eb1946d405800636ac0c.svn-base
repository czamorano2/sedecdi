package es.imserso.sede.data;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.CategoriaFamiliaNumerosaDTO;
import es.imserso.hermes.session.webservice.dto.ClasePensionDTO;
import es.imserso.hermes.session.webservice.dto.EstadoCivilDTO;
import es.imserso.hermes.session.webservice.dto.OpcionDTO;
import es.imserso.hermes.session.webservice.dto.PlazoDTO;
import es.imserso.hermes.session.webservice.dto.ProcedenciaPensionDTO;
import es.imserso.hermes.session.webservice.dto.ProvinciaDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTO;
import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.hermes.session.webservice.dto.TemporadaDTO;
import es.imserso.hermes.session.webservice.dto.TurnoDTO;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.service.cache.hermes.CrunchifyInMemoryCache;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.CategoriasFamiliaNumerosa;
import es.imserso.sede.util.rest.client.ClasesPension;
import es.imserso.sede.util.rest.client.Destinos;
import es.imserso.sede.util.rest.client.EstadosCiviles;
import es.imserso.sede.util.rest.client.Plazo;
import es.imserso.sede.util.rest.client.ProcedenciasPension;
import es.imserso.sede.util.rest.client.Provincias;
import es.imserso.sede.util.rest.client.RestClient;
import es.imserso.sede.util.rest.client.hermes.SelectOneMenuStringList;

/**
 * Gestiona los datos que se obtienen de Hermes de forma remota.
 * <p>
 * Precisamente por obtenerse de forma remota y el coste que eso conlleva, este
 * componente optimiza los recursos utilizando una caché para aquellos datos que
 * apenas son modificados (provincias, estados civiles, etc)
 * 
 * @author 11825775
 *
 */
@ApplicationScoped
public class TurismoRemoteRepository {

	private static final Logger log = Logger.getLogger(TurismoRemoteRepository.class.getName());

	private static final int HOURS_1 = 3600;

	@Inject
	Instance<PropertyComponent> propertyComponentInstance;

	List<SimpleDTOI> lastEstadosCiviles;
	List<SimpleDTOI> lastProvincias;
	List<SimpleDTOI> lastCategoriasFamiliaNumerosa;
	List<SimpleDTOI> lastClasesPensiones;
	List<SimpleDTOI> lastOpciones;
	List<SimpleDTOI> lastProcedenciasPensiones;
	List<String> lastSexos;
	PlazoDTO lastPlazoTurnoPorDefecto;
	TurnoDTO lastTurnoPorDefecto;
	TemporadaDTO lastTemporadaTurnoPorDefecto;

	CrunchifyInMemoryCache<CachesTurismo, List<SimpleDTOI>> hermesDtoLists;
	CrunchifyInMemoryCache<CachesTurismo, List<String>> hermesDtoSexos;
	CrunchifyInMemoryCache<CachesTurismo, PlazoDTO> hermesDtoPlazoTurnoPorDefecto;
	CrunchifyInMemoryCache<CachesTurismo, TurnoDTO> hermesDtoTurnoPorDefecto;
	CrunchifyInMemoryCache<CachesTurismo, TemporadaDTO> hermesDtoTemporadaTurnoPorDefecto;

	@PostConstruct
	public void onCreate() {
		lastEstadosCiviles = new ArrayList<SimpleDTOI>();
		lastProvincias = new ArrayList<SimpleDTOI>();
		lastCategoriasFamiliaNumerosa = new ArrayList<SimpleDTOI>();
		lastClasesPensiones = new ArrayList<SimpleDTOI>();
		lastOpciones = new ArrayList<SimpleDTOI>();
		lastProcedenciasPensiones = new ArrayList<SimpleDTOI>();
		lastSexos = new ArrayList<String>();

		initCaches();
	}

	/**
	 * Inicia/reinicia las cachés con la configuración actualizada.
	 */
	private void initCaches() {
		hermesDtoLists = new CrunchifyInMemoryCache<CachesTurismo, List<SimpleDTOI>>(70, 60, 10);
		hermesDtoSexos = new CrunchifyInMemoryCache<CachesTurismo, List<String>>(50, 60, 2);
		hermesDtoPlazoTurnoPorDefecto = new CrunchifyInMemoryCache<CachesTurismo, PlazoDTO>(HOURS_1, HOURS_1, 20);
		hermesDtoTurnoPorDefecto = new CrunchifyInMemoryCache<CachesTurismo, TurnoDTO>(HOURS_1, HOURS_1, 10);
		hermesDtoTemporadaTurnoPorDefecto = new CrunchifyInMemoryCache<CachesTurismo, TemporadaDTO>(HOURS_1, HOURS_1,
				10);
	}

	/**
	 * Reinicia las cachés con la configuración actualizada.
	 * <p>
	 * Pensado para ser invocado tras modificar algún parámetro de la configuración
	 * de las cachés.
	 */
	public void reconfigure() {

	}

	public List<SimpleDTOI> getEstadosCiviles() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.estados_civiles);
		if (list == null) {
			list = convert2DTOI(getEstadosCivilesDTO());
			if (list != null) {
				lastEstadosCiviles.clear();
				lastEstadosCiviles.addAll(list);
				hermesDtoLists.put(CachesTurismo.estados_civiles, list);
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<EstadoCivilDTO> getEstadosCivilesDTO() throws SedeException {
		return (List<EstadoCivilDTO>) ((EstadosCiviles) UtilsCDI.getBeanByReference(EstadosCiviles.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_ESTADOS_CIVILES)
				.execute();
	}

	public List<SimpleDTOI> getProvincias() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.provincias);
		if (list == null) {
			list = convert2DTOI(getProvinciasDTO());
			hermesDtoLists.put(CachesTurismo.provincias, list);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProvinciaDTO> getProvinciasDTO() throws SedeException {
		return (List<ProvinciaDTO>) ((Provincias) UtilsCDI.getBeanByReference(Provincias.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_PROVINCIAS)
				.execute();
	}

	public List<SimpleDTOI> getCategoriasFamiliaNumerosa() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.categorias_familia_numerosa);
		if (list == null) {
			list = convert2DTOI(getCategoriasFamiliaNumerosaDTO());
			hermesDtoLists.put(CachesTurismo.categorias_familia_numerosa, list);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<CategoriaFamiliaNumerosaDTO> getCategoriasFamiliaNumerosaDTO() throws SedeException {
		return (List<CategoriaFamiliaNumerosaDTO>) ((CategoriasFamiliaNumerosa) UtilsCDI.getBeanByReference(CategoriasFamiliaNumerosa.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_CATEGORIAS_FAMILIA_NUMEROSA)
				.execute();
	}

	public List<SimpleDTOI> getClasesPensiones() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.clases_pensiones);
		if (list == null) {
			list = convert2DTOI(getClasesPensionesDTO());
			hermesDtoLists.put(CachesTurismo.clases_pensiones, list);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<ClasePensionDTO> getClasesPensionesDTO() throws SedeException {
		return (List<ClasePensionDTO>) ((ClasesPension) UtilsCDI.getBeanByReference(ClasesPension.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_CLASES_PENSIONES)
				.execute();
	}

	public List<SimpleDTOI> getOpciones() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.destinos);
		if (list == null) {
			list = convert2DTOI(getOpcionesDTO());
			hermesDtoLists.put(CachesTurismo.destinos, list);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpcionDTO> getOpcionesDTO() throws SedeException {
		return (List<OpcionDTO>) ((Destinos) UtilsCDI.getBeanByReference(Destinos.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_OPCIONES)
				.execute();
	}

	public List<SimpleDTOI> getProcedenciasPensiones() throws SedeException {
		List<SimpleDTOI> list = hermesDtoLists.get(CachesTurismo.procedencias_pendiones);
		if (list == null) {
			list = convert2DTOI(getProcedenciasPensionesDTO());
			hermesDtoLists.put(CachesTurismo.procedencias_pendiones, list);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProcedenciaPensionDTO> getProcedenciasPensionesDTO() throws SedeException {
		return (List<ProcedenciaPensionDTO>) ((ProcedenciasPension) UtilsCDI.getBeanByReference(ProcedenciasPension.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_PROCEDENCIA_PENSIONES)
				.execute();
	}

	public List<String> getSexos() throws SedeException {
		List<String> list = hermesDtoSexos.get(CachesTurismo.sexos);
		if (list == null) {
			list = getSexosDTO();
			hermesDtoSexos.put(CachesTurismo.sexos, list);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getSexosDTO() throws SedeException {
		return (List<String>) ((SelectOneMenuStringList) UtilsCDI.getBeanByReference(SelectOneMenuStringList.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_SEXOS)
				.execute();
	}

	public PlazoDTO getPlazoTurnoPorDefecto() throws SedeException {
		PlazoDTO plazo = hermesDtoPlazoTurnoPorDefecto.get(CachesTurismo.plazo);
		if (plazo == null) {
			plazo = getPlazoDTO();
			hermesDtoPlazoTurnoPorDefecto.put(CachesTurismo.plazo, plazo);
		}
		return plazo;
	}
	
	private PlazoDTO getPlazoDTO() throws SedeException {
		return (PlazoDTO) ((Plazo) UtilsCDI.getBeanByReference(Plazo.class))
				.call(propertyComponentInstance.get().getTurismoResourcesURL() + RestClient.URL_SUFFIX_PLAZOTURNO_POR_DEFECTO)
				.execute();
	}

	private List<SimpleDTOI> convert2DTOI(List<?> list) throws SedeException {
		try {
			List<SimpleDTOI> dtoList = new ArrayList<SimpleDTOI>();
			for (Object o : list) {
				SimpleDTO dto = new SimpleDTO();
				dto.refactorByReflection(o);
				dtoList.add(dto);
			}
			return dtoList;

		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}
	}

}
