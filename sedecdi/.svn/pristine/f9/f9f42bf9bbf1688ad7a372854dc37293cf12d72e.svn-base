package es.imserso.sede.service.monitor.firma.service;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.service.registration.receipt.ReceiptServiceI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * Información sobre la accesibilidad a los servicios REST de Firma Electrónica.
 * 
 * @author 11825775
 *
 */
public class FirmaRestServicesInfo implements ServiceInfo {

	private static final long serialVersionUID = 2667651293794565597L;
	
	@Inject
	Logger log;

	@Inject
	ReceiptServiceI receiptService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isCritical()
	 */
	@Override
	public Boolean isCritical() {
		return Boolean.TRUE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceName()
	 */
	@Override
	public String getServiceName() {
		return "Servicios de Firma Electrónica";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#getServiceDescription()
	 */
	@Override
	public String getServiceDescription() {
		return "Información sobre la accesibilidad a los servicios REST de Firma Electrónica";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.monitor.ServiceInfo#isEnabled()
	 */
	@Override
	public Boolean isEnabled() throws SedeException {
		Boolean result = Boolean.FALSE;
		try {
			result = receiptService.testServices();
		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			result = Boolean.FALSE;
		}
		return result;
	}

}
