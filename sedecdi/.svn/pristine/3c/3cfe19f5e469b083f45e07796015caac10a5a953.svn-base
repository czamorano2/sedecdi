package es.imserso.sede.web.model.validator;

import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.Configuration;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.impl.termalismo.TermalismoDTO;
import es.imserso.sede.data.validation.group.GroupNotRepresentante;
import es.imserso.sede.data.validation.group.GroupRepresentante;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.ucm.UCMService;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.util.resources.ResourceQ;
import es.imserso.sede.web.util.route.ParamValues;

/**
 * Validador de {@link es.imserso.sede.data.dto.impl.TermalismoDto}
 * 
 * @author 11825775
 *
 */
@Dependent
public class TermalismoDtoValidator {

	@Inject
	Logger log;

	@Inject
	UCMService UCMservice;

	@Inject
	@ResourceQ
	Usuario usuario;

	@Inject
	ParamValues paramValues;

	/**
	 * Indica si se ha realizado ya la validación
	 */
	Boolean validated;
	/**
	 * Indica si ha pasado correctamente las validaciones
	 */
	Boolean validatedOK;
	/**
	 * Indica si ha pasado correctamente las validaciones
	 */
	Boolean validatedKO;

	Validator validator;

	@PostConstruct
	public void onCreate() {
		validated = Boolean.FALSE;
		validatedOK = Boolean.FALSE;
		validatedKO = Boolean.FALSE;

		Configuration<?> config = Validation.byDefaultProvider().configure();
		ValidatorFactory factory = config.buildValidatorFactory();
		validator = factory.getValidator();
		factory.close();
	}

	/**
	 * Realiza las validaciones oportunas para asegurar que los datos del DTO
	 * son correctos
	 * <p>
	 * Siempre se ejecutará antes del método
	 * {@link es.imserso.sede.web.view.alta.SolicitudTermalismoView#save()}
	 * 
	 * @throws ValidationException
	 */
	public void validate(@NotNull TermalismoDTO dto) throws ValidationException {
		validated = Boolean.TRUE;
		// try {
		dto.setCodigoSIA(TipoTramite.TERMALISMO.getSia());

		log.info("validando los datos del formuario ...");
		validateFormData(dto);

		log.info("validando fechas de tramitación...");
		UCMservice.validateProcedureTerms(dto.getCodigoSIA());

		validatedOK = Boolean.TRUE;
		validatedKO = Boolean.FALSE;

		// } catch (ValidationException e) {
		// // scroll al panel de mensajes
		// RequestContext.getCurrentInstance().scrollTo("TermalismoForm:messages");
		// // generamos el mensaje de error a mostrar al usuario
		// ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(),
		// TipoTramite.Termalismo,
		// "Validación de los datos introducidos",
		// Utils.getExceptionMessage(e));
		// validatedOK = Boolean.FALSE;
		// validatedKO = Boolean.TRUE;
		//
		// } catch (SedeException e) {
		// // scroll al panel de mensajes
		// RequestContext.getCurrentInstance().scrollTo("TermalismoForm:messages");
		// // generamos el mensaje de error a mostrar al usuario
		// ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(),
		// TipoTramite.Termalismo,
		// "Validación de los datos introducidos",
		// Utils.getExceptionMessage(e));
		// validatedOK = Boolean.FALSE;
		// validatedKO = Boolean.TRUE;
		// }
	}

	/**
	 * Ejecuta todas las validaciones necesarias para comprobar que los datos
	 * introcucidos son válidos
	 * 
	 * @return verdadero si pasa todas las validaciones
	 */
	private void validateFormData(@NotNull TermalismoDTO dto) throws ValidationException {

		int constraintViolationCounter = 0;

		// validamos los campos del formulario
		Set<ConstraintViolation<TermalismoDTO>> constraintViolations = validator
				.validate((TermalismoDTO) dto);

		if (constraintViolations.size() > 0) {
			log.warn("se han encontrado " + constraintViolations.size() + " errores de validación en el formulario:");
			for (ConstraintViolation<TermalismoDTO> violation : constraintViolations) {
				constraintViolationCounter++;
				log.warn(violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}

		}

		if (paramValues.isRepresentante()) {
			log.debug("validando campos cuando hay representante...");
			Set<ConstraintViolation<TermalismoDTO>> constraintViolationsRepresentante = validator
					.validate((TermalismoDTO) dto, GroupRepresentante.class);
			log.warn("se han encontrado " + constraintViolationsRepresentante.size()
					+ " errores de validación de representante en el formulario:");
			for (ConstraintViolation<TermalismoDTO> violation : constraintViolationsRepresentante) {

				constraintViolationCounter++;
				log.warn(violation.getPropertyPath() + ": " + violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}

		} else {
			log.debug("validando campos cuando no hay representante...");
			Set<ConstraintViolation<TermalismoDTO>> constraintViolationsNotRepresentante = validator
					.validate((TermalismoDTO) dto, GroupNotRepresentante.class);
			log.warn("se han encontrado " + constraintViolationsNotRepresentante.size()
					+ " errores de validación de NO representante en el formulario:");
			for (ConstraintViolation<TermalismoDTO> violation : constraintViolationsNotRepresentante) {

				constraintViolationCounter++;
				log.warn(violation.getPropertyPath() + ": " + violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}
		}

		if (constraintViolationCounter > 0) {
			if (FacesContext.getCurrentInstance().isValidationFailed()) {
				log.info("isValidationFailed = TRUE");
			}
			throw new ValidationException(String.format("Se han encontrado %d violaciones de reglas en el formulario",
					constraintViolationCounter));
		}

	}

	/**
	 * @return <code>true</code> si se ha realizado ya la validación del DTO
	 */
	public boolean alreadyValidated() {
		return validated;
	}

	/**
	 * @return <code>true</code> si ha pasado correctamente todas las
	 *         validaciones o <code>false</code> si aún no se ha validado o si
	 *         no ha pasado correctamente alguna validación
	 */
	public boolean isValid() {
		return validatedOK;
	}

	/**
	 * @return <code>true</code> si NO ha pasado correctamente todas las
	 *         validaciones o <code>false</code> si SI ha pasado correctamente
	 *         todas las validaciones
	 */
	public boolean isInvalid() {
		return validatedKO;
	}

}
