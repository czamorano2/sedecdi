package es.imserso.sede.process.scheduled.sync.hermes;

import java.io.IOException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.data.dto.util.DTOUtils;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraDTO;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.process.scheduled.sync.SyncEvent;
import es.imserso.sede.process.scheduled.sync.SyncEvent.SyncEventPhase;
import es.imserso.sede.service.converter.impl.PdfUtil;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.service.message.event.EventMessageSource;
import es.imserso.sede.service.registration.registry.ISicres.registers.RegistryServiceI;
import es.imserso.sede.service.registration.solicitud.SolicitudService;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentosRegistradosManager;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;

/**
 * Esta clase se encarga de sincronizar las solicitudes de la sede con las de
 * Hermes.
 * <p>
 * Busca las solicitudes de la sede que no existan aún en hermes y las da de
 * alta.
 * <p>
 * El criterio para buscar las solicitudes de la sede que no existan aún en
 * hermes es:
 * <ul>
 * <li>que el campo expedienteAplicacionGestora sea nulo</li>
 * <li>que hayan pasado más de 5 minutos desde la fecha de alta de la solicitud
 * </li>
 * </ul>
 * <p>
 * Normalmente esta clase será invocada en el arranque desde un EJB vinculado al
 * servicio Timer del contenedor.
 * 
 * @author 11825775
 *
 */
@Named
@Dependent
public class HermesSolicitudSynchronizer {

	@Inject
	private Logger log;

	@Inject
	TurismoRepository turismoRepository;

	@Inject
	RegistryServiceI registryService;

	@Inject
	SolicitudRepository solicitudRepository;

	@Inject
	SolicitudService solicitudService;

	@Inject
	Event<EventMessage> eventMessage;

	@Inject
	@HermesQ
	Event<SyncEvent> syncEvent;

	/**
	 * Sincroniza las solicitudes de la sede con las de Hermes
	 * 
	 * @throws SedeException
	 */
	public void batchSynchronize() throws SedeException {
		log.debug("obteniendo las solicitudes de turismo en la sede que no existan en Hermes...");

		List<Solicitud> list = solicitudRepository.findAllHermesUnsynchronized();
		if (!list.isEmpty()) {
			log.debugv("se han encontrado {0} de turismo solicitudes para sincronizar", list.size());

			syncEvent.fire(new SyncEvent(SyncEventPhase.SYNC_START));

			// damos de alta en hermes las solicitudes obtenidas
			for (Solicitud solicitud : list) {
				try {
					synchronize(solicitud);

				} catch (SedeException e) {
					String errmsg = "Error de sincronización de solicitud con Hermes: " + Utils.getExceptionMessage(e);
					log.error(errmsg);
					eventMessage.fire(new EventMessage(EventMessageSource.SYNC_HERMES,
							EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
				}
			}
			syncEvent.fire(new SyncEvent(SyncEventPhase.SYNC_END));
		} else {
			log.debug("no se han encontrado solicitudes de turismo para sincronizar");
		}

	}

	/**
	 * Sincroniza con Hermes la solicitud con el identificador especificado.
	 * 
	 * @param solicitudId
	 * @throws SedeException
	 */
	public void synchronize(Long solicitudId) throws SedeException {
		synchronize(solicitudRepository.findById(solicitudId));
	}

	/**
	 * Sincroniza con Hermes la solicitud especificada.
	 * 
	 * @param solicitudId
	 * @throws SedeException
	 */
	public void synchronize(Solicitud solicitud) throws SedeException {
		try {
			syncEvent.fire(new SyncEvent(SyncEventPhase.SYNC_EXPEDIENTE_START));
			doSynchronize(solicitud);
			syncEvent.fire(new SyncEvent(SyncEventPhase.SYNC_EXPEDIENTE_SUCCESSFUL));
			syncEvent.fire(new SyncEvent(SyncEventPhase.SYNC_EXPEDIENTE_END));

		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | ParseException
				| IOException | SedeException e) {
			syncEvent.fire(new SyncEvent(SyncEventPhase.SYNC_EXPEDIENTE_UNSUCCESSFUL));
			String errmsg = "No se pudo sincronizar la solicitud con id=" + solicitud.getId() + ": "
					+ Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new SedeException(errmsg, e);
		}
	}

	/**
	 * Sincroniza la solicitud de la sede con Hermes
	 * 
	 * @param solicitud
	 * @throws SedeException
	 * @throws ParseException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws IOException
	 */
	private void doSynchronize(Solicitud solicitud) throws SedeException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, ParseException, IOException {

		if (solicitud == null) {
			String errmsg = "Petición de sincronización de una solicitud nula";
			log.error(errmsg);
			throw new SedeException(errmsg);
		}

		log.debugv("sincronizando solicitud {0} ...)", solicitud.getId());
		DocumentosRegistradosManager adjuntos = solicitudService.getDocumentosSolicitud(solicitud);

		log.info("obtenemos el DTO...");
		Hashtable<String, String> solicitudTurismoHash = PdfUtil
				.extractData(adjuntos.getDocumentoSolicitud().getFichero());

		SolicitudTurismoDTO solicitudTurismoDTO = (SolicitudTurismoDTO) DTOUtils
				.getSolicitudTurismoDTO(solicitudTurismoHash, new SolicitudTurismoDTO());

		// se añaden los documentos adjuntados por el usuario
		solicitudTurismoDTO.setAttachedFiles(adjuntos.getDocumentosPersonales());

		log.info(solicitudTurismoDTO.getCodigoSIA());

		SolicitudTurismoDTO responseSolicitudTurismoDTO = turismoRepository.createRemoteSolicitud(solicitudTurismoDTO);

		if (responseSolicitudTurismoDTO != null) {
			log.info("responseSolicitudTurismoDTO OK!!");

			solicitudRepository.updateExpedienteAplicacionGestora(solicitud.getId(),
					new ObjectMapper().writeValueAsString(new TurismoExpedienteAplicacionGestoraDTO(
							responseSolicitudTurismoDTO.getId(), responseSolicitudTurismoDTO.getNumeroOrden())));

			log.info("Se ha sincronizado la solicitud con numero de registro (ISicres) "
					+ solicitud.getIdentificadorRegistro().getRegisterNumber());

		} else {
			log.error("responseSolicitudTurismoDTO KO!!");
			throw new SedeException();
		}
	}

}
