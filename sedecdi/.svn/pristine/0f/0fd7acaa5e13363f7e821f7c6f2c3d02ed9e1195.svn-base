package es.imserso.sede.web.rest;

import java.io.IOException;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;

import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraWrapper;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.registration.registry.RegistryBookManager;
import es.imserso.sede.service.registration.registry.ISicres.books.BookRegistryServiceI;
import es.imserso.sede.service.registration.registry.ISicres.registers.RegistryServiceI;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSDocument;
import es.imserso.sede.service.registration.registry.bean.InputRegisterI;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.SedecdiRestException;

/**
 * Endpoint para servicios generales de solicitudes
 * 
 * @author 11825775
 *
 */
@RequestScoped
@Path("/solicitud")
public class SolicitudEndpoint {

	private static final String NO_DISPONIBLE_EN_ESTE_MOMENTO = "No disponible en este momento";

	@Inject
	private Logger log;

	@Inject
	protected SolicitudRepository solicitudRepository;
	
	@Inject
	protected TurismoRepository turismoRepository;

	@Inject
	protected Instance<BookRegistryServiceI> bookRegistryServiceInstance;

	@Inject
	protected Instance<RegistryServiceI> registryServiceInstance;

	@Inject
	protected RegistryBookManager registryBookManager;

	/**
	 * @param cookieUsuario
	 *            para asegurar que está autenticado y para obtener el nif
	 * @return las solicitudes del usuario
	 */
	@GET
	@Path("/list")
	@Produces("application/json")
	public List<Solicitud> getSolicitudesUsuario(@NotNull @CookieParam("usuario") String cookieUsuario) {

		// validamos el parámetro antes de decodificar
		if (cookieUsuario == null) {
			log.error("error al decodificar la cookie con los datos del usuario: no hay cookie");
			// throw new HttpResponseException(HttpStatusCode.BadRequest);
			throw new SedecdiRestException("error al decodificar la cookie con los datos del usuario: no hay cookie",
					Response.status(Status.BAD_REQUEST).build());
		}
		if (cookieUsuario.trim().isEmpty()) {
			log.error("error al decodificar la cookie con los datos del usuario: la cadena a decodificar está vacía");
			throw new SedecdiRestException(
					"error al decodificar la cookie con los datos del usuario: la cadena a decodificar está vacía",
					Response.status(Status.LENGTH_REQUIRED).build());
		}

		log.info("obteniendo los datos de la cookie de usuario...");
		Usuario usuario = Utils.extractCookieData(cookieUsuario);

		return solicitudRepository.getSolicitudesUsuario(usuario.getNIF_CIF());
	}

	/**
	 * @param cookieUsuario
	 *            para asegurar que está autenticado y asegurar que la solicitud
	 *            devuelta pertenece al nif de la cookie
	 * @param solicitudid
	 *            id de la solicitud a devolver
	 * @param locale
	 * @return solicitud con el id especificado como parámetro y cuyo documento de
	 *         identificación coincida con el de la cookie
	 */
	@GET
	@Path("/detalle")
	@Produces("application/json")
	public Solicitud getSolicitudUsuario(@NotNull @CookieParam("usuario") String cookieUsuario,
			@NotNull @QueryParam("solicitudid") Long solicitudId, @QueryParam("locale") String locale) {

		Solicitud solicitud = null;

		try {

			log.debug(String.format("se pide el detalle de la solicitud con id=%s", solicitudId));

			// TODO ver qué hacemos con el locale

			log.info("obteniendo los datos de la cookie de usuario...");
			Usuario usuario = Utils.extractCookieData(cookieUsuario);

			solicitud = solicitudRepository.getSolicitudUsuario(solicitudId);

			if (solicitud.getTramite().getCodigoSIA().equals(TipoTramite.TURISMO.getSia())) {
				// si la solicitud es de turismo debemos devolver el estado de
				// la solicitud en Hermes

				if (solicitud.getExpedienteAplicacionGestora() == null
						|| solicitud.getExpedienteAplicacionGestora().isEmpty()) {
					throw new SedecdiRestException(String.format(
							"no se puede buscar una solicitud si el expediente de la solicitud (id=%d) gestora es nulo",
							solicitud.getId()), Response.status(Status.NOT_FOUND).build());
				}

				log.debug(String.format("buscando solicitud con los datos del expediente de la aplicación gestora: ",
						solicitud.getExpedienteAplicacionGestora()));

				String estadoAplicacionGestora;
				try {
					estadoAplicacionGestora = turismoRepository.getRemoteEstadoSolicitud(new TurismoExpedienteAplicacionGestoraWrapper()
									.build(solicitud.getExpedienteAplicacionGestora()));
					solicitud.setEstadoAsString(estadoAplicacionGestora);

				} catch (Exception e) {
					log.error(String.format(
							"no se pudo obtener el estado de la solicitud (id=%s) de la aplicación gestora (HERMES): %s",
							solicitud.getId(), Utils.getExceptionMessage(e)));
					solicitud.setEstadoAsString(NO_DISPONIBLE_EN_ESTE_MOMENTO);
				}

				log.info(String.format("establecemos el estado de la solicitud de turismo a %s",
						solicitud.getEstadoAsString()));

			} else if (solicitud.getTramite().getCodigoSIA().equals(TipoTramite.TERMALISMO.getSia())) {
				// si la solicitud es de termalismo debemos devolver el estado
				// de la solicitud en Termalismo

				// FIXME implementar cuando esté listo el cliente rest de
				// termalismo
			}

			Utils.checkMatchingNIFs(solicitud, usuario);

		} catch (SedeException e) {
			throw new SedecdiRestException(e);
		}

		return solicitud;
	}

	/**
	 * @param cookieUsuario
	 * @param solicitudid
	 * @return lista con los documentos de la solicitud especificada (pdf de la
	 *         solicitud, adjuntos, etc)
	 */
	@GET
	@Path("/documents")
	@Produces("application/json")
	public ArrayOfWSDocument getSolicitudPdfDocuments(@NotNull @CookieParam("usuario") String cookieUsuario,
			@NotNull @QueryParam("solicitudid") Long solicitudId) {

		log.debug(String.format("se pide una lista de los documentos de la solicitud con id=%s", solicitudId));

		log.info("obteniendo los datos de la cookie de usuario...");
		Usuario usuario = Utils.extractCookieData(cookieUsuario);

		Solicitud solicitud = solicitudRepository.getSolicitudUsuario(solicitudId);

		try {
			Utils.checkMatchingNIFs(solicitud, usuario);

			InputRegisterI inputRegister = registryServiceInstance.get()
					.getInputRegister(registryBookManager
							.getBook(solicitud.getIdentificadorRegistro().getRegisterNumber().substring(0, 4)).getId(),
							solicitud.getIdentificadorRegistro().getRegisterIdentification());
			return inputRegister.getDocuments();

		} catch (NumberFormatException | SedeException | IOException e) {
			log.error(String.format("error al obtener los documentos del registro de entrada: %s",
					Utils.getExceptionMessage(e)));
			throw new SedecdiRestException(e);
		}
	}

	/**
	 * Devuelve el documento especificado de la solicitud también especificada.
	 * <p>
	 * Normalmente se invoca para descargar el documento al navegador
	 * 
	 * @param cookieUsuario
	 * @param solicitudId
	 * @param registrationNumber
	 * @param pageIndex
	 * @param documentIndex
	 * @return documento
	 */
	@GET
	@Path("/document")
	@Produces("application/pdf")
	public Response getSolicitudPdfDocument(@NotNull @CookieParam("usuario") String cookieUsuario,
			@NotNull @QueryParam("solicitudid") Long solicitudId,
			@NotNull @QueryParam("registrationnumber") Integer registrationNumber,
			@NotNull @QueryParam("pageindex") Integer pageIndex,
			@NotNull @QueryParam("documentindex") Integer documentIndex) {

		log.info(String.format(
				"se pide descargar un documento de la solicitud %s con identificador de registro %d con índice de página %d e índice de documento %d, ",
				solicitudId, registrationNumber, pageIndex, documentIndex));

		try {
			Usuario usuario = Utils.extractCookieData(cookieUsuario);

			Solicitud solicitud = solicitudRepository.getSolicitudUsuario(solicitudId);

			// obtenemos el id del book del registro
			Integer bookId = null;
			if (solicitud.getIdentificadorRegistro() != null) {
				bookId = solicitud.getIdentificadorRegistro().getBookIdentification();
			} else {
				bookId = registryBookManager.getBook(solicitud.getFechaAlta()).getId();
			}

			Utils.checkMatchingNIFs(solicitud, usuario);

			log.debug("obteniendo el documento del registro ...");
			byte[] document = registryServiceInstance.get().getAttachedDocument(bookId, registrationNumber,
					documentIndex, pageIndex);
			log.info("documento obtenido del registro OK!");
			ResponseBuilder response = Response.ok((Object) document);
			response.header("Content-Disposition", "attachment; filename='doc_" + bookId.toString() + "_"
					+ registrationNumber.toString() + "_" + documentIndex.toString() + "_" + pageIndex + "'");
			log.info("devolviendo fichero como array de bytes...");
			return response.build();

		} catch (NumberFormatException | SedeException | IOException e) {
			log.error(String.format("error al obtener los documentos del registro de entrada: %s",
					Utils.getExceptionMessage(e)));
			throw new SedecdiRestException(e);
		}

	}

	/**
	 * Devuelve el documento especificado por nombre de la solicitud también
	 * especificada.
	 * <p>
	 * Normalmente se invoca para descargar el documento al navegador
	 * 
	 * @param cookieUsuario
	 * @param bookId
	 * @param registrationNumber
	 * @param documentName
	 */
	@GET
	@Path("/document/get2")
	@Produces("application/pdf")
	public Response getSolicitudPdfDocument2(@NotNull @CookieParam("usuario") String cookieUsuario,
			@NotNull @QueryParam("solicitudid") Long solicitudId, @NotNull @QueryParam("bookid") Integer bookId,
			@NotNull @QueryParam("documentname") String documentName) {

		log.debug(String.format("se pide descargar un documento del libro %d, solicititud %s, nombre %s",
				bookId.toString(), solicitudId.toString(), documentName));

		Usuario usuario = Utils.extractCookieData(cookieUsuario);

		Solicitud solicitud = solicitudRepository.getSolicitudUsuario(solicitudId);

		try {
			Utils.checkMatchingNIFs(solicitud, usuario);

			InputRegisterI inputRegister = registryServiceInstance.get().getInputRegister(bookId,
					solicitud.getIdentificadorRegistro().getRegisterIdentification());

			WSDocument selectedDocument = null;
			List<WSDocument> documentList = inputRegister.getDocuments().getWSDocument();
			for (WSDocument wsDocument : documentList) {
				if (wsDocument.getName().trim().equalsIgnoreCase(documentName.trim())) {
					log.debug(String.format("-- wsDocument: ", wsDocument.getName()));
					selectedDocument = wsDocument;
					break;
				}
			}

			if (selectedDocument != null) {
				throw new SedecdiRestException(
						String.format("no se encuentra el documento del libro %d, solicititud %s, nombre %s",
								bookId.toString(), solicitudId.toString(), documentName));
			}

			byte[] document = null; // TODO buscar la forma de obtener byte[] de
									// selectedDocument

			ResponseBuilder response = Response.ok((Object) document);
			response.header("Content-Disposition", "attachment; filename='" + selectedDocument + "'");
			return response.build();

		} catch (NumberFormatException | SedeException | IOException e) {
			log.error(String.format("error al obtener los documentos del registro de entrada: %s",
					Utils.getExceptionMessage(e)));
			throw new SedecdiRestException(e);
		}

	}

}
