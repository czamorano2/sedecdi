package es.imserso.sede.service.registration.receipt.test;

import static org.junit.Assert.assertNotNull;

import java.nio.file.Paths;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.imserso.sede.PackageRoot;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.service.registration.receipt.ReceiptServiceI;
import es.imserso.sede.util.Utils;

@RunWith(Arquillian.class)
public class ReceiptServiceImplTest {

	// @Deployment
	// public static JavaArchive createTestArchive() {
	// System.out.println("Lanzando ReceiptServiceImplTest ...");
	// JavaArchive ja = ShrinkWrap.create(JavaArchive.class, "test.jar")
	// .addPackages(true, PackageRoot.class.getPackage())
	// .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	// System.out.println(ja.toString());
	// return ja;
	//
	// }

	@Deployment
	public static WebArchive createTestArchive() {
		System.out.println("\n\n************************* Lanzando ReceiptServiceImplTest ***************\n\n");

		WebArchive archive = ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, PackageRoot.class.getPackage())
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

		System.out.println(archive.toString(true));

		return archive;
	}

	@Inject
	ReceiptServiceI receiptService;

	@Inject
	PropertyComponent propertyCompnent;

	@Inject
	Logger log;

	@Test
	@InSequence(1)
	public void testRegister() throws Exception {
		log.info("iniciando test de firma de documento pdf ...");
		try {
			byte[] document2Bsigned = Utils
					.getBytesFromFile(Paths.get(propertyCompnent.getTestFolder() + "/entrada/firma/FicheroAFirmar.pdf"));
			log.info("documento preparado para ser firmado...");
			byte[] signedDocument = receiptService.signReceipt(document2Bsigned);
			log.info("el documento ha sido firmado!");
			assertNotNull("", signedDocument);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		System.out.println("\n\n *****  FIN DEL TEST *****\n\n");
	}

}
