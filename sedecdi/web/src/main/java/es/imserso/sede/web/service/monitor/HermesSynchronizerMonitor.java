package es.imserso.sede.web.service.monitor;

import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

import es.imserso.sede.process.scheduled.sync.SyncEvent;
import es.imserso.sede.process.scheduled.sync.SyncEvent.SyncEventPhase;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Controla el estado del proceso de sincronización de solicitudes con Hermes.
 * 
 * @author 11825775
 *
 */
@Named
@ApplicationScoped
public class HermesSynchronizerMonitor {

	@Inject
	Logger log;

	@Inject
	@Push
	private PushContext synchronizerChannel;

	public void sendMessage() {
		String msg = "stateChanged";
		synchronizerChannel.send(msg);
	}

	public Date now() {
		return new Date(System.currentTimeMillis());
	}

	/**
	 * @param syncEvent
	 *            evento que informa de la fase actual del proceso de sincronización
	 *            de solicitudes con Hermes.
	 */
	public void onSyncEvent(@Observes @NotNull SyncEvent event) {

		if (SyncEventPhase.SYNC_START.equals(event.getPhase())) {
			log.debug("Comienza el proceso de sincronización de solicitudes con Hermes...");
			sendMessage();

		} else if (SyncEventPhase.SYNC_EXPEDIENTE_START.equals(event.getPhase())) {
			log.debug("Comienza el proceso de sincronización de una solicitud con Hermes...");

		} else if (SyncEventPhase.SYNC_EXPEDIENTE_SUCCESSFUL.equals(event.getPhase())) {
			log.debug("Finaliza satisfactoriamente el proceso de sincronización de una solicitud...");

		} else if (SyncEventPhase.SYNC_EXPEDIENTE_UNSUCCESSFUL.equals(event.getPhase())) {
			log.debug("Finaliza erróneamente el proceso de sincronización una solicitud con Hermes...");

		} else if (SyncEventPhase.SYNC_EXPEDIENTE_END.equals(event.getPhase())) {
			log.debug("Finaliza el proceso de sincronización de solicitudes con Hermes...");

		} else if (SyncEventPhase.SYNC_END.equals(event.getPhase())) {
			log.debug("Finaliza el proceso de sincronización de solicitudes con Hermes...");
			sendMessage();

		} else {
			String errmsg = String.format("evento desconocido: %s", event.getPhase());
			log.errorv(errmsg);
			throw new SedeRuntimeException(errmsg);
		}

	}

}
