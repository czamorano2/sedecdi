package es.imserso.sede.web.view.consulta;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistrado;
import es.imserso.hermes.session.webservice.dto.TipoDocumentoRegistrado;
import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.util.TurismoExpedienteAplicacionGestoraWrapper;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoTramite;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.service.registration.solicitud.SolicitudService;
import es.imserso.sede.service.registration.solicitud.documentos.DocumentosRegistradosManager;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.view.ViewUtils;

/**
 * Muestra las solicitudes del usuario dadas de alta en la Sede Electrónica
 * 
 * @author 11825775
 *
 */
@Named("solicitudesUsuarioView")
@ViewScoped
@Secure
public class SolicitudesUsuarioView implements Serializable {

	private static final long serialVersionUID = 4669574960404541810L;

	@Inject
	Logger log;

	@Inject
	SolicitudRepository solicitudRepository;

	@Inject
	SolicitudService solicitudService;

	@Inject
	@ResourceQ
	private Usuario usuario;

	private Solicitud selectedSolicitud;
	private Solicitud selectedSolicitudFull;

	private DocumentosRegistradosManager adjuntos;

	private StreamedContent pdfSolicitud;
	private StreamedContent pdfSolicitudAdjuntada;
	private StreamedContent pdfJustificante;
	private List<StreamedContent> documentosAdjuntados;

	@Inject
	Event<EventMessage> eventMessage;

	/**
	 * lista de solicitudes del usuario
	 */
	private List<Solicitud> list;

	@PostConstruct
	public void onCreate() {
		log.debug("PostConstruct...");
		search();
	}

	/**
	 * carga la lista de solicitudes del usuario
	 */
	public void search() {
		try {
			log.debug("searching...");
			eventMessage
					.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_INFO, "prueba de persistencia de mensajes"));
			list = solicitudRepository.getSolicitudesUsuario(usuario.getNIF_CIF());

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Solicitudes Sede Electrónica", errmsg));
		}
	}

	/**
	 * Obtiene una solicitud completa del repositorio
	 */
	public void loadDetail() {
		try {
			Long idSolicitud = selectedSolicitud.getId();
			selectedSolicitudFull = solicitudRepository.getSolicitudUsuario(idSolicitud);
			if (selectedSolicitudFull == null) {
				ViewUtils.manageViewError(log, FacesContext.getCurrentInstance(), TipoTramite.PROPOSITO_GENERAL,
						"No se pudo recuperar la solicitud",
						"No se encuentra la solicitud con identificador " + idSolicitud);
			}
			this.adjuntos = solicitudService.getDocumentosSolicitud(selectedSolicitudFull);

			this.pdfSolicitud = new DefaultStreamedContent(
					new ByteArrayInputStream(this.adjuntos.getDocumentoSolicitud().getFichero()), "application/pdf",
					TipoDocumentoRegistrado.SOLICITUD.getNombreDocumentoPorDefecto() + ".pdf");

			if (this.adjuntos.getDocumentoSolicitudAdjunta() != null) {
				this.pdfSolicitudAdjuntada = new DefaultStreamedContent(
						new ByteArrayInputStream(this.adjuntos.getDocumentoSolicitudAdjunta().getFichero()),
						"application/pdf",
						TipoDocumentoRegistrado.SOLICITUD_ADJUNTA.getNombreDocumentoPorDefecto() + ".pdf");
			}
			this.pdfJustificante = new DefaultStreamedContent(
					new ByteArrayInputStream(this.adjuntos.getDocumentoJustificante().getFichero()), "application/pdf",
					TipoDocumentoRegistrado.JUSTIFICANTE.getNombreDocumentoPorDefecto() + ".pdf");

			this.documentosAdjuntados = new ArrayList<StreamedContent>();
			for (DocumentoRegistrado doc : this.adjuntos.getDocumentosAdjuntados()) {
				this.documentosAdjuntados.add(new DefaultStreamedContent(new ByteArrayInputStream(doc.getFichero()),
						null, TipoDocumentoRegistrado.PERSONAL.getNombreDocumentoPorDefecto() + ".pdf"));
			}

			if (selectedSolicitudFull.isGESTIONADOPORAPLICACION()) {

				// se obtiene el estado del expediente en la aplicación gestora

				// Turismo
				if (selectedSolicitudFull.getTramite().isTurismo()
						&& StringUtils.isNotEmpty(selectedSolicitudFull.getExpedienteAplicacionGestora())) {
					selectedSolicitudFull.setEstadoAsString(
							((TurismoRepository) UtilsCDI.getBeanByReference(TurismoRepository.class))
									.getRemoteEstadoSolicitud(new TurismoExpedienteAplicacionGestoraWrapper()
											.build(selectedSolicitudFull.getExpedienteAplicacionGestora())));
				}

				// Termalismo
				if (selectedSolicitudFull.getTramite().isTermalismo()) {
					// FIXME implementar estado de la solicitud en la aplicación gestora
					// selectedSolicitudFull.setEstadoAsString(...);
				}
			}

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Detalle de Solicitud", errmsg));
		}
	}

	public void onRowSelect(SelectEvent event) {
		setSelectedSolicitud((Solicitud) event.getObject());
		FacesMessage msg = new FacesMessage("Solicitud Selected: ", ((Solicitud) event.getObject()).getId().toString());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public List<Solicitud> getList() {
		return list == null ? new ArrayList<Solicitud>() : list;
	}

	public void setList(List<Solicitud> list) {
		this.list = list;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Solicitud getSelectedSolicitud() {
		return selectedSolicitud;
	}

	public void setSelectedSolicitud(Solicitud selectedSolicitud) {
		this.selectedSolicitud = selectedSolicitud;
		loadDetail();
	}

	public DocumentosRegistradosManager getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(DocumentosRegistradosManager adjuntos) {
		this.adjuntos = adjuntos;
	}

	public StreamedContent getPdfSolicitud() {
		return pdfSolicitud;
	}

	public void setPdfSolicitud(StreamedContent pdfSolicitud) {
		this.pdfSolicitud = pdfSolicitud;
	}

	public StreamedContent getPdfSolicitudAdjuntada() {
		return pdfSolicitudAdjuntada;
	}

	public void setPdfSolicitudAdjuntada(StreamedContent pdfSolicitudAdjuntada) {
		this.pdfSolicitudAdjuntada = pdfSolicitudAdjuntada;
	}

	public StreamedContent getPdfJustificante() {
		return pdfJustificante;
	}

	public void setPdfJustificante(StreamedContent pdfJustificante) {
		this.pdfJustificante = pdfJustificante;
	}

	public List<StreamedContent> getDocumentosAdjuntados() {
		return documentosAdjuntados;
	}

	public void setDocumentosAdjuntado(List<StreamedContent> documentos) {
		this.documentosAdjuntados = documentos;
	}

	public StreamedContent getDocumentoAdjuntado(int index) {
		log.debugv("se ha solicitado el documento adjunto con índice {0}", index);
		return this.documentosAdjuntados.get(index);
	}

	public Solicitud getSelectedSolicitudFull() {
		return selectedSolicitudFull;
	}

	public void setSelectedSolicitudFull(Solicitud selectedSolicitudFull) {
		this.selectedSolicitudFull = selectedSolicitudFull;
	}

}
