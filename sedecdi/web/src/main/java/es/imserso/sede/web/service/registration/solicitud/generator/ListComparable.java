package es.imserso.sede.web.service.registration.solicitud.generator;

import java.util.List;

/**
 * Compara una lista de objetos externa contra una lista de objetos interna.
 * <p>
 * Pensado para comparar campos del DTO de alta de solicitud de trámite con
 * campos de la plantila pdf donde se van a incrustar los datos del DTO
 * 
 * @author 11825775
 *
 */
public interface ListComparable {

	/**
	 * Compara
	 * <p>
	 * Normalmente estas listas se obtendrán de un {@code HashMap<?>.keys()} de
	 * forma que se resuelva si tienen o no las mismas claves
	 * <p>
	 * 
	 * <ul>
	 * <li>0: se dan por iguales</li>
	 * <li>1: la lista interna contiene elementos no existentes en la lista
	 * externa</li>
	 * <li>2: la lista externa contiene elementos no existentes en la lista
	 * interna</li>
	 * <li>2: ambas listas contienen elementos no existentes en la otra lista</li>
	 * </ul>
	 * 
	 * @param intList
	 *            lista interna (de la propia instancia)
	 * @param extList
	 *            lista externa (de otra instancia)
	 * @return
	 * 
	 */
	default boolean compare(List<String> intList, List<String> extList) {
		boolean result = true;

		// miramos si todos los valoras de la lista interna existen en la externa
		java.util.Iterator<String> it = intList.listIterator();
		while (it.hasNext()) {
			String key = it.next();
			if (!extList.contains(key)) {
				// la lista interna contiene elementos no existentes en la lista externa
				result = false;
				break;
			}
		}

		// miramos si todos los valoras de la lista externa existen en la interna
		it = extList.listIterator();
		while (it.hasNext()) {
			String key = it.next();
			if (!intList.contains(key)) {
				// la lista externa contiene elementos no existentes en la lista interna
				result = false;
				break;
			}
		}

		return result;
	}

}
