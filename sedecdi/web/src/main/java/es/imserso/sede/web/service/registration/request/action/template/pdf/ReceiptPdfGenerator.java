package es.imserso.sede.web.service.registration.request.action.template.pdf;

import java.util.Map;
import java.util.function.Function;

import javax.inject.Inject;

import es.imserso.sede.service.converter.impl.PdfUtil;
import es.imserso.sede.service.registration.receipt.ReceiptServiceI;
import es.imserso.sede.util.resources.qualifier.ReceiptQ;
import es.imserso.sede.web.service.registration.request.action.persist.PersistedRequestDTOI;
import es.imserso.sede.web.service.registration.request.action.template.ReceiptTemplateDataMap;
import es.imserso.sede.web.service.registration.request.action.template.ReceiptTemplateSupplier;
import es.imserso.sede.web.util.ByteArrayWrapper;

/**
 * Genera y firma el PDF de la solicitud a partir del template y del Map con los
 * datos de la solicitud
 * 
 * @author 11825775
 *
 */
@ReceiptQ
public class ReceiptPdfGenerator implements Function<PersistedRequestDTOI, ByteArrayWrapper> {

	// @Inject
	// @ResourceQ
	// Supplier<ByteArrayWrapper> receiptTemplateSupplier;
	//
	// @Inject
	// @ResourceQ
	// Function<Solicitud, Map<String, String>> receiptTemplateDataSupplier;

	@Inject
	ReceiptTemplateSupplier receiptTemplateSupplier;

	@Inject
	ReceiptTemplateDataMap receiptTemplateDataMap;

	@Inject
	ReceiptServiceI receiptService;

	@Override
	public ByteArrayWrapper apply(PersistedRequestDTOI dto) {

		// obtiene el template de la base de datos
		ByteArrayWrapper template = receiptTemplateSupplier.get();

		// obtiene el Map con los datos que se incrustarán en el template
		Map<String, String> data = receiptTemplateDataMap.apply(dto.getRequestDatabaseSolicitud());

		// genera y firma el PDF
		return new ByteArrayWrapper(receiptService.signReceipt(PdfUtil.generatePdf(data, template.getBytes())));
	}
}
