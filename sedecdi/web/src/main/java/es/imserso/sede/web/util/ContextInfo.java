package es.imserso.sede.web.util;

import java.security.Principal;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;

import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.util.resources.qualifier.ResourceQ;

/**
 * EJB que actúa como proveedor de información del contexto.
 * <p>
 * El contexto de sesión se inyecta usando la anotación Resource.
 * 
 * @author 11825775
 *
 */
@Stateless
@Named
public class ContextInfo {

	public static final String CLAVE_COOKIE_NAME = "usuario";
	/**
	 * Session context injected using the resource annotation
	 */
	@Resource
	private SessionContext ctx;

	/**
	 * Secured EJB method using security annotations
	 */
	public String getSecurityInfo() {

		String sPrincipal = null;
		Principal principal = ctx.getCallerPrincipal();
		if (principal != null) {
			sPrincipal = principal.toString();

		}
		return sPrincipal;
	}

	/**
	 * @return nombre del usuario logado
	 */
	@Produces
	@Named
	public String getUserPrincipal() {
		String sPrincipal = null;
		Principal principal = ctx.getCallerPrincipal();
		if (principal != null) {
			sPrincipal = principal.toString();

		}
		return sPrincipal;
	}

	/**
	 * @return true si existe un usuario logado.
	 */
	@Produces
	@Named
	public boolean isLoggedIn() {
		return !((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession()
				.isNew();
	}

	/**
	 * @return usuario con los datos obtenidos de la cookie generada por
	 *         validaclave en base64
	 * @throws JAXBException
	 */
	@Produces
	@ResourceQ
	@Named
	@SessionScoped
	public Usuario getUsuario() throws JAXBException {
		
		//FIXME reponer lectura de cookie
//		Cookie[] cookies = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
//				.getCookies();
//
//		Usuario usuario = new Usuario();
//		for (int i = 0; i < cookies.length; i++) {
//			if (cookies[i].getName().equals(CLAVE_COOKIE_NAME)) {
//				usuario = CookieUtils.checkCookieUsuario(cookies[i].getValue());
//			}
//		}
		
		Usuario usuario = new Usuario();
		usuario.setNombre("LUIS");
		usuario.setApellido1("MONTES");
		usuario.setApellido2("GARZA");
		usuario.setNIF_CIF("11825775A");
		return usuario;
	}

	/**
	 * termina la sesión
	 */
	public String logout() {
		((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession()
				.invalidate();
		return "home";
	}

}
