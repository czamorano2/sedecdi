package es.imserso.sede.web.util.route;

import java.io.Serializable;

import es.imserso.sede.util.Utils;

/**
 * Value Bean con los datos de los parámetros de la petición
 * 
 * @author 11825775
 *
 */
public class ParamValue implements Serializable {

	private static final long serialVersionUID = -5548276892174348258L;
	/**
	 * valor del parámetro
	 */
	private String value;
	/**
	 * indica si el parámetro tiene valor o viene vacío
	 */
	private boolean empty;
	
	/**
	 * indica si viene el parámetro en la petición
	 */
	private boolean present;


	public ParamValue() {
		checkValue(this.value);
	}

	public ParamValue(String _value) {
		checkValue(_value);
	}

	private void checkValue(String _value) {
		if (_value == null) {
			this.present = false;
			this.empty = true;
		} else {
			if (Utils.isEmptyTrimmed(_value)) {
				this.present = true;
				this.empty = false;
			} else {
				this.present = true;
				this.empty = false;
				this.value = _value;
			}
		}
	}

	public boolean isEmpty() {
		return empty;
	}

	public boolean isPresent() {
		return present;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		checkValue(value);
	}

}
