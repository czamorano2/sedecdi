package es.imserso.sede.web.service.registration.solicitud.impl.generico;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.impl.GenericoDTO;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.web.service.registration.request.action.validation.GenericoDtoValidator;
import es.imserso.sede.web.service.registration.solicitud.impl.SolicitudRegistration;

/**
 * 
 * @author 11825775
 *
 */
@GenericoSolicitudRegistrationQ
public class GenericoSolicitudRegistration extends SolicitudRegistration {

	private static final long serialVersionUID = -1517265125277476566L;

	@Inject
	Logger log;

	@Inject
	GenericoDtoValidator validator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.solicitud.impl.SolicitudRegistration
	 * #validateDTO()
	 */
	@Override
	protected void validate() throws ValidationException {
		super.validate();

		validator.accept((GenericoDTO) dtoInstance);
	}

}
