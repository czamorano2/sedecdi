package es.imserso.sede.web.service.registration.solicitud.impl.termalismo;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.impl.termalismo.TermalismoDTO;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.web.service.registration.request.action.validation.TermalismoDtoValidator;
import es.imserso.sede.web.service.registration.solicitud.impl.SolicitudRegistration;

/**
 * Registra en Hermes una solicitud de turismo recogida del frontal.
 * 
 * @author 11825775
 *
 */
@TermalismoSolicitudRegistrationQ
public class TermalismoSolicitudRegistration extends SolicitudRegistration {

	private static final long serialVersionUID = -1517265125277476566L;

	@Inject
	Logger log;

	@Inject
	TermalismoDtoValidator validator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.solicitud.impl.SolicitudRegistration
	 * #validateDTO()
	 */
	@Override
	protected void validate() throws ValidationException {
		super.validate();

		log.debug("validando DTO de termalismo...");
		validator.accept((TermalismoDTO) dtoInstance);
	}

}
