package es.imserso.sede.web.service.registration.request.action.persist.registry;

import java.util.function.Function;

import javax.inject.Inject;

import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.web.service.registration.request.action.persist.PersistedRequestDTOI;

/**
 * Función que persiste el PDF de la solicitud en el registro electrónico
 * 
 * @author 11825775
 *
 */
public class RegistryPersister implements Function<PersistedRequestDTOI, IdentificadorRegistroDTO> {

	// @Inject
	// @ResourceQ
	// Function<InputRegistryData, IdentificadorRegistroDTO>
	// requestRegistryPersister;

	@Inject
	InveSicresRequestPersister requestRegistryPersister;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.function.Function#apply(java.lang.Object)
	 */
	@Override
	public IdentificadorRegistroDTO apply(PersistedRequestDTOI dto) {
		return requestRegistryPersister.apply(new InputRegistryData(dto.getRequestDTO(),
				dto.getRequestDatabaseSolicitud().getId(), dto.getRequestPdf()));
	}

}
