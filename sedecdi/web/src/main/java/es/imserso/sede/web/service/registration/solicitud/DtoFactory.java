package es.imserso.sede.web.service.registration.solicitud;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.impl.GenericoDTO;
import es.imserso.sede.data.dto.impl.PropositoGeneralDTO;
import es.imserso.sede.data.dto.impl.termalismo.TermalismoDTO;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.util.route.ParamValues;
import es.imserso.sede.web.util.route.ParamValues.Params;

/**
 * Proveedor de intancias de DTOs
 * <p>
 * Los DTOs de este package deben tener el constructor inaccesible para el
 * exterior, de forma que cuando la aplicación necesite una nueva instancia de
 * DTO se la tenga que pedir a una factoría, la cual sabrá cómo inicializar el
 * DTO
 * 
 * @author 11825775
 *
 */
@Dependent
@DtoFactoryQ
public class DtoFactory implements Serializable {

	private static final long serialVersionUID = -5536203433452540544L;

	/**
	 * Datos del usuario obtenidos de la cookie
	 * <p>
	 * Si la petición no incluye la cookie <code>usuario</code> será null
	 */
	@Inject
	@ResourceQ
	Usuario usuario;

	/**
	 * Datos relevantes especificados en los parámetros
	 */
	@Inject
	ParamValues paramValues;

	@Inject
	Instance<DtoTurismoFactory> dtoTurismoFactoryInstance;

	/**
	 * Crea una instancia del Dto de propósito general
	 * 
	 * @return
	 */
	public PropositoGeneralDTO createPropositoGeneralDtoInstance() {
		PropositoGeneralDTO dto = new PropositoGeneralDTO() {
			private static final long serialVersionUID = 1L;
		};

		// dto.setCodigoSIA(paramValues.getParamValue(Params.sia).getValue());

		// if (usuario != null) {
		// if (paramValues.isRepresentante()) {
		// dto.getsetNombreRepresentante(usuario.getNombre());
		// dto.setApellido1Representante(usuario.getApellido1());
		// dto.setApellido2Representante(usuario.getApellido2());
		// dto.setDocumentoIdentificacionRepresentante(usuario.getNIF_CIF());
		// } else {
		// dto.setNombre(usuario.getNombre());
		// dto.setApellido1(usuario.getApellido1());
		// dto.setApellido2(usuario.getApellido2());
		// dto.setDocumentoIdentificacion(usuario.getNIF_CIF());
		// }
		// }
		return dto;
	}

	/**
	 * Crea una instancia del Dto genérico
	 * 
	 * @return
	 */
	public GenericoDTO createGenericoDtoInstance() {
		GenericoDTO dto = new GenericoDTO() {
			private static final long serialVersionUID = 1L;
		};

		// dto.setCodigoSIA(paramValues.getParamValue(Params.sia).getValue());
		//
		// if (usuario != null) {
		// if (paramValues.isRepresentante()) {
		// dto.setNombreRepresentante(usuario.getNombre());
		// dto.setApellido1Representante(usuario.getApellido1());
		// dto.setApellido2Representante(usuario.getApellido2());
		// dto.setDocumentoIdentificacionRepresentante(usuario.getNIF_CIF());
		// } else {
		// dto.setNombre(usuario.getNombre());
		// dto.setApellido1(usuario.getApellido1());
		// dto.setApellido2(usuario.getApellido2());
		// dto.setDocumentoIdentificacion(usuario.getNIF_CIF());
		// }
		// }
		return dto;
	}

	public TermalismoDTO createTermalismoDtoInstance() {
		TermalismoDTO dto = new TermalismoDTO() {
			private static final long serialVersionUID = 1L;
		};

		dto.setCodigoSIA(paramValues.getParamValue(Params.sia).getValue());

		if (usuario != null) {
			dto.getSolicitante().setNombre(usuario.getNombre());
			dto.getSolicitante().setApellido1(usuario.getApellido1());
			dto.getSolicitante().setApellido2(usuario.getApellido2());
			dto.getSolicitante().setDocumentoIdentificacion(usuario.getNIF_CIF());
		}
		return dto;
	}

	/**
	 * Crea una instancia del Dto de edición de solicitud de turismo inicializada
	 * con los datos del certificado del usuario y los datos de la temporada, turno
	 * y plazo
	 * 
	 * @return
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO createTurismoEditDtoInstance(Long solicitudId) throws SedeException {
		return dtoTurismoFactoryInstance.get().createEditDtoInstance(solicitudId);
	}

}
