package es.imserso.sede.web.service.registration.request.action.persist;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.model.Solicitud;

/**
 * DTO con la información de la solicitud que se persiste en el sistema
 * <p>
 * Además de la información del formulario de SolicitudDTOI, contiene
 * información de la persistencia de la solicitud en el sistema
 * 
 * @author 11825775
 *
 */
public class PersistedRequestDTO implements PersistedRequestDTOI {

	private SolicitudDTOI requestDTO;

	private Solicitud requestDatabaseSolicitud;

	private IdentificadorRegistroDTO identificadorRegistroElectronico;

	private byte[] requestPdf;

	private byte[] receiptPdf;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#getRequestDTO()
	 */
	@Override
	public SolicitudDTOI getRequestDTO() {
		return requestDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#setRequestDTO(es.imserso.sede.data.dto.solicitud.
	 * SolicitudDTOI)
	 */
	@Override
	public void setRequestDTO(SolicitudDTOI dto) {
		requestDTO = dto;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#getRequestDatabaseId()
	 */
	@Override
	public Solicitud getRequestDatabaseSolicitud() {
		return requestDatabaseSolicitud;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#setRequestDatabaseId(java.lang.Long)
	 */
	@Override
	public void setRequestDatabaseSolicitud(Solicitud solicitud) {
		requestDatabaseSolicitud = solicitud;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#getRequestPdf()
	 */
	@Override
	public byte[] getRequestPdf() {
		return requestPdf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#setRequestPdf(byte[])
	 */
	@Override
	public void setRequestPdf(byte[] bytes) {
		requestPdf = bytes;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#getReceiptPdf()
	 */
	@Override
	public byte[] getReceiptPdf() {
		return receiptPdf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#setReceiptPdf(byte[])
	 */
	@Override
	public void setReceiptPdf(byte[] bytes) {
		receiptPdf = bytes;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#getIdentificadorRegistroElectronico()
	 */
	@Override
	public IdentificadorRegistroDTO getIdentificadorRegistroElectronico() {
		return identificadorRegistroElectronico;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.web.service.registration.request.action.persist.
	 * PersistedRequestDTOI#setIdentificadorRegistroElectronico(es.imserso.sede.data
	 * .dto.util.IdentificadorRegistroDTO)
	 */
	@Override
	public void setIdentificadorRegistroElectronico(IdentificadorRegistroDTO identificador) {
		identificadorRegistroElectronico = identificador;

	}

}
