package es.imserso.sede.web.view.consulta;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.auth.secure.Secure;

/**
 * Muestra las solicitudes del usuario dadas de alta en la Sede Electrónica
 * 
 * @author 11825775
 *
 */
@Named("solicitudUsuarioView")
@ViewScoped
@Secure
public class SolicitudUsuarioView implements Serializable {

	private static final long serialVersionUID = -6226130726194796626L;

	@Inject
	Logger log;

	@Inject
	SolicitudRepository solicitudRepository;

	@Inject
	@ResourceQ
	private Usuario usuario;

	private Long id;

	private Solicitud solicitud;

	@PostConstruct
	public void onCreate() {
		log.debug("PostConstruct...");
		retrieve();
	}

	/**
	 * carga la lista de solicitudes del usuario
	 */
	public void retrieve() {
		try {
			log.debugv("obteniendo solicitud {0} ...", getId());
			this.solicitud = solicitudRepository.findById(getId());

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Detalle de solicitud", errmsg));
		}
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
