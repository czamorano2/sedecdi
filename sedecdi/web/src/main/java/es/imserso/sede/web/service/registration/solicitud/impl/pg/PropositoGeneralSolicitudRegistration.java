package es.imserso.sede.web.service.registration.solicitud.impl.pg;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.impl.PropositoGeneralDTO;
import es.imserso.sede.web.service.registration.request.action.validation.PropositoGeneralDtoValidator;
import es.imserso.sede.web.service.registration.solicitud.impl.SolicitudRegistration;

/**
 * Registra en Hermes una solicitud de turismo recogida del frontal.
 * 
 * @author 11825775
 *
 */
@PropositoGeneralSolicitudRegistrationQ
public class PropositoGeneralSolicitudRegistration extends SolicitudRegistration {

	private static final long serialVersionUID = -1517265125277476566L;

	@Inject
	Logger log;

	@Inject
	PropositoGeneralDtoValidator validator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.solicitud.impl.SolicitudRegistration
	 * #validateDTO()
	 */
	@Override
	protected void validate() {
		super.validate();

		validator.accept((PropositoGeneralDTO) dtoInstance);
	}

}
