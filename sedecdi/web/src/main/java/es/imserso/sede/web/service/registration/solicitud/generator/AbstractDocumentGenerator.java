package es.imserso.sede.web.service.registration.solicitud.generator;

/**
 * Genera el documento que se va a registrar en el inveSicres con los datos de la solicitud.
 * 
 * @author 11825775
 *
 */
public abstract class AbstractDocumentGenerator {
	
	/**
	 * Genera el documento que se va a registrar en el inveSicres con los datos de la solicitud
	 */
	public final void generateDocument() {
		collectSource();
		validateSource();
		generateTarget();
	}
	
	/**
	 * Recoge los elementos necesarios para generar el documento
	 */
	public abstract void collectSource();
	
	/**
	 * Asegura que los elementos proporcionados sean válidos
	 */
	public abstract void validateSource();
	
	/**
	 * Genera el documento que se va a registrar en el inveSicres con los datos de la solicitud.
	 */
	public abstract void generateTarget();

}
