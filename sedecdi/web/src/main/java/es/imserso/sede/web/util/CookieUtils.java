package es.imserso.sede.web.util;

import java.io.StringReader;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.util.exception.SedecdiRestException;

public class CookieUtils {

	private static final String ERROR_COOKIE_CADENA_A_DECODIFICAR_NULA = "error al decodificar la cookie con los datos del usuario: la cadena a decodificar es nula";

	private static final String ERROR_COOKIE_CADENA_A_DECODIFICAR_VACIA = "error al decodificar la cookie con los datos del usuario: la cadena a decodificar está vacía";

	private static final String UTF_8 = "UTF-8";

	private static Logger log = Logger.getLogger(CookieUtils.class);

	/**
	 * @param base64EncodedCookie
	 *            cadena de texto conteniendo la cookie en Base64
	 * @return dto de usuario con los datos obtenidos de la cookie
	 * @throws JAXBException
	 */
	public static synchronized Usuario decodeCookie(String base64EncodedCookie) throws JAXBException {

		log.debug("decodificando cookie de usuario...");

		// validamos el parámetro antes de decodificar
		if (base64EncodedCookie == null) {
			String errmsg = ERROR_COOKIE_CADENA_A_DECODIFICAR_NULA;
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, Response.status(Status.NO_CONTENT).build());
		}
		if (base64EncodedCookie.trim().isEmpty()) {
			String errmsg = ERROR_COOKIE_CADENA_A_DECODIFICAR_VACIA;
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, Response.status(Status.LENGTH_REQUIRED).build());
		}

		String decodedValue = StringUtils.newString(Base64.decodeBase64(base64EncodedCookie), UTF_8);
		log.debug("decodedValue value: " + decodedValue);

		Usuario usuario = (Usuario) (JAXBContext.newInstance(Usuario.class).createUnmarshaller()
				.unmarshal(new StringReader(decodedValue)));
		log.debug("cookie de usuario decodificada!");

		return usuario;
	}

	/**
	 * Comprueba que exista una cookie llamada <code>usuario</code>
	 * 
	 * @return
	 * @throws JAXBException
	 */
	public static synchronized Usuario checkCookieUsuario(String base64EncodedCookie) throws JAXBException {
		log.debug("decodificando cookie de usuario...");

		// validamos el parámetro antes de decodificar
		if (base64EncodedCookie == null) {
			String errmsg = "error al decodificar la cookie con los datos del usuario: no hay cookie";
			log.error(errmsg);
			// throw new HttpResponseException(HttpStatusCode.BasdRequest);
			throw new SedecdiRestException(errmsg, Response.status(Status.NO_CONTENT).build());
		}
		if (base64EncodedCookie.trim().isEmpty()) {
			String errmsg = ERROR_COOKIE_CADENA_A_DECODIFICAR_VACIA;
			log.error(errmsg);
			throw new SedecdiRestException(errmsg, Response.status(Status.LENGTH_REQUIRED).build());
		}

		String decodedValue = StringUtils.newString(Base64.decodeBase64(base64EncodedCookie), UTF_8);
		log.debug("decodedValue value: " + decodedValue);

		Usuario usuario = (Usuario) (JAXBContext.newInstance(Usuario.class).createUnmarshaller()
				.unmarshal(new StringReader(decodedValue)));
		log.debug("cookie de usuario decodificada!");

		return usuario;
	}

}
