package es.imserso.sede.web.auth;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.jboss.logging.Logger;

@Logged
@Interceptor
public class LoggedInterceptor implements Serializable {
	
	private static final long serialVersionUID = 7913319686720931601L;
	
	private Logger logger = Logger.getLogger(LoggedInterceptor.class);

	public LoggedInterceptor() {
    }
	
	@AroundInvoke
    public Object logMethodEntry(InvocationContext invocationContext)
            throws Exception {
		logger.debug("Entering method: "
                + invocationContext.getMethod().getName() + " in class "
                + invocationContext.getMethod().getDeclaringClass().getName());

        return invocationContext.proceed();
    }

}
