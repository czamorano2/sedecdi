package es.imserso.sede.web.util.decorator;

import javax.annotation.PostConstruct;
import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.interceptor.Interceptor;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.impl.turismo.SolicitanteTurismoDTOI;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.util.route.ParamValues;

@Decorator
@Priority(Interceptor.Priority.APPLICATION)
public abstract class SolicitanteTurismoDecorator implements SolicitanteTurismoDTOI {

	private Logger log = Logger.getLogger(SolicitanteTurismoDecorator.class.getName());

	@Inject
	@Delegate
	@Any
	private SolicitanteTurismoDTOI solicitante;

	/**
	 * Datos del usuario obtenidos de la cookie
	 * <p>
	 * Si la petición no incluye la cookie <code>usuario</code> será null
	 */
	@Inject
	@ResourceQ
	Usuario usuario;

	/**
	 * Datos relevantes especificados en los parámetros
	 */
	@Inject
	ParamValues paramValues;

	boolean decorate;

	@PostConstruct
	public void onCreate() {
		decorate = (usuario != null && !paramValues.isRepresentante());
		if (decorate) {
			log.debug("decoramos el DTO de solicitante");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitanteDTOI#getNombre()
	 */
	@Override
	public String getNombre() {
		String value = null;
		
		if (decorate) {
			value = usuario.getNombre();
			log.debugv("decoramos el Nombre de solicitante: {0}",value );
		} else {
			value = solicitante.getNombre();
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitanteDTOI#getApellido1()
	 */
	@Override
	public String getApellido1() {
		String value = null;
		if (decorate) {
			value = usuario.getApellido1();
			log.debugv("decoramos el Apellido1 de solicitante: {0}",value );
		} else {
			value = solicitante.getApellido1();
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitanteDTOI#getApellido2()
	 */
	@Override
	public String getApellido2() {
		String value = null;
		if (decorate) {
			value = usuario.getApellido2();
			log.debugv("decoramos el Apellido2 de solicitante: {0}",value );
		} else {
			value = solicitante.getApellido2();
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.SolicitanteDTOI#getDocumentoIdentificacion()
	 */
	@Override
	public String getDocumentoIdentificacion() {
		String value = null;
		if (decorate) {
			value = usuario.getNIF_CIF();
			log.debugv("decoramos el nombre de solicitante: {0}",value );
		} else {
			value = solicitante.getDocumentoIdentificacion();
		}
		return value;
	}

}
