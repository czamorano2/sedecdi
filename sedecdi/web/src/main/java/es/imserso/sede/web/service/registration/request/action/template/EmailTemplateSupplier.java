package es.imserso.sede.web.service.registration.request.action.template;

import java.util.function.Supplier;

import javax.inject.Inject;

import es.imserso.sede.data.TramiteRepository;
import es.imserso.sede.model.PlantillaEmail;
import es.imserso.sede.web.util.route.ParamValues;

/**
 * Obtiene de la base de datos la plantilla de email
 * 
 * @author 11825775
 *
 */
public class EmailTemplateSupplier implements Supplier<PlantillaEmail> {

	@Inject
	ParamValues params;

	@Inject
	TramiteRepository tramiteRepository;

	@Override
	public PlantillaEmail get() {
		return tramiteRepository.findPlantillaEmailBySIA(params.getSia());
	}

}