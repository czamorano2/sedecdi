package es.imserso.sede.web.service.registration.request.action.persist.registry;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;

/**
 * Datos para persistir en el registro electrónico
 * 
 * @author 11825775
 *
 */
public class InputRegistryData {

	/**
	 * DTO con los datos de la solicitud
	 */
	private SolicitudDTOI dto;
	/**
	 * id de la solicitud en la base de datos
	 */
	private long requestId;
	/**
	 * array de bytes del PDF de la solicitud
	 */
	private byte[] requestPdf;

	public InputRegistryData(SolicitudDTOI dto, long requestId, byte[] requestPdf) {
		this.setDto(dto);
		this.setRequestId(requestId);
		this.setRequestPdf(requestPdf);
	}

	public SolicitudDTOI getDto() {
		return dto;
	}

	public void setDto(SolicitudDTOI dto) {
		this.dto = dto;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public byte[] getRequestPdf() {
		return requestPdf;
	}

	public void setRequestPdf(byte[] requestPdf) {
		this.requestPdf = requestPdf;
	}

}
