package es.imserso.sede.web.config;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.model.EventMessage;
import es.imserso.sede.process.scheduled.sync.hermes.HermesSolicitudSynchronizer;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.service.message.event.EventMessageSource;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.web.service.monitor.RemoteServicesMonitor;

/**
 * Este componente se inicia al arrancar el módulo web de trámites.
 * <p>
 * Gestiona las acciones a realizar al arrancar el módulo web de trámite.
 * 
 * @author 11825775
 */
@Startup
@Singleton
public class StartupManager {

	@Inject
	Logger log;

	@Inject
	RemoteServicesMonitor remoteServicesMonitor;
	
	@Inject
	HermesSolicitudSynchronizer hermesSolicitudSynchronizer;
	
	@Inject
	Event<EventMessage> eventMessage;

	/**
	 * Ejecuta la comprobación de los servicios web de las aplicaciones remotas.
	 */
	@Schedule(info="Ejecuta la comprobación de los servicios web de las aplicaciones remotas", year = "*", month = "*", dayOfMonth = "*", dayOfWeek = "*", hour = "*/3" ,persistent = false)
	public void checkGlobalWebServicesState() {
		try {
			log.info("Se inicia la comprobación de los servicios web de las aplicaciones remotas");
			remoteServicesMonitor.calculateGlobalState();
			log.info("Finaliza la comprobación de los servicios web de las aplicaciones remotas");
			
		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageSource.WEB_SERVICES_MONITOR, EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
		}
	}
	
	/**
	 * Sincroniza las solicitudes de la sede en Hermes
	 */
	@Schedule(info="sincroniza las solicitudes de la sede con las de Hermes", year = "*", month = "*", dayOfMonth = "*", dayOfWeek = "*", hour = "*/3" ,persistent = false)
	public void synchonizeHermes() {
		try {
			log.info("Se inicia la sincronización de las solicitudes con Hermes");
			hermesSolicitudSynchronizer.batchSynchronize();
			log.info("Finaliza la sincronización de las solicitudes con Hermes");
			
		} catch (SedeException e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageSource.SYNC_HERMES, EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
		}
	}

}
