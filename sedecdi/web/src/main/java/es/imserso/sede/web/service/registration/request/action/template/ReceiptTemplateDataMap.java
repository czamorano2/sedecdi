package es.imserso.sede.web.service.registration.request.action.template;

import java.util.Map;
import java.util.function.Function;

import es.imserso.sede.model.Solicitud;

/**
 * Devuelve el Map con los datos del resguardo a incrustar en el template
 * 
 * @author 11825775
 *
 */
public class ReceiptTemplateDataMap implements Function<Solicitud, Map<String, String>> {

	@Override
	public Map<String, String> apply(Solicitud solicitud) {
		return solicitud.extractReceiptHash();
	}
}
