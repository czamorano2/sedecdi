/**
 * 
 */
package es.imserso.sede.web.service.message.manager;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.EventMessageRepository;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.mail.MailManager;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.util.route.ParamValues;
import es.imserso.sede.web.util.route.ParamValues.Params;

/**
 * Gestor de mensajes de error de la Sede Electrónica.
 * <p>
 * Estos mensajes pueden ayudar a los planificadores a gestionar mejor la
 * aplicación.
 * <p>
 * Si los mensajes son de nivel ERROR o FATAL se envía un email a los
 * planificadores.
 * 
 * @author 11825775
 *
 */
@SessionScoped
public class ErrorMessageManager extends AbstractMessageManager implements Serializable {

	private static final long serialVersionUID = -4958849794708898082L;

	Logger log = Logger.getLogger(ErrorMessageManager.class);

	/**
	 * Datos del usuario obtenidos de la cookie
	 * <p>
	 * Si la petición no incluye la cookie <code>usuario</code> será null
	 */
	@Inject
	@ResourceQ
	Usuario usuario;

	/**
	 * Datos relevantes especificados en los parámetros
	 */
	@Inject
	ParamValues paramValues;

	@Inject
	EventMessageRepository eventMessageRepository;

	@Inject
	MailManager mailManager;

	@PostConstruct
	public void onCreate() {
		clearMessageList();

		MINIMUM_MESSAGE_LEVEL = EventMessageLevel.MESSAGE_LEVEL_INFO;
		MAX_MESSAGES = 100;
		MESSAGES_TO_DELETE_ON_MESSAGE_OVERFLOW = 1;
	}

	public void onMessage(@Observes EventMessage eventMessage) {
		log.debug("recibido evento EventMessage");

		if (paramValues != null) {

			eventMessage.setAut(paramValues.getParamValue(Params.aut).getValue());
			eventMessage.setLocale(paramValues.getParamValue(Params.locale).getValue());
			eventMessage.setRepresentante(paramValues.getParamValue(Params.representante).getValue());
			eventMessage.setSia(paramValues.getSia());
			eventMessage.setAction(paramValues.getTipoAccion());
		}

		if (usuario != null) {
			eventMessage.setUsuario(usuario.getNIF_CIF());
		}

		eventMessage.setSessionId(FacesContext.getCurrentInstance().getExternalContext().getSessionId(false));

		eventMessageRepository.persist(eventMessage);

		if (eventMessage.getLevel().getPriority() >= EventMessageLevel.MESSAGE_LEVEL_ERROR.getPriority()) {
			try {
				mailManager.sendNotificationToPlanificadores(eventMessage.getMessage());
			} catch (Exception e) {
				log.error("No se ha podido enviar el email de incidencia a los planificadores: "
						+ Utils.getExceptionMessage(e));
			}
		}
	}

	public List<EventMessage> getAll() {
		return eventMessageRepository.getAll();
	}

}
