package es.imserso.sede.web.service.registration.solicitud;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;

import es.imserso.sede.data.RegistroTramiteRepository;
import es.imserso.sede.model.RegistroTramite;
import es.imserso.sede.model.RegistroTramiteFase;
import es.imserso.sede.service.registration.event.RegistrationPhase;
import es.imserso.sede.service.registration.event.RegistrationPhaseEvent;
import es.imserso.sede.service.registration.event.RegistrationResult;
import es.imserso.sede.util.Utils;

/**
 * Gestiona los apuntes de la operación de registro del trámite para tener una traza de lo ocurrido durante la grabación de la solicitud.
 * 
 * 
 * @author 11825775
 *
 */
@SessionScoped
public class RegTramiteBean implements Serializable {

	private static final long serialVersionUID = -3362074710652404276L;

	Logger log = Logger.getLogger(RegTramiteBean.class);

	@Inject
	RegistroTramiteRepository repository;

	@Inject
	RegTramiteFactory regTramiteFactory;

	RegistroTramite entity;

	boolean persisted;

	@PostConstruct
	public void initialize() {
		
		// creamos la entidad donde se van a ir incorporando los apuntes
		entity = regTramiteFactory.buildEntity();

		persisted = false;
	}

	/**
	 * Captura de evento lanzado cuando se está ejecutando una de las fases del
	 * registro de una solicitud
	 * 
	 * @param phase
	 *            fase que se está ejecutando
	 */
	public void onRegistrationPhaseEvent(@Observes RegistrationPhaseEvent registrationPhaseEvent) {

		//
		addEvent(registrationPhaseEvent);

		if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_INIT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.INIT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_INIT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.VIEW_LOAD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.VIEW_SAVE)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_VALIDATION)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.VALIDATION)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_VALIDATION)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_PERSIST_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.PERSIST_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_PERSIST_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_PERSIST_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.PERSIST_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_PERSIST_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.GENERATE_RECEIPT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.SIGN_RECEIPT)) {

		} else if (registrationPhaseEvent.getRegistrationPhase()
				.equals(RegistrationPhase.BEFORE_PERSIST_RECEIPT_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase()
				.equals(RegistrationPhase.PERSIST_RECEIPT_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase()
				.equals(RegistrationPhase.AFTER_PERSIST_RECEIPT_ON_REGISTER)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.BEFORE_UPDATE_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.UPDATE_ON_BBDD)) {

		} else if (registrationPhaseEvent.getRegistrationPhase().equals(RegistrationPhase.AFTER_UPDATE_ON_BBDD)) {

		}
	}

	/**
	 * La operación de registro de la solicitud ha terminado, ya sea correcta o
	 * incorrectamente
	 * 
	 * @param result
	 *            resultado de la operación (OK o KO)
	 */
	public void onRegistrationResultEvent(@Observes RegistrationResult result) {

		try {
			if (result.getResult().equals(RegistrationResult.Result.RESULT_OK)) {
				endSuccessfully();
			}
			if (result.getResult().equals(RegistrationResult.Result.RESULT_KO)) {
				endUnsuccessfully(result.getData());
			}
		} catch (Exception e) {
			log.error("error al persistir los apuntes de la operación de registro: " + Utils.getExceptionMessage(e));
		}
	}

	/**
	 * Añade un apunte para persistir
	 * 
	 * @param phase
	 *            fase del evento
	 */
	public void addEvent(RegistrationPhaseEvent registrationPhaseEvent) {
		RegistroTramiteFase rtf = new RegistroTramiteFase();
		rtf.setFecha(new Date(System.currentTimeMillis()));
		rtf.setPhase(registrationPhaseEvent.getRegistrationPhase());
		if (StringUtils.isNotEmpty(registrationPhaseEvent.getEventData())) {
			rtf.setDatos(registrationPhaseEvent.getEventData());
		}
		rtf.setRegistroTramite(entity);
		entity.getRegistroTramiteFases().add(rtf);
	}

	/**
	 * Finaliza la grabación de apuntes habiéndose realizado el registro
	 * satisfactoriamente
	 * 
	 * @throws Exception
	 */
	public void endSuccessfully() throws Exception {
		addEvent(new RegistrationPhaseEvent(RegistrationPhase.TERMINATE_OK));
		persist();
	}

	/**
	 * Finaliza la grabación de apuntes tras terminar el registro de la solicitud de
	 * forma no satisfactoria
	 * 
	 * @param cause
	 *            (opcional) causa de la incidencia
	 * @throws Exception
	 */
	public void endUnsuccessfully(String cause) throws Exception {
		addEvent(new RegistrationPhaseEvent(RegistrationPhase.TERMINATE_KO, cause));
		persist();
	}

	/**
	 * persiste los apuntes en bbdd
	 * 
	 * @throws Exception
	 */
	private void persist() throws Exception {

		if (persisted) {
			log.warn("ya se han persistido los apuntes!!");
			return;
		}

		log.debug("persistiendo apuntes del registro...");
		try {
			entity.setFin(new Date(System.currentTimeMillis()));
			repository.persist(entity);
			log.info("apuntes del registro persistidos");

		} catch (Exception e) {
			log.error("Error al persistir los apuntes: " + Utils.getExceptionMessage(e));
			throw e;

		} finally {
			persisted = true;
		}
	}

	// @PreDestroy
	// public void preDestroy() {
	// log.debug("on preDestroy...");
	// }

	// @PreRemove
	// public void preRemove() {
	// log.debug("on preRemove...");
	//
	// }

	/**
	 * @return {@code true} si ya se ha persistido la entidad asociada, o bien
	 *         {@code false} si aún no se ha persistido.
	 */
	public boolean persisted() {
		return persisted;
	}


}
