package es.imserso.sede.web.service.registration.request.action.template;

import java.util.Map;
import java.util.function.Function;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;

/**
 * Devuelve el Map con los datos de la solicitud a incrustar en el template
 * 
 * @author 11825775
 *
 */
public class RequestTemplateDataMap implements Function<SolicitudDTOI, Map<String, String>> {

	@Override
	public Map<String, String> apply(SolicitudDTOI dto) {
		return dto.extractHashtable();
	}
}
