package es.imserso.sede.web.view.admin;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;

import org.omnifaces.config.WebXml;

/**
 * Proporciona información del descriptor web de la aplicación
 * 
 * @author 11825775
 *
 */
@Model
public class WebXmlBean {

	/**
	 * Obtiene la lista de páginas de bienvenida (<welcome-file-list>
	 */
	private List<String> welcomeFiles;
	/**
	 * Obtiene un mapeo de todas las vistas de error por excepción (si la key es
	 * null representa la vista de error por defecto, si la hay)
	 */
	private Map<Class<Throwable>, String> errorPageLocations;
	/**
	 * Vista de autenticación (<form-login-page>)
	 */
	private String formLoginPage;
	/**
	 * Mapeo de los patrones de todas la reglas de seguridad (<security-constraint>)
	 * y los roles asociados
	 */
	private Map<String, Set<String>> securityConstraints;
	/**
	 * Comprueba si está permitido el acceso a ciertas URLs para los roles de los
	 * patrones de seguridad
	 */
	private boolean accessAllowed;
	/**
	 * Timeout de la sesión
	 */
	private int sessionTimeout = WebXml.INSTANCE.getSessionTimeout();

	@PostConstruct
	public void onCreate() {
		welcomeFiles = WebXml.INSTANCE.getWelcomeFiles();
		setErrorPageLocations(WebXml.INSTANCE.getErrorPageLocations());
		formLoginPage = WebXml.INSTANCE.getFormLoginPage();
		securityConstraints = WebXml.INSTANCE.getSecurityConstraints();
		accessAllowed = WebXml.INSTANCE.isAccessAllowed("/admin.xhtml", "admin");
	}

	public List<String> getWelcomeFiles() {
		return welcomeFiles;
	}

	public void setWelcomeFiles(List<String> welcomeFiles) {
		this.welcomeFiles = welcomeFiles;
	}

	public String getFormLoginPage() {
		return formLoginPage;
	}

	public void setFormLoginPage(String formLoginPage) {
		this.formLoginPage = formLoginPage;
	}

	public Map<String, Set<String>> getSecurityConstraints() {
		return securityConstraints;
	}

	public void setSecurityConstraints(Map<String, Set<String>> securityConstraints) {
		this.securityConstraints = securityConstraints;
	}

	public boolean isAccessAllowed() {
		return accessAllowed;
	}

	public void setAccessAllowed(boolean accessAllowed) {
		this.accessAllowed = accessAllowed;
	}

	public int getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	public Map<Class<Throwable>, String> getErrorPageLocations() {
		return errorPageLocations;
	}

	public void setErrorPageLocations(Map<Class<Throwable>, String> errorPageLocations) {
		this.errorPageLocations = errorPageLocations;
	}

}
