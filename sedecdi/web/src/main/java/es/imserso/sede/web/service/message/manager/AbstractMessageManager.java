package es.imserso.sede.web.service.message.manager;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.service.message.event.EventMessageSource;
import es.imserso.sede.util.Utils;

public abstract class AbstractMessageManager {

	private static final String MSG_ERR_OLD_MESSAGES_DELETION = "error al eliminar los mensajes antiguos: {0}";

	private static final String MSG_EXCEPTION = "error al leer la lista de mensajes: {0}";

	private static final String TRACE_LIST_ELEMENTS = "la lista de mensajes tiene {0, number, integer} elementos";

	protected static EventMessageLevel MINIMUM_MESSAGE_LEVEL;
	protected static int MAX_MESSAGES;
	protected static int MESSAGES_TO_DELETE_ON_MESSAGE_OVERFLOW;

	private Logger log = Logger.getLogger(AbstractMessageManager.class);

	/**
	 * mensajes generales
	 */
	protected List<EventMessage> messages;

	/**
	 * constructor
	 */
	public AbstractMessageManager() {
		clearMessageList();

		MINIMUM_MESSAGE_LEVEL = EventMessageLevel.MESSAGE_LEVEL_INFO;
		MAX_MESSAGES = 100;
		MESSAGES_TO_DELETE_ON_MESSAGE_OVERFLOW = 1;

	}

	/**
	 * resetea la lista de mensajes
	 */
	public void clearMessageList() {
		messages = new ArrayList<EventMessage>(MAX_MESSAGES);
	}

	/**
	 * devuelve los mensajes de igual nivel que el especificado
	 * 
	 * @param level
	 *            indica qué mensajes se devolverán
	 * @return lista de mensajes
	 */
	public List<EventMessage> getMessagesOfLevel(int level) {

		List<EventMessage> list = new ArrayList<EventMessage>();

		try {
			for (EventMessage message : messages) {
				if (message.getLevel().getPriority() == level) {
					list.add(message);
				}
			}
		} catch (Exception e) {
			log.warnv(MSG_EXCEPTION, Utils.getExceptionMessage(e));
		}
		return list;

	}

	/**
	 * devuelve los mensajes de igual o mayor nivel del especificado
	 * 
	 * @param level
	 *            indica qué mensajes se devolverán
	 * @return lista de mensajes
	 */
	public List<EventMessage> getMessagesFromLevel(EventMessageLevel level) {

		List<EventMessage> list = new ArrayList<EventMessage>();

		try {
			log.tracev(TRACE_LIST_ELEMENTS, messages.size());
			for (EventMessage message : messages) {
				if (message.getLevel().getPriority() >= level.getPriority()) {
					list.add(message);
				}
			}
		} catch (Exception e) {
			log.warnv(MSG_EXCEPTION, Utils.getExceptionMessage(e));
		}
		return list;

	}

	/**
	 * devuelve los mensajes de igual o mayor nivel del por defecto
	 * 
	 * @return lista de mensajes
	 */
	public List<EventMessage> getMessagesFromDefaultLevel() {
		return getMessagesFromLevel(MINIMUM_MESSAGE_LEVEL);
	}

	/**
	 * devuelve todos los mensajes de la lista
	 * 
	 * @return lista de mensajes
	 */
	public List<EventMessage> getAllMessages() {
		return messages;
	}

	/**
	 * @param level
	 *            nivel del cual se quiere obtener el último mensaje
	 * @return el último mensaje del nivel especificado
	 */
	public EventMessage getLastMessageFromLevel(EventMessageLevel level) {
		List<EventMessage> list = getMessagesFromLevel(level);
		if (list == null || list.size() == 0) {
			return new EventMessage(EventMessageLevel.MESSAGE_LEVEL_TRACE, "");
		}
		return list.get(list.size() - 1);
	}

	/**
	 * devuelve el último mensaje del nivel por defecto
	 * 
	 * @return el último mensaje del nivel por defecto
	 */
	public EventMessage getLastMessageFromDefaultLevel() {
		return getLastMessageFromLevel(MINIMUM_MESSAGE_LEVEL);
	}

	/**
	 * @param processMessage
	 */
	public void addMessage(EventMessage message) {
		addMessage(message, true);
	}

	/**
	 * @param processMessage
	 */
	public void addMessage(EventMessage message, boolean logMessage) {
		controlMessages();
		messages.add(message);
		if (logMessage) {
			log.info(message.getMessage());
		} else {
			log.trace(message.getMessage());
		}
	}

	/**
	 * @param source
	 * @param level
	 * @param message
	 * @param description
	 * @param logMessage
	 */
	public void addMessage(EventMessageSource source, EventMessageLevel level, String message, String description,
			boolean logMessage) {
		controlMessages();
		if (level.getPriority() >= MINIMUM_MESSAGE_LEVEL.getPriority()) {
			messages.add(new EventMessage(source, level, message, description));
		}

		if (logMessage) {
			log.info(message);
		} else {
			log.trace(message);
		}
	}

	/**
	 * @param source
	 * @param level
	 * @param message
	 * @param description
	 */
	public void addMessage(EventMessageSource source, EventMessageLevel level, String message, String description) {
		controlMessages();
		if (level.getPriority() >= MINIMUM_MESSAGE_LEVEL.getPriority()) {
			messages.add(new EventMessage(source, level, message, description));
		}
		log.trace(message);
	}

	/**
	 * @param source
	 * @param level
	 * @param message
	 */
	public void addMessage(EventMessageSource source, EventMessageLevel level, String message) {
		controlMessages();
		if (level.getPriority() >= MINIMUM_MESSAGE_LEVEL.getPriority()) {
			messages.add(new EventMessage(source, level, message));
		}
		log.trace(message);
	}

	/**
	 * @param source
	 * @param level
	 * @param message
	 * @param logMessage
	 */
	public void addMessage(EventMessageSource source, EventMessageLevel level, String message, boolean logMessage) {
		controlMessages();
		if (level.getPriority() >= MINIMUM_MESSAGE_LEVEL.getPriority()) {
			messages.add(new EventMessage(source, level, message));
		}

		if (logMessage) {
			log.info(message);
		} else {
			log.trace(message);
		}
	}

	/**
	 * @param level
	 * @param message
	 */
	public void addMessage(EventMessageLevel level, String message) {
		controlMessages();
		if (level.getPriority() >= MINIMUM_MESSAGE_LEVEL.getPriority()) {
			messages.add(new EventMessage(level, message));
		}
		log.trace(message);
	}

	/**
	 * @param level
	 * @param message
	 * @param logMessage
	 */
	public void addMessage(EventMessageLevel level, String message, boolean logMessage) {
		controlMessages();
		if (level.getPriority() >= MINIMUM_MESSAGE_LEVEL.getPriority()) {
			messages.add(new EventMessage(level, message));
		}

		if (logMessage) {
			log.info(message);
		} else {
			log.trace(message);
		}
	}

	private void controlMessages() {
		if (messages.size() >= (MAX_MESSAGES - 1)) {
			deleteOldMessages();
		}
	}

	private void deleteOldMessages() {
		try {
			messages = messages.subList(MESSAGES_TO_DELETE_ON_MESSAGE_OVERFLOW, messages.size());
		} catch (Exception e) {
			log.warnv(MSG_ERR_OLD_MESSAGES_DELETION, Utils.getExceptionMessage(e));
		}
	}

}
