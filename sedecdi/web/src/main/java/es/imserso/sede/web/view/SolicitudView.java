package es.imserso.sede.web.view;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;

public interface SolicitudView {

	/**
	 * @return si se muestran o no los campos del representate
	 */
	boolean isShowRepresentante();

	/**
	 * @return si se deshabilitan los campos de nombre, apellidos y NIF del
	 *         representante (normalmente porque son los de la cookie <b>usuario</b>
	 *         y no se pueden modificar)
	 */
	boolean isDisabledRepresentanteMainFields();

	/**
	 * @return si se deshabilitan los campos de nombre, apellidos y NIF del
	 *         solicitante (normalmente porque son los de la cookie <b>usuario</b> y
	 *         no se pueden modificar)
	 */
	boolean isDisabledSolicitanteMainFields();

	/**
	 * Informa que se ha cargado la vista de la solicitud
	 */
	void init();

	/**
	 * Cancela el alta de la solicitud
	 */
	String cancel();

	/**
	 * @return Datos de la solicitud
	 */
	SolicitudDTOI getDto();

}