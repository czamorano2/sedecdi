package es.imserso.sede.web.view;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.omnifaces.util.Messages;

import es.imserso.sede.data.dto.TramiteUCM;
import es.imserso.sede.util.Global;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.rest.client.ucm.UCMrestClient;

@Named
@SessionScoped
public class TestView implements Serializable {

	private static final long serialVersionUID = -2895082527674206481L;

	Logger log = Logger.getLogger(TestView.class);

	private String firstName;

	@Inject
	UCMrestClient client;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String save() {
		log.info("saved!");
		return null;
	}

	public void getTramiteFromUCM() {
		try {
			log.warn("prueba de decorador de logs (WARN)");
			log.error("prueba de decorador de logs (ERROR)");
			log.fatal("prueba de decorador de logs (FATAL)");
			TramiteUCM tramite = client.getDatosTramite(Global.CODIGO_SIA_TURISMO);
			log.info(tramite.toString());

		} catch (SedeException e) {
			Messages.addError(null, "Error al obtener el trámite de turismo del UCM: " + Utils.getExceptionMessage(e));
		}
	}

}
