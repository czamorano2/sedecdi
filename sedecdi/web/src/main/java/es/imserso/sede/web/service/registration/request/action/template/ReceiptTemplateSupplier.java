package es.imserso.sede.web.service.registration.request.action.template;

import java.util.function.Supplier;

import javax.inject.Inject;

import es.imserso.sede.data.TramiteRepository;
import es.imserso.sede.web.util.ByteArrayWrapper;
import es.imserso.sede.web.util.route.ParamValues;

/**
 * Obtiene una plantilla de resguardo de la base de datos en función del código SIA de lla
 * petición
 * 
 * @author 11825775
 *
 */
public class ReceiptTemplateSupplier  implements Supplier<ByteArrayWrapper> {

	@Inject
	ParamValues params;

	@Inject
	TramiteRepository tramiteRepository;

	@Override
	public ByteArrayWrapper get() {
		return new ByteArrayWrapper(tramiteRepository.findJustificantePdfBySIA(params.getSia()).getValor());
	}

}
