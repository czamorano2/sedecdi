package es.imserso.sede.web.service.registration.solicitud.impl.turismo;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.impl.turismo.TurismoDTO;
import es.imserso.sede.util.exception.ValidationException;
import es.imserso.sede.web.service.registration.request.action.validation.TurismoDtoValidator;
import es.imserso.sede.web.service.registration.solicitud.impl.SolicitudRegistration;

/**
 * Registra en Hermes una solicitud de turismo recogida del frontal.
 * 
 * @author 11825775
 *
 */
@TurismoSolicitudRegistrationQ
public class TurismoSolicitudRegistration extends SolicitudRegistration {

	private static final long serialVersionUID = -8384911844749976241L;

	@Inject
	Logger log;

	@Inject
	TurismoDtoValidator validator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.solicitud.impl.SolicitudRegistration
	 * #validateDTO()
	 */
	@Override
	protected void validate() throws ValidationException {
		super.validate();
		
		log.debug("validando DTO de turismo...");
		validator.accept((TurismoDTO) dtoInstance);
	}

}
