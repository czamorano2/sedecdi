package es.imserso.sede.web.service.registration.solicitud;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.model.RegistroTramite;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.util.route.ParamValues;

/**
 * Provee de entidades para el registro del trámite, inicializadas
 * convenientemente.
 * 
 * @author 11825775
 *
 */
@Dependent
public class RegTramiteFactory implements Serializable {

	private static final long serialVersionUID = -1955531093651461417L;

	/**
	 * Datos del usuario obtenidos de la cookie
	 * <p>
	 * Si la petición no incluye la cookie <code>usuario</code> será null
	 */
	@Inject
	@ResourceQ
	Usuario usuario;

	/**
	 * Datos relevantes especificados en los parámetros
	 */
	@Inject
	ParamValues paramValues;

	/**
	 * @return entidad RegistroTramite inicializada convenientemenmte
	 */
	public RegistroTramite buildEntity() {
		RegistroTramite rt = new RegistroTramite();

		rt.setAccion(paramValues.getTipoAccion());
		rt.setInicio(new Date(System.currentTimeMillis()));
		rt.setSia(paramValues.getSia());
		rt.setUsuario(usuario.getNIF_CIF());
		rt.setJsessionId(FacesContext.getCurrentInstance().getExternalContext().getSessionId(false));
		return rt;
	}

}
