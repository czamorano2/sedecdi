package es.imserso.sede.web.service.registration.solicitud;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.web.util.route.ParamValues.Params;

@Dependent
class DtoTurismoFactory extends DtoFactory {

	private static final long serialVersionUID = -1724574087978368534L;

	@Inject
	TurismoRepository turismoRepository;

	/**
	 * Crea una instancia del Dto de edición de solicitud de turismo inicializada
	 * con los datos del certificado del usuario y los datos de la temporada, turno
	 * y plazo
	 * 
	 * @param solicitudId
	 *            identificador de la solicitud en Hermes
	 * @return DTO con los datos de la solicitud obtenidos de Hermes
	 * @throws SedeException
	 */
	public SolicitudTurismoDTO createEditDtoInstance(Long solicitudId) throws SedeException {
		SolicitudTurismoDTO dto = new SolicitudTurismoDTO() {
			private static final long serialVersionUID = 1L;
		};

		dto = turismoRepository.getRemoteSolicitudById(solicitudId);

		dto.setCodigoSIA(paramValues.getParamValue(Params.sia).getValue());

		return dto;
	}

}
