package es.imserso.sede.web.model.validator;

import javax.validation.constraints.NotNull;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.util.exception.ValidationException;

/**
 * Interface para validar un dto de solicitud
 * 
 * @author 11825775
 *
 */
public interface DtoValidatorI {

	/**
	 * Realiza las validaciones oportunas para asegurar que los datos del DTO son
	 * correctos
	 * <p>
	 * Siempre se ejecutará antes de ejecutar las fases del registro
	 * 
	 * @throws ValidationException
	 *             si la solicitud no es válida
	 */
	void validate(@NotNull SolicitudDTOI dto) throws ValidationException;

}
