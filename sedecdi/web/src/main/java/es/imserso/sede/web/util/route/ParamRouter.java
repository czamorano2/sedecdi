package es.imserso.sede.web.util.route;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.service.ucm.UCMService;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeRuntimeException;
import es.imserso.sede.util.resources.qualifier.ResourceQ;

/**
 * Redirige la petición en función de sus parámetros
 * <p>
 * Es una especie de route-params de AngularJS
 * 
 * @author 11825775
 *
 */
@Model
public class ParamRouter {

	@Inject
	Logger log;

	@Inject
	ParamValues paramValues;

	@Inject
	@ResourceQ
	Usuario usuario;

	@Inject
	UCMService ucmService;

	private String viewMessage = "vista de entrutamiento de peticiones";

	@PostConstruct
	public void onCreate() {
		log.debug("routing...");
		route();
	}

	/**
	 * Redirige a una vista determinada por los parámetros de la petición
	 * 
	 * @return outcome
	 */
	public String route() {

		// se comenta porque esta validación la hace el interceptor con el
		// binding @Secure
		// TramiteUCM tramiteUCM =
		// ucmService.getTramiteUCM(paramValues.getParamValue(Params.sia).getValue());
		// if (tramiteUCM.isExigeautenticacionclave() && usuario == null) {
		// String errmsg = "NECESITA AUTENTICARSE PARA ACCEDER A ESTE
		// RECURSO!!";
		// log.warn(errmsg);
		// FacesContext.getCurrentInstance().addMessage(null, new
		// FacesMessage(errmsg));
		// return null;
		// }

		String outcome = null;

		if (paramValues.isConsulta()) {
			if (paramValues.isTurismo()) {
				// consulta de todas las solicitudes del usuario de turismo
				// (existentes en Hermes)
				outcome = "/app/tramite/turismo/list.xhtml";
			} else if (paramValues.isTermalismo()) {
				// consulta de todas las solicitudes del usuario de termalismo
				// (exitentes en Termalsmo)
				outcome = "/app/tramite/termalismo/list.xhtml";
			} else {
				// consulta de las solicitudes del usuario dadas de alta desde
				// la sede electrónica
				outcome = "/app/consulta/list.xhtml";
			}
		} else if (paramValues.isEdicion()) {
			if (paramValues.isTurismo()) {
				// consulta de todas las solicitudes del usuario de turismo
				// (existentes en Hermes)
				outcome = "/app/tramite/turismo/list.xhtml";
			} else if (paramValues.isTermalismo()) {
				// consulta de todas las solicitudes del usuario de termalismo
				// (exitentes en Termalsmo)
				outcome = "/app/tramite/termalismo/list.xhtml";
			} else {
				throw new SedeRuntimeException("Trámite desconocido para edición");
			}
		} else {
			if (paramValues.isAlta()) {
				if (paramValues.isTurismo()) {
					outcome = "/app/tramite/turismo/create.jsf";
				} else if (paramValues.isTermalismo()) {
					outcome = "/app/tramite/termalismo/create.jsf";
				} else if (paramValues.isPropositoGeneral()) {
					outcome = "/app/tramite/proposito-general/create.jsf";
				} else {
					outcome = "/app/tramite/generico/create.jsf";
				}
			}
		}

		log.info("route: outcome => " + outcome);

		if (outcome == null) {
			outcome = "/error?errorMessage=router no configurado para esta URI";
		} else {
			outcome = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
					.getContextPath() + outcome + "?" + buildParametersUrl();
		}

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(outcome);

		} catch (IOException e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.warn(errmsg);
			outcome = "error?errorMessage=" + errmsg;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(errmsg));
			return null;
		}
		return outcome;
	}

	private String buildParametersUrl() {
		Map<String, String> requestParameterMap = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		StringBuilder parametersUrl = new StringBuilder();
		Iterator<?> it = requestParameterMap.keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			parametersUrl.append(key + "=" + requestParameterMap.get(key) + "&");
		}
		return parametersUrl.toString().substring(0, parametersUrl.toString().length() - 1);
	}

	public String getViewMessage() {
		return viewMessage;
	}

	public void setViewMessage(String viewMessage) {
		this.viewMessage = viewMessage;
	}

}
