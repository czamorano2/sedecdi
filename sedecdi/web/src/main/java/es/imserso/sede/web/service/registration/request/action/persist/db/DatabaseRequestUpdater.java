package es.imserso.sede.web.service.registration.request.action.persist.db;

import java.util.function.Consumer;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.model.Solicitud;

/**
 * Consumer que persiste el DTO de la solicitud en la base de datos
 * 
 * @author 11825775
 *
 */
public class DatabaseRequestUpdater implements Consumer<Solicitud> {
	
	private static final Logger log = Logger.getLogger(DatabaseRequestUpdater.class.getName());
	
	@Inject
	SolicitudRepository solicitudRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.function.Consumer#accept(java.lang.Object)
	 */
	@Override
	public void accept(Solicitud solicitud) {
	
		log.debug("actualizando solicitud...");

		solicitudRepository.update(solicitud);

	}

}
