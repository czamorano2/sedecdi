package es.imserso.sede.web.util.resources;

import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.enterprise.inject.Produces;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.util.resources.qualifier.ReceiptQ;
import es.imserso.sede.util.resources.qualifier.RequestQ;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.service.registration.request.action.persist.PersistedRequestDTOI;
import es.imserso.sede.web.service.registration.request.action.persist.db.DatabaseRequestPersister;
import es.imserso.sede.web.service.registration.request.action.persist.db.DatabaseRequestUpdater;
import es.imserso.sede.web.service.registration.request.action.persist.registry.InputRegistryData;
import es.imserso.sede.web.service.registration.request.action.persist.registry.InveSicresRequestPersister;
import es.imserso.sede.web.service.registration.request.action.persist.registry.RegistryPersister;
import es.imserso.sede.web.service.registration.request.action.template.ReceiptTemplateDataMap;
import es.imserso.sede.web.service.registration.request.action.template.ReceiptTemplateSupplier;
import es.imserso.sede.web.service.registration.request.action.template.RequestTemplateDataMap;
import es.imserso.sede.web.service.registration.request.action.template.RequestTemplateSupplier;
import es.imserso.sede.web.service.registration.request.action.template.pdf.ReceiptPdfGenerator;
import es.imserso.sede.web.service.registration.request.action.template.pdf.RequestPdfGenerator;
import es.imserso.sede.web.service.registration.request.action.validation.GenericoDtoValidator;
import es.imserso.sede.web.service.registration.request.action.validation.PropositoGeneralDtoValidator;
import es.imserso.sede.web.service.registration.request.action.validation.TermalismoDtoValidator;
import es.imserso.sede.web.service.registration.request.action.validation.TurismoDtoValidator;
import es.imserso.sede.web.util.ByteArrayWrapper;
import es.imserso.sede.web.util.route.ParamValues;

public class Resources {

	@ResourceQ
	@SuppressWarnings("unchecked")
	@Produces
	public Consumer<SolicitudDTOI> getRequestValidator(ParamValues paramValues) {
		if (paramValues.isTurismo()) {
			return (Consumer<SolicitudDTOI>) UtilsCDI.getBeanByReference(TurismoDtoValidator.class);

		} else if (paramValues.isTermalismo()) {
			return (Consumer<SolicitudDTOI>) UtilsCDI.getBeanByReference(TermalismoDtoValidator.class);

		} else if (paramValues.isPropositoGeneral()) {
			return (Consumer<SolicitudDTOI>) UtilsCDI.getBeanByReference(PropositoGeneralDtoValidator.class);

		} else {
			return (Consumer<SolicitudDTOI>) UtilsCDI.getBeanByReference(GenericoDtoValidator.class);

		}
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<SolicitudDTOI, Solicitud> getDatabasePersister() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<SolicitudDTOI, Solicitud>) UtilsCDI.getBeanByReference(DatabaseRequestPersister.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Consumer<Solicitud> getDatabaseUpdater() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Consumer<Solicitud>) UtilsCDI.getBeanByReference(DatabaseRequestUpdater.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@RequestQ
	@Produces
	public Function<PersistedRequestDTOI, ByteArrayWrapper> getRequestPdfGenerator() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<PersistedRequestDTOI, ByteArrayWrapper>) UtilsCDI
				.getBeanByReference(RequestPdfGenerator.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@ReceiptQ
	@Produces
	public Function<PersistedRequestDTOI, ByteArrayWrapper> getReceiptPdfGenerator() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<PersistedRequestDTOI, ByteArrayWrapper>) UtilsCDI
				.getBeanByReference(ReceiptPdfGenerator.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<PersistedRequestDTOI, IdentificadorRegistroDTO> getRegistryPersister() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<PersistedRequestDTOI, IdentificadorRegistroDTO>) UtilsCDI
				.getBeanByReference(RegistryPersister.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Supplier<ByteArrayWrapper> getRequestTemplateSupplier() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Supplier<ByteArrayWrapper>) UtilsCDI.getBeanByReference(RequestTemplateSupplier.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<SolicitudDTOI, Map<String, String>> getRequestTemplateDataMap() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<SolicitudDTOI, Map<String, String>>) UtilsCDI.getBeanByReference(RequestTemplateDataMap.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Supplier<ByteArrayWrapper> getReceiptTemplateSupplier() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Supplier<ByteArrayWrapper>) UtilsCDI.getBeanByReference(ReceiptTemplateSupplier.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<Solicitud, Map<String, String>> getReceiptTemplateDataMap() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<Solicitud, Map<String, String>>) UtilsCDI.getBeanByReference(ReceiptTemplateDataMap.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<InputRegistryData, IdentificadorRegistroDTO> getRequestRegistryPersister() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<InputRegistryData, IdentificadorRegistroDTO>) UtilsCDI
				.getBeanByReference(InveSicresRequestPersister.class);
	}

}
