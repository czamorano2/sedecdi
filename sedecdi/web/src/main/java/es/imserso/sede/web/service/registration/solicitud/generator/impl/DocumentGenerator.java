/**
 * 
 */
package es.imserso.sede.web.service.registration.solicitud.generator.impl;

import java.util.ArrayList;
import java.util.List;

import es.imserso.sede.web.service.registration.solicitud.generator.AbstractDocumentGenerator;
import es.imserso.sede.web.service.registration.solicitud.generator.ListComparable;

/**
 * @author 11825775
 *
 */
public class DocumentGenerator extends AbstractDocumentGenerator {

	private List<ListComparable> sourceList = new ArrayList<ListComparable>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.generator.
	 * AbstractDocumentGenerator#collectSource()
	 */
	@Override
	public void collectSource() {
		sourceList.add(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.generator.
	 * AbstractDocumentGenerator#validateSource()
	 */
	@Override
	public void validateSource() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.solicitud.generator.
	 * AbstractDocumentGenerator#generateTarget()
	 */
	@Override
	public void generateTarget() {
		// TODO Auto-generated method stub

	}

	public List<ListComparable> getSourceList() {
		return sourceList;
	}

	public void setSourceList(List<ListComparable> sourceList) {
		this.sourceList = sourceList;
	}

	public void addSource(ListComparable source) {
		this.sourceList.add(source);
	}

}
