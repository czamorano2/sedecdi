package es.imserso.sede.web.service.registration.request.action.validation;

import java.util.function.Consumer;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.Configuration;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.jboss.logging.Logger;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.service.ucm.UCMService;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.util.route.ParamValues;

public class DtoValidator implements Consumer<SolicitudDTOI> {

	private static final Logger log = Logger.getLogger(DtoValidator.class.getName());

	@Inject
	UCMService UCMservice;

	@Inject
	@ResourceQ
	Usuario usuario;

	@Inject
	ParamValues paramValues;

	@Inject
	PropertyComponent propertyComponent;

	/**
	 * Indica si se ha realizado ya la validación
	 */
	Boolean validated;
	/**
	 * Indica si ha pasado correctamente las validaciones
	 */
	Boolean validatedOK;
	/**
	 * Indica si ha pasado correctamente las validaciones
	 */
	Boolean validatedKO;

	Validator validator;

	@PostConstruct
	public void onCreate() {
		validated = Boolean.FALSE;
		validatedOK = Boolean.FALSE;
		validatedKO = Boolean.FALSE;
		
		log.debug("post construct...");

		Configuration<?> config = Validation.byDefaultProvider().configure();
		ValidatorFactory factory = config.buildValidatorFactory();
		validator = factory.getValidator();
		factory.close();
	}

	public void accept(es.imserso.sede.data.dto.solicitud.SolicitudDTOI dto) {
		// TODO Auto-generated method stub
		
	}

}
