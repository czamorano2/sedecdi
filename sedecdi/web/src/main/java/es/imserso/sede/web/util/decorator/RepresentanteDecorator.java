package es.imserso.sede.web.util.decorator;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.data.dto.persona.RepresentanteDTOI;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.util.route.ParamValues;

@Decorator
public abstract class RepresentanteDecorator implements RepresentanteDTOI, Serializable {

	private static final long serialVersionUID = 1098497445303746629L;

	private Logger log = Logger.getLogger(RepresentanteDecorator.class);

	@Inject
	@Delegate
	@Any
	RepresentanteDTOI representante;

	/**
	 * Datos del usuario obtenidos de la cookie
	 * <p>
	 * Si la petición no incluye la cookie <code>usuario</code> será null
	 */
	@Inject
	@ResourceQ
	Usuario usuario;

	/**
	 * Datos relevantes especificados en los parámetros
	 */
	@Inject
	ParamValues paramValues;

	boolean decorate;

	@PostConstruct
	public void onCreate() {
		decorate = (usuario != null && paramValues.isRepresentante());
		if (decorate) {
			log.info("decoramos el DTO de representante");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.PersonaDTOI#getNombre()
	 */
	@Override
	public String getNombre() {
		String value = null;
		if (decorate) {
			value = usuario.getNombre();
		} else {
			value = representante.getNombre();
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.PersonaDTOI#getApellido1()
	 */
	@Override
	public String getApellido1() {
		String value = null;
		if (decorate) {
			value = usuario.getApellido1();
		} else {
			value = representante.getApellido1();
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.PersonaDTOI#getApellido2()
	 */
	@Override
	public String getApellido2() {
		String value = null;
		if (decorate) {
			value = usuario.getApellido2();
		} else {
			value = representante.getApellido2();
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.data.dto.PersonaDTOI#getDocumentoIdentificacion()
	 */
	@Override
	public String getDocumentoIdentificacion() {
		String value = null;
		if (decorate) {
			value = usuario.getNIF_CIF();
		} else {
			value = representante.getDocumentoIdentificacion();
		}
		return value;
	}

}