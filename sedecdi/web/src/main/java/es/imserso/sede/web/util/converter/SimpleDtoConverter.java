package es.imserso.sede.web.util.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.SimpleDTO;

@FacesConverter("simpleDtoConverter")
public class SimpleDtoConverter implements Converter {
	
	Logger log = Logger.getLogger(SimpleDtoConverter.class);

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		String[] strArr = value.split("-");
		if (strArr.length == 2) {
			SimpleDTO simpleDTO = new SimpleDTO(Long.parseLong(strArr[1]), strArr[0]);
			return simpleDTO;
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			if (value instanceof SimpleDTO) {
				SimpleDTO simpleDTO = (SimpleDTO) value;
				return new StringBuffer(simpleDTO.getDescripcion()).append("-").append(simpleDTO.getId()).toString();
			} else {
				log.debug("Unexpected SimpleDTO: "+value.getClass().getName() + "-" + value.toString());
			}
			
		}
		return null;
	}

}
