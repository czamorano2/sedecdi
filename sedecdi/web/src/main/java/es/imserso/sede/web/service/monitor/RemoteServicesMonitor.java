package es.imserso.sede.web.service.monitor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

import es.imserso.sede.service.monitor.AppInfo;
import es.imserso.sede.service.monitor.ServiceInfo;
import es.imserso.sede.service.monitor.firma.FirmaQ;
import es.imserso.sede.service.monitor.isicres.ISicresQ;
import es.imserso.sede.service.monitor.termalismo.TermalismoQ;
import es.imserso.sede.service.monitor.turismo.TurismoQ;
import es.imserso.sede.util.resources.qualifier.UCMDataSourceQ;

/**
 * Monitoriza el estado de los diferentes servicios remotos que proveen a la
 * sede Electrónoca.
 * 
 * @author 11825775
 *
 */
@Named
@ApplicationScoped
public class RemoteServicesMonitor {

	@Inject
	Logger log;

	@Inject
	@TurismoQ
	Instance<AppInfo> hermesAppInfoInstance;

	@Inject
	@UCMDataSourceQ
	Instance<AppInfo> ucmAppInfoInstance;

	@Inject
	@ISicresQ
	Instance<AppInfo> iSicresAppInfoInstance;

	@Inject
	@TermalismoQ
	Instance<AppInfo> termalismoAppInfoInstance;

	@Inject
	@FirmaQ
	Instance<AppInfo> firmaAppInfoInstance;

	/**
	 * Aplicaciones remotas monitorizadas
	 */
	List<AppInfo> remoteApplicationList;

	/**
	 * Indica si el estado global de las aplicaciones es correcto
	 */
	private Boolean globalStateOk = Boolean.FALSE;

	@Inject
	@Push
	private PushContext remoteServicesChannel;

	public void sendMessage() {
		String msg = "stateChanged";
		remoteServicesChannel.send(msg);
	}

	@PostConstruct
	public void onCreate() {
		loadApps();
	}

	public void loadApps() {
		log.debug("cargando lista de aplicaciones remotas...");
		remoteApplicationList = new ArrayList<AppInfo>();
		remoteApplicationList.add(hermesAppInfoInstance.get());
		remoteApplicationList.add(ucmAppInfoInstance.get());
		remoteApplicationList.add(iSicresAppInfoInstance.get());
		remoteApplicationList.add(termalismoAppInfoInstance.get());
		remoteApplicationList.add(firmaAppInfoInstance.get());

		log.debugv("lista de aplicaciones remotas cargada con {0} elementos", remoteApplicationList.size());
	}

	public void calculateGlobalState() {
		// refrescamos los componentes
		loadApps();

		Boolean lastGlobalStateOk = this.globalStateOk;

		// calculamos el estado global de los servicios web remotos
		this.globalStateOk = remoteApplicationList.stream().allMatch(app -> app.webServicesActive());

		if (!lastGlobalStateOk.equals(this.globalStateOk)) {
			String msg = String.format(
					"ACHTUNG!! Ha cambiado el estado global de los servicios web de las aplicaciones de %s a %s",
					lastGlobalStateOk, this.globalStateOk);
			log.info(msg);
			sendMessage();
		}
	}

	/**
	 * @return Aplicaciones remotas monitorizadas
	 */
	public List<AppInfo> getRemoteApplicationList() {
		return remoteApplicationList;
	}

	/**
	 * @param app
	 *            Aplicación que contiene los servicios
	 * @return servicios de la aplicación especificada
	 */
	public List<ServiceInfo> getAppServices(AppInfo app) {
		return app.getServices();
	}

	public Boolean getGlobalStateOk() {
		return globalStateOk;
	}

	public void setGlobalStateOk(Boolean globalStateOk) {
		this.globalStateOk = globalStateOk;
	}

}
