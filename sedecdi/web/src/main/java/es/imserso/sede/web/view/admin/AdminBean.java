package es.imserso.sede.web.view.admin;

import static org.omnifaces.util.Faces.getContext;
import static org.omnifaces.util.Messages.addGlobalInfo;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import org.omnifaces.component.output.cache.CacheFactory;

/**
 * Bean de respaldo de la vista de administración del frontal de la Sede
 * Electrónica
 * 
 * @author 11825775
 *
 */
@Model
public class AdminBean {

	@Inject
	@Push
	private PushContext remoteServicesChannel;

	public void sendMessage() {
		String msg = "prueba de Web Sockets!";
		remoteServicesChannel.send(msg);
	}

	/**
	 * Borra el contenido de las caches de los datos auxiliares obtenidos de Hermes
	 * (provincias, estados civiles, opciones, etc)
	 */
	public void resetTurismoCaches() {
		addGlobalInfo(
				"se van a eliminar las caches de datos auxiliares de Hermes (provincias, estados civiles, destinos, etc)");
		CacheFactory.getCache(getContext(), "application").remove("firstCache");
		addGlobalInfo("eliminadas caches de datos auxiliares de Hermes!");
	}

}
