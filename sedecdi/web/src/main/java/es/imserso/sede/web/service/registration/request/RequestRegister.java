/**
 * 
 */
package es.imserso.sede.web.service.registration.request;

import java.util.function.Consumer;
import java.util.function.Function;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.resources.qualifier.ReceiptQ;
import es.imserso.sede.util.resources.qualifier.RequestQ;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.service.registration.request.action.persist.PersistedRequestDTO;
import es.imserso.sede.web.service.registration.request.action.persist.PersistedRequestDTOI;
import es.imserso.sede.web.util.ByteArrayWrapper;

/**
 * Registra una solicitud de un trámite.
 * 
 * @author 11825775
 *
 */
public class RequestRegister implements RequestRegisterI {

	private static Logger log = Logger.getLogger(RequestRegister.class.getName());

	 @Inject
	 @ResourceQ
	 Consumer<SolicitudDTOI> requestValidator;

	 @Inject
	 @ResourceQ
	 Function<SolicitudDTOI, Solicitud> databasePersister;

	 @Inject
	 @ResourceQ
	 @RequestQ
	 Function<PersistedRequestDTOI, ByteArrayWrapper> requestPdfGenerator;

	 @Inject
	 @ResourceQ
	 Function<PersistedRequestDTOI, IdentificadorRegistroDTO> registryPersister;

	 @Inject
	 @ResourceQ
	 @ReceiptQ
	 Function<PersistedRequestDTOI, ByteArrayWrapper> receiptPdfGenerator;

	 @Inject
	 @ResourceQ
	 Consumer<Solicitud> databaseUpdater;

//	@Inject
//	DtoValidator requestValidator;
//
//	@Inject
//	RequestPdfGenerator requestPdfGenerator;
//
//	@Inject
//	ReceiptPdfGenerator receiptPdfGenerator;
//
//	@Inject
//	RegistryPersister registryPersister;
//
//	@Inject
//	DatabaseRequestPersister databasePersister;
//
//	@Inject
//	DatabaseRequestUpdater databaseUpdater;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.web.service.registration.request.RequestRegister#register()
	 */
	@Override
	public void register(SolicitudDTOI dto) throws SedeException {

		log.info("registering request ...");

		PersistedRequestDTOI persistedDTO = new PersistedRequestDTO();

		requestValidator.accept(dto);

		persistedDTO.setRequestDatabaseSolicitud(databasePersister.apply(dto));

		persistedDTO.setRequestPdf(requestPdfGenerator.apply(persistedDTO).getBytes());

		persistedDTO.getRequestDatabaseSolicitud().setIdentificadorRegistroDTO(registryPersister.apply(persistedDTO));

		persistedDTO.setReceiptPdf(receiptPdfGenerator.apply(persistedDTO).getBytes());

		databaseUpdater.accept(persistedDTO.getRequestDatabaseSolicitud());

	}

}
