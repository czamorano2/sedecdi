package es.imserso.sede.web.view.test;

public class User {

	private String firstname;
	private String lastname;

	public User() {

	}

	public User(String firstName, String lastname) {
		this.setFirstname(firstName);
		this.setLastname(lastname);
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}



}
