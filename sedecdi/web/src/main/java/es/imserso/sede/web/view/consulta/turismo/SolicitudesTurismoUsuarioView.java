package es.imserso.sede.web.view.consulta.turismo;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import es.imserso.hermes.session.webservice.dto.SimpleDTOI;
import es.imserso.hermes.session.webservice.dto.SolicitudTurismoDTO;
import es.imserso.sede.data.TurismoRepository;
import es.imserso.sede.data.dto.Usuario;
import es.imserso.sede.model.EventMessage;
import es.imserso.sede.service.message.event.EventMessageLevel;
import es.imserso.sede.util.Global;
import es.imserso.sede.util.Utils;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.resources.qualifier.ResourceQ;
import es.imserso.sede.web.auth.secure.Secure;
import es.imserso.sede.web.service.registration.solicitud.DtoFactory;
import es.imserso.sede.web.service.registration.solicitud.DtoFactoryQ;
import es.imserso.sede.web.util.route.ParamValues;

/**
 * Muestra las solicitudes del usuario de la temporada actual dadas de alta en
 * Hermes
 * 
 * @author 11825775
 *
 */
@Named("solicitudesTurismoUsuarioView")
@ViewScoped
@Secure
public class SolicitudesTurismoUsuarioView implements Serializable {

	private static final long serialVersionUID = 4669574966404541810L;

	@Inject
	Logger log;

	@Inject
	@ResourceQ
	private Usuario usuario;

	@Inject
	private ParamValues paramValues;

	@Inject
	TurismoRepository turismoRepository;

	@Inject
	@DtoFactoryQ
	DtoFactory dtoFactory;

	@Inject
	Event<EventMessage> eventMessage;

	private SolicitudTurismoDTO selectedSolicitud;
	
	/**
	 * indica si el modo es consulta o edición
	 */
	private boolean modoConsulta;

	private StreamedContent pdfCartaAcreditacion;
	private boolean habilitadaDescargaCartaAcreditacion;

	/**
	 * lista de solicitudes del usuario
	 */
	private List<SolicitudTurismoDTO> list;

	private List<SimpleDTOI> provinciaList;
	private List<SimpleDTOI> estadosCiviles;
	private List<SimpleDTOI> opciones;
	private List<SimpleDTOI> sexos;

	@PostConstruct
	public void onCreate() {
		log.debug("PostConstruct...");
		loadLists();
		modoConsulta= paramValues.isConsulta();

	}

	/**
	 * carga la lista de solicitudes de turismo del usuario
	 */
	public void loadLists() {
		try {
			log.debugv("se van a obtener de Hermes las solicitudes del DI {0} ...", usuario.getNIF_CIF());
			list = turismoRepository.getRemoteSolicitudesByDI(usuario.getNIF_CIF());

			if (list == null) {
				String errmsg = "No se han podido obtener las solicitudes de turismo del documento  "
						+ usuario.getNIF_CIF();
				log.warn(errmsg);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Solicitudes de Turismo", errmsg));
			} else {
				log.infov("se han obtenido de Hermes {0} solicitudes del DI {1}", list.size(), usuario.getNIF_CIF());
			}

			setProvinciaList(turismoRepository.getProvincias());
			setEstadosCiviles(turismoRepository.getEstadosCiviles());
			setOpciones(turismoRepository.getOpciones());
			setSexos(turismoRepository.getSexos());

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Solicitudes de Turismo", errmsg));
		}
	}

	/**
	 * Actualiza la solicitud con las modificaciones realizadas por el usuario
	 */
	public void save() {
		log.info("actualizando solicitud en Hermes...");

		try {
			validate(selectedSolicitud);
			turismoRepository.updateRemoteSolicitud(selectedSolicitud);

			log.infov("solicitud {0} sido modificada satisfactoriamente", selectedSolicitud.getId());
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Modificación de expedientes de Turismo", "Su solicitud ha sido modificada satisfactoriamente");
			RequestContext.getCurrentInstance().showMessageInDialog(message);

			// TODO ver si hay que cerrar el formulario o si lo cierra el usuario cuando
			// termine de actualizar el expediente
			// RequestContext.getCurrentInstance().execute("PF('editDialog').hide();");
			// org.primefaces.PrimeFaces.current().executeScript("PF('editDialog').hide();");

		} catch (Exception e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Modificación de expedientes de Turismo", errmsg));
			RequestContext.getCurrentInstance().scrollTo("mainPanel");
		}
	}

	/**
	 * Validación de los campos del formulario contenidos en el DTO
	 * 
	 * @param dto
	 *            Data Object a validar
	 * @throws SedeException
	 */
	private void validate(SolicitudTurismoDTO dto) throws SedeException {
		if (dto.getPeticion1OpcionId() == null) {
			throw new SedeException("Debe seleccionar un primer destino");
		}

		if (dto.getPeticion1OpcionId().equals(dto.getPeticion2OpcionId())) {
			throw new SedeException("Los destinos deben ser distintos");
		}

		// TODO añadir más validaciones

	}

	/**
	 * Cancela la edición de la solicitud
	 */
	public void cancel() {
		log.info("el usuario ha cancelado la modificación de la solicitud de turismo!");
	}

	public List<SolicitudTurismoDTO> getList() {
		return list == null ? new ArrayList<SolicitudTurismoDTO>() : list;
	}

	public void setList(List<SolicitudTurismoDTO> list) {
		this.list = list;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the selectedSolicitud
	 */
	public SolicitudTurismoDTO getSelectedSolicitud() {
		return selectedSolicitud;
	}

	/**
	 * @param selectedSolicitud
	 *            the selectedSolicitud to set
	 */
	public void setSelectedSolicitud(SolicitudTurismoDTO selectedSolicitud) {
		this.selectedSolicitud = selectedSolicitud;
	}

	public List<SimpleDTOI> getProvinciaList() {
		return provinciaList;
	}

	public void setProvinciaList(List<SimpleDTOI> provinciaList) {
		this.provinciaList = provinciaList;
	}

	public List<SimpleDTOI> getEstadosCiviles() {
		return estadosCiviles;
	}

	public void setEstadosCiviles(List<SimpleDTOI> estadosCiviles) {
		this.estadosCiviles = estadosCiviles;
	}

	public List<SimpleDTOI> getSexos() {
		return sexos;
	}

	public void setSexos(List<SimpleDTOI> sexos) {
		this.sexos = sexos;
	}

	public List<SimpleDTOI> getOpciones() {
		return opciones;
	}

	public void setOpciones(List<SimpleDTOI> opciones) {
		this.opciones = opciones;
	}

	public ParamValues getParamValues() {
		return paramValues;
	}

	public void setParamValues(ParamValues paramValues) {
		this.paramValues = paramValues;
	}

	public StreamedContent getPdfCartaAcreditacion() {
		this.pdfCartaAcreditacion = null;

		try {
			this.pdfCartaAcreditacion = new DefaultStreamedContent(
					new ByteArrayInputStream(turismoRepository.getCartaAcreditacion(selectedSolicitud.getId())),
					Global.CONTENT_TYPE_APPLICATION_PDF, Global.NOMBRE_FICHERO_CARTA_DE_ACREDITACION_TURISMO);

		} catch (SedeException e) {
			String errmsg = Utils.getExceptionMessage(e);
			log.error(errmsg);
			eventMessage.fire(new EventMessage(EventMessageLevel.MESSAGE_LEVEL_ERROR, errmsg));
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"No se pudo obtener la carta de acreditación", errmsg));
		}

		return pdfCartaAcreditacion;
	}

	public void setPdfCartaAcreditacion(StreamedContent pdfCartaAcreditacion) {
		this.pdfCartaAcreditacion = pdfCartaAcreditacion;
	}

	public boolean isHabilitadaDescargaCartaAcreditacion() throws SedeException {
		this.habilitadaDescargaCartaAcreditacion = turismoRepository.isCartaAcreditaciónAccesible();
		return habilitadaDescargaCartaAcreditacion;
	}

	public void setHabilitadaDescargaCartaAcreditacion(boolean habilitadaDescargaCartaAcreditacion) {
		this.habilitadaDescargaCartaAcreditacion = habilitadaDescargaCartaAcreditacion;
	}

	public boolean isModoConsulta() {
		return modoConsulta;
	}

	public void setModoConsulta(boolean modoConsulta) {
		this.modoConsulta = modoConsulta;
	}

}
