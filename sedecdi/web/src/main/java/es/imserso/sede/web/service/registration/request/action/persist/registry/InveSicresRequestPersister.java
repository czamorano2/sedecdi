package es.imserso.sede.web.service.registration.request.action.persist.registry;

import java.util.function.Function;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.service.registration.registry.RegistryBookManager;
import es.imserso.sede.service.registration.registry.ISicres.registers.RegistryServiceI;
import es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI;

/**
 * Registra la solicitud en inveSicres
 * 
 * @author 11825775
 *
 */
public class InveSicresRequestPersister implements Function<InputRegistryData, IdentificadorRegistroDTO> {

	private static final Logger log = Logger.getLogger(InveSicresRequestPersister.class.getName());

	@Inject
	protected RegistryServiceI registryService;

	@Inject
	private RegistryBookManager registryBookManager;

	@Override
	public IdentificadorRegistroDTO apply(InputRegistryData data) {
		InputRegisterResponseI newRegisterResponse = registryService.registerNewSolicitud(
				registryBookManager.getCurrentBook().getId(), data.getDto().getAttachedFiles(), data.getRequestPdf(),
				data.getRequestId(), data.getDto().getCodigoSIA());

		log.info(String.format("solicitud registrada con el identificador de registro %d y número de registro %s",
				newRegisterResponse.getFolderId(), newRegisterResponse.getNumber()));

		return new IdentificadorRegistroDTO(newRegisterResponse.getBookId(), newRegisterResponse.getFolderId(),
				newRegisterResponse.getNumber());
	}

}
