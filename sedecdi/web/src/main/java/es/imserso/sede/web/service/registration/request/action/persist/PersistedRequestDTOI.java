package es.imserso.sede.web.service.registration.request.action.persist;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.model.Solicitud;

/**
 * DTO con la información de la solicitud que se persiste en el sistema
 * <p>
 * Además de la información del formulario de SolicitudDTOI, contiene
 * información de la persistencia de la solicitud en el sistema
 * 
 * @author 11825775
 *
 */
public interface PersistedRequestDTOI {

	/**
	 * @return DTO con la información del formulario de alta
	 */
	SolicitudDTOI getRequestDTO();

	/**
	 * @param dto
	 *            DTO con la información del formulario de alta
	 */
	void setRequestDTO(SolicitudDTOI dto);

	/**
	 * @return solicitud persistida en la base de datos
	 */
	Solicitud getRequestDatabaseSolicitud();

	/**
	 * @param solicitud
	 *            solicitud persistida en la base de datos
	 */
	void setRequestDatabaseSolicitud(Solicitud solicitud);

	/**
	 * @return identificador del registro electrónico (libro, carpeta y número de
	 *         registro)
	 */
	IdentificadorRegistroDTO getIdentificadorRegistroElectronico();

	/**
	 * @param identificador
	 *            Identificador del registro electrónico (libro, carpeta y número de
	 *            registro)
	 */
	void setIdentificadorRegistroElectronico(IdentificadorRegistroDTO identificador);

	/**
	 * @return array de bytes del PDF de la solicitud
	 */
	byte[] getRequestPdf();

	/**
	 * @param bytes
	 *            array de bytes del PDF de la solicitud
	 */
	void setRequestPdf(byte[] bytes);

	/**
	 * @return array de bytes del PDF del resguardo
	 */
	byte[] getReceiptPdf();

	/**
	 * @param bytes
	 *            array de bytes del PDF del resguardo
	 */
	void setReceiptPdf(byte[] bytes);

}
