

// nombre del botón de cancelar
var cancelButtonIds = ['solicitud:done','solicitud:cancel'];


// código de la tecla que invoca al botón de cancelar
//var kCode = 27; //escape
var kCode = 114; //F3




// función que hace click en el botón de cancelar si se ha presionado la tecla
// especificada
document.onkeydown = handleKeys;

function handleKeys(e) {
	try {
		e = e || event;
//		if (!e) e = window.event;
		
		

//		alert(e.keyCode);
		if (e.keyCode == kCode) {
			pressButton(cancelButtonIds);
			e.cancelBubble = true;

			//e.stopPropagation works in Firefox.
			if (e.stopPropagation) {
				e.stopPropagation();
				e.preventDefault();
			}
		}
	} catch (e) {
//		alert(e);
	}
	return;
}

// hace click en el botón
function pressButton(buttonIds) {
	try {
		if (clickable(buttonIds)) {
			for (var b = 0; b<buttonIds.length; b++) {
				if (document.getElementById(buttonIds[b]) != null) {
					document.getElementById(buttonIds[b]).click();
					return;
				}
			}
		}
	} catch (e) {
//		alert(e);
	}

}

// indica si un elemento es un botón clickable
function clickable(buttonIds) {
	var frms = document.forms;
	var elms;
	var el;
	var s = '';
	try {
		for ( var f = 0; f < frms.length; f++) {
			elms = frms[f].getElementsByTagName("input");
			for ( var e = 0; e < elms.length; e++) {
				el = elms[e];
				for (var b = 0; b<buttonIds.length; b++) {
					if (el.id == buttonIds[b] && (el.type == 'button' || el.type == 'submit')
							&& !el.hidden && !el.disabled) {
						return true;
					}
				}
			}
		}
	} catch (e) {
//		alert(e);
	}
	return false;
}