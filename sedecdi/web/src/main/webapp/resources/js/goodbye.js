/**
 * Esta funcionalidad es exclusivamente para la vista SolicitudEdit.xhtml
 * <p>
 * Controla que se abandone la pagina sin grabar los cambios.
 * <p>
 * En la página donde se use habrá que poner js_Salir a true cuando no queramos que nos pida
 * confirmación para abandonar la página por ejemplo en un botón así: onclick="js_Salir=true;"
 * <p>
 * Tomada de http://www.openjs.com/scripts/events/exit_confirmation.php
 * 
 * @param e Evento
 */
js_Salir = new Boolean();
js_Salir = false;
function goodbye(e) {
 	if (js_Salir || !cambioEnFormularios()){
	 	return;
	}
	if(!e) e = window.event;
	//e.cancelBubble is supported by IE - this will kill the bubbling process.
	e.cancelBubble = true;
	e.returnValue = 'Se perderán los cambios que haya realizado.'; //This is displayed on the dialog

	//e.stopPropagation works in Firefox.
	if (e.stopPropagation) {
		e.stopPropagation();
		e.preventDefault();
	}
	
	//return es necesario solo para Chrome y Safari
	return "¿Está seguro que quiere salir sin grabar cambios?";
} 

window.onbeforeunload=goodbye;

function setJs_Salir(valor){
	js_Salir=valor;
}

/* 
 * FormChanges(string FormID | DOMelement FormNode)
 * Returns an array of changed form elements.
 * An empty array indicates no changes have been made.
 * NULL indicates that the form does not exist.
 *
 * By Craig Buckler,		http://twitter.com/craigbuckler
 * of OptimalWorks.net		http://optimalworks.net/
 * for SitePoint.com		http://sitepoint.com/
 * 
 * Refer to http://blogs.sitepoint.com/javascript-form-change-checker/
 *
 * This code can be used without restriction. 
 */
function FormChanges(form) {

	// get form
	if (typeof form == "string") form = document.getElementById(form);
	if (!form || !form.nodeName || form.nodeName.toLowerCase() != "form") return null;
	
	// find changed elements
	var changed = [], n, c, def, o, ol, opt;
	for (var e = 0, el = form.elements.length; e < el; e++) {
		n = form.elements[e];
		c = false;
		
		switch (n.nodeName.toLowerCase()) {
		
			// select boxes
			case "select":
				def = 0;
				for (o = 0, ol = n.options.length; o < ol; o++) {
					opt = n.options[o];
					c = c || (opt.selected != opt.defaultSelected);
					if (opt.defaultSelected) def = o;
				}
				if (c && !n.multiple) c = (def != n.selectedIndex);
				break;
			
			// input / textarea
			case "textarea":
			case "input":
				
				switch (n.type.toLowerCase()) {
					case "checkbox":
					case "radio":
						// checkbox / radio
						c = (n.checked != n.defaultChecked);
						break;
					default:
						// standard values
						c = (n.value != n.defaultValue);
						break;				
				}
				break;
		}
		
		if (c) changed.push(n);
	}
	
	return changed;

}

function cambioEnFormularios() {
	try {
    	return (FormChanges(document.getElementById('solicitudForm')).length > 0);
	} catch(err) {
		//alert(err);
	}
}